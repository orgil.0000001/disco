namespace Orgil {
	public enum Tag { Untagged, Respawn, Finish, EditorOnly, MainCamera, Player, GameController, Coin, Bot, DanceFloor, Ball, Bullet }
	public class Lyr { public const int Default = 0, TransparentFX = 1, IgnoreRaycast = 2, Ball = 3, Water = 4, UI = 5, Cube = 6; }
	public class Lm { public const int Default = 1, TransparentFX = 2, IgnoreRaycast = 4, Ball = 8, Water = 16, UI = 32, Cube = 64; }
}