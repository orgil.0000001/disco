using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;

public class DanceFloor : Mb {
    [HideInInspector]
    public DanceFloorData data;
    [HideInInspector]
    public List<Material> mats = new List<Material>();
    [HideInInspector]
    public GameObject wallGo;
    [HideInInspector]
    public Vector3 playerPos;
    [HideInInspector]
    public List<Bot> bots = new List<Bot>();
    [HideInInspector]
    public int botDoneCnt = 0;
    public bool IsBotDone => botDoneCnt == bots.Count;
    [HideInInspector]
    public bool isBall = false;
    [HideInInspector]
    public Ball ball = null;
    [HideInInspector]
    public bool isBonus = false;
    float t = 0;
    private void Update() {
        if (IsPlaying) {
            t += Dt;
            if (t >= Ls.I.dfTm) {
                t = 0;
                UpdCol();
            }
            if (ball) {
                ball.TleDy(Dt * 90);
            }
        }
    }
    public void Bonus() {
        isBonus = true;
    }
    public void UpdCol() {
        mats.ForEach(x => x.color = Rnd.B ? Ls.I.e.dfDef : Ls.I.e.df);
    }
    //public void BotPos() {
    //    if (bots.Count == 1) {
    //        bots[0].pos = Tp;
    //    } else if (bots.Count == 2) {
    //        bots[0].pos = go.TfPnt(V3.X(-Ls.I.botSpc.x / 2));
    //        bots[1].pos = go.TfPnt(V3.X(Ls.I.botSpc.x / 2));
    //    } else {
    //        float r = M.Sqrt(Ls.I.botSpc.x * Ls.I.botSpc.x / (2 - 2 * M.Cos(360f / bots.Count)));
    //        for (int i = 0; i < bots.Count; i++)
    //            bots[i].pos = go.TfPnt(Ang.Xz(360f * i / bots.Count - 90, r));
    //    }
    //}
}
