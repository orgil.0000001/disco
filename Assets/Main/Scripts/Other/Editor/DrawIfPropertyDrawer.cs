using System;
using UnityEditor;
using UnityEngine;
using Orgil;

[CustomPropertyDrawer(typeof(DrawIfAttribute))]
public class DrawIfPropertyDrawer : PropertyDrawer {
    DrawIfAttribute attr;
    SerializedProperty comparedField;
    private float propertyHeight;

    bool IsNumericType(Type t) {
        return IsInt(t) || IsLong(t) || IsFloat(t) || IsDouble(t);
    }

    bool IsInt(Type t) {
        return t.IsType<sbyte>() || t.IsType<byte>() || t.IsType<short>() || t.IsType<ushort>() || t.IsType<int>() || t.IsType<uint>();
    }

    bool IsLong(Type t) {
        return t.IsType<long>() || t.IsType<ulong>();
    }

    bool IsFloat(Type t) {
        return t.IsType<float>();
    }

    bool IsDouble(Type t) {
        return t.IsType<double>() || t.IsType<decimal>();
    }

    object GetValue(Type t, SerializedProperty sp) {
        object res = null;
        if (t.IsType<AnimationCurve>())
            res = sp.animationCurveValue;
        else if (t.IsType<bool>())
            res = sp.boolValue;
        else if (t.IsType<BoundsInt>())
            res = sp.boundsIntValue;
        else if (t.IsType<Bounds>())
            res = sp.boundsValue;
        else if (t.IsType<Color>())
            res = sp.colorValue;
        else if (IsDouble(t))
            res = sp.doubleValue;
        else if (IsFloat(t))
            res = sp.floatValue;
        else if (IsInt(t)
        || t.IsType<char>())
            res = sp.intValue;
        else if (IsLong(t))
            res = sp.longValue;
        else if (t.IsType<object>())
            res = sp.objectReferenceValue;
        else if (t.IsType<Quaternion>())
            res = sp.quaternionValue;
        else if (t.IsType<RectInt>())
            res = sp.rectIntValue;
        else if (t.IsType<Rect>())
            res = sp.rectValue;
        else if (t.IsType<string>())
            res = sp.stringValue;
        else if (t.IsType<Vector2Int>())
            res = sp.vector2IntValue;
        else if (t.IsType<Vector2>())
            res = sp.vector2Value;
        else if (t.IsType<Vector3Int>())
            res = sp.vector3IntValue;
        else if (t.IsType<Vector3>())
            res = sp.vector3Value;
        else if (t.IsType<Vector4>())
            res = sp.vector4Value;
        return res;
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
        return propertyHeight;
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
        attr = attribute as DrawIfAttribute;
        object value = GetValue(attr.type, property.serializedObject.FindProperty(attr.name));
        bool active = false;
        if (attr.compTp == CompTp.Or) {
            for (int i = 0; i < attr.values.Length; i++)
                if (value.Equals(attr.values[i])) {
                    active = true;
                    break;
                }
        } else if (attr.compTp == CompTp.Equals) {
            if (value.Equals(attr.value))
                active = true;
        } else if (attr.compTp == CompTp.NotEqual) {
            if (!value.Equals(attr.value))
                active = true;
        } else if (IsNumericType(attr.type)) {
            double a = 0, b = 0;
            if (IsInt(attr.type)) {
                a = (int)value;
                b = (int)attr.value;
            } else if (IsLong(attr.type)) {
                a = (long)value;
                b = (long)attr.value;
            } else if (IsFloat(attr.type)) {
                a = (float)value;
                b = (float)attr.value;
            } else if (IsDouble(attr.type)) {
                a = (double)value;
                b = (double)attr.value;
            }
            switch (attr.compTp) {
                case CompTp.GreaterThan:
                    if (a > b) active = true;
                    break;
                case CompTp.SmallerThan:
                    if (a < b) active = true;
                    break;
                case CompTp.SmallerOrEqual:
                    if (a <= b) active = true;
                    break;
                case CompTp.GreaterOrEqual:
                    if (a >= b) active = true;
                    break;
            }
        }
        propertyHeight = base.GetPropertyHeight(property, label);
        if (active) {
            EditorGUI.PropertyField(position, property);
        } else {
            if (attr.disableTp == DisableTp.ReadOnly) {
                GUI.enabled = false;
                EditorGUI.PropertyField(position, property);
                GUI.enabled = true;
            } else {
                propertyHeight = 0f;
            }
        }
    }
}