﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;

public class Bot : Character {
    public GameObject gunGo, mfGo;
    //[HideInInspector]
    //public Vector3 pos;
    [HideInInspector]
    public DanceFloor df;
    bool isAim = false, isShot = false, isHit = false, isDone = false, isBonus = false;
    GameObject cGo;
    Tf aimTf = Tf.PR(V3.V(0.24f, 0.04f, 0.19f), V3.V(-44, -123, 47)),
        idleTf = Tf.PR(V3.V(0.29f, 0.1f, 0.25f), V3.V(3, -140, 103));
    float t = 0;
    private void Start() {
        Init();
        cGo = go.ChildGo(0);
        an = cGo.An();
        Anim("Idle");
    }
    private void Update() {
        if (IsPlaying && !isDone) {
            if (isShot && Player.I.isStand) {
                t += Dt;
                if (t >= Ls.I.botShotTm) {
                    t = 0;
                    Ins(Ls.I.bulletPf, mfGo.Tp(), Q.O, Ls.I.tf).RenMatCol(Ls.I.e.bullet);
                    StaCor(MfCor());
                }
            }
        }
    }
    public void UpdCol(Color c) {
        go.Child(0, 0).RenMats().ForEach(x => x.color = c);
    }
    IEnumerator MfCor() {
        Dst(Ins(Ls.I.shotSongPf, mfGo.Tp(), Q.O, Ls.I.tf), 1);
        mfGo.Show();
        yield return Wf.S(0.3f);
        mfGo.Hide();
    }
    public void Hit() {
        if (!isAim) {
            foreach (Bot bot in df.bots) {
                if (bot == this)
                    StaCor(bot.HitCor());
                else
                    StaCor(bot.AimCor());
            }
            Player.Score(10);
        } else if (!isHit) {
            StaCor(HitCor());
            Player.Score(10);
        }
    }
    public IEnumerator HitCor() {
        isAim = true;
        isHit = true;
        Anim("Hit", 40);
        yield return Wf.F(68);
        isHit = false;
        if (!isDone) {
            isShot = true;
            Anim("Aim", 40);
        }
    }
    public IEnumerator AimCor() {
        isAim = true;
        Anim("IdleAim");
        yield return Wf.F(52);
        if (!isDone) {
            isShot = true;
            Anim("Aim", 40);
        }
    }
    public void Bonus(DanceFloor df) {
        isBonus = true;
        this.df = df;
        UpdCol(Ls.I.e.bot2);
        DstCol();
        isDone = true;
        df.botDoneCnt++;
    }
    public void DstCol() {
        Dst(rb);
        go.Bc().isTrigger = true;
        go.Sc().isTrigger = true;
    }
    private void OnTriggerEnter(Collider other) {
        if (other.Tag(Tag.DanceFloor)) {
            df = other.Gc<DanceFloor>();
            UpdCol(Ls.I.e.bot2);
            DstCol();
            isShot = false;
            Anim("Idle");
            isDone = true;
            df.botDoneCnt++;
            Player.Score(20);
            Ls.I.AddPnt();
        }
    }
    public void Anim(string name, float rot = 0) {
        an.Play(name);
        cGo.TleY(rot);
        if (name == "IdleAim" || name == "Aim" || name == "Hit") {
            gunGo.Tlp(aimTf.p);
            gunGo.Tlr(aimTf.q);
        } else if (name == "Idle" || name == "Walk") {
            gunGo.Tlp(idleTf.p);
            gunGo.Tlr(idleTf.q);
        } else {
            Dst(gunGo);
        }
    }
}