﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Orgil;
using System.IO;
using System.Linq;

public class Player : Character {
    public static Player I {
        get {
            if (_.Null())
                _ = GameObject.FindObjectOfType<Player>();
            return _;
        }
    }
    static Player _;
    public Image targetImg;
    public Transform gunTf, shotPntTf;
    [HideInInspector]
    public bool isStand = false;
    bool isShot = false, isClk = false, isFlag = false, isWait = false, isSta = false;
    float t = 0;
    RaycastHit hit;
    [HideInInspector]
    public int dfIdx = 0, life = 100;
    DanceFloor Df => Ls.I.dfs[dfIdx];
    Vector3 staPos;
    Quaternion staRot, flagRot;
    private void Awake() {
        _ = this;
    }
    void Start() {
    }
    void Update() {
        if (IsPlaying) {
            if (isWait) {
                ClkShot(false, true);
                t += Dt;
                Df.wallGo.TlpY(M.Lerp(0, -1.01f, t / Ls.I.waitTm));
                if (t >= Ls.I.waitTm) {
                    Df.wallGo.TlpY(-1.01f);
                    isWait = false;
                    dfIdx++;
                    if (dfIdx == Ls.I.dfs.Count) {
                        isFlag = true;
                        flagRot = Q.Y(Ang.LookD(staPos, Ls.I.l.flagPos));
                    }
                    isShot = false;
                }
            } else if (isFlag) {
                ClkShot(false, true);
                Tp = V3.Move(Tp, Ls.I.l.flagPos, Dt * Ls.I.playerSpd);
                Tr = Q.Rot(Tr, flagRot, Dt * 90);
                if (V3.Dis(Tp, Ls.I.l.flagPos) < 0.01f) {
                    Tp = Ls.I.l.flagPos;
                    Gc.I.LevelCompleted();
                }
            } else if (isShot) {
                ClkShot(true, true);
            } else {
                ClkShot(false, true);
                Tp = V3.Move(Tp, Df.playerPos, Dt * Ls.I.playerSpd);
                UpdRot(Df.Tr, Df.playerPos);
                gunTf.localRotation = Q.Rot(gunTf.localRotation, Q.O, Dt * 90);
                if (V3.Dis(Tp, Df.playerPos) < 0.01f) {
                    Tp = Df.playerPos;
                    Tr = Df.Tr;
                    isShot = true;
                    UpdSta();
                }
            }
        }
    }
    void ClkShot(bool isSit, bool isShot) {
        if (IsMbD) {
            mp = Mp;
            isClk = true;
            isStand = true;
            isSta = true;
        }
        if (IsMb) {
            gunTf.localEulerAngles = V3.Xy(
                M.C_(Ang.Rep180(gunTf.localEulerAngles.x + (mp.y - Mp.y) * Ls.I.mpSpd.y), Ls.I.rotLim.y, Ls.I.rotLim.z),
                M.C_(Ang.Rep180(gunTf.localEulerAngles.y + (Mp.x - mp.x) * Ls.I.mpSpd.x), -Ls.I.rotLim.x, Ls.I.rotLim.x)
            );
            mp = Mp;
        }
        if (IsMbU && isSta) {
            isClk = false;
            if (isSit)
                t = 0;
            if (isShot)
                Shot();
        }
        if (!isClk) {
            t += Dt;
            if (t >= Ls.I.sitWaitTm)
                isStand = false;
        }
        if (isSit)
            Tp = Tp.Y(M.Move(Tp.y, isStand ? 0 : -1, Dt * 5));
        targetImg.color = targetImg.color.A(M.Move(targetImg.color.a, isSit ? (isStand ? 1 : 0) : 1, Dt * 5));
    }
    public static void Score(int add) {
        Gc.Score += add;
        Cc.I.Score();
    }
    void UpdRot(Quaternion rot, Vector3 pos) {
        Tr = Q.Lerp(staRot, rot, 1 - V3.Dis(Tp.Xz(), pos.Xz()) / V3.Dis(staPos.Xz(), pos.Xz()));
    }
    public void UpdPos(Vector3 pos) {
        Tp = pos;
        UpdSta();
    }
    void UpdSta() {
        staPos = Tp;
        staRot = Tr;
    }
    public void NxtMv() {
        t = 0;
        isWait = true;
    }
    public void Shot() {
        Ball ball = Ins(Ls.I.ballPf, shotPntTf.position, shotPntTf.rotation, Ls.I.tf);
        ball.Tls = V3.V(Ls.I.ballSz);
        ball.rb.mass = Ls.I.ballM;
        GameObject cGo = ball.ChildGo(0);
        cGo.Mf().sharedMesh = Ls.I.ballMsh;
        cGo.RenMatCol(Ls.I.e.ball);
        Ray ray = Camera.main.ScreenPointToRay(V3.Xy(Screen.width / 2, Screen.height / 2));
        bool isDef = true;
        if (Physics.Raycast(ray, out hit)) {
            isDef = false;
            ball.rb.V((hit.point - ball.Tp).normalized * Ls.I.ballSpd);
            //Vector3 v = Hpm.Ang_V0(ball.Tp, hit.point, 10);
            //if (v.tS() != "(NaN, NaN, NaN)") {
            //    isDef = false;
            //    ball.rb.V(v);
            //}
        }
        if (isDef)
            ball.rb.V(shotPntTf.forward * Ls.I.ballSpd);
        Dst(Ins(Ls.I.shotSongPf, shotPntTf.position, Q.O, Ls.I.tf), 1);
    }
    private void OnTriggerEnter(Collider other) {
        if (other.Tag(Tag.Bullet)) {
            life -= Ls.I.botShotPwr;
            float f = life.F() / Ls.I.l.life;
            Ls.I.healthImg.fillAmount = f;
            Ls.I.healthImg.color = Ls.I.healthG.Evaluate(f);
            if (life <= 0)
                Gc.I.GameOver();
            Dst(other.gameObject);
        }
    }
    public void Reset() { }
}