﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

namespace Orgil {
    public enum AnchorPresets { TopLeft, TopCenter, TopRight, MiddleLeft, MiddleCenter, MiddleRight, BottomLeft, BottonCenter, BottomRight, BottomStretch, VerStretchLeft, VerStretchRight, VerStretchCenter, HorStretchTop, HorStretchMiddle, HorStretchBottom, StretchAll }
    public enum PivotPresets { TopLeft, TopCenter, TopRight, MiddleLeft, MiddleCenter, MiddleRight, BottomLeft, BottomCenter, BottomRight }
    public static class ExtLyr {
        public static int LyrLm(this int a) { return 1 << a; }
        public static string LyrName(this int a) { return LayerMask.LayerToName(a); }
        public static int LmLyr(this int a) { return 1 >> a; }
        public static string LmName(this int a) { return LayerMask.LayerToName(1 >> a); }
        public static int NameLyr(this string a) { return LayerMask.NameToLayer(a); }
        public static int NameLm(this string a) { return 1 << LayerMask.NameToLayer(a); }
    }
    public static class ExtTypeCast {
        public static int Frm(this float a) { return (int)(a * 60); }
        public static float Sec(this int a) { return a / 60f; }
        public static int I(this bool a) { return a ? 1 : 0; }
        public static int I2(this bool a) { return a ? 0 : 1; }
        public static float F(this bool a) { return a ? 1f : 0f; }
        public static string S(this bool a) { return a.ToString(); }
        public static int Sign(this bool a) { return a ? 1 : -1; }
        public static bool B(this int a) { return a == 1; }
        public static float F(this int a) { return a; }
        public static string S(this int a) { return a.ToString(); }
        public static int Sign(this int a) { return a >= 0 ? 1 : -1; }
        public static bool B(this float a) { return M.Apx(a, 1f); }
        public static int I(this float a) { return (int)a; }
        public static string S(this float a) { return a.ToString(); }
        public static int Sign(this float a) { return a >= 0f ? 1 : -1; }
        public static string TimeS(this float a) { return System.TimeSpan.FromSeconds(M.FloorI(a)).tS().Sub(3); }
        public static string S<T>(this List<T> a, System.Func<T, string> f, string separator = ", ", string sta = "", string end = "") { return a.Parse(f).S(separator, sta, end); }
        public static string S<T>(this IEnumerable<T> a, string separator = ", ", string sta = "", string end = "") { return sta + string.Join(separator, a) + end; }
        public static string S2<T>(this List<List<T>> a, string sep1 = ", ", string sep2 = "\n", string sta = "[", string end = "]") { string s = ""; a.ForEach(x => s += x.S(sep1, sta, end) + sep2); return s.SubLast(0, sep2.Length); }
        public static T[] Arr<T>(this IEnumerable<T> a) { return a.ToArray(); }
        public static List<T> List<T>(this IEnumerable<T> a) { return a.ToList(); }
        public static string S(this Transform a, Space spc = Space.Self, bool isName = false) { return (isName ? a.name : "") + (spc == Space.Self ? "(" + a.localPosition + ", " + a.localEulerAngles + ", " + a.localScale + ")" : "(" + a.position + ", " + a.eulerAngles + ", " + a.lossyScale + ")"); }
        public static string S(this Tf a, bool isName = false) { return (isName ? a.n : "") + "(" + a.p.S() + ", " + a.r.S() + ", " + a.s.S() + ")"; }
        public static Tf Tf(this Transform a, Space spc = Space.Self, bool isName = false) { return spc == Space.Self ? new Tf(isName ? a.name : "", a.localPosition, a.localEulerAngles, a.localScale) : new Tf(isName ? a.name : "", a.position, a.eulerAngles, a.lossyScale); }
        public static string tS<T>(this T a) { return a.ToString(); }
        public static List<Color> Cols(this Texture2D a) { return a.GetPixels(0, 0, a.width, a.height).ToList(); }
        public static Sprite Spr(this Texture2D tex) { return Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0.5f, 0.5f)); }
        public static List<Vector2> ListV2(this List<Vector3> a) { return a.Parse(x => x.V2()); }
        public static List<Vector2> ListXz(this List<Vector3> a) { return a.Parse(x => x.Xz()); }
        public static List<Vector3> ListV3(this List<Vector2> a) { return a.Parse(x => x.V3()); }
        public static List<Vector3> ListXz(this List<Vector2> a, float y = 0) { return a.Parse(x => x.Xz(y)); }
    }
    public static class ExtOther {
        public static void En(this Behaviour a, bool isEnabled = true) { a.enabled = isEnabled; }
        public static string Fill(this string a, char c, int n) {
            if (a.Null())
                a = c.N(n);
            else if (a.Length < n)
                a += c.N(n - a.Length);
            return a;
        }
        public static Vector4 SwapU(this Vector4 a, bool isSwap = true) { return isSwap ? V4.V(a.z, a.y, a.x, a.w) : a; }
        public static Vector4 SwapV(this Vector4 a, bool isSwap = true) { return isSwap ? V4.V(a.x, a.w, a.z, a.y) : a; }
        public static Vector4 SwapUv(this Vector4 a, bool isSwapU = true, bool isSwapV = true) { return V4.V(isSwapU ? a.z : a.x, isSwapV ? a.w : a.y, isSwapU ? a.x : a.z, isSwapV ? a.y : a.w); }
        public static Vector2 Uv(this Vector4 a, float u, float v) { return V2.V(M.Lerp(a.x, a.z, u), M.Lerp(a.y, a.w, v)); }
        public static Vector4 Uv(this Vector4 a, float tilingY) { return a.Div(1, tilingY, 1, tilingY); }
        public static Vector2 Uv(this Vector4 a, Vector2 min, Vector2 max, Vector2 p) { return V2.V(M.Remap(p.x, min.x, max.x, a.x, a.z), M.Remap(p.y, min.y, max.y, a.y, a.w)); }
        public static Vector2 Uv(this Vector4 a, Vector3 min, Vector3 max, Vector3 p) { return V2.V(M.Remap(p.x, min.x, max.x, a.x, a.z), M.Remap(p.z, min.z, max.z, a.y, a.w)); }
        public static List<Vector2> Uv(this List<Vector2> a, List<Vector2> data, Vector4 uv) {
            Vector2 min = V2.Min(data), max = V2.Max(data);
            data.ForEach(x => a.Add(uv.Uv(min, max, x)));
            return a;
        }
        public static float Len(this Animator a, string name) { return a.runtimeAnimatorController.animationClips.List().Find(x => x.name == name).length; }
        public static float StaD(this int n) { return (1 - n) / 2f; }
        public static int I(this Nzp a) { return a == Nzp.N ? -1 : a == Nzp.Z ? 0 : 1; }
        public static bool IsOdd(this int a) { return a % 2 == 1; }
        public static bool IsEven(this int a) { return a % 2 == 0; }
        public static bool IsMod(this int a, int n, int m = 0) { return a % n == m; }
        public static float Rnd(this Vector2 a) { return Orgil.Rnd.Rng(a.x, a.y); }
        public static float Rnd(this Vector2Int a) { return Orgil.Rnd.Rng(a.x, a.y); }
        public static float RndIn(this Vector2Int a) { return Orgil.Rnd.RngIn(a.x, a.y); }
        public static float RndD(this float a, float d) { return Orgil.Rnd.Rng(a - d, a + d); }
        public static bool Null(this object a) { return a == null; }
        public static bool NotNull(this object a) { return a != null; }
        public static bool IsDef<T>(this T a) { return a.Equals(default(T)); }
        public static bool IsType<T, V>(this T a) { return typeof(T) == typeof(V); }
        public static bool IsType<T>(this System.Type a) { return a == typeof(T); }
        public static bool IsRt(this RectTransform a, Vector3 mp) { return a.rect.Contains(a.InverseTransformPoint(mp)); }
        public static void Anchor(this RectTransform a, AnchorPresets anchors, int x = 0, int y = 0) {
            a.anchoredPosition = new Vector3(x, y, 0);
            switch (anchors) {
                case AnchorPresets.TopLeft: a.anchorMin = new Vector2(0, 1); a.anchorMax = new Vector2(0, 1); break;
                case AnchorPresets.TopCenter: a.anchorMin = new Vector2(0.5f, 1); a.anchorMax = new Vector2(0.5f, 1); break;
                case AnchorPresets.TopRight: a.anchorMin = new Vector2(1, 1); a.anchorMax = new Vector2(1, 1); break;
                case AnchorPresets.MiddleLeft: a.anchorMin = new Vector2(0, 0.5f); a.anchorMax = new Vector2(0, 0.5f); break;
                case AnchorPresets.MiddleCenter: a.anchorMin = new Vector2(0.5f, 0.5f); a.anchorMax = new Vector2(0.5f, 0.5f); break;
                case AnchorPresets.MiddleRight: a.anchorMin = new Vector2(1, 0.5f); a.anchorMax = new Vector2(1, 0.5f); break;
                case AnchorPresets.BottomLeft: a.anchorMin = new Vector2(0, 0); a.anchorMax = new Vector2(0, 0); break;
                case AnchorPresets.BottonCenter: a.anchorMin = new Vector2(0.5f, 0); a.anchorMax = new Vector2(0.5f, 0); break;
                case AnchorPresets.BottomRight: a.anchorMin = new Vector2(1, 0); a.anchorMax = new Vector2(1, 0); break;
                case AnchorPresets.HorStretchTop: a.anchorMin = new Vector2(0, 1); a.anchorMax = new Vector2(1, 1); break;
                case AnchorPresets.HorStretchMiddle: a.anchorMin = new Vector2(0, 0.5f); a.anchorMax = new Vector2(1, 0.5f); break;
                case AnchorPresets.HorStretchBottom: a.anchorMin = new Vector2(0, 0); a.anchorMax = new Vector2(1, 0); break;
                case AnchorPresets.VerStretchLeft: a.anchorMin = new Vector2(0, 0); a.anchorMax = new Vector2(0, 1); break;
                case AnchorPresets.VerStretchCenter: a.anchorMin = new Vector2(0.5f, 0); a.anchorMax = new Vector2(0.5f, 1); break;
                case AnchorPresets.VerStretchRight: a.anchorMin = new Vector2(1, 0); a.anchorMax = new Vector2(1, 1); break;
                case AnchorPresets.StretchAll: a.anchorMin = new Vector2(0, 0); a.anchorMax = new Vector2(1, 1); break;
            }
        }
        public static void Pivot(this RectTransform a, PivotPresets pivot) {
            switch (pivot) {
                case PivotPresets.TopLeft: a.pivot = new Vector2(0, 1); break;
                case PivotPresets.TopCenter: a.pivot = new Vector2(0.5f, 1); break;
                case PivotPresets.TopRight: a.pivot = new Vector2(1, 1); break;
                case PivotPresets.MiddleLeft: a.pivot = new Vector2(0, 0.5f); break;
                case PivotPresets.MiddleCenter: a.pivot = new Vector2(0.5f, 0.5f); break;
                case PivotPresets.MiddleRight: a.pivot = new Vector2(1, 0.5f); break;
                case PivotPresets.BottomLeft: a.pivot = new Vector2(0, 0); break;
                case PivotPresets.BottomCenter: a.pivot = new Vector2(0.5f, 0); break;
                case PivotPresets.BottomRight: a.pivot = new Vector2(1, 0); break;
            }
        }
        public static void RenMode(this Material a, RenMd rm) {
            switch (rm) {
                case RenMd.Opaque:
                    a.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
                    a.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
                    a.SetInt("_ZWrite", 1);
                    a.DisableKeyword("_ALPHATEST_ON");
                    a.DisableKeyword("_ALPHABLEND_ON");
                    a.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                    a.renderQueue = -1;
                    break;
                case RenMd.Cutout:
                    a.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
                    a.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
                    a.SetInt("_ZWrite", 1);
                    a.EnableKeyword("_ALPHATEST_ON");
                    a.DisableKeyword("_ALPHABLEND_ON");
                    a.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                    a.renderQueue = 2450;
                    break;
                case RenMd.Fade:
                    a.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
                    a.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                    a.SetInt("_ZWrite", 0);
                    a.DisableKeyword("_ALPHATEST_ON");
                    a.EnableKeyword("_ALPHABLEND_ON");
                    a.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                    a.renderQueue = 3000;
                    break;
                case RenMd.Transparent:
                    a.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
                    a.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                    a.SetInt("_ZWrite", 0);
                    a.DisableKeyword("_ALPHATEST_ON");
                    a.DisableKeyword("_ALPHABLEND_ON");
                    a.EnableKeyword("_ALPHAPREMULTIPLY_ON");
                    a.renderQueue = 3000;
                    break;
            }
        }
        ///<summary>үеийн өнгөнөөс tile-г авна</summary>
        public static List<LevelTile> TexTiles(this List<Color> cols, Vector2Int sz, Color emptyCol, bool isCorner) {
            List<LevelTile> res = new List<LevelTile>();
            List<Vector2Int> levelDirs = new List<Vector2Int>() {
                new Vector2Int (-1, 0), new Vector2Int (-1, 1), new Vector2Int (0, 1), new Vector2Int (1, 1),
                new Vector2Int (1, 0), new Vector2Int (1, -1), new Vector2Int (0, -1), new Vector2Int (-1, -1),
            };
            System.Func<int, int, int, int, List<Color>, Color, bool> isEmpty = (x, y, w, h, cs, empCol) => (x < 0 || x >= w || y < 0 || y >= h || cs[A.TexIdx(x, y, w, h)] == empCol);
            for (int y = 0; y < sz.y; y++) {
                for (int x = 0; x < sz.x; x++) {
                    List<bool> actives = new List<bool>() { !isEmpty(x, y, sz.x, sz.y, cols, emptyCol) };
                    for (int i = 0; i < 4; i++) {
                        bool dir0 = !isEmpty(x + levelDirs[i * 2].x, y + levelDirs[i * 2].y, sz.x, sz.y, cols, emptyCol),
                            dir1 = !isEmpty(x + levelDirs[i * 2 + 1].x, y + levelDirs[i * 2 + 1].y, sz.x, sz.y, cols, emptyCol),
                            dir2 = !isEmpty(x + levelDirs[(i * 2 + 2) % 8].x, y + levelDirs[(i * 2 + 2) % 8].y, sz.x, sz.y, cols, emptyCol);
                        actives.Add(isCorner ? (actives[0] ? (dir0 || dir1 || dir2) : (dir0 && dir2)) :
                            (actives[0] ? ((dir0 && dir1) || (dir1 && dir2) || (dir2 && dir0) || (dir0 && !dir1) || (dir2 && !dir1)) : (dir0 && dir1 && dir2)));
                    }
                    if (actives.Contains(true))
                        res.Add(new LevelTile(new Vector2Int(x, y), actives.Arr()));
                }
            }
            return res;
        }
        ///<summary>зурагнаас үеийн өнгийг авна</summary>
        public static List<Color> TexLevelColors(this Texture2D tex, Color borderCol, int level, out Vector2Int sz) {
            int w = tex.width, h = tex.height;
            List<Color> cols = tex.Cols();
            for (int y = 1, lvl = 0; y < h; y++) {
                int maxY = 0;
                for (int x = 0; x < w; x++) {
                    if (cols[A.TexIdx(x, y, w, h)] == borderCol) {
                        Vector2Int tmpSize = cols.TexRectSize(x, y, w, h);
                        if (++lvl == level) {
                            sz = tmpSize - V2I.I * 2;
                            return tex.GetPixels(x + 1, h - y - sz.y - 1, sz.x, sz.y).List();
                        }
                        if (maxY < tmpSize.y)
                            maxY = tmpSize.y;
                        x += tmpSize.x;
                    }
                }
                y += maxY;
            }
            sz = V2I.O;
            return null;
        }
        ///<summary>зурагнаас үеийн хүрээний хэмжэээг авна</summary>
        public static Vector2Int TexRectSize(this List<Color> cols, int x, int y, int w, int h) {
            Color col = cols[A.TexIdx(x, y, w, h)];
            Vector2Int res = V2I.O;
            for (int ix = x; ix < w && cols[A.TexIdx(ix, y, w, h)] == col; ix++)
                res.x++;
            for (int iy = y; iy < h && cols[A.TexIdx(x, iy, w, h)] == col; iy++)
                res.y++;
            return res;
        }
        ///<summary>зурагны зүүн дээд булангийн i-р өнгийг уншиж авна</summary>
        public static Color TexData(this Texture2D tex, int i) { return tex.GetPixel(i, tex.height - 1); }
    }
    public static class ExtRb {
        public static void G(this Rigidbody a, bool useGravity = true) { a.useGravity = useGravity; }
        public static void NoG(this Rigidbody a) { a.useGravity = false; }
        public static void K(this Rigidbody a, bool isKinematic = true) { a.isKinematic = isKinematic; }
        public static void NoK(this Rigidbody a) { a.isKinematic = false; }
        public static void Int(this Rigidbody a, int interpolate) { a.interpolation = (RigidbodyInterpolation)interpolate; }
        public static void Int(this Rigidbody a, RigidbodyInterpolation interpolate) { a.interpolation = interpolate; }
        public static void Int0(this Rigidbody a) { a.interpolation = RigidbodyInterpolation.None; }
        public static void IntInt(this Rigidbody a) { a.interpolation = RigidbodyInterpolation.Interpolate; }
        public static void IntExt(this Rigidbody a) { a.interpolation = RigidbodyInterpolation.Extrapolate; }
        public static void ColDet(this Rigidbody a, int collisionDetection) { a.collisionDetectionMode = (CollisionDetectionMode)collisionDetection; }
        public static void ColDet(this Rigidbody a, CollisionDetectionMode collisionDetection) { a.collisionDetectionMode = collisionDetection; }
        public static void ColDetDis(this Rigidbody a) { a.collisionDetectionMode = CollisionDetectionMode.Discrete; }
        public static void ColDetCon(this Rigidbody a) { a.collisionDetectionMode = CollisionDetectionMode.Continuous; }
        public static void ColDetConD(this Rigidbody a) { a.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic; }
        public static void ColDetConS(this Rigidbody a) { a.collisionDetectionMode = CollisionDetectionMode.ContinuousSpeculative; }
        public static void Con(this Rigidbody a, bool isFrzPosX = false, bool isFrzPosY = false, bool isFrzPosZ = false, bool isFrzRotX = false, bool isFrzRotY = false, bool isFrzRotZ = false) { a.constraints = (isFrzPosX ? RigidbodyConstraints.FreezePositionX : 0) | (isFrzPosY ? RigidbodyConstraints.FreezePositionY : 0) | (isFrzPosZ ? RigidbodyConstraints.FreezePositionZ : 0) | (isFrzRotX ? RigidbodyConstraints.FreezeRotationX : 0) | (isFrzRotY ? RigidbodyConstraints.FreezeRotationY : 0) | (isFrzRotZ ? RigidbodyConstraints.FreezeRotationZ : 0); }
        public static void Con0(this Rigidbody a) { a.constraints = RigidbodyConstraints.None; }
        public static void ConFrzPos(this Rigidbody a) { a.constraints = RigidbodyConstraints.FreezePosition; }
        public static void ConFrzRot(this Rigidbody a) { a.constraints = RigidbodyConstraints.FreezeRotation; }
        public static void ConFrzAll(this Rigidbody a) { a.constraints = RigidbodyConstraints.FreezeAll; }
        public static void Av0(this Rigidbody a) { a.angularVelocity = V3.O; }
        public static void Av(this Rigidbody a, Vector3 v) { a.angularVelocity = v; }
        public static void Avx(this Rigidbody a, float x) { a.angularVelocity = a.angularVelocity.X(x); }
        public static void Avy(this Rigidbody a, float y) { a.angularVelocity = a.angularVelocity.Y(y); }
        public static void Avz(this Rigidbody a, float z) { a.angularVelocity = a.angularVelocity.Z(z); }
        public static void Avxy(this Rigidbody a, float x, float y) { a.angularVelocity = a.angularVelocity.Xy(x, y); }
        public static void Avxz(this Rigidbody a, float x, float z) { a.angularVelocity = a.angularVelocity.Xz(x, z); }
        public static void Avyz(this Rigidbody a, float y, float z) { a.angularVelocity = a.angularVelocity.Yz(y, z); }
        public static void Av(this Rigidbody a, float x, float y, float z) { a.angularVelocity = V3.V(x, y, z); }
        public static void V0(this Rigidbody a) { a.velocity = V3.O; }
        public static void V(this Rigidbody a, Vector3 v) { a.velocity = v; }
        public static void Vx(this Rigidbody a, float x) { a.velocity = a.velocity.X(x); }
        public static void Vy(this Rigidbody a, float y) { a.velocity = a.velocity.Y(y); }
        public static void Vz(this Rigidbody a, float z) { a.velocity = a.velocity.Z(z); }
        public static void Vxy(this Rigidbody a, float x, float y) { a.velocity = a.velocity.Xy(x, y); }
        public static void Vxz(this Rigidbody a, float x, float z) { a.velocity = a.velocity.Xz(x, z); }
        public static void Vyz(this Rigidbody a, float y, float z) { a.velocity = a.velocity.Yz(y, z); }
        public static void V(this Rigidbody a, float x, float y, float z) { a.velocity = V3.V(x, y, z); }
    }
    public static class ExtGen {
        public static List<T> ListN<T>(this T a, int n) {
            List<T> res = new List<T>();
            for (int i = 0; i < n; i++)
                res.Add(a);
            return res;
        }
        public static T Sum<T>(this List<T> a) {
            if (a.IsNe())
                return default;
            T sum = a[0];
            for (int i = 1; i < a.Count; i++)
                sum = Gen.Op(sum, a[i], OpTp.Add);
            return sum;
        }
        public static List<T> ListF<T>(this List<T> a, System.Func<T, T> f) {
            List<T> res = new List<T>();
            a.ForEach(x => res.Add(f(x)));
            return res;
        }
    }
    public static class ExtMsh {
        public static List<Vector3> PntsVs2NewVs(this List<Vector3> pnts, List<Vector3> vs, bool isXz) {
            List<Vector3> vs2 = vs.Copy();
            for (int i = 0; i < vs2.Count; i++)
                for (int j = 0; j < pnts.Count; j++)
                    if (M.Apx(vs2[i].x, pnts[j].x, 0.001f) && (isXz ? M.Apx(vs2[i].z, pnts[j].z, 0.001f) : M.Apx(vs2[i].y, pnts[j].y, 0.001f))) {
                        vs2[i] = pnts[j];
                        break;
                    }
            return vs2;
        }
        public static List<int> PntsVsTs2NewTs(this List<Vector2> a, Geometry.Shp2D shp, bool isCw = true, int tsDIdx = 0) { return a.ListXz().PntsVsTs2NewTs(shp.vs.List(), shp.ts.List(), true, isCw, tsDIdx); }
        public static List<int> PntsVsTs2NewTs(this List<Vector3> a, Geometry.Shp2D shp, bool isXz, bool isCw = true, int tsDIdx = 0) { return a.PntsVsTs2NewTs(shp.vs.List(), shp.ts.List(), isXz, isCw, tsDIdx); }
        public static List<int> PntsVsTs2NewTs(this List<Vector3> a, List<Vector3> vs, List<int> ts, bool isXz, bool isCw = true, int tsDIdx = 0) {
            List<int> idx = new List<int>();
            for (int i = 0; i < vs.Count; i++)
                for (int j = 0; j < a.Count; j++)
                    if (M.Apx(vs[i].x, a[j].x) && (isXz ? M.Apx(vs[i].z, a[j].z) : M.Apx(vs[i].y, a[j].y))) {
                        idx.Add(j);
                        break;
                    }
            List<int> ts2 = new List<int>();
            for (int i = 0; i < ts.Count; i += 3)
                ts2.FcT(isCw, tsDIdx + idx[ts[i]], tsDIdx + idx[ts[i + 1]], tsDIdx + idx[ts[i + 2]]);
            return ts2;
        }
        public static List<Vector2> Fc(this List<Vector2> a, Vector2 b, Vector2 c, Vector2 d, Vector2 e) { return a.Add(Msh.Fc(b, c, d, e)); }
        public static List<Vector2> Fc2(this List<Vector2> a, Vector2 b, Vector2 c, Vector2 d, Vector2 e) { return a.Add(Msh.Fc2(b, c, d, e)); }
        public static List<Vector2> FcI(this List<Vector2> a, Vector2 b, Vector2 c, Vector2 d, Vector2 e) { return a.Add(Msh.FcI(b, c, d, e)); }
        public static List<Vector2> Fc(this List<Vector2> a, bool isCw, Vector2 b, Vector2 c, Vector2 d, Vector2 e) { return a.Add(Msh.Fc(isCw, b, c, d, e)); }
        public static List<int> FcT(this List<int> a, int b, int c, int d) { return a.Add(b, c, d); }
        public static List<int> FcTI(this List<int> a, int b, int c, int d) { return a.Add(b, d, c); }
        public static List<int> FcT(this List<int> a, bool isCw, int b, int c, int d) { return isCw ? a.Add(b, c, d) : a.Add(b, d, c); }
        public static List<int> Fc(this List<int> a, int b, int c, int d, int e) { return a.Add(Msh.Fc(b, c, d, e)); }
        public static List<int> Fc2(this List<int> a, int b, int c, int d, int e) { return a.Add(Msh.Fc2(b, c, d, e)); }
        public static List<int> FcI(this List<int> a, int b, int c, int d, int e) { return a.Add(Msh.FcI(b, c, d, e)); }
        public static List<int> Fc2I(this List<int> a, int b, int c, int d, int e) { return a.Add(Msh.Fc2I(b, c, d, e)); }
        public static List<int> Fc(this List<int> a, bool isCw, int b, int c, int d, int e) { return a.Add(Msh.Fc(isCw, b, c, d, e)); }
        public static List<int> Fc2(this List<int> a, bool isCw, int b, int c, int d, int e) { return a.Add(Msh.Fc2(isCw, b, c, d, e)); }
        public static List<int> TsFill(this List<int> a, int n, int staIdx, int dIdx, bool isCw) {
            for (int i = 1; i <= n - 2; i++)
                a.FcT(isCw, staIdx, staIdx + (i + 1) * dIdx, staIdx + i * dIdx);
            return a;
        }
        public static List<int> TsOne(this List<int> a, int n, int oneStaIdx, int oneDIdx, int staIdx, int dIdx, bool isCw, bool isLoop = true) {
            for (int i = 0; i < n; i++)
                a.FcT(isCw, oneStaIdx + i * oneDIdx, staIdx + (isLoop ? (i + 1) % n : i + 1) * dIdx, staIdx + i * dIdx);
            return a;
        }
        public static List<int> TsLoop(this List<int> a, int n, int staIdx, int dIdx, int staIdx2, int dIdx2, bool isCw, int d = 1, int dn = 0) {
            for (int i = 0; i < n; i += d)
                a.Fc(isCw, staIdx + i * dIdx, staIdx2 + i * dIdx2, staIdx2 + (i + 1) % (n + dn) * dIdx2, staIdx + (i + 1) % (n + dn) * dIdx);
            return a;
        }
        public static List<Vector3> NorR(this List<Vector3> a, float r) { return a.ListF(x => x.normalized * r); }
        public static List<Vector3> Scl(this List<Vector3> a, Vector3 s) { return a.ListF(x => V3.Scl(x, s)); }
        public static List<Vector3> Pos(this List<Vector3> a, Vector3 p) { return a.ListF(x => x + p); }
        public static List<Vector2> Scl(this List<Vector2> a, Vector2 s) { return a.ListF(x => V3.Scl(x, s)); }
        public static List<Vector2> Pos(this List<Vector2> a, Vector2 p) { return a.ListF(x => x + p); }
    }
    public static class ExtArr {
        public static int PosNearIdx<T>(this T[] a, Vector3 p) { return a.List().PosNearIdx(p); }
        public static T PosNear<T>(this T[] a, Vector3 p, T def = default) { return a.List().PosNear(p, def); }
        public static int NearIdx<T>(this T[] a, T t) { return a.List().NearIdx(t); }
        public static T Near<T>(this T[] a, T t, T def = default) { return a.List().Near(t, def); }
        public static T[] DisSort<T>(this T[] a, T t) { return a.List().DisSort(t).Arr(); }
        public static T[] Rng<T>(this T[] a, int i, int n) { return a.List().GetRange(i, n).Arr(); }
        public static T[] Rng<T>(this T[] a, int n) { return a.Rng(0, n); }
        public static T[] RngLast<T>(this T[] a, int n) { return a.Rng(a.Length - n, n); }
        public static T[] Rmv<T>(this T[] a, T t) { return a.List().Rmv(t).Arr(); }
        public static T[] RmvAt<T>(this T[] a, int i) { return a.List().RmvAt(i).Arr(); }
        public static T[] RmvAtLast<T>(this T[] a, int i = 0) { return a.List().RmvAtLast(i).Arr(); }
        public static T[] RmvRng<T>(this T[] a, int i, int n) { return a.List().RmvRng(i, n).Arr(); }
        public static T Rnd<T>(this T[] a) { return Orgil.Rnd.Arr(a); }
        public static T RndI<T>(this T[] a, int i) { return Orgil.Rnd.ArrI(i, a); }
        public static T RndE<T>(this T[] a, T e) { return Orgil.Rnd.ArrE(e, a); }
        public static T[] Rnd<T>(this T[] a, int n) { return Orgil.Rnd.Arr(n, a); }
        public static bool IsCount<T>(this T[] a) { return a.Length > 0; }
        public static bool IsEmpty<T>(this T[] a) { return a.Length == 0; }
        public static bool IsNe<T>(this T[] a) { return a == null || a.Length == 0; }
        public static T[] Copy<T>(this T[] a) { return (T[])a.Clone(); }
        public static T CIdx<T>(this T[] a, int idx) { return a[M.CIdx(idx, a.Length)]; }
        public static T RepIdx<T>(this T[] a, int idx) { return a[M.RepIdx(idx, a.Length)]; }
        public static T PingPongIdx<T>(this T[] a, int idx) { return a[M.PingPongIdx(idx, a.Length)]; }
        public static T RepCIdx<T>(this T[] a, int idx, bool isRep) { return a[M.RepCIdx(idx, a.Length, isRep)]; }
        public static T RepPingPongIdx<T>(this T[] a, int idx, bool isRep) { return a[M.RepPingPongIdx(idx, a.Length, isRep)]; }
        public static T PingPongCIdx<T>(this T[] a, int idx, bool isPingPong) { return a[M.PingPongCIdx(idx, a.Length, isPingPong)]; }
        public static T RepIdx<T>(this T[] a, int idx, RepTp rt) { return a[M.RepIdx(idx, a.Length, rt)]; }
        public static T Last<T>(this T[] a, int idx = 0) { return a[a.Length - idx - 1]; }
        public static void Print<T>(this T[] a, string separator = ", ", string sta = "", string end = "") { Debug.Log(a.S(separator, sta, end)); }
        public static T[] Add1<T>(this T[] a, T t) { return a.List().Add1(t).Arr(); }
        public static T[] Add<T>(this T[] a, bool isAdd, T t) { return isAdd ? a.Add1(t) : a; }
        public static T[] Add<T>(this T[] a, bool isAdd, List<T> add) { return isAdd ? a.Add(add) : a; }
        public static T[] Add<T>(this T[] a, bool isAdd, T[] add) { return isAdd ? a.Add(add) : a; }
        public static T[] Add<T>(this T[] a, List<T> add) { return a = A.Add2List(a.List(), add).Arr(); }
        public static T[] Add<T>(this T[] a, params T[] add) { return a = A.Add2List(a.List(), add.List()).Arr(); }
        public static V[] Parse<T, V>(this T[] a) { return a.List().Parse(x => (V)(object)x).Arr(); }
        public static V[] Parse<T, V>(this T[] a, System.Func<T, V> func) { return a.List().Parse(func).Arr(); }
        public static T[] Parse<T>(this string[] a, System.Func<string, T> func) { return a.List().Parse(func).Arr(); }
        public static string[] RmvNull(this string[] a) { return a.List().RmvNull().Arr(); }
        public static string[] RmvSpc(this string[] a) { return a.List().RmvSpc().Arr(); }
        public static string RmvSpcStr(this string[] a, string separator = ", ") { return a.List().RmvSpcStr(separator); }
        public static void Swap<T>(this T[] a, int i, int j) {
            T tmp = a[i];
            a[i] = a[j];
            a[j] = tmp;
        }
        public static void Shuffle<T>(this T[] a) {
            for (int i = 0; i < a.Length; i++)
                a.Swap(i, Orgil.Rnd.Idx(a.Length));
        }
    }
    public static class ExtList {
        static Dictionary<System.Type, System.Func<object, object, float>> DisFuncDic = new Dictionary<System.Type, System.Func<object, object, float>>() { //
            { typeof(int), (a, b) => M.Dis((int)a, (int)b) },
            { typeof(float), (a, b) => M.Dis((float)a, (float)b) },
            { typeof(Vector2), (a, b) => V2.Dis((Vector2)a, (Vector2)b) },
            { typeof(Vector2Int), (a, b) => V2I.Dis((Vector2Int)a, (Vector2Int)b) },
            { typeof(Vector3), (a, b) => V3.Dis((Vector3)a, (Vector3)b) },
            { typeof(Vector3Int), (a, b) => V3I.Dis((Vector3Int)a, (Vector3Int)b) },
            { typeof(Vector4), (a, b) => V4.Dis((Vector4)a, (Vector4)b) },
            { typeof(GameObject), (a, b) => V3.Dis(((GameObject)a).Tp(), ((GameObject)b).Tp()) },
            { typeof(Component), (a, b) => V3.Dis(((Component)a).Tp(), ((Component)b).Tp()) },
        };
        static Dictionary<System.Type, System.Func<object, Vector3, float>> PosDisFuncDic = new Dictionary<System.Type, System.Func<object, Vector3, float>>() { //
            { typeof(Vector3), (a, b) => V3.Dis((Vector3)a, b) },
            { typeof(GameObject), (a, b) => V3.Dis(((GameObject)a).Tp(), b) },
            { typeof(Component), (a, b) => V3.Dis(((Component)a).Tp(), b) },
        };
        static System.Type Tp<T>(T t) { return t is Component ? typeof(Component) : t.GetType(); }
        public static int PosNearIdx<T>(this List<T> a, Vector3 p) {
            int idx = -1;
            if (a.IsCount()) {
                System.Type tp = Tp(a[0]);
                if (DisFuncDic.ContainsKey(tp)) {
                    System.Func<object, Vector3, float> f = PosDisFuncDic[tp];
                    float minDis = M.MaxVal, dis;
                    for (int i = 0; i < a.Count; i++) {
                        dis = f(a[i], p);
                        if (dis < minDis) {
                            minDis = dis;
                            idx = i;
                        }
                    }
                }
            }
            return idx;
        }
        public static T PosNear<T>(this List<T> a, Vector3 p, T def = default) {
            int idx = PosNearIdx(a, p);
            return idx == -1 ? def : a[idx];
        }
        public static int NearIdx<T>(this List<T> a, T t) {
            int idx = -1;
            if (a.IsCount()) {
                System.Type tp = Tp(a[0]);
                if (DisFuncDic.ContainsKey(tp)) {
                    System.Func<object, object, float> f = DisFuncDic[tp];
                    float minDis = M.MaxVal, dis;
                    for (int i = 0; i < a.Count; i++) {
                        dis = f(a[i], t);
                        if (dis < minDis) {
                            minDis = dis;
                            idx = i;
                        }
                    }
                }
            }
            return idx;
        }
        public static T Near<T>(this List<T> a, T t, T def = default) {
            int idx = NearIdx(a, t);
            return idx == -1 ? def : a[idx];
        }
        public static List<T> DisSort<T>(this List<T> a, T t) {
            List<T> res = new List<T>();
            if (a.IsCount()) {
                System.Type tp = Tp(a[0]);
                if (DisFuncDic.ContainsKey(tp)) {
                    System.Func<object, object, float> f = DisFuncDic[tp];
                    List<(int, float)> disLis = new List<(int, float)>();
                    for (int i = 0; i < a.Count; i++)
                        disLis.Add((i, f(a[i], t)));
                    disLis.Sort((x, y) => x.Item2.CompareTo(y.Item2));
                    disLis.ForEach(x => res.Add(a[x.Item1]));
                }
            }
            return a;
        }
        public static List<T> Rng<T>(this List<T> a, int i, int n) { return a.GetRange(i, n); }
        public static List<T> Rng<T>(this List<T> a, int n) { return a.GetRange(0, n); }
        public static List<T> RngLast<T>(this List<T> a, int n) { return a.GetRange(a.Count - n, n); }
        public static List<T> Rmv<T>(this List<T> a, T t) { a.Remove(t); return a; }
        public static List<T> RmvAt<T>(this List<T> a, int i) { a.RemoveAt(i); return a; }
        public static List<T> RmvAtLast<T>(this List<T> a, int i = 0) { a.RemoveAt(a.Count - 1 - i); return a; }
        public static List<T> RmvRng<T>(this List<T> a, int i, int n) { a.RemoveRange(i, n); return a; }
        public static T Rnd<T>(this List<T> a) { return Orgil.Rnd.List(a); }
        public static T RndI<T>(this List<T> a, int i) { return Orgil.Rnd.ListI(i, a); }
        public static T RndE<T>(this List<T> a, T e) { return Orgil.Rnd.ListE(e, a); }
        public static List<T> Rnd<T>(this List<T> a, int n) { return Orgil.Rnd.List(n, a); }
        public static bool IsCount<T>(this List<T> a) { return a.Count > 0; }
        public static bool IsEmpty<T>(this List<T> a) { return a.Count == 0; }
        public static bool IsNe<T>(this List<T> a) { return a == null || a.Count == 0; }
        public static List<T> Copy<T>(this List<T> a) { return a.GetRange(0, a.Count); }
        public static T CIdx<T>(this List<T> a, int i) { return a[M.CIdx(i, a.Count)]; }
        public static T RepIdx<T>(this List<T> a, int i) { return a[M.RepIdx(i, a.Count)]; }
        public static T PingPongIdx<T>(this List<T> a, int i) { return a[M.PingPongIdx(i, a.Count)]; }
        public static T RepCIdx<T>(this List<T> a, int i, bool isRep) { return a[M.RepCIdx(i, a.Count, isRep)]; }
        public static T RepPingPongIdx<T>(this List<T> a, int i, bool isRep) { return a[M.RepPingPongIdx(i, a.Count, isRep)]; }
        public static T PingPongCIdx<T>(this List<T> a, int i, bool isPingPong) { return a[M.PingPongCIdx(i, a.Count, isPingPong)]; }
        public static T RepIdx<T>(this List<T> a, int i, RepTp rt) { return a[M.RepIdx(i, a.Count, rt)]; }
        public static T Last<T>(this List<T> a, int i = 0) { return a[a.Count - i - 1]; }
        public static void Print<T>(this List<T> a, string separator = ", ", string sta = "", string end = "") { Debug.Log(a.S(separator, sta, end)); }
        public static List<T> Add1<T>(this List<T> a, T t) { a.Add(t); return a; }
        public static List<T> Add<T>(this List<T> a, bool isAdd, T t) { return isAdd ? a.Add1(t) : a; }
        public static List<T> Add<T>(this List<T> a, bool isAdd, List<T> add) { return isAdd ? a.Add(add) : a; }
        public static List<T> Add<T>(this List<T> a, bool isAdd, T[] add) { return isAdd ? a.Add(add) : a; }
        public static List<T> Add<T>(this List<T> a, List<T> add) { return a = A.Add2List(a, add); }
        public static List<T> Add<T>(this List<T> a, params T[] add) { return a = A.Add2List(a, add.List()); }
        public static List<V> Parse<T, V>(this List<T> a) { return a.Parse(x => (V)(object)x); }
        public static List<V> Parse<T, V>(this List<T> a, System.Func<T, V> func) {
            List<V> res = new List<V>();
            a.ForEach(x => res.Add(func(x)));
            return res;
        }
        public static List<T> Parse<T>(this List<string> a, System.Func<string, T> func) {
            List<T> res = new List<T>();
            foreach (string x in a)
                if (!x.IsNe())
                    res.Add(func(x));
            return res;
        }
        public static List<string> RmvNull(this List<string> a) { return a.Parse(x => x); }
        public static List<string> RmvSpc(this List<string> a) { return a.Parse(x => x.RmvSpc()); }
        public static string RmvSpcStr(this List<string> a, string separator = ", ") { return a.RmvSpc().S(separator); }
        public static void Swap<T>(this List<T> a, int i, int j) {
            T tmp = a[i];
            a[i] = a[j];
            a[j] = tmp;
        }
        public static void Shuffle<T>(this List<T> a) {
            for (int i = 0; i < a.Count; i++)
                a.Swap(i, Orgil.Rnd.Idx(a.Count));
        }
    }
    public static class ExtChar {
        public static bool IsU(this char a) { return System.Char.IsUpper(a); }
        public static bool IsL(this char a) { return System.Char.IsLower(a); }
        public static char U(this char a) { return System.Char.ToUpper(a); }
        public static char L(this char a) { return System.Char.ToLower(a); }
        public static string S(this char a) { return a.ToString(); }
        public static string N(this char a, int n) { return a.S().N(n); }
    }
    public static class ExtStr {
        static string[] StrArr(string a, string sta = "") { return (a.IsS(sta + "(") && a.IsE(")") ? a.Sub(sta.Length + 1, a.Length - sta.Length - 2) : a).Split(','); }
        static float StrF(string[] arr, int i) { return arr.Length > i ? float.Parse(arr[i]) : 0; }
        static int StrI(string[] arr, int i) { return arr.Length > i ? int.Parse(arr[i]) : 0; }
        public static string N(this string a, int n) { string res = ""; for (int i = 0; i < n; i++) res += a; return res; }
        public static string Format(this string a, params object[] args) { return string.Format(a, args); }
        public static string Fmt(this string a, params object[] keyDatas) {
            if (keyDatas.Length == 2)
                return a.Contains(keyDatas[0].tS()) ? a.Replace(keyDatas[0].tS(), keyDatas[1].tS()) : a + keyDatas[1];
            else if (keyDatas.Length > 2)
                for (int i = 0, n = keyDatas.Length - (keyDatas.Length % 2 == 0 ? 0 : 1); i < n; i += 2)
                    a = a.Contains(keyDatas[i].tS()) ? a.Replace(keyDatas[i].tS(), keyDatas[i + 1].tS()) : a;
            return a;
        }
        public static int Idx(this string a, char c) { return a.IndexOf(c); }
        public static int Idx(this string a, string v) { return a.IndexOf(v); }
        public static int LastIdx(this string a, char c) { return a.LastIndexOf(c); }
        public static int LastIdx(this string a, string v) { return a.LastIndexOf(v); }
        public static string[] Split0(this string a, string c = " ") { int idx = a.Idx(c); return new string[] { a.Sub(0, idx), a.Sub(idx + c.Length, a.Length - idx - c.Length) }; }
        public static string Abr(this string a) { string res = a[0].U().tS(); for (int i = 1; i < a.Length; i++) if (a[i].IsU()) res += a[i].L(); return res; }
        public static string Fu(this string a) { return a.Sub(0, 1).U() + a.Sub(1); }
        public static string Fl(this string a) { return a.Sub(0, 1).L() + a.Sub(1); }
        public static string Pc(this string a) { string res = ""; a.RgxSplit("\\W+").List().ForEach(x => res += x.Sub(0, 1).U() + x.Sub(1).L()); return res; }
        public static string Cc(this string a) { return a.Pc().Fl(); }
        public static string SubLast(this string a, int idx, int lastIdx) { return a.Sub(idx, a.Length - idx - lastIdx); }
        public static string Sub(this string a, int idx, int n) { return a.Substring(idx, n); }
        public static string Sub(this string a, int idx) { return a.Substring(idx); }
        public static string Rpl(this string a, char oldC, char newC) { return a.Replace(oldC, newC); }
        public static string Rpl(this string a, string oldS, string newS) { return a.Replace(oldS, newS); }
        public static string Rmv(this string a, string s) { return a.Replace(s, ""); }
        public static string L(this string a) { return a.ToLower(); }
        public static string U(this string a) { return a.ToUpper(); }
        public static bool Contains(this string a, params string[] arr) { return arr.List().Contains(a); }
        public static string RmvSpc(this string a) { return Rgx.Rmv(a, "\\s+"); }
        public static string RmvUw(this string a) { return Rgx.Rmv(a, "\\W+"); }
        public static string[] RgxSplit(this string a, string pattern) { return Rgx.Split(a, pattern); }
        public static string[] RgxSplitUw(this string a) { return Rgx.Split(a.Trim(), "\\W+"); }
        public static string[] RgxSplitSpc(this string a) { return Rgx.Split(a.Trim(), "\\s+"); }
        public static bool RgxIsMatch(this string a, string pattern) { return Rgx.IsMatch(a, pattern); }
        public static string RgxRpl(this string a, string pattern, string replacement) { return Rgx.Rpl(a, pattern, replacement); }
        public static string RgxRmv(this string a, string pattern) { return Rgx.Rmv(a, pattern); }
        public static bool IsNe(this string a) { return string.IsNullOrEmpty(a); }
        public static bool IsNSpc(this string a) { return string.IsNullOrWhiteSpace(a); }
        public static string RplS(this string a, string oldStr, string newStr) { return a.IsS(oldStr) ? newStr + a.Sub(oldStr.Length, a.Length - oldStr.Length) : a; }
        public static string RplE(this string a, string oldStr, string newStr) { return a.IsE(oldStr) ? a.Sub(0, a.Length - oldStr.Length) + newStr : a; }
        public static int[] IntArr(this string a) { return a == "" ? new int[0] : a.RgxSplit("\\D+").Parse(x => int.Parse(x)); }
        public static List<int> IntList(this string a) { return a == "" ? new List<int>() : a.RgxSplit("\\D+").Parse(x => int.Parse(x)).List(); }
        public static bool IsS(this string a, string sta) { return a.StartsWith(sta); }
        public static bool IsE(this string a, string end) { return a.EndsWith(end); }
        public static bool B(this string a) { return a == "True" ? true : false; }
        public static int I(this string a) { int res; if (int.TryParse(a, out res)) return res; return 0; }
        public static float F(this string a) { float res; if (float.TryParse(a, out res)) return res; return 0f; }
        public static Color C(this string a) { string[] arr = StrArr(a, "RGBA"); return new Color(StrF(arr, 0), StrF(arr, 1), StrF(arr, 2), StrF(arr, 3)); }
        public static Quaternion Q(this string a) { string[] arr = StrArr(a); return new Quaternion(StrF(arr, 0), StrF(arr, 1), StrF(arr, 2), StrF(arr, 3)); }
        public static Vector2 V2(this string a) { string[] arr = StrArr(a); return new Vector2(StrF(arr, 0), StrF(arr, 1)); }
        public static Vector2Int V2I(this string a) { string[] arr = StrArr(a); return new Vector2Int(StrI(arr, 0), StrI(arr, 1)); }
        public static Vector3 V3(this string a) { string[] arr = StrArr(a); return new Vector3(StrF(arr, 0), StrF(arr, 1), StrF(arr, 2)); }
        public static Vector3Int V3I(this string a) { string[] arr = StrArr(a); return new Vector3Int(StrI(arr, 0), StrI(arr, 1), StrI(arr, 2)); }
        public static Vector4 V4(this string a) { string[] arr = StrArr(a); return new Vector4(StrF(arr, 0), StrF(arr, 1), StrF(arr, 2), StrF(arr, 3)); }
        public static char[] Arr(this string a) { return a.ToCharArray(); }
        public static Color Hex(this string a) { if (Rgx.IsMatch(a.ToUpper(), Rgx.HexCol)) { Color res; if (ColorUtility.TryParseHtmlString(a[0] != '#' ? "#" + a : a, out res)) return res; } return Color.clear; }
        public static T Enum<T>(this string a) { return (T)System.Enum.Parse(typeof(T), a); }
        public static Tf Tf(this string a, bool isName = false) { string[] arr0 = a.Split0("(("); string[] arr = ("(" + arr0[1].SubLast(0, 1)).Replace("), (", ");(").Split(';'); return new Tf(isName ? arr0[0] : "", arr[0].V3(), arr[1].V3(), arr[2].V3()); }
    }
    public static class ExtQ {
        public static Quaternion Nor(this Quaternion a) { return a.normalized; }
        public static Quaternion Inv(this Quaternion a) { return Quaternion.Inverse(a); }
        public static string S(this Quaternion a) { return "(" + a.x + ", " + a.y + ", " + a.z + ", " + a.w + ")"; }
        public static Vector3 E(this Quaternion a) { return a.eulerAngles; }
    }
    public static class ExtC {
        public static Color R(this Color c, float r) { return new Color(r, c.g, c.b, c.a); }
        public static Color G(this Color c, float g) { return new Color(c.r, g, c.b, c.a); }
        public static Color B(this Color c, float b) { return new Color(c.r, c.g, b, c.a); }
        public static Color A(this Color c, float a) { return new Color(c.r, c.g, c.b, a); }
        public static Color Rg(this Color c, float r, float g) { return new Color(r, g, c.b, c.a); }
        public static Color Rb(this Color c, float r, float b) { return new Color(r, c.g, b, c.a); }
        public static Color Ra(this Color c, float r, float a) { return new Color(r, c.g, c.b, a); }
        public static Color Gb(this Color c, float g, float b) { return new Color(c.r, g, b, c.a); }
        public static Color Ga(this Color c, float g, float a) { return new Color(c.r, g, c.b, a); }
        public static Color Ba(this Color c, float b, float a) { return new Color(c.r, c.g, b, a); }
        public static Color Rgb(this Color c, float r, float g, float b) { return new Color(r, g, b, c.a); }
        public static Color Rga(this Color c, float r, float g, float a) { return new Color(r, g, c.b, a); }
        public static Color Rba(this Color c, float r, float b, float a) { return new Color(r, c.g, b, a); }
        public static Color Gba(this Color c, float g, float b, float a) { return new Color(c.r, g, b, a); }
        public static Color Dr(this Color c, float dr, bool isRep = false) { return new Color(M.RepC01(c.r + dr, isRep), c.g, c.b, c.a); }
        public static Color Dg(this Color c, float dg, bool isRep = false) { return new Color(c.r, M.RepC01(c.g + dg, isRep), c.b, c.a); }
        public static Color Db(this Color c, float db, bool isRep = false) { return new Color(c.r, c.g, M.RepC01(c.b + db, isRep), c.a); }
        public static Color Da(this Color c, float da, bool isRep = false) { return new Color(c.r, c.g, c.b, M.RepC01(c.a + da, isRep)); }
        public static Color Drgba(this Color c, float dr = 0, float dg = 0, float db = 0, float da = 0, bool isRep = false) { return new Color(M.RepC01(c.r + dr, isRep), M.RepC01(c.g + dg, isRep), M.RepC01(c.b + db, isRep), M.RepC01(c.a + da, isRep)); }
        public static Color Rgba(this Color c, float r = -1, float g = -1, float b = -1, float a = -1, float dr = 0, float dg = 0, float db = 0, float da = 0, bool isRep = false) { return new Color(M.RepC01(r < 0 ? c.r : r + dr, isRep), M.RepC01(g < 0 ? c.g : g + dg, isRep), M.RepC01(b < 0 ? c.b : b + db, isRep), M.RepC01(a < 0 ? c.a : a + da, isRep)); }
        public static float H(this Color c) { return c.Hsv(0); }
        public static float S(this Color c) { return c.Hsv(1); }
        public static float V(this Color c) { return c.Hsv(2); }
        public static Color H(this Color c, float h) { float[] hsv = c.Hsv(); return C.HsvRgb(h, hsv[1], hsv[2]); }
        public static Color S(this Color c, float s) { float[] hsv = c.Hsv(); return C.HsvRgb(hsv[0], s, hsv[2]); }
        public static Color V(this Color c, float v) { float[] hsv = c.Hsv(); return C.HsvRgb(hsv[0], hsv[1], v); }
        public static Color Hs(this Color c, float h, float s) { float[] hsv = c.Hsv(); return C.HsvRgb(h, s, hsv[2]); }
        public static Color Hv(this Color c, float h, float v) { float[] hsv = c.Hsv(); return C.HsvRgb(h, hsv[1], v); }
        public static Color Sv(this Color c, float s, float v) { float[] hsv = c.Hsv(); return C.HsvRgb(hsv[0], s, v); }
        public static Color Dh(this Color c, float dh, bool isRep = false) { float[] hsv = c.Hsv(); return C.HsvRgb(M.RepC01(hsv[0] + dh, isRep), hsv[1], hsv[2]); }
        public static Color Ds(this Color c, float ds, bool isRep = false) { float[] hsv = c.Hsv(); return C.HsvRgb(hsv[0], M.RepC01(hsv[1] + ds, isRep), hsv[2]); }
        public static Color Dv(this Color c, float dv, bool isRep = false) { float[] hsv = c.Hsv(); return C.HsvRgb(hsv[0], hsv[1], M.RepC01(hsv[2] + dv, isRep)); }
        public static Color Dhsva(Color c, float dh = 0, float ds = 0, float dv = 0, float da = 0, bool isRep = false) { float[] hsv = c.Hsv(); return C.HsvaRgba(M.RepC01(hsv[0] + dh, isRep), M.RepC01(hsv[1] + ds, isRep), M.RepC01(hsv[2] + dv, isRep), M.RepC01(c.a + da, isRep)); }
        public static Color Hsva(Color c, float h = -1, float s = -1, float v = -1, float a = -1, float dh = 0, float ds = 0, float dv = 0, float da = 0, bool isRep = false) { float[] hsv = c.Hsv(); return C.HsvaRgba(M.RepC01(h < 0 ? hsv[0] : h + dh, isRep), M.RepC01(s < 0 ? hsv[1] : s + ds, isRep), M.RepC01(v < 0 ? hsv[2] : v + dv, isRep), M.RepC01(a < 0 ? c.a : a + da, isRep)); }
        public static float[] Hsv(this Color c) { float[] hsv = new float[3]; C.RgbHsv(c, out hsv[0], out hsv[1], out hsv[2]); return hsv; }
        public static float Hsv(this Color c, int i) { return c.Hsv()[i]; }
        public static Vector4 V4(this Color a) { return a; }
        public static string Hex(this Color a, bool isA = true, bool isSharp = true) { return (isSharp ? "#" : "") + (isA ? ColorUtility.ToHtmlStringRGBA(a) : ColorUtility.ToHtmlStringRGB(a)); }
        public static string tS(this Color a) { return "RGBA(" + a.r + ", " + a.g + ", " + a.b + ", " + a.a + ")"; }
    }
    public static class ExtV2 {
        public static Vector2 X(this Vector2 v, float x) { return new Vector2(x, v.y); }
        public static Vector2 Y(this Vector2 v, float y) { return new Vector2(v.x, y); }
        public static Vector2 Nn(this Vector2 v) { return new Vector2(-v.x, -v.y); }
        public static Vector2 Nz(this Vector2 v) { return new Vector2(-v.x, 0); }
        public static Vector2 Np(this Vector2 v) { return new Vector2(-v.x, v.y); }
        public static Vector2 Zn(this Vector2 v) { return new Vector2(0, -v.y); }
        public static Vector2 Zp(this Vector2 v) { return new Vector2(0, v.y); }
        public static Vector2 Pn(this Vector2 v) { return new Vector2(v.x, -v.y); }
        public static Vector2 Pz(this Vector2 v) { return new Vector2(v.x, 0); }
        public static Vector2 Scl(this Vector2 a, Vector2 v) { return V2.Scl(a, v); }
        public static Vector2 Div(this Vector2 a, Vector2 v) { return V2.Div(a, v); }
        public static Vector2 Scl(this Vector2 a, float x, float y) { return V2.Scl(a, V2.V(x, y)); }
        public static Vector2 Div(this Vector2 a, float x, float y) { return V2.Div(a, V2.V(x, y)); }
        public static Vector2Int FloorI(this Vector2 a) { return Vector2Int.FloorToInt(a); }
        public static Vector2Int RoundI(this Vector2 a) { return Vector2Int.RoundToInt(a); }
        public static Vector2Int CeilI(this Vector2 a) { return Vector2Int.CeilToInt(a); }
        public static Vector2 Nor(this Vector2 a) { return a.normalized; }
        public static float Mag(this Vector2 a) { return a.magnitude; }
        public static Vector2 MagC(this Vector2 a, float maxLen) { return V2.MagC(a, maxLen); }
        public static float SqrMag(this Vector2 a) { return V2.SqrMag(a); }
        public static Vector2 V2I(this Vector2 a) { return new Vector2Int((int)a.x, (int)a.y); }
        public static Vector3 V3(this Vector2 a) { return a; }
        public static Vector3Int V3I(this Vector2 a) { return new Vector3Int((int)a.x, (int)a.y, 0); }
        public static Vector4 V4(this Vector2 a) { return a; }
        public static Vector3 Xz(this Vector2 v, float y = 0) { return new Vector3(v.x, y, v.y); }
        public static string S(this Vector2 a) { return "(" + a.x + ", " + a.y + ")"; }
    }
    public static class ExtV2I {
        public static Vector2Int X(this Vector2Int v, int x) { return new Vector2Int(x, v.y); }
        public static Vector2Int Y(this Vector2Int v, int y) { return new Vector2Int(v.x, y); }
        public static Vector2Int Nn(this Vector2Int v) { return new Vector2Int(-v.x, -v.y); }
        public static Vector2Int Nz(this Vector2Int v) { return new Vector2Int(-v.x, 0); }
        public static Vector2Int Np(this Vector2Int v) { return new Vector2Int(-v.x, v.y); }
        public static Vector2Int Zn(this Vector2Int v) { return new Vector2Int(0, -v.y); }
        public static Vector2Int Zp(this Vector2Int v) { return new Vector2Int(0, v.y); }
        public static Vector2Int Pn(this Vector2Int v) { return new Vector2Int(v.x, -v.y); }
        public static Vector2Int Pz(this Vector2Int v) { return new Vector2Int(v.x, 0); }
        public static Vector2Int Scl(this Vector2Int a, Vector2Int v) { return V2I.Scl(a, v); }
        public static Vector2Int Div(this Vector2Int a, Vector2Int v) { return V2I.Div(a, v); }
        public static Vector2Int Scl(this Vector2Int a, int x, int y) { return V2I.Scl(a, V2I.V(x, y)); }
        public static Vector2Int Div(this Vector2Int a, int x, int y) { return V2I.Div(a, V2I.V(x, y)); }
        public static float Mag(this Vector2Int a) { return a.magnitude; }
        public static float SqrMag(this Vector2Int a) { return V2I.SqrMag(a); }
        public static Vector2 V2(this Vector2Int a) { return a; }
        public static Vector3 V3(this Vector2Int a) { return new Vector3(a.x, a.y, 0); }
        public static Vector3Int V3I(this Vector2Int a) { return new Vector3Int(a.x, a.y, 0); }
        public static Vector4 V4(this Vector2Int a) { return new Vector4(a.x, a.y, 0, 0); }
        public static Vector3Int Xz(this Vector2Int v) { return new Vector3Int(v.x, 0, v.y); }
        public static string S(this Vector2Int a) { return "(" + a.x + ", " + a.y + ")"; }
    }
    public static class ExtV3 {
        public static Vector3 X(this Vector3 v, float x) { return new Vector3(x, v.y, v.z); }
        public static Vector3 Y(this Vector3 v, float y) { return new Vector3(v.x, y, v.z); }
        public static Vector3 Z(this Vector3 v, float z) { return new Vector3(v.x, v.y, z); }
        public static Vector3 Xy(this Vector3 v, float x, float y) { return new Vector3(x, y, v.z); }
        public static Vector3 Xz(this Vector3 v, float x, float z) { return new Vector3(x, v.y, z); }
        public static Vector3 Yz(this Vector3 v, float y, float z) { return new Vector3(v.x, y, z); }
        public static Vector3 Nnn(this Vector3 v) { return new Vector3(-v.x, -v.y, -v.z); }
        public static Vector3 Nnz(this Vector3 v) { return new Vector3(-v.x, -v.y, 0); }
        public static Vector3 Nnp(this Vector3 v) { return new Vector3(-v.x, -v.y, v.z); }
        public static Vector3 Nzn(this Vector3 v) { return new Vector3(-v.x, 0, -v.z); }
        public static Vector3 Nzz(this Vector3 v) { return new Vector3(-v.x, 0, 0); }
        public static Vector3 Nzp(this Vector3 v) { return new Vector3(-v.x, 0, v.z); }
        public static Vector3 Npn(this Vector3 v) { return new Vector3(-v.x, v.y, -v.z); }
        public static Vector3 Npz(this Vector3 v) { return new Vector3(-v.x, v.y, 0); }
        public static Vector3 Npp(this Vector3 v) { return new Vector3(-v.x, v.y, v.z); }
        public static Vector3 Znn(this Vector3 v) { return new Vector3(0, -v.y, -v.z); }
        public static Vector3 Znz(this Vector3 v) { return new Vector3(0, -v.y, 0); }
        public static Vector3 Znp(this Vector3 v) { return new Vector3(0, -v.y, v.z); }
        public static Vector3 Zzn(this Vector3 v) { return new Vector3(0, 0, -v.z); }
        public static Vector3 Zzp(this Vector3 v) { return new Vector3(0, 0, v.z); }
        public static Vector3 Zpn(this Vector3 v) { return new Vector3(0, v.y, -v.z); }
        public static Vector3 Zpz(this Vector3 v) { return new Vector3(0, v.y, 0); }
        public static Vector3 Zpp(this Vector3 v) { return new Vector3(0, v.y, v.z); }
        public static Vector3 Pnn(this Vector3 v) { return new Vector3(v.x, -v.y, -v.z); }
        public static Vector3 Pnz(this Vector3 v) { return new Vector3(v.x, -v.y, 0); }
        public static Vector3 Pnp(this Vector3 v) { return new Vector3(v.x, -v.y, v.z); }
        public static Vector3 Pzn(this Vector3 v) { return new Vector3(v.x, 0, -v.z); }
        public static Vector3 Pzz(this Vector3 v) { return new Vector3(v.x, 0, 0); }
        public static Vector3 Pzp(this Vector3 v) { return new Vector3(v.x, 0, v.z); }
        public static Vector3 Ppn(this Vector3 v) { return new Vector3(v.x, v.y, -v.z); }
        public static Vector3 Ppz(this Vector3 v) { return new Vector3(v.x, v.y, 0); }
        public static Vector3 Scl(this Vector3 a, Vector3 v) { return V3.Scl(a, v); }
        public static Vector3 Div(this Vector3 a, Vector3 v) { return V3.Div(a, v); }
        public static Vector3 Scl(this Vector3 a, float x, float y, float z) { return V3.Scl(a, V3.V(x, y, z)); }
        public static Vector3 Div(this Vector3 a, float x, float y, float z) { return V3.Div(a, V3.V(x, y, z)); }
        public static Vector3Int FloorI(this Vector3 a) { return Vector3Int.FloorToInt(a); }
        public static Vector3Int RoundI(this Vector3 a) { return Vector3Int.RoundToInt(a); }
        public static Vector3Int CeilI(this Vector3 a) { return Vector3Int.CeilToInt(a); }
        public static Vector3 Nor(this Vector3 a) { return a.normalized; }
        public static float Mag(this Vector3 a) { return a.magnitude; }
        public static Vector3 MagC(this Vector3 a, float maxLen) { return V3.MagC(a, maxLen); }
        public static float SqrMag(this Vector3 a) { return V3.SqrMag(a); }
        public static Vector2 V2(this Vector3 a) { return a; }
        public static Vector2Int V2I(this Vector3 a) { return new Vector2Int((int)a.x, (int)a.y); }
        public static Vector3Int V3I(this Vector3 a) { return new Vector3Int((int)a.x, (int)a.y, (int)a.z); }
        public static Vector4 V4(this Vector3 a) { return a; }
        public static Vector2 Xz(this Vector3 v) { return new Vector2(v.x, v.z); }
        public static string S(this Vector3 a) { return "(" + a.x + ", " + a.y + ", " + a.z + ")"; }
    }
    public static class ExtV3I {
        public static Vector3Int X(this Vector3Int v, int x) { return new Vector3Int(x, v.y, v.z); }
        public static Vector3Int Y(this Vector3Int v, int y) { return new Vector3Int(v.x, y, v.z); }
        public static Vector3Int Z(this Vector3Int v, int z) { return new Vector3Int(v.x, v.y, z); }
        public static Vector3Int Xy(this Vector3Int v, int x, int y) { return new Vector3Int(x, y, v.z); }
        public static Vector3Int Xz(this Vector3Int v, int x, int z) { return new Vector3Int(x, v.y, z); }
        public static Vector3Int Yz(this Vector3Int v, int y, int z) { return new Vector3Int(v.x, y, z); }
        public static Vector3Int Nnn(this Vector3Int v) { return new Vector3Int(-v.x, -v.y, -v.z); }
        public static Vector3Int Nnz(this Vector3Int v) { return new Vector3Int(-v.x, -v.y, 0); }
        public static Vector3Int Nnp(this Vector3Int v) { return new Vector3Int(-v.x, -v.y, v.z); }
        public static Vector3Int Nzn(this Vector3Int v) { return new Vector3Int(-v.x, 0, -v.z); }
        public static Vector3Int Nzz(this Vector3Int v) { return new Vector3Int(-v.x, 0, 0); }
        public static Vector3Int Nzp(this Vector3Int v) { return new Vector3Int(-v.x, 0, v.z); }
        public static Vector3Int Npn(this Vector3Int v) { return new Vector3Int(-v.x, v.y, -v.z); }
        public static Vector3Int Npz(this Vector3Int v) { return new Vector3Int(-v.x, v.y, 0); }
        public static Vector3Int Npp(this Vector3Int v) { return new Vector3Int(-v.x, v.y, v.z); }
        public static Vector3Int Znn(this Vector3Int v) { return new Vector3Int(0, -v.y, -v.z); }
        public static Vector3Int Znz(this Vector3Int v) { return new Vector3Int(0, -v.y, 0); }
        public static Vector3Int Znp(this Vector3Int v) { return new Vector3Int(0, -v.y, v.z); }
        public static Vector3Int Zzn(this Vector3Int v) { return new Vector3Int(0, 0, -v.z); }
        public static Vector3Int Zzp(this Vector3Int v) { return new Vector3Int(0, 0, v.z); }
        public static Vector3Int Zpn(this Vector3Int v) { return new Vector3Int(0, v.y, -v.z); }
        public static Vector3Int Zpz(this Vector3Int v) { return new Vector3Int(0, v.y, 0); }
        public static Vector3Int Zpp(this Vector3Int v) { return new Vector3Int(0, v.y, v.z); }
        public static Vector3Int Pnn(this Vector3Int v) { return new Vector3Int(v.x, -v.y, -v.z); }
        public static Vector3Int Pnz(this Vector3Int v) { return new Vector3Int(v.x, -v.y, 0); }
        public static Vector3Int Pnp(this Vector3Int v) { return new Vector3Int(v.x, -v.y, v.z); }
        public static Vector3Int Pzn(this Vector3Int v) { return new Vector3Int(v.x, 0, -v.z); }
        public static Vector3Int Pzz(this Vector3Int v) { return new Vector3Int(v.x, 0, 0); }
        public static Vector3Int Pzp(this Vector3Int v) { return new Vector3Int(v.x, 0, v.z); }
        public static Vector3Int Ppn(this Vector3Int v) { return new Vector3Int(v.x, v.y, -v.z); }
        public static Vector3Int Ppz(this Vector3Int v) { return new Vector3Int(v.x, v.y, 0); }
        public static Vector3Int Scl(this Vector3Int a, Vector3Int v) { return V3I.Scl(a, v); }
        public static Vector3Int Div(this Vector3Int a, Vector3Int v) { return V3I.Div(a, v); }
        public static Vector3Int Scl(this Vector3Int a, int x, int y, int z) { return V3I.Scl(a, V3I.V(x, y, z)); }
        public static Vector3Int Div(this Vector3Int a, int x, int y, int z) { return V3I.Div(a, V3I.V(x, y, z)); }
        public static float Mag(this Vector3Int a) { return a.magnitude; }
        public static float SqrMag(this Vector3Int a) { return V3I.SqrMag(a); }
        public static Vector2 V2(this Vector3Int a) { return new Vector2(a.x, a.y); }
        public static Vector2Int V2I(this Vector3Int a) { return new Vector2Int(a.x, a.y); }
        public static Vector3 V3(this Vector3Int a) { return a; }
        public static Vector4 V4(this Vector3Int a) { return new Vector4(a.x, a.y, a.z, 0); }
        public static Vector2Int Xz(this Vector3Int v) { return new Vector2Int(v.x, v.z); }
        public static string S(this Vector3Int a) { return "(" + a.x + ", " + a.y + ", " + a.z + ")"; }
    }
    public static class ExtV4 {
        public static Vector4 X(this Vector4 v, float x) { return new Vector4(x, v.y, v.z, v.w); }
        public static Vector4 Y(this Vector4 v, float y) { return new Vector4(v.x, y, v.z, v.w); }
        public static Vector4 Z(this Vector4 v, float z) { return new Vector4(v.x, v.y, z, v.w); }
        public static Vector4 W(this Vector4 v, float w) { return new Vector4(v.x, v.y, v.z, w); }
        public static Vector4 Xy(this Vector4 v, float x, float y) { return new Vector4(x, y, v.z, v.w); }
        public static Vector4 Xz(this Vector4 v, float x, float z) { return new Vector4(x, v.y, z, v.w); }
        public static Vector4 Xw(this Vector4 v, float x, float w) { return new Vector4(x, v.y, v.z, w); }
        public static Vector4 Yz(this Vector4 v, float y, float z) { return new Vector4(v.x, y, z, v.w); }
        public static Vector4 Yw(this Vector4 v, float y, float w) { return new Vector4(v.x, y, v.z, w); }
        public static Vector4 Zw(this Vector4 v, float z, float w) { return new Vector4(v.x, v.y, z, w); }
        public static Vector4 Xyz(this Vector4 v, float x, float y, float z) { return new Vector4(x, y, z, v.w); }
        public static Vector4 Xyw(this Vector4 v, float x, float y, float w) { return new Vector4(x, y, v.z, w); }
        public static Vector4 Xzw(this Vector4 v, float x, float z, float w) { return new Vector4(x, v.y, z, w); }
        public static Vector4 Yzw(this Vector4 v, float y, float z, float w) { return new Vector4(v.x, y, z, w); }
        public static Vector4 Nnnn(this Vector4 v) { return new Vector4(-v.x, -v.y, -v.z, -v.w); }
        public static Vector4 Nnnz(this Vector4 v) { return new Vector4(-v.x, -v.y, -v.z, 0); }
        public static Vector4 Nnnp(this Vector4 v) { return new Vector4(-v.x, -v.y, -v.z, v.w); }
        public static Vector4 Nnzn(this Vector4 v) { return new Vector4(-v.x, -v.y, 0, -v.w); }
        public static Vector4 Nnzz(this Vector4 v) { return new Vector4(-v.x, -v.y, 0, 0); }
        public static Vector4 Nnzp(this Vector4 v) { return new Vector4(-v.x, -v.y, 0, v.w); }
        public static Vector4 Nnpn(this Vector4 v) { return new Vector4(-v.x, -v.y, v.z, -v.w); }
        public static Vector4 Nnpz(this Vector4 v) { return new Vector4(-v.x, -v.y, v.z, 0); }
        public static Vector4 Nnpp(this Vector4 v) { return new Vector4(-v.x, -v.y, v.z, v.w); }
        public static Vector4 Nznn(this Vector4 v) { return new Vector4(-v.x, 0, -v.z, -v.w); }
        public static Vector4 Nznz(this Vector4 v) { return new Vector4(-v.x, 0, -v.z, 0); }
        public static Vector4 Nznp(this Vector4 v) { return new Vector4(-v.x, 0, -v.z, v.w); }
        public static Vector4 Nzzn(this Vector4 v) { return new Vector4(-v.x, 0, 0, -v.w); }
        public static Vector4 Nzzz(this Vector4 v) { return new Vector4(-v.x, 0, 0, 0); }
        public static Vector4 Nzzp(this Vector4 v) { return new Vector4(-v.x, 0, 0, v.w); }
        public static Vector4 Nzpn(this Vector4 v) { return new Vector4(-v.x, 0, v.z, -v.w); }
        public static Vector4 Nzpz(this Vector4 v) { return new Vector4(-v.x, 0, v.z, 0); }
        public static Vector4 Nzpp(this Vector4 v) { return new Vector4(-v.x, 0, v.z, v.w); }
        public static Vector4 Npnn(this Vector4 v) { return new Vector4(-v.x, v.y, -v.z, -v.w); }
        public static Vector4 Npnz(this Vector4 v) { return new Vector4(-v.x, v.y, -v.z, 0); }
        public static Vector4 Npnp(this Vector4 v) { return new Vector4(-v.x, v.y, -v.z, v.w); }
        public static Vector4 Npzn(this Vector4 v) { return new Vector4(-v.x, v.y, 0, -v.w); }
        public static Vector4 Npzz(this Vector4 v) { return new Vector4(-v.x, v.y, 0, 0); }
        public static Vector4 Npzp(this Vector4 v) { return new Vector4(-v.x, v.y, 0, v.w); }
        public static Vector4 Nppn(this Vector4 v) { return new Vector4(-v.x, v.y, v.z, -v.w); }
        public static Vector4 Nppz(this Vector4 v) { return new Vector4(-v.x, v.y, v.z, 0); }
        public static Vector4 Nppp(this Vector4 v) { return new Vector4(-v.x, v.y, v.z, v.w); }
        public static Vector4 Znnn(this Vector4 v) { return new Vector4(0, -v.y, -v.z, -v.w); }
        public static Vector4 Znnz(this Vector4 v) { return new Vector4(0, -v.y, -v.z, 0); }
        public static Vector4 Znnp(this Vector4 v) { return new Vector4(0, -v.y, -v.z, v.w); }
        public static Vector4 Znzn(this Vector4 v) { return new Vector4(0, -v.y, 0, -v.w); }
        public static Vector4 Znzz(this Vector4 v) { return new Vector4(0, -v.y, 0, 0); }
        public static Vector4 Znzp(this Vector4 v) { return new Vector4(0, -v.y, 0, v.w); }
        public static Vector4 Znpn(this Vector4 v) { return new Vector4(0, -v.y, v.z, -v.w); }
        public static Vector4 Znpz(this Vector4 v) { return new Vector4(0, -v.y, v.z, 0); }
        public static Vector4 Znpp(this Vector4 v) { return new Vector4(0, -v.y, v.z, v.w); }
        public static Vector4 Zznn(this Vector4 v) { return new Vector4(0, 0, -v.z, -v.w); }
        public static Vector4 Zznz(this Vector4 v) { return new Vector4(0, 0, -v.z, 0); }
        public static Vector4 Zznp(this Vector4 v) { return new Vector4(0, 0, -v.z, v.w); }
        public static Vector4 Zzzn(this Vector4 v) { return new Vector4(0, 0, 0, -v.w); }
        public static Vector4 Zzzp(this Vector4 v) { return new Vector4(0, 0, 0, v.w); }
        public static Vector4 Zzpn(this Vector4 v) { return new Vector4(0, 0, v.z, -v.w); }
        public static Vector4 Zzpz(this Vector4 v) { return new Vector4(0, 0, v.z, 0); }
        public static Vector4 Zzpp(this Vector4 v) { return new Vector4(0, 0, v.z, v.w); }
        public static Vector4 Zpnn(this Vector4 v) { return new Vector4(0, v.y, -v.z, -v.w); }
        public static Vector4 Zpnz(this Vector4 v) { return new Vector4(0, v.y, -v.z, 0); }
        public static Vector4 Zpnp(this Vector4 v) { return new Vector4(0, v.y, -v.z, v.w); }
        public static Vector4 Zpzn(this Vector4 v) { return new Vector4(0, v.y, 0, -v.w); }
        public static Vector4 Zpzz(this Vector4 v) { return new Vector4(0, v.y, 0, 0); }
        public static Vector4 Zpzp(this Vector4 v) { return new Vector4(0, v.y, 0, v.w); }
        public static Vector4 Zppn(this Vector4 v) { return new Vector4(0, v.y, v.z, -v.w); }
        public static Vector4 Zppz(this Vector4 v) { return new Vector4(0, v.y, v.z, 0); }
        public static Vector4 Zppp(this Vector4 v) { return new Vector4(0, v.y, v.z, v.w); }
        public static Vector4 Pnnn(this Vector4 v) { return new Vector4(v.x, -v.y, -v.z, -v.w); }
        public static Vector4 Pnnz(this Vector4 v) { return new Vector4(v.x, -v.y, -v.z, 0); }
        public static Vector4 Pnnp(this Vector4 v) { return new Vector4(v.x, -v.y, -v.z, v.w); }
        public static Vector4 Pnzn(this Vector4 v) { return new Vector4(v.x, -v.y, 0, -v.w); }
        public static Vector4 Pnzz(this Vector4 v) { return new Vector4(v.x, -v.y, 0, 0); }
        public static Vector4 Pnzp(this Vector4 v) { return new Vector4(v.x, -v.y, 0, v.w); }
        public static Vector4 Pnpn(this Vector4 v) { return new Vector4(v.x, -v.y, v.z, -v.w); }
        public static Vector4 Pnpz(this Vector4 v) { return new Vector4(v.x, -v.y, v.z, 0); }
        public static Vector4 Pnpp(this Vector4 v) { return new Vector4(v.x, -v.y, v.z, v.w); }
        public static Vector4 Pznn(this Vector4 v) { return new Vector4(v.x, 0, -v.z, -v.w); }
        public static Vector4 Pznz(this Vector4 v) { return new Vector4(v.x, 0, -v.z, 0); }
        public static Vector4 Pznp(this Vector4 v) { return new Vector4(v.x, 0, -v.z, v.w); }
        public static Vector4 Pzzn(this Vector4 v) { return new Vector4(v.x, 0, 0, -v.w); }
        public static Vector4 Pzzz(this Vector4 v) { return new Vector4(v.x, 0, 0, 0); }
        public static Vector4 Pzzp(this Vector4 v) { return new Vector4(v.x, 0, 0, v.w); }
        public static Vector4 Pzpn(this Vector4 v) { return new Vector4(v.x, 0, v.z, -v.w); }
        public static Vector4 Pzpz(this Vector4 v) { return new Vector4(v.x, 0, v.z, 0); }
        public static Vector4 Pzpp(this Vector4 v) { return new Vector4(v.x, 0, v.z, v.w); }
        public static Vector4 Ppnn(this Vector4 v) { return new Vector4(v.x, v.y, -v.z, -v.w); }
        public static Vector4 Ppnz(this Vector4 v) { return new Vector4(v.x, v.y, -v.z, 0); }
        public static Vector4 Ppnp(this Vector4 v) { return new Vector4(v.x, v.y, -v.z, v.w); }
        public static Vector4 Ppzn(this Vector4 v) { return new Vector4(v.x, v.y, 0, -v.w); }
        public static Vector4 Ppzz(this Vector4 v) { return new Vector4(v.x, v.y, 0, 0); }
        public static Vector4 Ppzp(this Vector4 v) { return new Vector4(v.x, v.y, 0, v.w); }
        public static Vector4 Pppn(this Vector4 v) { return new Vector4(v.x, v.y, v.z, -v.w); }
        public static Vector4 Pppz(this Vector4 v) { return new Vector4(v.x, v.y, v.z, 0); }
        public static Vector4 Scl(this Vector4 a, Vector4 v) { return V4.Scl(a, v); }
        public static Vector4 Div(this Vector4 a, Vector4 v) { return V4.Div(a, v); }
        public static Vector4 Scl(this Vector4 a, float x, float y, float z, float w) { return V4.Scl(a, V4.V(x, y, z, w)); }
        public static Vector4 Div(this Vector4 a, float x, float y, float z, float w) { return V4.Div(a, V4.V(x, y, z, w)); }
        public static Vector4 Nor(this Vector4 a) { return a.normalized; }
        public static float Mag(this Vector4 a) { return a.magnitude; }
        public static Vector4 MagC(this Vector4 a, float maxLen) { return V4.MagC(a, maxLen); }
        public static float SqrMag(this Vector4 a) { return V4.SqrMag(a); }
        public static Color C(this Vector4 a) { return a; }
        public static Vector2 V2(this Vector4 a) { return a; }
        public static Vector2Int V2I(this Vector4 a) { return new Vector2Int((int)a.x, (int)a.y); }
        public static Vector3 V3(this Vector4 a) { return a; }
        public static Vector3Int V3I(this Vector4 a) { return new Vector3Int((int)a.x, (int)a.y, (int)a.z); }
        public static string S(this Vector4 a) { return "(" + a.x + ", " + a.y + ", " + a.z + ", " + a.w + ")"; }
    }
    public static class ExtData {
        public static void Set(this Data a, object val) {
            if (Gc.Datas.ContainsKey(a)) {
                string s = a.tS();
                System.Type type = Gc.Datas[a].GetType();
                if (type.IsType<bool>()) PlayerPrefs.SetInt(s, ((bool)val).I());
                else if (type.IsType<int>()) PlayerPrefs.SetInt(s, (int)val);
                else if (type.IsType<float>()) PlayerPrefs.SetFloat(s, (float)val);
                else if (type.IsType<string>()) PlayerPrefs.SetString(s, (string)val);
                else if (type.IsType<Color>()) PlayerPrefs.SetString(s, ((Color)val).tS());
                else if (type.IsType<Quaternion>()) PlayerPrefs.SetString(s, ((Quaternion)val).S());
                else if (type.IsType<Vector2>()) PlayerPrefs.SetString(s, ((Vector2)val).S());
                else if (type.IsType<Vector2Int>()) PlayerPrefs.SetString(s, ((Vector2Int)val).S());
                else if (type.IsType<Vector3>()) PlayerPrefs.SetString(s, ((Vector3)val).S());
                else if (type.IsType<Vector3Int>()) PlayerPrefs.SetString(s, ((Vector3Int)val).S());
                else if (type.IsType<Vector4>()) PlayerPrefs.SetString(s, ((Vector4)val).S());
            }
        }
        public static T Get<T>(this Data a) {
            object res = null;
            if (Gc.Datas.ContainsKey(a)) {
                object data = Gc.Datas[a];
                string s = a.tS();
                System.Type type = data.GetType();
                if (type.IsType<bool>()) res = PlayerPrefs.GetInt(s, ((bool)data).I()).B();
                else if (type.IsType<int>()) res = PlayerPrefs.GetInt(s, (int)data);
                else if (type.IsType<float>()) res = PlayerPrefs.GetFloat(s, (float)data);
                else if (type.IsType<string>()) res = PlayerPrefs.GetString(s, (string)data);
                else if (type.IsType<Color>()) res = PlayerPrefs.GetString(s, ((Color)data).tS()).C();
                else if (type.IsType<Quaternion>()) res = PlayerPrefs.GetString(s, ((Quaternion)data).S()).Q();
                else if (type.IsType<Vector2>()) res = PlayerPrefs.GetString(s, ((Vector2)data).S()).V2();
                else if (type.IsType<Vector2Int>()) res = PlayerPrefs.GetString(s, ((Vector2Int)data).S()).V2I();
                else if (type.IsType<Vector3>()) res = PlayerPrefs.GetString(s, ((Vector3)data).S()).V3();
                else if (type.IsType<Vector3Int>()) res = PlayerPrefs.GetString(s, ((Vector3Int)data).S()).V3I();
                else if (type.IsType<Vector4>()) res = PlayerPrefs.GetString(s, ((Vector4)data).S()).V4();
            }
            return (T)res;
        }
        public static bool B(this Data a) { return a.Get<bool>(); }
        public static int I(this Data a) { return a.Get<int>(); }
        public static float F(this Data a) { return a.Get<float>(); }
        public static string S(this Data a) { return a.Get<string>(); }
        public static Color C(this Data a) { return a.Get<Color>(); }
        public static Quaternion Q(this Data a) { return a.Get<Quaternion>(); }
        public static Vector2 V2(this Data a) { return a.Get<Vector2>(); }
        public static Vector2Int V2I(this Data a) { return a.Get<Vector2Int>(); }
        public static Vector3 V3(this Data a) { return a.Get<Vector3>(); }
        public static Vector3Int V3I(this Data a) { return a.Get<Vector3Int>(); }
        public static Vector4 V4(this Data a) { return a.Get<Vector4>(); }
        public static void SetList<T>(this Data a, List<T> val) {
            if (typeof(T) == typeof(Color)) a.Set(val.S(x => ((Color)(object)x).tS(), ";"));
            else if (typeof(T) == typeof(Quaternion)) a.Set(val.S(x => ((Quaternion)(object)x).S(), ";"));
            else if (typeof(T) == typeof(Vector2)) a.Set(val.S(x => ((Vector2)(object)x).S(), ";"));
            else if (typeof(T) == typeof(Vector3)) a.Set(val.S(x => ((Vector3)(object)x).S(), ";"));
            else if (typeof(T) == typeof(Vector4)) a.Set(val.S(x => ((Vector4)(object)x).S(), ";"));
            else a.Set(val.S(";"));
        }
        public static void SetList<T>(this Data a, List<T> val, System.Func<T, string> f) { a.Set(val.S(f, ";")); }
        public static List<object> GetListObj<T>(this Data key, System.Func<string, T> f) { return key.S().Split(';').List().Parse(x => (object)f(x)); }
        public static List<T> GetList<T>(this Data a, System.Func<string, T> f) { return a.S().Split(';').List().Parse(x => f(x)); }
        public static List<T> GetList<T>(this Data a) {
            List<object> res = new List<object>();
            if (typeof(T) == typeof(bool)) res = GetListObj(a, ExtStr.B);
            else if (typeof(T) == typeof(int)) res = GetListObj(a, ExtStr.I);
            else if (typeof(T) == typeof(float)) res = GetListObj(a, ExtStr.F);
            else if (typeof(T) == typeof(string)) res = GetListObj(a, x => x);
            else if (typeof(T) == typeof(Color)) res = GetListObj(a, ExtStr.C);
            else if (typeof(T) == typeof(Quaternion)) res = GetListObj(a, ExtStr.Q);
            else if (typeof(T) == typeof(Vector2)) res = GetListObj(a, ExtStr.V2);
            else if (typeof(T) == typeof(Vector2Int)) res = GetListObj(a, ExtStr.V2I);
            else if (typeof(T) == typeof(Vector3)) res = GetListObj(a, ExtStr.V3);
            else if (typeof(T) == typeof(Vector3Int)) res = GetListObj(a, ExtStr.V3I);
            else if (typeof(T) == typeof(Vector4)) res = GetListObj(a, ExtStr.V4);
            else res = GetListObj(a, x => (T)(object)x);
            return res.Parse(x => (T)x);
        }
        public static List<bool> ListB(this Data a) { return GetList(a, ExtStr.B); }
        public static List<int> ListI(this Data a) { return GetList(a, ExtStr.I); }
        public static List<float> ListF(this Data a) { return GetList(a, ExtStr.F); }
        public static List<string> ListS(this Data a) { return GetList(a, x => x); }
        public static List<Color> ListC(this Data a) { return GetList(a, ExtStr.C); }
        public static List<Quaternion> ListQ(this Data a) { return GetList(a, ExtStr.Q); }
        public static List<Vector2> ListV2(this Data a) { return GetList(a, ExtStr.V2); }
        public static List<Vector2Int> ListV2I(this Data a) { return GetList(a, ExtStr.V2I); }
        public static List<Vector3> ListV3(this Data a) { return GetList(a, ExtStr.V3); }
        public static List<Vector3Int> ListV3I(this Data a) { return GetList(a, ExtStr.V3I); }
        public static List<Vector4> ListV4(this Data a) { return GetList(a, ExtStr.V4); }
    }
}