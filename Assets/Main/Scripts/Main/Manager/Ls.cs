﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Orgil;

[System.Serializable]
public class DanceFloorData {
    public Vector3 pos;
    public float rot;
    public int sz, botCnt, song;
    public DanceFloorData(Vector3 pos, float rot, int sz, int botCnt, int song) {
        this.pos = pos;
        this.rot = rot;
        this.sz = sz;
        this.botCnt = botCnt;
        this.song = song;
    }
}

[System.Serializable]
public class EnvData {
    public Color player, player2, floor, roof, wallLr, wallBf,
        dfBor, dfDef, df, dfLr, dfF,
        stick, dfWall, ball, ball2, bot, bot2, bullet;
    public List<Color> cubes;
}

[System.Serializable]
public class Level {
    public int env, life;
    public Vector3 playerPos;
    public List<DanceFloorData> dfs;
    public Vector3 flagPos;
}

public class Ls : Singleton<Ls> { // Level Spawner
    public Arrow bulletPf;
    public GameObject shotSongPf, flagPf, impactDirtPf, cubePf;
    public float h;
    [Header("DanceFloor")]
    public DanceFloor dfPf;
    public GameObject dfTilePf, wallPf;
    public float dfSz, dfSpc, dfTm, wallDis, dfW;
    public Vector3 wallSz;
    public Vector2 botSpc;
    [Header("Player")]
    public Vector3 rotLim = V3.V(30, -30, 0);
    public Vector2 mpSpd = V2.V(0.03f, 0.02f);
    public float sitWaitTm = 0.5f, playerSpd = 10, waitTm = 1, botShotTm = 1, bulletSpd = 10;
    public int botShotPwr = 10;
    public Image healthImg;
    public Gradient healthG;
    [Header("Ball")]
    public Ball ballPf;
    public float ballSz = 0.5f, ballM = 0.5f, ballDstTm = 1, ballSpd = 30;
    [Header("Bonus")]
    public int bonusSz = 15, bonusBotCnt = 3;
    public float bonusSpc = 1, bonusStaDis = 20, bonusX = 1;
    [Header("Cube")]
    public float minCubeSz = 0.5f, maxCubeSz = 1.5f;
    public int cubeCnt = 50;
    public List<EnvData> envs;
    public List<Level> levels;
    public bool useLvl = false;
    public int lvl = 1;
    int LvlIdx => Lvl - 1;
    int Lvl => GetLvl(Gc.Level, levels.Count, 1, levels.Count);
    [HideInInspector]
    public List<DanceFloor> dfs = new List<DanceFloor>();
    [HideInInspector]
    public List<DanceFloor> bonusDfs = new List<DanceFloor>();
    [HideInInspector]
    public Level l;
    [HideInInspector]
    public EnvData e;
    [HideInInspector]
    public Mesh ballMsh;
    int cnt = 0, shotCnt = 0;
    float cubeW = 2, gunLen = 1;
    Transform cubesTf;
    public void AddPnt() {
        shotCnt++;
        Cc.I.LevelBar(Gc.Level, shotCnt.F() / cnt);
    }
    public void Init() {
        if (useLvl)
            Gc.Level = lvl;
        LoadLevel();
        LeaderBoardData.SetDatas();
    }
    public void LoadLevel() {
        cubesTf = go.Child(1);
        healthImg.color = healthG.Evaluate(1);
        ballMsh = new Mesh();
        Msh.UvSphere(ref ballMsh, 20, 10, 0.5f, false, true);
        l = levels[LvlIdx];
        e = envs[l.env];
        Player.I.UpdPos(l.playerPos);
        Player.I.go.ChildGo(0, 1, 0, 0).RenMatCol(e.player);
        Player.I.go.ChildGo(0, 1, 1, 0).RenMatCol(e.player2);
        Player.I.life = l.life;
        l.dfs.ForEach(x => DanceFloor(x));
        DanceFloorData bonusData = new DanceFloorData(dfs.Last().playerPos, 0, bonusSz, bonusBotCnt, 0);
        float dis = V3.Dis(bonusData.pos, l.flagPos), bonusDfSz = DfSz(bonusData.sz);
        int n = M.FloorI((dis - bonusStaDis) / (bonusDfSz + bonusSpc));
        Tf pTf = Tf.PR(bonusData.pos, Ang.LookD(bonusData.pos, l.flagPos));
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < 2; j++) {
                Tf dfTf = pTf * Tf.PR(V3.Xz(j == 0 ? -bonusX : bonusX, bonusStaDis + (bonusDfSz + bonusSpc) * i + bonusDfSz * 0.5f), j == 0 ? -90 : 90);
                DanceFloor(new DanceFloorData(dfTf.p, dfTf.r.y, bonusData.sz, bonusData.botCnt, 0), true);
            }
        }
        float dis2 = bonusDfSz * n + bonusSpc * (n - 1);
        CrtCube(pTf * Tf.PS(V3.Z(bonusStaDis + dis2 / 2), V3.V(bonusDfSz * 2 + bonusX * 2, 1, dis2)));
        CrtCube(pTf * Tf.PS(V3.Z(dis / 2 + 2.5f), V3.V(cubeW, 1, dis + 5)));
        for (int i = 0; i < dfs.Count; i++) {
            Vector3 a = i == 0 ? Player.I.Tp : dfs[i - 1].playerPos, b = dfs[i].playerPos;
            CrtCube(Tf.PRS((a + b) / 2, Ang.LookD(a, b), V3.V(cubeW, 1, V3.Dis(a, b))));
        }
        Vector3 min = V3.posInf, max = V3.negInf;
        for (int i = 0; i < dfs.Count; i++) {
            min = V3.Min(min, dfs[i].Tp, dfs[i].wallGo.Tp());
            max = V3.Max(max, dfs[i].Tp, dfs[i].wallGo.Tp());
        }
        for (int i = 0; i < bonusDfs.Count; i++) {
            min = V3.Min(min, bonusDfs[i].Tp);
            max = V3.Max(max, bonusDfs[i].Tp);
        }
        min = V3.Min(min, l.flagPos, Player.I.Tp) - V3.Xz(5);
        max = V3.Max(max, l.flagPos, Player.I.Tp) + V3.Xz(5);
        Vector3 avg = (min + max) / 2, sz = max - min;
        PosScl(go.ChildGo(0, 0), V3.V(min.x, h / 2, avg.z), V3.V(1, h, sz.z), e.wallLr);
        PosScl(go.ChildGo(0, 1), V3.V(max.x, h / 2, avg.z), V3.V(1, h, sz.z), e.wallLr);
        PosScl(go.ChildGo(0, 2), V3.V(avg.x, -0.5f, avg.z), V3.V(sz.x, 1, sz.z), e.floor);
        PosScl(go.ChildGo(0, 3), V3.V(avg.x, h + 0.5f, avg.z), V3.V(sz.x, 1, sz.z), e.roof);
        PosScl(go.ChildGo(0, 4), V3.V(avg.x, h / 2, min.z), V3.V(sz.x, h, 1), e.wallBf);
        PosScl(go.ChildGo(0, 5), V3.V(avg.x, h / 2, max.z), V3.V(sz.x, h, 1), e.wallBf);
        Ins(flagPf, l.flagPos, pTf.q, tf);
        dfs.ForEach(x => cnt += x.bots.Count + 1);
        for (int i = 0; i < cubeCnt; i++) {
            for (int j = 0; j < 100; j++) {
                Vector3 s = V3.V(Rnd.Rng(minCubeSz, maxCubeSz)), p = Rnd.Pos(min, max).Y(s.y / 2), r = V3.Y(Rnd.Ang);
                if (!Physics.CheckBox(p, s / 2, Q.E(r), Lm.Cube)) {
                    CrtCube(Tf.PRS(p, r, s), e.cubes.Rnd());
                    break;
                }
            }
        }
    }
    float DfSz(int n) { return n * dfSz + (n + 1) * dfSpc; }
    void CrtBot(DanceFloor df, Vector3 pos, float rot, bool isBonus = false) {
        Bot bot = Bs.I.Create(pos, V3.Y(rot));
        bot.df = df;
        bot.UpdCol(e.bot);
        if (isBonus)
            bot.Bonus(df);
        df.bots.Add(bot);
    }
    public void DanceFloor(DanceFloorData data, bool isBonus = false) {
        float sz = DfSz(data.sz);
        Tf pTf = Tf.PR(data.pos, data.rot), dfTf = pTf * (isBonus ? Tf.P(V3.Z(sz / 2)) : Tf.P(V3.Z(gunLen + wallSz.z + wallDis + sz / 2)));
        DanceFloor df = Ins(dfPf, dfTf.p, dfTf.q, tf);
        df.data = data;
        df.Child(1).TlpY(h);
        df.Child(1).Childs().ForEach(x => x.RenMatCol(e.stick));
        Transform parTf = df.Child(0);
        Crt.PfGo(dfTilePf, dfTf * Tf.PS(V3.Y(0.001f), V3.V(sz) * 0.1f), parTf).RenMatCol(e.dfBor);
        for (int i = 0; i < data.sz; i++)
            for (int j = 0; j < data.sz; j++)
                df.mats.Add(Crt.PfGo(dfTilePf, dfTf * Tf.PS(V3.V((data.sz.StaD() + j) * (dfSz + dfSpc), 0.002f, (data.sz.StaD() + i) * (dfSz + dfSpc)), V3.V(dfSz * 0.1f)), parTf).RenMat());
        if (isBonus) {
            Dst(df.ChildGo(2));
            df.Bonus();
            for (int i = 0; i < data.botCnt; i++)
                CrtBot(df, dfTf.Pnt(V3.X((data.botCnt.StaD() + i) * botSpc.x)), data.rot + 180, true);
            bonusDfs.Add(df);
        } else {
            for (int i = 0, m = M.RoundI(sz / botSpc.x), n = (data.botCnt - 1) / m; i <= n; i++)
                for (int j = 0, o = data.botCnt / m == i ? data.botCnt % m : m; j < o; j++)
                    CrtBot(df, dfTf.Pnt(V3.Xz((o.StaD() + j) * botSpc.x, -sz / 2 - (i + 0.5f) * botSpc.y)), data.rot + 180);
            df.Bc(V3.V(sz - 1, 0.2f, sz - 1), V3.Y(0.1f), true, null, GcTp.Add);
            PosScl(df.ChildGo(2, 0), V3.Xy(-sz / 2 - dfW / 2, h / 2), V3.V(dfW, h, sz), e.dfLr);
            PosScl(df.ChildGo(2, 1), V3.Xy(sz / 2 + dfW / 2, h / 2), V3.V(dfW, h, sz), e.dfLr);
            PosScl(df.ChildGo(2, 2), V3.Yz(h / 2, sz / 2 + dfW / 2), V3.V(sz, h, dfW), e.dfF);
            df.wallGo = Crt.PfGo(wallPf, Tf.NPRS("Wall", pTf.Pnt(V3.Z(1 + wallSz.z / 2)), data.rot, wallSz), df.tf);
            df.wallGo.Child(0).RenMatCol(e.dfWall);
            df.playerPos = pTf.p;
            dfs.Add(df);
            Vector3 colSz = V3.V(sz + dfW * 2, 1, gunLen + wallSz.z + wallDis + sz + dfSz);
            CrtCube(pTf * Tf.PS(V3.Z(colSz.z / 2), colSz));
        }
        df.UpdCol();
    }
    void CrtCube(Tf pTf, Color c = default) {
        GameObject cubeGo = Ins(cubePf, pTf.p, pTf.q, cubesTf), cGo = cubeGo.ChildGo(0);
        cubeGo.BcSz(pTf.s);
        cGo.Tls(pTf.s);
        if (c.IsDef()) {
            cGo.Hide();
        } else {
            cGo.RenMatCol(c);
        }
    }
    void PosScl(GameObject go, Vector3 pos, Vector3 scl, Color col) {
        go.Tlp(pos);
        go.Tls(scl);
        go.RenMatCol(col);
    }
    int GetLvl(int lvl, int lvlCnt, int rnd1, int rnd2) {
        if (lvlCnt <= 1)
            return 1;
        if (Data.LevelData.S().IsNe())
            Data.LevelData.SetList(A.ListAp(1, 1, lvlCnt));
        List<int> lis = Data.LevelData.ListI();
        if (lvl <= lis.Count) {
            return lis[lvl - 1];
        } else {
            int prvLvl = lis.Last();
            for (int i = lis.Count + 1; i <= lvl; i++) {
                prvLvl = prvLvl == rnd1 ? Rnd.RngIn(rnd1 + 1, rnd2) : prvLvl == rnd2 ? Rnd.RngIn(rnd1, rnd2 - 1) : Rnd.P((prvLvl - rnd1).F() / (rnd2 - rnd1)) ? Rnd.RngIn(rnd1, prvLvl - 1) : Rnd.RngIn(prvLvl + 1, rnd2);
                lis.Add(prvLvl);
            }
            Data.LevelData.SetList(lis);
            return prvLvl;
        }
    }
}
// ░ ▒ ▓ █ ▄ ▀ ■
// ┌ ┬ ┐ ─ ╔ ╦ ╗ ═
// ├ ┼ ┤ │ ╠ ╬ ╣ ║
// └ ┴ ┘   ╚ ╩ ╝
// ⁰¹²³⁴⁵⁶⁷⁸⁹ ⁻⁺⁼⁽⁾ ⁱⁿ superscript
// ₀₁₂₃₄₅₆₇₈₉ ₋₊₌₍₎⨧ ᵣᵤₐᵢⱼₓₑᵥₒₔ ᵪᵧᵦᵨᵩ subscript
// _ ¯ ~ ≡ ‗ ¦ ¨ ¬ · |
// ñ Ñ @ ¿ ? ¡ ! : / \ frequently-used
// á é í ó ú Á É Í Ó Ú vowels acute accent
// ä ë ï ö ü Ä Ë Ï Ö Ü vowels with diaresis
// ½ ¼ ¾ ¹ ³ ² ƒ ± × ÷ mathematical symbols
// $ £ ¥ ¢ ¤ ® © ª º ° commercial / trade symbols
// " ' ( ) [ ] { } « » quotes and parenthesis