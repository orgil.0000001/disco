using UnityEditor;
using UnityEngine;
using Orgil;

public partial class EditorMenuItem : Editor {

    [MenuItem("GameObject/UI/ExceptionHandler")]
    public static void ExceptionHandler() {
        CreatePrefab("ExceptionHandler", "ExceptionHandler");
    }

    public static GameObject CreatePrefab(string path, string name) {
        GameObject go = (GameObject)PrefabUtility.InstantiatePrefab(A.LoadGo(path));
        go.name = name;
        go.Par(UnityEditor.Selection.activeGameObject.transform, false);
        return go;
    }

    public static GameObject Create(string path, string name) {
        GameObject go = Mb.Ins(A.LoadGo(path));
        go.name = name;
        go.Par(UnityEditor.Selection.activeGameObject.transform, false);
        return go;
    }
}