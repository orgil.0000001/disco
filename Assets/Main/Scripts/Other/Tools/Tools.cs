using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;
using System.Linq.Expressions;
using UnityEditor;

namespace Orgil {
    public enum OpTp { Add, Sub, Mul, Div }
    public enum RenMd { Opaque, Cutout, Fade, Transparent }
    public enum RoadDir { LD, FD, RD, L, F, R, LU, FU, RU }
    public enum RoadCrtDirTp { Nxt, NxtAvg, PrvNxt, PrvNxtAvg }
    public enum Vib { Success, Warning, Error, Light, Medium, Heavy }
    public enum CardTp { English_Black, English, Russian, Old, Face, Letter, Russian_A, Old_A, Uno }
    public enum CardLetter { DA, D2, D3, D4, D5, D6, D7, D8, D9, DJ, DQ, DK, CA, C2, C3, C4, C5, C6, C7, C8, C9, CJ, CQ, CK, HA, H2, H3, H4, H5, H6, H7, H8, H9, HJ, HQ, HK, SA, S2, S3, S4, S5, S6, S7, S8, S9, SJ, SQ, SK, JR, JB }
    public enum UnoCardLetter { R0, R1, R2, R3, R4, R5, R6, R7, R8, R9, RS, RR, RD2, Y0, Y1, Y2, Y3, Y4, Y5, Y6, Y7, Y8, Y9, YS, YR, YD2, G0, G1, G2, G3, G4, G5, G6, G7, G8, G9, GS, GR, GD2, B0, B1, B2, B3, B4, B5, B6, B7, B8, B9, BS, BR, BD2, W, WD4 }
    public enum CardBackTp { Lion_Black = 65, Lion_Green, Lion_Red, Lion_Purple, Lion_Blue, Sword_Black, Sword_Green, Sword_Red, Sword_Purple, Sword_Blue, O_Red, O_Blue, Fill_Black = 52, Fill_Green, Fill_Red, Fill_Purple, Fill_Blue, Border_Black, Border_Green, Border_Red, Border_Purple, Border_Blue, Q_Red, Q_Black, DLine_Red = 39, DLine_Blue, Line_Red, Line_Blue, ILine_Red, ILine_Blue, Rect_Red, Rect_Blue, Russian_Green, Russian_Blue, Bicycle_Red, Bicycle_Blue, Blue_Pattern = 26, Red_Pattern, Blue_Dot, Rect_Border_Red, Blank, Pattern_Brown, Pattern_Y, Pattern_Light_Y, Pattern_Purple, Pattern_Blue, Pattern_Green, Pattern_Red, English_Black_JR = 13, English_Black_JB, English_JR, English_JB, Russian_JR, Russian_JB, Old_JR, Old_JB, Russian_A_DA, Old_A_CA, Face_JR = 0, Face_JB, Letter_JR, Letter_JB, JR, JB, Uno_W, Uno_WD4, Uno }
    public enum SphereTp { Tetrahedron, Hexahedron, Octahedron, Dodecahedron, Icosahedron }
    public enum FillTp { None, One, Fill, Connect }
    public enum UvTp { Dis, Fc, Cor }
    public enum CompTp { Equals, NotEqual, GreaterThan, SmallerThan, SmallerOrEqual, GreaterOrEqual, Or }
    public enum DisableTp { ReadOnly, DontDraw }
    public enum GcTp { Get, Add, GetAdd }
    [System.AttributeUsage(System.AttributeTargets.Property | System.AttributeTargets.Field, AllowMultiple = true)]
    public class DrawIfAttribute : PropertyAttribute {
        public string name;
        public System.Type type;
        public object value;
        public CompTp compTp;
        public DisableTp disableTp;
        public object[] values;
        public DrawIfAttribute(string name, System.Type type, object value,
            CompTp comparisonType = CompTp.Equals, DisableTp disablingType = DisableTp.DontDraw) {
            this.name = name;
            this.type = type;
            this.value = value;
            this.compTp = comparisonType;
            this.disableTp = disablingType;
        }
        public DrawIfAttribute(string name, System.Type type, params object[] values) {
            this.name = name;
            this.type = type;
            this.values = values;
            compTp = CompTp.Or;
            disableTp = DisableTp.DontDraw;
        }
    }
    public class PntData {
        public Vector3 p;
        public bool isFc;
        public float uv;
        public Vector2 r;
        public PntData(Vector3 p, bool isFc, float uv, Vector2 r) { this.p = p; this.isFc = isFc; this.uv = uv; this.r = r; }
    }
    public class TreeData<T> {
        public T parent;
        public List<T> childs = new List<T>();
        public List<TreeData<T>> datas = new List<TreeData<T>>();
        public TreeData(T parent) { this.parent = parent; }
        public TreeData(T parent, params T[] childs) { this.parent = parent; this.childs = childs.List(); }
        public TreeData(T parent, params TreeData<T>[] datas) { this.parent = parent; this.datas = datas.List(); }
        public void Do(GameObject go, System.Action<GameObject, TreeData<T>, int[]> f) { Do(this, go, f); }
        void Do(TreeData<T> a, GameObject go, System.Action<GameObject, TreeData<T>, int[]> f, string childs = "") {
            if (a.NotNull()) {
                f(go, a, childs.IntArr());
                for (int i = 0; i < a.datas.Count; i++)
                    Do(a.datas[i], go, f, childs + " " + i);
            }
        }
    }
    [System.Serializable]
    public class LevelTile {
        public Vector2Int pos;
        public bool[] actives = new bool[5];
        public LevelTile(Vector2Int pos, bool isBody, bool isLF, bool isRF, bool isRB, bool isLB) { this.pos = pos; actives[0] = isBody; actives[1] = isLF; actives[2] = isRF; actives[3] = isRB; actives[4] = isLB; }
        public LevelTile(Vector2Int pos, bool[] actives) { this.pos = pos; this.actives = actives; }
    }
    public class RoadData {
        public RoadDir dir;
        public Vector3Int pos;
        public Quaternion rot;
        public RoadData(RoadDir dir, Vector3Int pos, Quaternion rot) { this.dir = dir; this.pos = pos; this.rot = rot; }
    }
    [System.Serializable]
    public class VarTime<T> {
        public T val;
        public float t = 0;
        public VarTime(T val, float t) { this.val = val; this.t = t; }
        public void Set(T val) { this.val = val; t = Time.time; }
        public void Set(T val, float t) { this.val = val; this.t = t; }
        public void Set<V>(V obj, string n, T val, float t) { Gc.I.VarTimeSet(obj, n, val, t); }
    }
    [System.Serializable]
    public class Tf {
        public static Tf New => new Tf();
        public string n = "";
        public Vector3 p = V3.O, r = V3.O, s = V3.I;
        public Quaternion q => Q.E(r);
        public Tf C => new Tf(p, r, s);
        public Tf Cn => new Tf(n, p, r, s);
        public Tf(string n, Vector3 p, Vector3 r, Vector3 s) { this.n = n; this.p = p; this.r = r; this.s = s; }
        public Tf(string n, Vector3 p, Quaternion r, Vector3 s) { this.n = n; this.p = p; this.r = r.eulerAngles; this.s = s; }
        public Tf(string n, Vector3 p, Vector3 r) { this.n = n; this.p = p; this.r = r; }
        public Tf(string n, Vector3 p, Quaternion r) { this.n = n; this.p = p; this.r = r.eulerAngles; }
        public Tf(string n, Vector3 p) { this.n = n; this.p = p; }
        public Tf(string n) { this.n = n; }
        public Tf(Vector3 p, Vector3 r, Vector3 s) { this.p = p; this.r = r; this.s = s; }
        public Tf(Vector3 p, Quaternion r, Vector3 s) { this.p = p; this.r = r.eulerAngles; this.s = s; }
        public Tf(Vector3 p, Vector3 r) { this.p = p; this.r = r; }
        public Tf(Vector3 p, Quaternion r) { this.p = p; this.r = r.eulerAngles; }
        public Tf(Vector3 p) { this.p = p; }
        public Tf() { }
        public void Set(string n, Vector3 p, Vector3 r, Vector3 s) { this.n = n; this.p = p; this.r = r; this.s = s; }
        public static Tf NPRS(string n, Vector3 p, Vector3 r, Vector3 s) { return new Tf(n, p, r, s); }
        public static Tf NPRS(string n, Vector3 p, Vector3 r, float s) { return new Tf(n, p, r, V3.V(s)); }
        public static Tf NPRS(string n, Vector3 p, Quaternion r, Vector3 s) { return new Tf(n, p, r, s); }
        public static Tf NPRS(string n, Vector3 p, Quaternion r, float s) { return new Tf(n, p, r, V3.V(s)); }
        public static Tf NPRS(string n, Vector3 p, float r, Vector3 s) { return new Tf(n, p, V3.Y(r), s); }
        public static Tf NPRS(string n, Vector3 p, float r, float s) { return new Tf(n, p, V3.Y(r), V3.V(s)); }
        public static Tf NPR(string n, Vector3 p, Vector3 r) { return new Tf(n, p, r, V3.I); }
        public static Tf NPR(string n, Vector3 p, Quaternion r) { return new Tf(n, p, r, V3.I); }
        public static Tf NPR(string n, Vector3 p, float r) { return new Tf(n, p, V3.Y(r), V3.I); }
        public static Tf NPS(string n, Vector3 p, Vector3 s) { return new Tf(n, p, V3.O, s); }
        public static Tf NPS(string n, Vector3 p, float s) { return new Tf(n, p, V3.O, V3.V(s)); }
        public static Tf NRS(string n, Vector3 r, Vector3 s) { return new Tf(n, V3.O, r, s); }
        public static Tf NRS(string n, Vector3 r, float s) { return new Tf(n, V3.O, r, V3.V(s)); }
        public static Tf NRS(string n, Quaternion r, Vector3 s) { return new Tf(n, V3.O, r, s); }
        public static Tf NRS(string n, Quaternion r, float s) { return new Tf(n, V3.O, r, V3.V(s)); }
        public static Tf NRS(string n, float r, Vector3 s) { return new Tf(n, V3.O, V3.Y(r), s); }
        public static Tf NRS(string n, float r, float s) { return new Tf(n, V3.O, V3.Y(r), V3.V(s)); }
        public static Tf PRS(Vector3 p, Vector3 r, Vector3 s) { return new Tf("", p, r, s); }
        public static Tf PRS(Vector3 p, Vector3 r, float s) { return new Tf("", p, r, V3.V(s)); }
        public static Tf PRS(Vector3 p, Quaternion r, Vector3 s) { return new Tf("", p, r, s); }
        public static Tf PRS(Vector3 p, Quaternion r, float s) { return new Tf("", p, r, V3.V(s)); }
        public static Tf PRS(Vector3 p, float r, Vector3 s) { return new Tf("", p, V3.Y(r), s); }
        public static Tf PRS(Vector3 p, float r, float s) { return new Tf("", p, V3.Y(r), V3.V(s)); }
        public static Tf NP(string n, Vector3 p) { return new Tf(n, p, V3.O, V3.I); }
        public static Tf NR(string n, Vector3 r) { return new Tf(n, V3.O, r, V3.I); }
        public static Tf NR(string n, Quaternion r) { return new Tf(n, V3.O, r, V3.I); }
        public static Tf NR(string n, float r) { return new Tf(n, V3.O, V3.Y(r), V3.I); }
        public static Tf NS(string n, Vector3 s) { return new Tf(n, V3.O, V3.O, s); }
        public static Tf NS(string n, float s) { return new Tf(n, V3.O, V3.O, V3.V(s)); }
        public static Tf PR(Vector3 p, Vector3 r) { return new Tf("", p, r, V3.I); }
        public static Tf PR(Vector3 p, Quaternion r) { return new Tf("", p, r, V3.I); }
        public static Tf PR(Vector3 p, float r) { return new Tf("", p, V3.Y(r), V3.I); }
        public static Tf PS(Vector3 p, Vector3 s) { return new Tf("", p, V3.O, s); }
        public static Tf PS(Vector3 p, float s) { return new Tf("", p, V3.O, V3.V(s)); }
        public static Tf RS(Vector3 r, Vector3 s) { return new Tf("", V3.O, r, s); }
        public static Tf RS(Vector3 r, float s) { return new Tf("", V3.O, r, V3.V(s)); }
        public static Tf RS(Quaternion r, Vector3 s) { return new Tf("", V3.O, r, s); }
        public static Tf RS(Quaternion r, float s) { return new Tf("", V3.O, r, V3.V(s)); }
        public static Tf RS(float r, Vector3 s) { return new Tf("", V3.O, V3.Y(r), s); }
        public static Tf RS(float r, float s) { return new Tf("", V3.O, V3.Y(r), V3.V(s)); }
        public static Tf N(string n) { return new Tf(n, V3.O, V3.O, V3.I); }
        public static Tf P(Vector3 p) { return new Tf("", p, V3.O, V3.I); }
        public static Tf R(Vector3 r) { return new Tf("", V3.O, r, V3.I); }
        public static Tf R(Quaternion r) { return new Tf("", V3.O, r, V3.I); }
        public static Tf R(float r) { return new Tf("", V3.O, V3.Y(r), V3.I); }
        public static Tf S(Vector3 s) { return new Tf("", V3.O, V3.O, s); }
        public static Tf S(float s) { return new Tf("", V3.O, V3.O, V3.V(s)); }
        public Tf Np(string n, Vector3 p) { this.n = n; this.p = p; return this; }
        public Tf Nr(string n, Vector3 r) { this.n = n; this.r = r; return this; }
        public Tf Nr(string n, Quaternion r) { this.n = n; this.r = r.eulerAngles; return this; }
        public Tf Nr(string n, float r) { this.n = n; this.r = V3.Y(r); return this; }
        public Tf Ns(string n, Vector3 s) { this.n = n; this.s = s; return this; }
        public Tf Ns(string n, float s) { this.n = n; this.s = V3.V(s); return this; }
        public Tf Pr(Vector3 p, Vector3 r) { this.p = p; this.r = r; return this; }
        public Tf Pr(Vector3 p, Quaternion r) { this.p = p; this.r = r.eulerAngles; return this; }
        public Tf Pr(Vector3 p, float r) { this.p = p; this.r = V3.Y(r); return this; }
        public Tf Ps(Vector3 p, Vector3 s) { this.p = p; this.s = s; return this; }
        public Tf Ps(Vector3 p, float s) { this.p = p; this.s = V3.V(s); return this; }
        public Tf Rs(Vector3 r, Vector3 s) { this.r = r; this.s = s; return this; }
        public Tf Rs(Vector3 r, float s) { this.r = r; this.s = V3.V(s); return this; }
        public Tf Rs(Quaternion r, Vector3 s) { this.r = r.eulerAngles; this.s = s; return this; }
        public Tf Rs(Quaternion r, float s) { this.r = r.eulerAngles; this.s = V3.V(s); return this; }
        public Tf Rs(float r, Vector3 s) { this.r = V3.Y(r); this.s = s; return this; }
        public Tf Rs(float r, float s) { this.r = V3.Y(r); this.s = V3.V(s); return this; }
        public Tf N_(string n) { this.n = n; return this; }
        public Tf P_(Vector3 p) { this.p = p; return this; }
        public Tf R_(Vector3 r) { this.r = r; return this; }
        public Tf R_(Quaternion r) { this.r = r.eulerAngles; return this; }
        public Tf R_(float r) { this.r = V3.Y(r); return this; }
        public Tf S_(Vector3 s) { this.s = s; return this; }
        public Tf S_(float s) { this.s = V3.V(s); return this; }
        public Tf Mul(params Tf[] tfs) { return MUL(Mb.Arr(this).Add(tfs)); }
        public Vector3 Pnt(Vector3 pos) { return Pnt(this, pos); }
        public Vector3 InvPnt(Vector3 pos) { return InvPnt(this, pos); }
        public Vector3 Dir(Vector3 dir) { return Dir(q, dir); }
        public Vector3 Rot(Vector3 pos) { return Rot(q, pos); }
        public Vector3 RotAround(Vector3 pnt, Vector3 axis, float ang) { return RotAround(this, pnt, axis, ang); }
        public Quaternion LookAt(Vector3 tar, Vector3 up) { return LookAt(p, tar, up); }
        public static Tf MUL(params Tf[] tfs) {
            Tf res = tfs[tfs.Length - 1];
            for (int i = tfs.Length - 2; i >= 0; i--)
                res = new Tf(tfs[i].Pnt(res.p), tfs[i].q * res.q, V3.Scl(tfs[i].s, res.s));
            return res;
        }
        public static Tf operator *(Tf a, Tf b) { return MUL(a, b); }
        public static explicit operator Tf(Transform a) { return a.Tf(Space.World, true); }
        public static Vector3 Pnt(Tf tf, Vector3 pos) { return Matrix4x4.TRS(tf.p, tf.q, tf.s).MultiplyPoint3x4(pos); }
        public static Vector3 InvPnt(Tf tf, Vector3 pos) { return Matrix4x4.TRS(tf.p, tf.q, tf.s).inverse.MultiplyPoint3x4(pos); }
        public static Vector3 Dir(Quaternion rot, Vector3 dir) { return rot * dir; }
        public static Vector3 Dir(Vector3 rot, Vector3 dir) { return Dir(Q.E(rot), dir); }
        public static Vector3 Rot(Quaternion rot, Vector3 pos) { return Matrix4x4.Rotate(rot).MultiplyPoint3x4(pos); }
        public static Vector3 Rot(Vector3 rot, Vector3 pos) { return Rot(Q.E(rot), pos); }
        public static Vector3 RotAround(Tf tf, Vector3 pnt, Vector3 axis, float ang) { return pnt + Q.AngAxis(ang, axis) * (tf.p - pnt); }
        public static Quaternion LookAt(Vector3 pos, Vector3 tar, Vector3 up) { return Q.LookRot(tar - pos, up); }
    }
    public class Fo {
        public int frm = 0;
        public string childs = "0";
        public float px = 10000, py = 10000, pz = 10000;
        public float rx = 10000, ry = 10000, rz = 10000;
        public float sx = 10000, sy = 10000, sz = 10000;
        public float cr = 10000, cg = 10000, cb = 10000, ca = 10000;
        public Fo() { }
        public Fo(int frm) { this.frm = frm; }
        public Fo(float sec) { this.frm = sec.Frm(); }
        public Fo(string childs) { this.childs = childs; }
        public Fo(params int[] childs) { this.childs = childs.S(" "); }
        public Fo(int frm, string childs) { this.frm = frm; this.childs = childs; }
        public Fo(float sec, string childs) { this.frm = sec.Frm(); this.childs = childs; }
        public Fo(int frm, string childs, float px, float py, float pz, float rx, float ry, float rz, float sx, float sy, float sz, float cr, float cg, float cb, float ca) { this.frm = frm; this.childs = childs; this.px = px; this.py = py; this.pz = pz; this.rx = rx; this.ry = ry; this.rz = rz; this.sx = sx; this.sy = sy; this.sz = sz; this.cr = cr; this.cg = cg; this.cb = cb; this.ca = ca; }
        public static Fo Fc(int frm, int child) => new Fo(frm, "" + child);
        public static Fo Fc(float sec, int child) => new Fo(sec, "" + child);
        public static Fo Fc(int frm) => new Fo(frm, "");
        public static Fo Fc(float sec) => new Fo(sec, "");
        public static Fo F(int frm) => new Fo(frm);
        public static Fo F(float sec) => new Fo(sec);
        public static Fo F(string childs) => new Fo(childs);
        public static Fo F(params int[] childs) => new Fo(childs);
        public static Fo F(int frm, string childs) => new Fo(frm, childs);
        public static Fo F(float sec, string childs) => new Fo(sec, childs);
        public Fo Frm(int frm) { this.frm = frm; return this; }
        public Fo Frm(float sec) { this.frm = sec.Frm(); return this; }
        public Fo Path(string childs) { this.childs = childs; return this; }
        public Fo Path(params int[] childs) { this.childs = childs.S(" "); return this; }
        public Fo P(float x, float y, float z) { px = x; py = y; pz = z; return this; }
        public Fo P(float v) { px = v; py = v; pz = v; return this; }
        public Fo P(Vector3 p) { px = p.x; py = p.y; pz = p.z; return this; }
        public Fo Pxy(float x, float y) { px = x; py = y; return this; }
        public Fo Pxz(float x, float z) { px = x; pz = z; return this; }
        public Fo Pyz(float y, float z) { py = y; pz = z; return this; }
        public Fo Px(float x) { px = x; return this; }
        public Fo Py(float y) { py = y; return this; }
        public Fo Pz(float z) { pz = z; return this; }
        public Fo R(float x, float y, float z) { rx = x; ry = y; rz = z; return this; }
        public Fo R(float v) { rx = v; ry = v; rz = v; return this; }
        public Fo R(Vector3 r) { rx = r.x; ry = r.y; rz = r.z; return this; }
        public Fo R(Quaternion q) { Vector3 r = q.eulerAngles; rx = r.x; ry = r.y; rz = r.z; return this; }
        public Fo Rxy(float x, float y) { rx = x; ry = y; return this; }
        public Fo Rxz(float x, float z) { rx = x; rz = z; return this; }
        public Fo Ryz(float y, float z) { ry = y; rz = z; return this; }
        public Fo Rx(float x) { rx = x; return this; }
        public Fo Ry(float y) { ry = y; return this; }
        public Fo Rz(float z) { rz = z; return this; }
        public Fo S(float x, float y, float z) { sx = x; sy = y; sz = z; return this; }
        public Fo S(float v) { sx = v; sy = v; sz = v; return this; }
        public Fo S(Vector3 s) { sx = s.x; sy = s.y; sz = s.z; return this; }
        public Fo Sxy(float x, float y) { sx = x; sy = y; return this; }
        public Fo Sxz(float x, float z) { sx = x; sz = z; return this; }
        public Fo Syz(float y, float z) { sy = y; sz = z; return this; }
        public Fo Sx(float x) { sx = x; return this; }
        public Fo Sy(float y) { sy = y; return this; }
        public Fo Sz(float z) { sz = z; return this; }
        public Fo C(float r, float g, float b, float a) { cr = r; cg = g; cb = b; ca = a; return this; }
        public Fo C(float r, float g, float b) { cr = r; cg = g; cb = b; return this; }
        public Fo C(string hex) { Color c = hex.Hex(); cr = c.r; cg = c.g; cb = c.b; return this; }
        public Fo Ca(string hex) { Color c = hex.Hex(); cr = c.r; cg = c.g; cb = c.b; ca = c.a; return this; }
        public Fo C(Color c) { cr = c.r; cg = c.g; cb = c.b; return this; }
        public Fo Ca(Color c) { cr = c.r; cg = c.g; cb = c.b; ca = c.a; return this; }
        public Fo Cr(float r) { cr = r; return this; }
        public Fo Cg(float g) { cg = g; return this; }
        public Fo Cb(float b) { cb = b; return this; }
        public Fo Ca(float a) { ca = a; return this; }
    }
    public static partial class A {
        public static bool IsStarting => Gc.State == GameState.Starting;
        public static bool IsPlaying => Gc.State == GameState.Playing;
        public static bool IsPause => Gc.State == GameState.Pause;
        public static bool IsLevelCompleted => Gc.State == GameState.LevelCompleted;
        public static bool IsGameOver => Gc.State == GameState.GameOver;
        public static bool IsSettings => Gc.State == GameState.Settings;
        public static bool IsShifting => Gc.State == GameState.Shifting;
        public static bool IsMb => Input.GetMouseButton(0);
        public static bool IsMbD => Input.GetMouseButtonDown(0);
        public static bool IsMbU => Input.GetMouseButtonUp(0);
        public static Vector3 Mp => Input.mousePosition;
        public static Vector3 G { get => Physics.gravity; set => Physics.gravity = value; }
        public static float Dt => Time.deltaTime;
        public static List<string> PerfectWords = new List<string>() { "PERFECT", "AMAZING", "FANTASTIC", "AWESOME", "SUPER" };
        public static List<string> GoodWords = new List<string>() { "GOOD", "NICE", "GREAT", "ABSOLUTE", "CLEAR" };
        public static readonly List<string> Names = new List<string>() { "Guy", "Hill", "Brittany", "Townsend", "Santos", "Roberson", "Jeffrey", "Jackson", "Lindsey", "Wheeler", "Madeline", "Cobb", "Paula", "Gibbs", "Angel", "Underwood", "Judy", "Adkins", "Johnathan", "Quinn", "Isabel", "Ray", "Kerry", "Schneider", "Milton", "Martin", "Robyn", "Farmer", "Clifford", "Dennis", "Leslie", "Marshall", "Marc", "Wise", "Alfonso", "Holmes", "Tyrone", "Gilbert", "Catherine", "Simon", "Nina", "Bowers", "Darryl", "Potter", "Jennie", "Sherman", "Danny", "Caldwell", "David", "James", "Adam", "Oliver", "Doyle", "Stephens", "Cody", "Mcguire", "Gerardo", "Hanson", "Jo", "Sanders", "Owen", "Andrews", "Archie", "Garcia", "Eric", "Turner", "Ella", "Cook", "Rudolph", "Franklin", "Perry", "Lindsey", "Dianne", "Medina", "Jane", "Rhodes", "Woodrow", "Gonzales", "Lila", "Ballard", "Christian", "Walsh", "Jon", "Hall", "Kate", "Love", "Elizabeth", "Griffin", "Elsie", "Keller", "Rosalie", "Fitzgerald", "Deanna", "Harris", "Cristina", "Gregory", "Jean", "Griffith", "Lori", "Olson", "Laura", "Bass", "Rex", "Lee", "Tara", "Morrison", "Dan", "Haynes", "Frances", "Shelton", "Lucas", "Walters", "Juana", "Singleton", "Wallace", "Shaw", "Craig", "Rivera", "Juanita", "Norris", "Tommy", "Gomez", "Roman", "Castro", "Carroll", "Todd", "Jaime", "Poole", "Scott", "Craig", "Laurie", "Frank", "Emily", "Hampton", "Guillermo", "Lynch", "Andre", "Flowers", "Betty", "Gutierrez", "Josh", "Willis", "Karen", "Williamson", "Peggy", "Peters", "Timmy", "Gross", "Joan", "Gill", "Cora", "Gonzalez", "Jacquelyn", "Curtis", "Traci", "Bates", "Homer", "Larson", "Tabitha", "Butler", "Molly", "Holt", "Arthur", "Meyer", "Jessie", "Torres", "Dora", "Foster", "Marty", "Stone", "Teri", "Berry", "Spencer", "Norton", "Hazel", "Reid", "Stuart", "Goodwin", "Oscar", "Freeman", "Ross", "Weber", "Michele", "Diaz", "Bernadette", "Chapman", "Erick", "Fleming", "Shannon", "Burke", "Vicky", "Maldonado", "Nancy", "Lewis", "Nathaniel", "Mcgee", "Alex", "Ferguson", "Johanna", "Estrada" };
        public static string MetricPrefix(int n) { return n < -8 ? "e" + n * 3 : n < 0 ? new List<string>() { "m", "µ", "n", "p", "f", "a", "z", "y" }[-n - 1] : n <= 8 ? new List<string>() { "", "k", "M", "G", "T", "D", "E", "Z", "Y" }[n] : "e+" + n * 3; }
        public static string MeshStr(MeshFilter mf) {
            Mesh m = mf.sharedMesh;
            Material[] mats = mf.Gc<Renderer>().sharedMaterials;
            var sb = new System.Text.StringBuilder();
            sb.Append("g ").Append(mf.name).Append("\n");
            foreach (Vector3 v in m.vertices)
                sb.Append(string.Format("v {0} {1} {2}\n", v.x, v.y, v.z));
            sb.Append("\n");
            foreach (Vector3 v in m.normals)
                sb.Append(string.Format("vn {0} {1} {2}\n", v.x, v.y, v.z));
            sb.Append("\n");
            foreach (Vector3 v in m.uv)
                sb.Append(string.Format("vt {0} {1}\n", v.x, v.y));
            for (int material = 0; material < m.subMeshCount; material++) {
                sb.Append("\n").Append("usemtl ").Append(mats[material].name).Append("\n").Append("usemap ").Append(mats[material].name).Append("\n");
                int[] triangles = m.GetTriangles(material);
                for (int i = 0; i < triangles.Length; i += 3)
                    sb.Append(string.Format("f {0}/{0}/{0} {1}/{1}/{1} {2}/{2}/{2}\n", triangles[i] + 1, triangles[i + 1] + 1, triangles[i + 2] + 1));
            }
            return sb.tS();
        }
        public static void MeshFile(MeshFilter mf, string name) {
            using (var sw = new System.IO.StreamWriter(name))
                sw.Write(MeshStr(mf));
        }
        public static void Def<T>(ref T v, T def) { if (v.IsDef() || v.Null()) v = def; }
        public static void Def<T1, T2>(ref T1 v1, T1 def1, ref T2 v2, T2 def2) { Def(ref v1, def1); Def(ref v2, def2); }
        public static void Def<T1, T2, T3>(ref T1 v1, T1 def1, ref T2 v2, T2 def2, ref T3 v3, T3 def3) { Def(ref v1, def1); Def(ref v2, def2); Def(ref v3, def3); }
        public static void Def<T1, T2, T3, T4>(ref T1 v1, T1 def1, ref T2 v2, T2 def2, ref T3 v3, T3 def3, ref T4 v4, T4 def4) { Def(ref v1, def1); Def(ref v2, def2); Def(ref v3, def3); Def(ref v4, def4); }
        public static void Def<T1, T2, T3, T4, T5>(ref T1 v1, T1 def1, ref T2 v2, T2 def2, ref T3 v3, T3 def3, ref T4 v4, T4 def4, ref T5 v5, T5 def5) { Def(ref v1, def1); Def(ref v2, def2); Def(ref v3, def3); Def(ref v4, def4); Def(ref v5, def5); }
        public static void Def<T1, T2, T3, T4, T5, T6>(ref T1 v1, T1 def1, ref T2 v2, T2 def2, ref T3 v3, T3 def3, ref T4 v4, T4 def4, ref T5 v5, T5 def5, ref T6 v6, T6 def6) { Def(ref v1, def1); Def(ref v2, def2); Def(ref v3, def3); Def(ref v4, def4); Def(ref v5, def5); Def(ref v6, def6); }
        public static void Def<T1, T2, T3, T4, T5, T6, T7>(ref T1 v1, T1 def1, ref T2 v2, T2 def2, ref T3 v3, T3 def3, ref T4 v4, T4 def4, ref T5 v5, T5 def5, ref T6 v6, T6 def6, ref T7 v7, T7 def7) { Def(ref v1, def1); Def(ref v2, def2); Def(ref v3, def3); Def(ref v4, def4); Def(ref v5, def5); Def(ref v6, def6); Def(ref v7, def7); }
        public static void Def<T1, T2, T3, T4, T5, T6, T7, T8>(ref T1 v1, T1 def1, ref T2 v2, T2 def2, ref T3 v3, T3 def3, ref T4 v4, T4 def4, ref T5 v5, T5 def5, ref T6 v6, T6 def6, ref T7 v7, T7 def7, ref T8 v8, T8 def8) { Def(ref v1, def1); Def(ref v2, def2); Def(ref v3, def3); Def(ref v4, def4); Def(ref v5, def5); Def(ref v6, def6); Def(ref v7, def7); Def(ref v8, def8); }
        public static List<int> ListAp(int a, int d, int n) {
            List<int> res = new List<int>();
            for (int i = 1; i <= n; i++)
                res.Add(M.Ap(a, d, i));
            return res;
        }
        public static List<float> ListAp(float a, float d, float n) {
            List<float> res = new List<float>();
            for (int i = 1; i <= n; i++)
                res.Add(M.Ap(a, d, i));
            return res;
        }
        public static List<T> Fill<T>(ref List<T> a, T v, int n) {
            if (a.Null())
                a = v.ListN(n);
            else if (a.Count < n)
                a.Add(v.ListN(n - a.Count));
            return a;
        }
        public static List<List<T>> Fill2<T>(ref List<List<T>> a, T v, int n, int m) {
            if (a.Null()) {
                a = v.ListN(m).ListN(n);
            } else {
                if (a.Null())
                    a = new List<List<T>>();
                for (int i = 0; i < n; i++)
                    if (i < a.Count) {
                        if (a[i].Null())
                            a[i] = v.ListN(m);
                        else if (a[i].Count < m)
                            a[i].Add(v.ListN(m - a[i].Count));
                    } else
                        a.Add(v.ListN(m));
            }
            return a;
        }
        public static void Swap<T>(ref T a, ref T b) { T t = a; a = b; b = t; }
        public static List<T> Add2List<T>(List<T> a, List<T> b) {
            List<T> res = a;
            b.ForEach(x => res.Add(x));
            return res;
        }
        public static void Screenshot(string path, string name) {
            IO.CheckCrtDir(path);
#if UNITY_5
        Application.CaptureScreenshot(path + "/" + name + ".png", 1);
#else
            ScreenCapture.CaptureScreenshot(path + "/" + name + ".png", 1);
#endif
        }
        public static T LoadAsset<T>(string path) {
#if UNITY_EDITOR
            return (T)(object)AssetDatabase.LoadAssetAtPath(path, typeof(T));
#else
        return default(T);
#endif
        }
        public static void SetGameViewScale() {
#if UNITY_EDITOR
            System.Reflection.Assembly assembly = typeof(EditorWindow).Assembly;
            System.Type type = assembly.GetType("UnityEditor.GameView");
            EditorWindow v = EditorWindow.GetWindow(type);
            var defScaleField = type.GetField("m_defaultScale", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
            float defaultScale = 0.1f;
            var areaField = type.GetField("m_ZoomArea", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
            var areaObj = areaField.GetValue(v);
            var scaleField = areaObj.GetType().GetField("m_Scale", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
            scaleField.SetValue(areaObj, new Vector2(defaultScale, defaultScale));
#endif
        }
        public static void SaveOpenScenes() {
#if UNITY_EDITOR
            UnityEditor.SceneManagement.EditorSceneManager.SaveOpenScenes();
#endif
        }
        public static void PlayerSettingsChange() {
#if UNITY_EDITOR
            PlayerSettings.companyName = Gc.I.companyName;
            PlayerSettings.productName = Gc.I.productName;
            PlayerSettings.bundleVersion = Gc.I.version;
            string[] arr = Gc.I.companyWebSite.Split('.');
            PlayerSettings.applicationIdentifier = Gc.I.bundleIdentifier == "" ? (Gc.I.companyWebSite == "" ? "com." + Gc.I.companyName.RmvUw().L() : arr[1] + "." + arr[0]) + "." + Gc.I.productName.RmvUw().L() : Gc.I.bundleIdentifier;
            Texture2D icon = A.LoadAsset<Texture2D>("Assets/Main/Sprites/icon.png");
            if (icon)
                PlayerSettings.SetIconsForTargetGroup(BuildTargetGroup.Unknown, new Texture2D[] { icon });
            //     int lstDotIdx = GC.companyWebSite.LastIndexOf('.');
            //     PlayerSettings.applicationIdentifier =
            //         GC.companyWebSite.Sub(lstDotIdx + 1, GC.companyWebSite.Length - lstDotIdx - 1) + "." +
            //         GC.companyWebSite.Sub(0, lstDotIdx) + "." +
            //         Rgx.Replace(GC.productName, "\\W+", "").ToLower();
            //     PlayerSettings.SplashScreen.show = false;
            //     PlayerSettings.SplashScreen.logos = null;
            //     Texture2D iphone = GC.iPhoneLaunchScreen, ipad = GC.iPadLaunchScreen;
            //     PlayerSettings.iOS.SetiPhoneLaunchScreenType(iOSLaunchScreenType.ImageAndBackgroundRelative);
            //     PlayerSettings.iOS.SetLaunchScreenImage(iphone, iOSLaunchScreenImageType.iPhonePortraitImage);
            //     PlayerSettings.iOS.SetiPadLaunchScreenType(iOSLaunchScreenType.ImageAndBackgroundRelative);
            //     PlayerSettings.iOS.SetLaunchScreenImage(ipad, iOSLaunchScreenImageType.iPadImage);
            //     PlayerSettings.SplashScreen.show = true;
            //     if (iphone)
            //         PlayerSettings.SplashScreen.background = TexSpr(iphone);
#endif
        }
        public static string[] Layers() {
#if UNITY_EDITOR
            return UnityEditorInternal.InternalEditorUtility.layers;
#else
        return new string[0];
#endif
        }
        public static string[] Tags() {
#if UNITY_EDITOR
            return UnityEditorInternal.InternalEditorUtility.tags;
#else
        return new string[0];
#endif
        }
        public static void Text3d(Vector3 pos, string word = "", bool isPerfect = true) { AnimUp3d(Mb.Ins(LoadGo("Main/Text3d"), pos, Q.O, Gc.I.transform), word, isPerfect); }
        public static void TextUi(string word = "", bool isPerfect = true) { AnimScl(Mb.Ins(LoadGo("Main/TextUi"), Cc.I.hud.transform), word, isPerfect); }
        public static void AnimUp3d(GameObject go, string word = "", bool isPerfect = true) { Gc.I.AnimTxtDst(go, new List<Fo>() { Fo.F(0).Py(0).Ca(0), Fo.F(10).Py(0).Ca(1), Fo.F(30).Py(1).Ca(1), Fo.F(40).Py(1.5f).Ca(0) }, true, word, isPerfect); }
        public static void AnimScl(GameObject go, string word = "", bool isPerfect = true) { Gc.I.AnimTxtDst(go, new List<Fo>() { Fo.F(0).S(0), Fo.F(30).S(1.2f), Fo.F(40).S(1), Fo.F(70).S(1), Fo.F(90).S(0) }, true, word, isPerfect); }
        public static void AnimTxtDst(GameObject go, List<Fo> lis, bool isSmt = true, string word = "", bool isPerfect = true) { Gc.I.AnimTxtDst(go, lis, isSmt, word, isPerfect); }
        public static IEnumerator AnimTxtDstCor(GameObject go, List<Fo> lis, bool isSmt = true, string word = "", bool isPerfect = true) { return Gc.I.AnimTxtDstCor(go, lis, isSmt, word, isPerfect); }
        public static void Anim(GameObject go, List<Fo> lis, bool isSmt = true) { Gc.I.Anim(go, lis, isSmt); }
        public static IEnumerator AnimCor(GameObject go, List<Fo> lis, bool isSmt = true) { return Gc.I.AnimCor(go, lis, isSmt); }
        public static int TexIdx(int x, int y, int w, int h) { return (h - y - 1) * w + x; }
        public static bool IsEnemyEnter(Transform tf, Vector3 enemyPos, float dis = 1.5f, float ang = 45f) { return IsEnemyEnter(tf.Tf(), enemyPos, dis, ang); }
        public static bool IsEnemyEnter(Tf tf, Vector3 enemyPos, float dis = 1.5f, float ang = 45f) { return V3.Dis(tf.p, enemyPos) < dis ? M.IsBet(Ang.Rep180(Ang.LookD(V3.O, Tf.InvPnt(tf, enemyPos))), -ang, ang) : false; }
        public static T Load<T>(string path) where T : Object { return Resources.Load<T>(path); }
        public static GameObject LoadGo(string path) { return Resources.Load<GameObject>(path); }
        public static string[] EnumStrArr<T>() { return System.Enum.GetValues(typeof(T)).OfType<object>().Select(x => x.tS()).Arr(); }
        public static T[] EnumArr<T>() { return System.Enum.GetValues(typeof(T)).OfType<object>().Select(x => (T)x).Arr(); }
        public static Vector4 SprBorder(float l, float r, float t, float b) { return new Vector4(l, b, r, t); }
        public static void SetSprBorder(string path, Vector4 border) { IO.SetMetaLine(path, "spriteBorder", "spriteBorder: {x: " + border.x + ", y: " + border.y + ", z: " + border.z + ", w: " + border.w + "}"); }
        public static List<T> FOsOTA<T>() { return Resources.FindObjectsOfTypeAll(typeof(T)).List().Parse<Object, T>(); }
        public static List<T> FOsOT<T>() { return Object.FindObjectsOfType(typeof(T)).List().Parse<Object, T>(); }
        public static System.Reflection.FieldInfo Field<T>(T a, string name) { return a.GetType().GetField(name, System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.NonPublic); }
        public static List<RoadData> RoadDirs2Datas(List<RoadDir> dirs) {
            List<RoadData> datas = new List<RoadData>();
            RoadDir prvDir, curDir;
            Vector3Int defPrvPrvPos = new Vector3Int(0, 0, -2), defPrvPos = new Vector3Int(0, 0, -1),
                prvPrvPos, prvPos, curPos;
            Quaternion curRot;
            for (int i = 0; i < dirs.Count; i++) {
                prvPrvPos = i == 0 ? defPrvPrvPos : i == 1 ? defPrvPos : datas[i - 2].pos;
                prvPos = i == 0 ? defPrvPos : datas[i - 1].pos;
                prvDir = i == 0 ? RoadDir.F : datas[i - 1].dir;
                curDir = dirs[i];
                curPos = RoadCurPos(prvDir, prvPrvPos, prvPos);
                curRot = RoadCurRot(prvPos, curPos);
                datas.Add(new RoadData(curDir, curPos, curRot));
            }
            return datas;
        }
        public static List<RoadData> RoadCreateDatas(int n, int staF, int endF) {
            bool isDone = false;
            List<RoadData> datas = new List<RoadData>();
            for (int i = 0; i < staF; i++) {
                Vector3Int curPos = RoadCurPos(RoadDir.F, RoadDatasPos(datas, i - 2), RoadDatasPos(datas, i - 1));
                datas.Add(new RoadData(RoadDir.F, curPos, RoadCurRot(RoadDatasPos(datas, i - 1), curPos)));
            }
            RoadCreateData(RoadDir.F, RoadDir.F, RoadDatasPos(datas, datas.Count - 2), RoadDatasPos(datas, datas.Count - 1),
                0, n - staF - endF, datas, ref isDone);
            for (int i = 0; i < endF; i++) {
                Vector3Int curPos = RoadCurPos(datas[datas.Count - 1].dir, datas[datas.Count - 2].pos, datas[datas.Count - 1].pos);
                datas.Add(new RoadData(RoadDir.F, curPos, RoadCurRot(datas[datas.Count - 1].pos, curPos)));
            }
            return datas;
        }
        static Vector3Int RoadDatasPos(List<RoadData> datas, int i) { return i < 0 ? new Vector3Int(0, 0, i) : datas[i].pos; }
        static void RoadCreateData(RoadDir prvDir, RoadDir curDir, Vector3Int prvPrvPos, Vector3Int prvPos, int iLen, int len,
            List<RoadData> datas, ref bool isDone) {
            if (isDone)
                return;
            if (iLen >= len) {
                isDone = true;
                return;
            }
            Vector3Int curPos = RoadCurPos(prvDir, prvPrvPos, prvPos);
            if (RoadIsOverlap(prvDir, prvPrvPos, prvPos, new List<RoadDir>() { curDir, RoadDir.F, RoadDir.F }, datas)) return;
            Quaternion curRot = RoadCurRot(prvPos, curPos);
            datas.Add(new RoadData(curDir, curPos, curRot));
            List<RoadDir> rndDirs = new List<RoadDir> { RoadDir.LD, RoadDir.FD, RoadDir.RD, RoadDir.L, RoadDir.F, RoadDir.R, RoadDir.LU, RoadDir.FU, RoadDir.RU };
            rndDirs.Shuffle();
            for (int i = 0; i < rndDirs.Count; i++)
                RoadCreateData(curDir, rndDirs[i], prvPos, curPos, iLen + 1, len, datas, ref isDone);
            if (isDone)
                return;
            datas.RemoveAt(datas.Count - 1);
        }
        static Vector3Int RoadCurPos(RoadDir prvDir, Vector3Int prvPrvPos, Vector3Int prvPos) {
            Vector3 t = prvPos.V3();
            Quaternion r = Q.LookRot(t - new Vector3(prvPrvPos.x, prvPos.y, prvPrvPos.z));
            Vector3 s = V3.I;
            Vector3 pos = new List<Vector3Int>() { V3I.nnz, V3I.znp, V3I.pnz, V3I.nzz, V3I.zzp, V3I.pzz, V3I.npz, V3I.zpp, V3I.ppz }[(int)prvDir].V3();
            return Matrix4x4.TRS(t, r, s).MultiplyPoint3x4(pos).V3I();
        }
        static Quaternion RoadCurRot(Vector3Int prvPos, Vector3Int curPos) { return Q.LookRot(curPos.V3() - new Vector3(prvPos.x, curPos.y, prvPos.z)); }
        static bool RoadIsOverlap(RoadDir prvDir, Vector3Int prvPrvPos, Vector3Int prvPos, List<RoadDir> dirs, List<RoadData> datas) {
            List<Vector3Int> posLis = new List<Vector3Int>();
            for (int i = 0; i < dirs.Count; i++)
                posLis.Add(RoadCurPos(i == 0 ? prvDir : dirs[i - 1], i == 0 ? prvPrvPos : i == 1 ? prvPos : posLis[i - 2], i == 0 ? prvPos : posLis[i - 1]));
            for (int i = 0; i < datas.Count; i++) {
                for (int j = 0; j < posLis.Count; j++) {
                    // if (datas[i].pos == posLis[j] - V3I.u * 2 ||
                    //     datas[i].pos == posLis[j] - V3I.u ||
                    //     datas[i].pos == posLis[j] ||
                    //     datas[i].pos == posLis[j] + V3I.u ||
                    //     datas[i].pos == posLis[j] + V3I.u * 2) {
                    //     return true;
                    // }
                    if (datas[i].pos == posLis[j] ||
                        (((int)dirs[j] / 3 == 2) && ( // ^
                            (datas[i].pos == posLis[j] + V3I.u) || // ^*
                            (datas[i].pos == posLis[j] + V3I.u * 2 && (int)datas[i].dir / 3 == 0) // ^^v
                        )) ||
                        (((int)dirs[j] / 3 == 1) && ( // -
                            (datas[i].pos == posLis[j] + V3I.u && (int)datas[i].dir / 3 == 0) || // ^v
                            (datas[i].pos == posLis[j] + V3I.d && (int)datas[i].dir / 3 == 2) // v^
                        )) ||
                        (((int)dirs[j] / 3 == 0) && ( // v
                            (datas[i].pos == posLis[j] + V3I.d) || // v*
                            (datas[i].pos == posLis[j] + V3I.d * 2 && (int)datas[i].dir / 3 == 2) // vv^
                        ))
                    ) return true;
                }
            }
            return false;
        }
        static void RoadAddDatas(RoadDir prvDir, Vector3Int prvPrvPos, Vector3Int prvPos, List<RoadDir> dirs, List<RoadData> datas,
            ref RoadDir refCurDir, ref Vector3Int refPrvPos, ref Vector3Int refCurPos) {
            List<Vector3Int> idxs = new List<Vector3Int>();
            for (int i = 0; i < dirs.Count; i++) {
                refPrvPos = i == 0 ? prvPos : idxs[i - 1];
                refCurDir = dirs[i];
                refCurPos = RoadCurPos(
                    i == 0 ? prvDir : dirs[i - 1],
                    i == 0 ? prvPrvPos : i == 1 ? prvPos : idxs[i - 2],
                    refPrvPos
                );
                idxs.Add(refCurPos);
                datas.Add(new RoadData(refCurDir, refCurPos, RoadCurRot(refPrvPos, refCurPos)));
            }
        }
        public static void Vibrate(long ms) {
#if UNITY_ANDROID && !UNITY_EDITOR
        Vibration.Vibrate(ms);
#endif
        }
        public static void Vibrate(Vib v) {
            if (Gc.IsVib) {
#if UNITY_EDITOR
                Debug.Log("Vibrate: " + v);
#elif UNITY_ANDROID && !UNITY_EDITOR
            switch (v) {
                case Vib.Success: Vibration.Vibrate(new long[] { 20, 20 }, -1); break;
                case Vib.Warning: Vibration.Vibrate(new long[] { 10, 10 }, -1); break;
                case Vib.Error: Vibration.Vibrate(new long[] { 20, 20, 20 }, -1); break;
                case Vib.Light: Vibration.Vibrate(10); break;
                case Vib.Medium: Vibration.Vibrate(15); break;
                case Vib.Heavy: Vibration.Vibrate(20); break;
            }
#elif UNITY_IPHONE && !UNITY_EDITOR
            switch (v) {
                case Vib.Success: TapticPlugin.TapticManager.Notification(TapticPlugin.Notification.Success); break;
                case Vib.Warning: TapticPlugin.TapticManager.Notification(TapticPlugin.Notification.Warning); break;
                case Vib.Error: TapticPlugin.TapticManager.Notification(TapticPlugin.Notification.Error); break;
                case Vib.Light: TapticPlugin.TapticManager.Impact(TapticPlugin.Impact.Light); break;
                case Vib.Medium: TapticPlugin.TapticManager.Impact(TapticPlugin.Impact.Medium); break;
                case Vib.Heavy: TapticPlugin.TapticManager.Impact(TapticPlugin.Impact.Heavy); break;
            }
#endif
            }
        }
    }
    public class Msh {
        public int vsN => vs.Count;
        public List<Vector3> vs = new List<Vector3>();
        public List<int> ts = new List<int>();
        public List<Vector2> uv = new List<Vector2>();
        public bool isVerNor = false;
        public Msh() { }
        public Msh(bool isVerNor) { this.isVerNor = isVerNor; }
        public Msh(List<Vector3> vs, List<int> ts, bool isVerNor = false) { this.vs = vs.Copy(); this.ts = ts.Copy(); this.isVerNor = isVerNor; }
        public Msh(List<Vector3> vs, List<int> ts, List<Vector2> uv, bool isVerNor = false) { this.vs = vs.Copy(); this.ts = ts.Copy(); this.uv = uv.Copy(); this.isVerNor = isVerNor; }
        public void Upd(ref Mesh mesh, List<Vector3> ns = null, bool isRn = true, bool isPrvUpdRn = false) {
            List<Vector3> vs2 = new List<Vector3>();
            List<int> ts2 = new List<int>();
            List<Vector3> ns2 = new List<Vector3>();
            List<Vector2> uv2 = new List<Vector2>();
            if (uv.Count < vs.Count)
                uv.Add(V2.O.ListN(vs.Count - uv.Count));
            if (isPrvUpdRn) {
                mesh.Clear();
                mesh.vertices = vs.Arr();
                mesh.triangles = ts.Arr();
                mesh.RecalculateNormals();
                ns = mesh.normals.List();
                isVerNor = false;
                isRn = false;
            }
            bool isNs = ns.NotNull();
            if (!isVerNor) {
                for (int i = 0; i < ts.Count; i++) {
                    vs2.Add(vs[ts[i]]);
                    ts2.Add(i);
                    if (isNs)
                        ns2.Add(ns[ts[i]]);
                    uv2.Add(uv[ts[i]]);
                }
            }
            mesh.Clear();
            mesh.vertices = isVerNor ? vs.Arr() : vs2.Arr();
            mesh.triangles = isVerNor ? ts.Arr() : ts2.Arr();
            if (isNs)
                mesh.normals = isVerNor ? ns.Arr() : ns2.Arr();
            mesh.uv = isVerNor ? uv.Arr() : uv2.Arr();
            if (isRn)
                mesh.RecalculateNormals();
            DelVer(ref mesh);
        }
        public static List<Vector3> BoxPnts = new List<Vector3>() { V3.nnn, V3.nnp, V3.pnp, V3.pnn, V3.npn, V3.npp, V3.ppp, V3.ppn };
        public static List<Vector2> Fc(Vector2 a, Vector2 b, Vector2 c, Vector2 d) { return new List<Vector2>() { a, b, c, a, c, d }; }
        public static List<Vector2> Fc2(Vector2 a, Vector2 b, Vector2 c, Vector2 d) { return new List<Vector2>() { a, b, d, d, b, c }; }
        public static List<Vector2> FcI(Vector2 a, Vector2 b, Vector2 c, Vector2 d) { return Fc2(d, c, b, a); }
        public static List<Vector2> Fc(bool isCw, Vector2 a, Vector2 b, Vector2 c, Vector2 d) { return isCw ? Fc(a, b, c, d) : FcI(a, b, c, d); }
        public static List<int> Fc(int a, int b, int c, int d) { return new List<int>() { a, b, c, a, c, d }; }
        public static List<int> Fc2(int a, int b, int c, int d) { return new List<int>() { a, b, d, d, b, c }; }
        public static List<int> FcI(int a, int b, int c, int d) { return Fc2(d, c, b, a); }
        public static List<int> Fc2I(int a, int b, int c, int d) { return Fc(d, c, b, a); }
        public static List<int> Fc(bool isCw, int a, int b, int c, int d) { return isCw ? Fc(a, b, c, d) : FcI(a, b, c, d); }
        public static List<int> Fc2(bool isCw, int a, int b, int c, int d) { return isCw ? Fc2(a, b, c, d) : Fc2I(a, b, c, d); }
        public static void Init(ref Mesh mesh, GameObject go) {
            if (!mesh) {
                mesh = new Mesh();
                go.Mf().sharedMesh = mesh;
                if (!go.Mr().material)
                    go.Mr().material = new Material(Shader.Find("Standard"));
            }
        }
        public static void UpdTf(ref Mesh mesh, Tf tf) {
            Vector3[] vs = mesh.vertices.Copy();
            Vector3[] ns = mesh.normals.Copy();
            for (int i = 0; i < vs.Length; i++) {
                vs[i] = Tf.Rot(tf.r, V3.Scl(vs[i], tf.s)) + tf.p;
                ns[i] = Tf.Rot(tf.r, ns[i]).normalized;
            }
            mesh.vertices = vs;
            mesh.normals = ns;
        }
        public static void DelVer(ref Mesh mesh) {
            List<Vector3> vs = mesh.vertices.List();
            List<int> ts = mesh.triangles.List();
            List<Vector3> ns = mesh.normals.List();
            List<Vector2> uv = mesh.uv.List();
            mesh.Clear();
            List<int> delIdxs = new List<int>();
            for (int i = 0; i < vs.Count; i++)
                if (!ts.Contains(i))
                    delIdxs.Add(i);
            delIdxs.Sort();
            for (int i = 0; i < delIdxs.Count; i++) {
                delIdxs[i] -= i;
                vs.RmvAt(delIdxs[i]);
                ns.RmvAt(delIdxs[i]);
                uv.RmvAt(delIdxs[i]);
                for (int j = 0; j < ts.Count; j++)
                    if (ts[j] >= delIdxs[i])
                        ts[j]--;
            }
            mesh.vertices = vs.Arr();
            mesh.triangles = ts.Arr();
            mesh.normals = ns.Arr();
            mesh.uv = uv.Arr();
        }
        public static void Add(ref Mesh mesh, params Mesh[] arr) {
            mesh.Clear();
            List<Vector3> vs = new List<Vector3>();
            List<int> ts = new List<int>();
            for (int i = 0, n = 0; i < arr.Length; i++) {
                vs.Add(arr[i].vertices);
                arr[i].triangles.List().ForEach(x => ts.Add(x + n));
                n += arr[i].vertices.Length;
            }
            mesh.vertices = vs.Arr();
            mesh.triangles = ts.Arr();
            mesh.RecalculateNormals();
        }
        public static void Road(ref Mesh mesh, List<List<Vector3>> pnts, List<Vector2> data, bool isLoop, bool isCw, bool isVerNor = false, float tilingY = 1, Vector4 bodyUv = default, Vector4 staUv = default, Vector4 endUv = default) {
            Msh msh = new Msh(isVerNor);
            int n = pnts.Count, n1 = n + isLoop.I(), n2 = n1 - 1, m = data.Count, m2 = m * 2;
            for (int i = 0; i < n1; i++)
                for (int j = 0; j < m2; j++) {
                    int j1 = (j + 1) / 2, j2 = j1 % m;
                    msh.vs.Add(pnts[i % n][j2]);
                    msh.uv.Add(bodyUv.Uv(j1.F() / m, i.F() / n2));
                }
            for (int i = 0; i < n2; i++)
                msh.ts.TsLoop(m2, i * m2, 1, (i + 1) * m2, 1, isCw, 2);
            if (!isLoop) {
                msh.vs.Add(pnts[0]).Add(pnts.Last());
                msh.uv.Uv(data, staUv.Uv(tilingY)).Uv(data, endUv.Uv(tilingY));
                msh.ts.Add(Geometry.Shp2D.Crt(data).Ts(isCw, n1 * m2)).Add(Geometry.Shp2D.Crt(data).Ts(!isCw, n1 * m2 + m));
            }
            msh.Upd(ref mesh);
        }
        public static void RoadRound(ref Mesh mesh, List<List<Vector3>> pnts, bool isLoop, float staAng, bool isCw, int tubeRing, float tubeR, bool isVerNor, Vector3 staP, Vector3 endP, float tilingY = 1, Vector4 bodyUv = default, Vector4 staUv = default, Vector4 endUv = default) {
            A.Def(ref bodyUv, V4.V(0, 0, M._2_3, 1), ref staUv, V4.V(M._2_3, 0, M._5_6, 1), ref endUv, V4.V(M._5_6, 0, 1, 1));
            Msh msh = new Msh(isVerNor);
            int n = pnts.Count, n1 = n + isLoop.I(), n2 = n1 - 1, m2 = pnts[0].Count, m = m2 - 1;
            for (int i = 0; i < n1; i++)
                for (int j = 0; j < m2; j++) {
                    msh.vs.Add(pnts[i % n][j]);
                    msh.uv.Add(bodyUv.SwapU(isCw).Uv(j.F() / m, i.F() / n2));
                }
            for (int i = 0; i < n2; i++)
                msh.ts.TsLoop(m2, i * m2, 1, (i + 1) * m2, 1, !isCw);
            if (!isLoop)
                for (int i = 0; i < 2; i++) {
                    Tf pTf = i == 0 ? Tf.PR(staP, V3.Xy(-90, (M.IsBet(Ang.Rep(staAng), 90, 270) ? 0 : 180) - Ang.LookD(pnts[0][0], staP, false))) : Tf.PR(endP, V3.Xy(90, (M.IsBet(Ang.Rep(staAng), 90, 270) ? 0 : 180) - Ang.LookD(pnts.Last()[0], endP, false)));
                    for (int j = 0; j <= tubeRing; j++)
                        for (int k = 0; k < m2; k++) {
                            msh.vs.Add(pTf.Pnt(Crv.AngRr((i == 0).Sign() * (staAng + isCw.Sign() * 360f * k / m), 90f * j / tubeRing, 0, tubeR, 0)));
                            msh.uv.Add((i == 0 ? staUv : endUv).SwapU(isCw).Uv(tilingY).Uv(k.F() / m, j.F() / tubeRing));
                        }
                    for (int j = 0, l = (i == 0 ? n1 : n1 + tubeRing + 1) * m2; j < tubeRing; j++)
                        msh.ts.TsLoop(m2, l + j * m2, 1, l + (j + 1) * m2, 1, i == isCw.I2());
                }
            msh.Upd(ref mesh);
        }
        public static void RoadRound(ref Mesh mesh, float staAng, bool isCw, List<Vector3> pnts, bool isLoop, int ring, int tubeRing, float tubeR, RoadCrtDirTp tp, bool isVerNor = false, float tilingY = 1, Vector4 bodyUv = default, Vector4 staUv = default, Vector4 endUv = default) {
            A.Def(ref bodyUv, V4.V(0, 0, M._2_3, 1), ref staUv, V4.V(M._2_3, 0, M._5_6, 1), ref endUv, V4.V(M._5_6, 0, 1, 1));
            RoadRound(ref mesh, Crv.RoadPnts(Tf.New, pnts, Crv.AngPnts(V2.O, ring, tubeR, staAng, isCw.Sign() * 360f, true, true), isLoop, tp), isLoop, staAng, isCw, tubeRing, tubeR, isVerNor, pnts[0], pnts.Last(), tilingY, bodyUv, staUv.SwapU(), endUv);
        }
        public static void SpiralRound(ref Mesh mesh, float staAng, bool isCw, int seg, int ring, int tubeRing, float l, float r1, float r2, float tubeR, float h, bool isRh, bool isVerNor = false, float tilingY = 1, Vector4 bodyUv = default, Vector4 staUv = default, Vector4 endUv = default) {
            A.Def(ref bodyUv, V4.V(0, 0, M._2_3, 1), ref staUv, V4.V(M._2_3, 0, M._5_6, 1), ref endUv, V4.V(M._5_6, 0, 1, 1));
            RoadRound(ref mesh, Crv.SpiralPnts(Tf.New, Crv.AngPnts(V2.O, ring, tubeR, staAng, isCw.Sign() * 360f, true, true), seg, l, r1, r2, h, isRh), false, staAng, isCw, tubeRing, tubeR, isVerNor, Crv.SpiralPnt(0, V2.O, l, r1, r2, h, isRh), Crv.SpiralPnt(1, V2.O, l, r1, r2, h, isRh), tilingY, bodyUv, staUv.SwapU(), endUv);
        }
        public static void Road(ref Mesh mesh, List<Vector3> pnts, List<Vector2> data, bool isLoop, RoadCrtDirTp tp, bool isVerNor = false, float tilingY = 1, Vector4 bodyUv = default, Vector4 staUv = default, Vector4 endUv = default) {
            A.Def(ref bodyUv, V4.V(0, 0, M._2_3, 1), ref staUv, V4.V(M._2_3, 0, M._5_6, 1), ref endUv, V4.V(M._5_6, 0, 1, 1));
            Road(ref mesh, Crv.RoadPnts(Tf.New, pnts, data, isLoop, tp), data, isLoop, true, isVerNor, tilingY, bodyUv, staUv, endUv.SwapU());
        }
        public static void Spiral(ref Mesh mesh, List<Vector2> data, int seg, float l, float r1, float r2, float h, bool isRh, bool isVerNor = false, float tilingY = 1, Vector4 bodyUv = default, Vector4 staUv = default, Vector4 endUv = default) {
            A.Def(ref bodyUv, V4.V(0, 0, M._2_3, 1), ref staUv, V4.V(M._2_3, 0, M._5_6, 1), ref endUv, V4.V(M._5_6, 0, 1, 1));
            Road(ref mesh, Crv.SpiralPnts(Tf.New, data, seg, l, r1, r2, h, isRh, false), data, false, !isRh, isVerNor, tilingY, bodyUv, staUv.SwapU(isRh), endUv.SwapU(!isRh));
        }
        // Plane, Circle
        public static void Plane(ref Mesh mesh, int seg, int segCor, float r, float rCor, UvTp segUt, UvTp ringUt, float uvAng) {
            Rm(ref mesh, Tf.New, Mb.List(V2.X(r)), seg, segCor, null, Mb.List(V4.V(rCor)), FillTp.None, FillTp.Fill, 0, 0, segUt, ringUt, true, false, "1", uvAng);
        }
        // Box Cup, Box Cup Round, Cup, Cup Round
        public static void Cup(ref Mesh mesh, int seg, int segCor, int ringCorDO, int ringCorU, int ringCorDI, float rO, float rI, float rCorDO, float rCorU, float rCorDI, float hO, float hI, UvTp segUt, UvTp ringUt, bool isVerNor, bool isUvFull, float uvAng) {
            Rm(ref mesh, Tf.New, Mb.List(V2.V(rO, -hO / 2), V2.V(rO, hO / 2), V2.V(rI, hO / 2), V2.V(rI, hO / 2 - hI)), seg, segCor, Mb.List(ringCorDO, ringCorU, ringCorU, ringCorDI), Mb.List(V4.V(rCorDO), V4.V(rCorU), V4.V(rCorU), V4.V(rCorDI)), FillTp.Fill, FillTp.Fill, 0, 0, segUt, ringUt, isVerNor, isUvFull, "1111", uvAng);
        }
        // Box Hole, Box Hole Round, Pipe, Pipe Round, Torus
        public static void Pipe(ref Mesh mesh, int seg, int segCor, int ringCor, float rO, float rI, float rCor, float h, UvTp segUt, UvTp ringUt, bool isVerNor, bool isUvFull, float uvAng) {
            Rm(ref mesh, Tf.New, Mb.List(V2.V(rI, -h / 2), V2.V(rO, -h / 2), V2.V(rO, h / 2), V2.V(rI, h / 2)), seg, segCor, ringCor.ListN(4), V4.V(rCor).ListN(4), FillTp.Connect, FillTp.Connect, 0, 0, segUt, ringUt, isVerNor, isUvFull, "1111", uvAng);
        }
        // Capsule, Cone, Cone Round, Cylinder, Cylinder Round, Box, Box Round
        public static void Cylinder(ref Mesh mesh, int seg, int segCor, int ringCor, float rD, float rU, float rCor, float h, UvTp segUt, UvTp ringUt, bool isVerNor, bool isUvFull, float uvAng) {
            Rm(ref mesh, Tf.New, Mb.List(V2.V(rD, -h / 2), V2.V(rU, h / 2)), seg, segCor, ringCor.ListN(2), V4.V(rCor).ListN(2), FillTp.Fill, FillTp.Fill, 0, 0, segUt, ringUt, isVerNor, isUvFull, "11", uvAng);
        }
        public static void Box(ref Mesh mesh, Vector3 sz, Vector4 bodyUv = default, Vector4 staUv = default, Vector4 endUv = default) {
            BoxRound(ref mesh, 0, 0, 0, sz, false, bodyUv, staUv, endUv);
        }
        public static void BoxHole(ref Mesh mesh, Vector3 sz, Vector2 szI, Vector2 pos) {
            System.Func<float, float, Vector2> Uv = (a, b) => V2.V(a / 6f, b / 2f);
            new Msh(BoxPnts.Scl(sz / 2).Add(BoxPnts.Scl(V3.V(szI.x, sz.y, szI.y) / 2).Pos(V3.Xz(pos.x, pos.y))).Add(BoxPnts.Scl(V3.V(sz.x, sz.y, szI.y) / 2).Pos(V3.Z(pos.y))), Fc(1, 5, 4, 0).Fc(0, 4, 7, 3).Fc(3, 7, 6, 2).Fc(2, 6, 5, 1).Fc(8, 12, 13, 9).Fc(9, 13, 14, 10).Fc(10, 14, 15, 11).Fc(11, 15, 12, 8).Fc(20, 21, 13, 12).Fc(21, 5, 6, 22).Fc(15, 14, 22, 23).Fc(4, 20, 23, 7).Fc(17, 16, 8, 9).Fc(16, 0, 3, 19).Fc(10, 11, 19, 18).Fc(1, 17, 18, 2)).Upd(ref mesh);
            float x1 = M.LerpInv(-sz.x / 2, sz.x / 2, pos.x - szI.x / 2), x2 = M.LerpInv(-sz.x / 2, sz.x / 2, pos.x + szI.x / 2), z1 = M.LerpInv(-sz.z / 2, sz.z / 2, pos.y - szI.y / 2), z2 = M.LerpInv(-sz.z / 2, sz.z / 2, pos.y + szI.y / 2);
            mesh.uv = Fc(Uv(0, 1), Uv(0, 2), Uv(1, 2), Uv(1, 1)).Fc(Uv(1, 1), Uv(1, 2), Uv(2, 2), Uv(2, 1)).Fc(Uv(2, 1), Uv(2, 2), Uv(3, 2), Uv(3, 1)).Fc(Uv(3, 1), Uv(3, 2), Uv(4, 2), Uv(4, 1)).Fc(Uv(0, 0), Uv(0, 1), Uv(1, 1), Uv(1, 0)).Fc(Uv(3, 0), Uv(3, 1), Uv(4, 1), Uv(4, 0)).Fc(Uv(2, 0), Uv(2, 1), Uv(3, 1), Uv(3, 0)).Fc(Uv(1, 0), Uv(1, 1), Uv(2, 1), Uv(2, 0)).Fc(Uv(4, 1 + z1), Uv(4, 1 + z2), Uv(4 + x1, 1 + z2), Uv(4 + x1, 1 + z1)).Fc(Uv(4, 1 + z2), Uv(4, 2), Uv(5, 2), Uv(5, 1 + z2)).Fc(Uv(4 + x2, 1 + z1), Uv(4 + x2, 1 + z2), Uv(5, 1 + z2), Uv(5, 1 + z1)).Fc(Uv(4, 1), Uv(4, 1 + z1), Uv(5, 1 + z1), Uv(5, 1)).Fc(Uv(5, 2 - z2), Uv(5, 2 - z1), Uv(5 + x1, 2 - z1), Uv(5 + x1, 2 - z2)).Fc(Uv(5, 2 - z1), Uv(5, 2), Uv(6, 2), Uv(6, 2 - z1)).Fc(Uv(5 + x2, 2 - z2), Uv(5 + x2, 2 - z1), Uv(6, 2 - z1), Uv(6, 2 - z2)).Fc(Uv(5, 1), Uv(5, 2 - z2), Uv(6, 2 - z2), Uv(6, 1)).Arr();
        }
        public static void BoxHoles(ref Mesh mesh, Vector3 sz, Vector3 szI, Vector3 pos) {
            Vector3 sz1 = sz / 2 + pos - szI / 2, sz2 = sz / 2 - pos - szI / 2, p1 = pos - szI / 2 - sz / 2, p2 = pos + szI / 2 + sz / 2;
            System.Func<bool, int, List<Vector3>> Pnts = (x, y) => x ? (y == 0 ? BoxPnts.Scl(sz1).Pos(p1) : y == 1 ? BoxPnts.Scl(sz1.X(sz2.x)).Pos(p1.X(p2.x)) : y == 2 ? BoxPnts.Scl(sz1.Y(sz2.y)).Pos(p1.Y(p2.y)) : BoxPnts.Scl(sz1.Z(sz2.z)).Pos(p1.Z(p2.z))) : (y == 0 ? BoxPnts.Scl(sz2).Pos(p2) : y == 1 ? BoxPnts.Scl(sz2.X(sz1.x)).Pos(p2.X(p1.x)) : y == 2 ? BoxPnts.Scl(sz2.Y(sz1.y)).Pos(p2.Y(p1.y)) : BoxPnts.Scl(sz2.Z(sz1.z)).Pos(p2.Z(p1.z)));
            new Msh(Pnts(true, 0).Add(Pnts(true, 3)).Add(Pnts(true, 2)).Add(Pnts(false, 1)).Add(Pnts(true, 1)).Add(Pnts(false, 2)).Add(Pnts(false, 3)).Add(Pnts(false, 0)).Add(Pnts(true, 0)).Add(Pnts(true, 1)).Add(Pnts(true, 2)).Add(Pnts(false, 3)).Add(Pnts(true, 3)).Add(Pnts(false, 2)).Add(Pnts(false, 1)).Add(Pnts(false, 0)).Add(Pnts(true, 0)).Add(Pnts(true, 1)).Add(Pnts(true, 3)).Add(Pnts(false, 2)).Add(Pnts(true, 2)).Add(Pnts(false, 3)).Add(Pnts(false, 1)).Add(Pnts(false, 0)), Fc(9, 13, 4, 0).Fc(13, 25, 24, 12).Fc(5, 17, 16, 4).Fc(25, 29, 20, 16).Fc(2, 6, 15, 11).Fc(7, 19, 18, 6).Fc(15, 27, 26, 14).Fc(18, 22, 31, 27).Fc(40, 44, 37, 33).Fc(45, 57, 56, 44).Fc(37, 49, 48, 36).Fc(56, 60, 53, 49).Fc(35, 39, 46, 42).Fc(39, 51, 50, 38).Fc(47, 59, 58, 46).Fc(51, 55, 62, 58).Fc(64, 68, 79, 75).Fc(68, 80, 83, 71).Fc(76, 88, 91, 79).Fc(80, 84, 95, 91).Fc(73, 77, 70, 66).Fc(78, 90, 89, 77).Fc(70, 82, 81, 69).Fc(89, 93, 86, 82).Fc(99, 103, 108, 104).Fc(100, 112, 115, 103).Fc(108, 120, 123, 111).Fc(115, 119, 124, 120).Fc(106, 110, 101, 97).Fc(110, 122, 121, 109).Fc(102, 114, 113, 101).Fc(122, 126, 117, 113).Fc(145, 144, 155, 154).Fc(144, 129, 130, 147).Fc(152, 137, 138, 155).Fc(129, 128, 139, 138).Fc(135, 134, 141, 140).Fc(133, 148, 151, 134).Fc(141, 156, 159, 142).Fc(151, 150, 157, 156).Fc(178, 179, 184, 185).Fc(176, 161, 162, 179).Fc(184, 169, 170, 187).Fc(162, 163, 168, 169).Fc(164, 165, 174, 175).Fc(165, 180, 183, 166).Fc(173, 188, 191, 174).Fc(180, 181, 190, 191)).Upd(ref mesh);
            Vector4 l = V4.V(sz2.z / sz.z, 1 - sz1.z / sz.z, sz1.y / sz.y, 1 - sz2.y / sz.y), r = V4.V(3 - l.y, 3 - l.x, l.z, l.w), b = V4.V(1 + sz1.x / sz.x, 2 - sz2.x / sz.x, sz1.y / sz.y, 1 - sz2.y / sz.y), f = V4.V(5 - b.y, 5 - b.x, b.z, b.w), d = V4.V(4 + sz1.x / sz.x, 5 - sz2.x / sz.x, sz2.z / sz.z, 1 - sz1.z / sz.z), u = V4.V(5 + sz1.x / sz.x, 6 - sz2.x / sz.x, sz1.z / sz.z, 1 - sz2.z / sz.z), l2 = l + V4.Zw(1, 1), r2 = r + V4.Zw(1, 1), b2 = b + V4.Zw(1, 1), f2 = f + V4.Zw(1, 1), d2 = d + V4.Zw(1, 1), u2 = u + V4.Zw(1, 1);
            System.Func<float, float, Vector2> Uv = (x, y) => V2.V(x / 6, y / 2);
            mesh.uv = Fc(Uv(0, 1), Uv(0, l2.z), Uv(1, l2.z), Uv(1, 1)).Fc(Uv(0, l2.z), Uv(0, l2.w), Uv(l2.x, l2.w), Uv(l2.x, l2.z)).Fc(Uv(l2.y, l2.z), Uv(l2.y, l2.w), Uv(1, l2.w), Uv(1, l2.z)).Fc(Uv(0, l2.w), Uv(0, 2), Uv(1, 2), Uv(1, l2.w)).Fc(Uv(r.x, 0), Uv(r.x, r.z), Uv(r.y, r.z), Uv(r.y, 0)).Fc(Uv(2, r.z), Uv(2, r.w), Uv(r.x, r.w), Uv(r.x, r.z)).Fc(Uv(r.y, r.z), Uv(r.y, r.w), Uv(3, r.w), Uv(3, r.z)).Fc(Uv(r.x, r.w), Uv(r.x, 1), Uv(r.y, 1), Uv(r.y, r.w)).Fc(Uv(l.x, 0), Uv(l.x, l.z), Uv(l.y, l.z), Uv(l.y, 0)).Fc(Uv(0, l.z), Uv(0, l.w), Uv(l.x, l.w), Uv(l.x, l.z)).Fc(Uv(l.y, l.z), Uv(l.y, l.w), Uv(1, l.w), Uv(1, l.z)).Fc(Uv(l.x, l.w), Uv(l.x, 1), Uv(l.y, 1), Uv(l.y, l.w)).Fc(Uv(2, 1), Uv(2, r2.z), Uv(3, r2.z), Uv(3, 1)).Fc(Uv(2, r2.z), Uv(2, r2.w), Uv(r2.x, r2.w), Uv(r2.x, r2.z)).Fc(Uv(r2.y, r2.z), Uv(r2.y, r2.w), Uv(3, r2.w), Uv(3, r2.z)).Fc(Uv(2, r2.w), Uv(2, 2), Uv(3, 2), Uv(3, r2.w)).Fc(Uv(1, 1), Uv(1, b2.z), Uv(2, b2.z), Uv(2, 1)).Fc(Uv(1, b2.z), Uv(1, b2.w), Uv(b2.x, b2.w), Uv(b2.x, b2.z)).Fc(Uv(b2.y, b2.z), Uv(b2.y, b2.w), Uv(2, b2.w), Uv(2, b2.z)).Fc(Uv(1, b2.w), Uv(1, 2), Uv(2, 2), Uv(2, b2.w)).Fc(Uv(f.x, 0), Uv(f.x, f.z), Uv(f.y, f.z), Uv(f.y, 0)).Fc(Uv(3, f.z), Uv(3, f.w), Uv(f.x, f.w), Uv(f.x, f.z)).Fc(Uv(f.y, f.z), Uv(f.y, f.w), Uv(4, f.w), Uv(4, f.z)).Fc(Uv(f.x, f.w), Uv(f.x, 1), Uv(f.y, 1), Uv(f.y, f.w)).Fc(Uv(b.x, 0), Uv(b.x, b.z), Uv(b.y, b.z), Uv(b.y, 0)).Fc(Uv(1, b.z), Uv(1, b.w), Uv(b.x, b.w), Uv(b.x, b.z)).Fc(Uv(b.y, b.z), Uv(b.y, b.w), Uv(2, b.w), Uv(2, b.z)).Fc(Uv(b.x, b.w), Uv(b.x, 1), Uv(b.y, 1), Uv(b.y, b.w)).Fc(Uv(3, 1), Uv(3, f2.z), Uv(4, f2.z), Uv(4, 1)).Fc(Uv(3, f2.z), Uv(3, f2.w), Uv(f2.x, f2.w), Uv(f2.x, f2.z)).Fc(Uv(f2.y, f2.z), Uv(f2.y, f2.w), Uv(4, f2.w), Uv(4, f2.z)).Fc(Uv(3, f2.w), Uv(3, 2), Uv(4, 2), Uv(4, f2.w)).Fc(Uv(4, 1), Uv(4, d2.z), Uv(5, d2.z), Uv(5, 1)).Fc(Uv(4, d2.z), Uv(4, d2.w), Uv(d2.x, d2.w), Uv(d2.x, d2.z)).Fc(Uv(d2.y, d2.z), Uv(d2.y, d2.w), Uv(5, d2.w), Uv(5, d2.z)).Fc(Uv(4, d2.w), Uv(4, 2), Uv(5, 2), Uv(5, d2.w)).Fc(Uv(u.x, 0), Uv(u.x, u.z), Uv(u.y, u.z), Uv(u.y, 0)).Fc(Uv(5, u.z), Uv(5, u.w), Uv(u.x, u.w), Uv(u.x, u.z)).Fc(Uv(u.y, u.z), Uv(u.y, u.w), Uv(6, u.w), Uv(6, u.z)).Fc(Uv(u.x, u.w), Uv(u.x, 1), Uv(u.y, 1), Uv(u.y, u.w)).Fc(Uv(d.x, 0), Uv(d.x, d.z), Uv(d.y, d.z), Uv(d.y, 0)).Fc(Uv(4, d.z), Uv(4, d.w), Uv(d.x, d.w), Uv(d.x, d.z)).Fc(Uv(d.y, d.z), Uv(d.y, d.w), Uv(5, d.w), Uv(5, d.z)).Fc(Uv(d.x, d.w), Uv(d.x, 1), Uv(d.y, 1), Uv(d.y, d.w)).Fc(Uv(5, 1), Uv(5, u2.z), Uv(6, u2.z), Uv(6, 1)).Fc(Uv(5, u2.z), Uv(5, u2.w), Uv(u2.x, u2.w), Uv(u2.x, u2.z)).Fc(Uv(u2.y, u2.z), Uv(u2.y, u2.w), Uv(6, u2.w), Uv(6, u2.z)).Fc(Uv(5, u2.w), Uv(5, 2), Uv(6, 2), Uv(6, u2.w)).Arr();
        }
        public static void BoxIn(ref Mesh mesh, Vector3 sz, Vector3 szI, Vector2 pos) {
            System.Func<float, float, Vector2> Uv = (a, b) => V2.V(a / 6f, b / 2f);
            new Msh(BoxPnts.Scl(sz / 2).Add(BoxPnts.Scl(szI / 2).Pos(V3.V(pos.x, sz.y / 2 - szI.y / 2, pos.y))).Add(Mb.List(V3.npn, V3.npp, V3.ppp, V3.ppn).Scl(V3.V(sz.x, sz.y, szI.z) / 2).Pos(V3.Z(pos.y))), Fc(1, 5, 4, 0).Fc(0, 4, 7, 3).Fc(3, 7, 6, 2).Fc(2, 6, 5, 1).Fc(8, 12, 13, 9).Fc(9, 13, 14, 10).Fc(10, 14, 15, 11).Fc(11, 15, 12, 8).Fc(16, 17, 13, 12).Fc(17, 5, 6, 18).Fc(15, 14, 18, 19).Fc(4, 16, 19, 7).Fc(1, 0, 3, 2).Fc(8, 9, 10, 11)).Upd(ref mesh);
            float x1 = M.LerpInv(-sz.x / 2, sz.x / 2, pos.x - szI.x / 2), x2 = M.LerpInv(-sz.x / 2, sz.x / 2, pos.x + szI.x / 2), z1 = M.LerpInv(-sz.z / 2, sz.z / 2, pos.y - szI.z / 2), z2 = M.LerpInv(-sz.z / 2, sz.z / 2, pos.y + szI.z / 2);
            mesh.uv = Fc(Uv(0, 0), Uv(0, 1), Uv(1, 1), Uv(1, 0)).Fc(Uv(1, 0), Uv(1, 1), Uv(2, 1), Uv(2, 0)).Fc(Uv(2, 0), Uv(2, 1), Uv(3, 1), Uv(3, 0)).Fc(Uv(3, 0), Uv(3, 1), Uv(4, 1), Uv(4, 0)).Fc(Uv(0, 1), Uv(0, 2), Uv(1, 2), Uv(1, 1)).Fc(Uv(3, 1), Uv(3, 2), Uv(4, 2), Uv(4, 1)).Fc(Uv(2, 1), Uv(2, 2), Uv(3, 2), Uv(3, 1)).Fc(Uv(1, 1), Uv(1, 2), Uv(2, 2), Uv(2, 1)).Fc(Uv(5, 1 + z1), Uv(5, 1 + z2), Uv(5 + x1, 1 + z2), Uv(5 + x1, 1 + z1)).Fc(Uv(5, 1 + z2), Uv(5, 2), Uv(6, 2), Uv(6, 1 + z2)).Fc(Uv(5 + x2, 1 + z1), Uv(5 + x2, 1 + z2), Uv(6, 1 + z2), Uv(6, 1 + z1)).Fc(Uv(5, 1), Uv(5, 1 + z1), Uv(6, 1 + z1), Uv(6, 1)).Fc(Uv(4, 0), Uv(4, 1), Uv(5, 1), Uv(5, 0)).Fc(Uv(4, 1), Uv(4, 2), Uv(5, 2), Uv(5, 1)).Arr();
        }
        public static void BoxRound(ref Mesh mesh, int seg, int ring, float r, Vector3 sz, bool isVerNor, Vector4 bodyUv = default, Vector4 staUv = default, Vector4 endUv = default) {
            A.Def(ref bodyUv, V4.V(0, 0, M._2_3, 1), ref staUv, V4.V(M._2_3, 0, M._5_6, 1), ref endUv, V4.V(M._5_6, 0, 1, 1));
            SegRing(ref mesh, Mb.List(Mb.List(V3.nnp, V3.nnn, V3.pnn, V3.pnp).Scl(sz / 2), Mb.List(V3.npp, V3.npn, V3.ppn, V3.ppp).Scl(sz / 2)), V4.V(r).ListN(4).ListN(2), seg.ListN(4), ring.ListN(2), "", "", UvTp.Cor, UvTp.Cor, V3.Y(-sz.y / 2), V3.Y(sz.y / 2), FillTp.Fill, FillTp.Fill, bodyUv, staUv.SwapV(), endUv, isVerNor, true, 0, true);
        }
        public static void BoxHoleRound(ref Mesh mesh, int corSeg, int corRing, float corR, float borW, Vector3 sz, bool isVerNor, Vector4 bodyUv = default, Vector4 staUv = default, Vector4 endUv = default) {
            A.Def(ref bodyUv, V4.V(0, 0, M._2_3, 1), ref staUv, V4.V(M._2_3, 0, M._5_6, 1), ref endUv, V4.V(M._5_6, 0, 1, 1));
            SegRing(ref mesh, Mb.List(Mb.List(V3.nnp, V3.nnn, V3.pnn, V3.pnp).Scl(sz / 2 - V3.Xz(corR * 2 + borW)), Mb.List(V3.nnp, V3.nnn, V3.pnn, V3.pnp).Scl(sz / 2), Mb.List(V3.npp, V3.npn, V3.ppn, V3.ppp).Scl(sz / 2), Mb.List(V3.npp, V3.npn, V3.ppn, V3.ppp).Scl(sz / 2 - V3.Xz(corR * 2 + borW))), V4.V(corR).ListN(4).ListN(4), corSeg.ListN(4), corRing.ListN(4), "", "", UvTp.Cor, UvTp.Cor, V3.Y(-sz.y / 2), V3.Y(sz.y / 2), FillTp.Connect, FillTp.Connect, bodyUv, staUv, endUv, isVerNor, true, 0, true);
        }
        public static void BoxCupRound(ref Mesh mesh, int corSeg, int corRingDO, int corRingU, int corRingDI, float corRDO, float corRU, float corRDI, float borW, float hI, Vector3 sz, bool isVerNor, Vector4 bodyUv = default, Vector4 staUv = default, Vector4 endUv = default) {
            A.Def(ref bodyUv, V4.V(0, 0, M._2_3, 1), ref staUv, V4.V(M._2_3, 0, M._5_6, 1), ref endUv, V4.V(M._5_6, 0, 1, 1));
            SegRing(ref mesh, Mb.List(Mb.List(V3.nnp, V3.nnn, V3.pnn, V3.pnp).Scl(sz / 2), Mb.List(V3.npp, V3.npn, V3.ppn, V3.ppp).Scl(sz / 2), Mb.List(V3.npp, V3.npn, V3.ppn, V3.ppp).Scl(sz / 2 - V3.Xz(corRU * 2 + borW)), Mb.List(V3.npp, V3.npn, V3.ppn, V3.ppp).Scl(sz / 2 - V3.Xz(corRU * 2 + borW).Y(hI))), Mb.List(V4.V(corRDO).ListN(4), V4.V(corRU).ListN(4), V4.V(corRU).ListN(4), V4.V(corRDI).ListN(4)), corSeg.ListN(4), Mb.List(corRingDO, corRingU, corRingU, corRingDI), "", "", UvTp.Cor, UvTp.Cor, V3.Y(-sz.y / 2), V3.Y(sz.y / 2 - hI), FillTp.Fill, FillTp.Fill, bodyUv, staUv.SwapV(), endUv, isVerNor, true, 0, true);
        }
        // umbrella savaa lamp candle key lock
        public static void Star(ref Mesh mesh, int seg, float rD, float rD2, float rU, float rU2, float h, float hU, float hD, Vector4 bodyUv = default, Vector4 staUv = default, Vector4 endUv = default) {
            A.Def(ref bodyUv, V4.V(0, 0, M._2_3, 1), ref staUv, V4.V(M._2_3, 0, M._5_6, 1), ref endUv, V4.V(M._5_6, 0, 1, 1));
            SegRing(ref mesh, Mb.List(Crv.StarPnts(seg, rD, rD2).ListXz(-h / 2), Crv.StarPnts(seg, rU, rU2).ListXz(h / 2)), V4.V(0, 0, 0, 0).ListN(seg * 2).ListN(2), 0.ListN(seg * 2), 0.ListN(2), "", "", UvTp.Cor, UvTp.Dis, V3.Y(-h / 2 - hD), V3.Y(h / 2 + hU), FillTp.One, FillTp.One, bodyUv, staUv.SwapV(), endUv, false, true, 0, true);
        }
        public static void Star2(ref Mesh mesh, int seg, float r, float r2, float hU, float hD, Vector4 bodyUv = default, Vector4 staUv = default, Vector4 endUv = default) {
            A.Def(ref bodyUv, V4.V(0, 0, M._2_3, 1), ref staUv, V4.V(M._2_3, 0, M._5_6, 1), ref endUv, V4.V(M._5_6, 0, 1, 1));
            SegRing(ref mesh, Mb.List(Crv.StarPnts(seg, r, r2).ListXz()), V4.V(0, 0, 0, 0).ListN(seg * 2).ListN(2), 0.ListN(seg * 2), 0.ListN(2), "", "", UvTp.Cor, UvTp.Dis, V3.Y(-hD), V3.Y(hU), FillTp.One, FillTp.One, bodyUv, staUv.SwapV(), endUv, false, true, 0, true);
        }
        public static void Stair(ref Mesh mesh, Vector3 sz, int n) {
            Msh msh = new Msh();
            Vector3 sz2 = V3.V(sz.x / 2, sz.y / n, sz.z / n);
            for (int i = 0; i < n; i++)
                msh.vs.Add(V3.V(-sz2.x, sz2.y * (n - i), sz2.z * i), V3.V(-sz2.x, sz2.y * (n - i), sz2.z * (i + 1)), V3.V(sz2.x, sz2.y * (n - i), sz2.z * (i + 1)), V3.V(sz2.x, sz2.y * (n - i), sz2.z * i));
            msh.vs.Add(V3.V(-sz2.x, 0, sz.z), V3.V(-sz2.x, 0, 0), V3.V(sz2.x, 0, 0), V3.V(sz2.x, 0, sz.z));
            System.Func<int, int, int> Idx = (a, b) => a * 4 + b;
            for (int i = 0; i < n; i++)
                msh.ts.Fc(Idx(i, 0), Idx(i, 1), Idx(i, 2), Idx(i, 3)).Fc(Idx(i, 1), Idx(i + 1, 0), Idx(i + 1, 3), Idx(i, 2)).FcT(msh.vs.Count - 3, Idx(i, 1), Idx(i, 0)).FcT(msh.vs.Count - 3, Idx(i + 1, 0), Idx(i, 1)).FcT(msh.vs.Count - 2, Idx(i, 3), Idx(i, 2)).FcT(msh.vs.Count - 2, Idx(i, 2), Idx(i + 1, 3));
            msh.ts.Fc(msh.vs.Count - 3, 0, 3, msh.vs.Count - 2).Fc(msh.vs.Count - 4, msh.vs.Count - 3, msh.vs.Count - 2, msh.vs.Count - 1);
            msh.Upd(ref mesh);
        }
        public static void Twist(ref Mesh mesh, int seg, int ring, float rD, float rU, float h, float rotN, bool isVerNor, Vector4 bodyUv = default, Vector4 staUv = default, Vector4 endUv = default) {
            A.Def(ref bodyUv, V4.V(0, 0, M._2_3, 1), ref staUv, V4.V(M._2_3, 0, M._5_6, 1), ref endUv, V4.V(M._5_6, 0, 1, 1));
            List<List<Vector3>> pnts = new List<List<Vector3>>();
            for (int i = 0; i <= ring; i++)
                pnts.Add(Crv.AngPnts(V2.O, seg, M.Lerp(rD, rU, i.F() / ring), 360f / seg * rotN * i / ring, 360, true, false).ListXz(-h / 2 + h * i / ring));
            SegRing(ref mesh, pnts, null, null, null, "", "", UvTp.Fc, UvTp.Dis, V3.Y(-h / 2), V3.Y(h / 2), FillTp.Fill, FillTp.Fill, bodyUv, staUv.SwapV(), endUv, isVerNor, true, 0, true);
        }
        public static void Sphere(ref Mesh mesh, SphereTp type, int sub, float r, bool isVerNor) {
            if (type == SphereTp.Tetrahedron)
                AddFaces(ref mesh, sub, r, isVerNor, Mb.List(V3.V(-1, -1 / M.Sqrt6, -1 / M.Sqrt3), V3.V(0, -1 / M.Sqrt6, 2 / M.Sqrt3), V3.V(1, -1 / M.Sqrt6, -1 / M.Sqrt3), V3.V(0, 3 / M.Sqrt6, 0)).NorR(r), Mb.List(1, 0, 2, 0, 1, 3, 0, 3, 2, 2, 3, 1));
            else if (type == SphereTp.Hexahedron)
                AddFaces(ref mesh, sub, r, isVerNor, BoxPnts.Scl(V3.V(r / M.Sqrt3)), Mb.List(1, 5, 4, 0, 3, 7, 6, 2, 1, 0, 3, 2, 4, 5, 6, 7, 0, 4, 7, 3, 2, 6, 5, 1), 4);
            else if (type == SphereTp.Octahedron)
                AddFaces(ref mesh, sub, r, isVerNor, Mb.List(V3.d, V3.nzn, V3.nzp, V3.pzp, V3.pzn, V3.u).NorR(r), Mb.List(0, 1, 4, 0, 2, 1, 0, 3, 2, 0, 4, 3, 5, 4, 1, 5, 1, 2, 5, 2, 3, 5, 3, 4));
            else if (type == SphereTp.Dodecahedron)
                AddFaces(ref mesh, sub, r, isVerNor, Mb.List(V3.V(-1, -1, -1), V3.V(-1, -1, 1), V3.V(1, -1, 1), V3.V(1, -1, -1), V3.V(-1, 1, -1), V3.V(-1, 1, 1), V3.V(1, 1, 1), V3.V(1, 1, -1), V3.V(0, -1 / M.GoldenRatio, -M.GoldenRatio), V3.V(0, -1 / M.GoldenRatio, M.GoldenRatio), V3.V(0, 1 / M.GoldenRatio, -M.GoldenRatio), V3.V(0, 1 / M.GoldenRatio, M.GoldenRatio), V3.V(-1 / M.GoldenRatio, -M.GoldenRatio, 0), V3.V(1 / M.GoldenRatio, -M.GoldenRatio, 0), V3.V(-1 / M.GoldenRatio, M.GoldenRatio, 0), V3.V(1 / M.GoldenRatio, M.GoldenRatio, 0), V3.V(-M.GoldenRatio, 0, -1 / M.GoldenRatio), V3.V(-M.GoldenRatio, 0, 1 / M.GoldenRatio), V3.V(M.GoldenRatio, 0, 1 / M.GoldenRatio), V3.V(M.GoldenRatio, 0, -1 / M.GoldenRatio)).NorR(r), Mb.List(4, 14, 15, 7, 10, 4, 10, 8, 0, 16, 4, 16, 17, 5, 14, 6, 15, 14, 5, 11, 6, 18, 19, 7, 15, 6, 11, 9, 2, 18, 3, 13, 12, 0, 8, 3, 19, 18, 2, 13, 3, 8, 10, 7, 19, 1, 17, 16, 0, 12, 1, 12, 13, 2, 9, 1, 9, 11, 5, 17), 5);
            else if (type == SphereTp.Icosahedron)
                AddFaces(ref mesh, sub, r, isVerNor, Mb.List(V3.V(-1, M.GoldenRatio, 0), V3.V(1, M.GoldenRatio, 0), V3.V(-1, -M.GoldenRatio, 0), V3.V(1, -M.GoldenRatio, 0), V3.V(0, -1, M.GoldenRatio), V3.V(0, 1, M.GoldenRatio), V3.V(0, -1, -M.GoldenRatio), V3.V(0, 1, -M.GoldenRatio), V3.V(M.GoldenRatio, 0, -1), V3.V(M.GoldenRatio, 0, 1), V3.V(-M.GoldenRatio, 0, -1), V3.V(-M.GoldenRatio, 0, 1)).NorR(r), Mb.List(0, 11, 5, 0, 5, 1, 0, 1, 7, 0, 7, 10, 0, 10, 11, 1, 5, 9, 5, 11, 4, 11, 10, 2, 10, 7, 6, 7, 1, 8, 3, 9, 4, 3, 4, 2, 3, 2, 6, 3, 6, 8, 3, 8, 9, 4, 9, 5, 2, 4, 11, 6, 2, 10, 8, 6, 7, 9, 8, 1));
        }
        public static void SectorSegment(ref Mesh mesh, float r, float h, float staAng, float ang, int seg, int corSeg, int corRing, float corSegR, float corRingR, bool isSector, bool isVerNor, Vector4 bodyUv = default, Vector4 staUv = default, Vector4 endUv = default) {
            A.Def(ref bodyUv, V4.V(0, 0, M._2_3, 1), ref staUv, V4.V(M._2_3, 0, M._5_6, 1), ref endUv, V4.V(M._5_6, 0, 1, 1));
            SegRing(ref mesh, Mb.List(Crv.AngPnts(V2.O, seg, r, staAng - ang / 2, ang, true, true).ListXz(-h / 2).Add(isSector, V3.Y(-h / 2)), Crv.AngPnts(V2.O, seg, r, staAng - ang / 2, ang, true, true).ListXz(h / 2).Add(isSector, V3.Y(h / 2))), V4.V(corSegR, corRingR, corSegR, corRingR).ListN(isSector ? seg + 2 : seg + 1).ListN(2), corSeg.ListN(isSector ? seg + 2 : seg + 1), corRing.ListN(2), "", "", UvTp.Cor, UvTp.Dis, Crv.AngV3(staAng, r / 2, -h / 2), Crv.AngV3(staAng, r / 2, h / 2), FillTp.Fill, FillTp.Fill, bodyUv, staUv.SwapV(), endUv, isVerNor, true, 0, true);
        }
        public static void HemiSphere(ref Mesh mesh, int seg, int ring, float r, bool isVerNor, bool isUvFull) {
            Rm(ref mesh, Tf.R(-90), Crv.AngPnts(V2.Y(-r / 2), ring, r, 0, 90, true, true), seg, 0, null, null, FillTp.Fill, FillTp.None, 0, 0, UvTp.Dis, UvTp.Dis, isVerNor, isUvFull, "1");
        }
        public static void UvSphere(ref Mesh mesh, int seg, int ring, float r, bool isVerNor, bool isUvFull) {
            Rm(ref mesh, Tf.R(-90), Crv.AngPnts(V2.O, ring, r, -90, 180, true, true), seg, 0, null, null, FillTp.None, FillTp.None, 0, 0, UvTp.Dis, UvTp.Dis, isVerNor, isUvFull);
        }
        public static void Cylinder(ref Mesh mesh, int seg, float rD, float rU, float h, bool isVerNor, bool isUvFull) {
            Rm(ref mesh, Tf.R(-90), Mb.List(V2.V(rD, -h / 2), V2.V(rU, h / 2)), seg, 0, null, null, FillTp.Fill, FillTp.Fill, 0, 0, UvTp.Dis, UvTp.Dis, isVerNor, isUvFull, "11");
        }
        public static void CylinderRound(ref Mesh mesh, int seg, int ring, float r, float corR, float h, bool isVerNor, bool isUvFull) {
            Rm(ref mesh, Tf.R(-90), Crv.AngPnts(V2.V(r - corR, corR - h / 2), ring, corR, -90, 90, true, true).Add(Crv.AngPnts(V2.V(r - corR, h / 2 - corR), ring, corR, 0, 90, true, true)), seg, 0, null, null, FillTp.Fill, FillTp.Fill, 0, 0, UvTp.Dis, UvTp.Dis, isVerNor, isUvFull);
        }
        public static void Capsule(ref Mesh mesh, int seg, int ring, float r, float h, bool isVerNor, bool isUvFull) {
            Rm(ref mesh, Tf.R(-90), Crv.AngPnts(V2.Y(r - h / 2), ring, r, -90, 90, true, true).Add(Crv.AngPnts(V2.Y(h / 2 - r), ring, r, 0, 90, true, true)), seg, 0, null, null, FillTp.None, FillTp.None, 0, 0, UvTp.Dis, UvTp.Dis, isVerNor, isUvFull);
        }
        public static void Torus(ref Mesh mesh, int seg, int ring, float r, float corR, bool isVerNor, bool isUvFull) {
            Rm(ref mesh, Tf.R(-90), Crv.AngPnts(V2.X(r - corR), ring, corR, -90, 360, true, true), seg, 0, null, null, FillTp.None, FillTp.None, 0, 0, UvTp.Dis, UvTp.Dis, isVerNor, isUvFull);
        }
        public static void Pipe(ref Mesh mesh, int seg, float rO, float rI, float h, bool isVerNor, bool isUvFull) {
            Rm(ref mesh, Tf.R(-90), Mb.List(V2.V(rI, -h / 2), V2.V(rO, -h / 2), V2.V(rO, h / 2), V2.V(rI, h / 2)), seg, 0, null, null, FillTp.Connect, FillTp.Connect, 0, 0, UvTp.Fc, UvTp.Fc, isVerNor, isUvFull, "1111");
        }
        public static void PipeRound(ref Mesh mesh, int seg, int ring, float rO, float rI, float corR, float h, bool isVerNor, bool isUvFull) {
            Rm(ref mesh, Tf.R(-90), Mb.List(V2.V(rI, -h / 2), V2.V(rO, -h / 2), V2.V(rO, h / 2), V2.V(rI, h / 2)), seg, 0, ring.ListN(4), V4.V(corR).ListN(4), FillTp.Connect, FillTp.Connect, 0, 0, UvTp.Dis, UvTp.Dis, isVerNor, isUvFull, "1111");
        }
        public static void PipeRound2(ref Mesh mesh, int seg, int ring, float r, float corR, float h, bool isVerNor, bool isUvFull) {
            Rm(ref mesh, Tf.R(-90), Mb.List(V2.V(r - corR * 2, -h / 2), V2.V(r, -h / 2), V2.V(r, h / 2), V2.V(r - corR * 2, h / 2)), seg, 0, ring.ListN(4), V4.V(corR).ListN(4), FillTp.Connect, FillTp.Connect, 0, 0, UvTp.Dis, UvTp.Dis, isVerNor, isUvFull, "1111");
        }
        public static void Cup(ref Mesh mesh, int seg, float r, float rI, float h, float hI, bool isVerNor, bool isUvFull) {
            Rm(ref mesh, Tf.R(-90), Mb.List(V2.V(r, -h / 2), V2.V(r, h / 2), V2.V(rI, h / 2), V2.V(rI, h / 2 - hI)), seg, 0, null, null, FillTp.Fill, FillTp.Fill, 0, 0, UvTp.Fc, UvTp.Fc, isVerNor, isUvFull, "1111");
        }
        public static void CupRound(ref Mesh mesh, int seg, int ring, int ringI, int ringO, float borW, float r, float corR, float rDI, float rDO, float h, bool isVerNor, bool isUvFull) {
            Rm(ref mesh, Tf.R(-90), Crv.AngPnts(V2.V(r - rDO, rDO - h / 2), ringO, rDO, -90, 90, true, true).Add(Crv.AngPnts(V2.V(r - corR, h / 2 - corR), ring, corR, 0, 90, true, true)).Add(Crv.AngPnts(V2.V(r - borW + corR, h / 2 - corR), ring, corR, 90, 90, true, true)).Add(Crv.AngPnts(V2.V(r - rDI - borW, rDI + borW - h / 2), ringI, rDI, 0, -90, true, true)), seg, 0, null, null, FillTp.Fill, FillTp.Fill, 0, 0, UvTp.Dis, UvTp.Dis, isVerNor, isUvFull);
        }
        public static void Pyramid(ref Mesh mesh, int seg, float r, float h, bool isVerNor, bool isUvFull) {
            Rm(ref mesh, Tf.R(-90), Mb.List(V2.V(r, -h / 2)), seg, 0, null, null, FillTp.Fill, FillTp.One, 0, h, UvTp.Fc, UvTp.Fc, isVerNor, isUvFull, "1");
        }
        public static void DiPyramid(ref Mesh mesh, int seg, float r, float h, bool isVerNor, bool isUvFull) {
            Rm(ref mesh, Tf.R(-90), Mb.List(V2.X(r)), seg, 0, null, null, FillTp.One, FillTp.One, -h / 2, h / 2, UvTp.Fc, UvTp.Fc, isVerNor, isUvFull, "1");
        }
        public static void RotModel(ref Mesh mesh, int seg, int ring, float r, float h, AnimationCurve ac, bool isVerNor, bool isUvFull) {
            List<Vector2> pnts = new List<Vector2>();
            string fc = "";
            for (int i = 0; i <= ring; fc += i == 0 || i == ring ? "1" : "0", i++)
                pnts.Add(V2.V(ac.Evaluate(i.F() / ring) * r, h * i / ring - h / 2));
            Rm(ref mesh, Tf.R(-90), pnts, seg, 0, null, null, FillTp.Fill, FillTp.Fill, 0, 0, UvTp.Dis, UvTp.Dis, isVerNor, isUvFull);
        }
        public static void Balloon(ref Mesh mesh, int seg, int ring, float r, float rH, float rI, float hH, float hD, float hU, bool isVerNor, bool isUvFull) {
            Rm(ref mesh, Tf.R(-90), Mb.List(V2.Y(2), V2.X(2), V2.X(3), V2.V(3, 1), V2.Y(4), V2.V(1, 5), V2.V(1, 6), V2.Y(7)).Scl(V2.V(rH / 3, hH / 7)).Pos(V2.X(rI)).Add(Crv.AngPnts(V2.O, ring, 1, -90, 90, false, true).Scl(V2.V(r - rI, hD)).Pos(V2.V(rI, hH + hD))).Add(Crv.AngPnts(V2.O, ring, 1, 0, 90, false, true).Scl(V2.V(r - rI, hU)).Pos(V2.V(rI, hH + hD))), seg, 0, null, null, FillTp.Fill, FillTp.None, 0, 0, UvTp.Dis, UvTp.Dis, isVerNor, isUvFull);
        }
        public static void ChessPawn(ref Mesh mesh, int seg, float r, float h, bool isVerNor, bool isUvFull) {
            Rm(ref mesh, Tf.R(-90), Mb.List(V2.V(0.3f, -0.5f), V2.V(0.3f, -0.475f), V2.V(0.275f, -0.45f), V2.V(0.3f, -0.4f), V2.V(0.3f, -0.375f), V2.V(0.25f, -0.3f), V2.V(0.2f, -0.275f), V2.V(0.125f, -0.2f), V2.V(0.15f, -0.15f), V2.V(0.1f, -0.1f), V2.V(0.075f, 0.1f), V2.V(0.1f, 0.2f), V2.V(0.125f, 0.2f), V2.V(0.125f, 0.225f), V2.V(0.05f, 0.25f), V2.V(0.08f, 0.28f), V2.V(0.11f, 0.325f), V2.V(0.125f, 0.375f), V2.V(0.11f, 0.425f), V2.V(0.08f, 0.47f), V2.Y(0.5f)).Scl(V2.V(r / 0.3f, h)), seg, 0, null, null, FillTp.Fill, FillTp.None, 0, 0, UvTp.Dis, UvTp.Dis, isVerNor, isUvFull, "111000001001111");
        }
        public static void SawBlade(ref Mesh mesh, int seg, float h, float r, float r2, float r3, float rI, float a1, float a2, float a3) {
            Msh msh = new Msh();
            float y = h / 2, y2 = -h / 2, dAng = -360f / seg;
            for (int i = 0; i < seg; i++)
                msh.vs.Add(Crv.AngV3(i * dAng, r, y), Crv.AngV3((i + a1) * dAng, r2, y), Crv.AngV3((i + a2) * dAng, r3, y), Crv.AngV3((i + a3) * dAng, r, y), Crv.AngV3(i * dAng, rI, y), Crv.AngV3(i * dAng, r, y2), Crv.AngV3((i + a1) * dAng, r2, y2), Crv.AngV3((i + a2) * dAng, r3, y2), Crv.AngV3((i + a3) * dAng, r, y2), Crv.AngV3(i * dAng, rI, y2));
            System.Func<int, int, int> Idx = (a, b) => (a % seg) * 10 + b;
            for (int i = 0; i < seg; i++)
                msh.ts.Fc(Idx(i, 0), Idx(i, 1), Idx(i, 2), Idx(i, 3)).Fc(Idx(i, 4), Idx(i, 0), Idx(i, 3), Idx(i + 1, 4)).Add(Idx(i + 1, 4), Idx(i, 3), Idx(i + 1, 0)).FcI(Idx(i, 5), Idx(i, 6), Idx(i, 7), Idx(i, 8)).FcI(Idx(i, 9), Idx(i, 5), Idx(i, 8), Idx(i + 1, 9)).Add(Idx(i + 1, 5), Idx(i, 8), Idx(i + 1, 9)).Fc(Idx(i, 6), Idx(i, 1), Idx(i, 0), Idx(i, 5)).Fc(Idx(i, 7), Idx(i, 2), Idx(i, 1), Idx(i, 6)).Fc(Idx(i, 8), Idx(i, 3), Idx(i, 2), Idx(i, 7)).Fc(Idx(i + 1, 5), Idx(i + 1, 0), Idx(i, 3), Idx(i, 8)).Fc(Idx(i, 9), Idx(i, 4), Idx(i + 1, 4), Idx(i + 1, 9));
            msh.Upd(ref mesh);
        }
        public static void Pencil(ref Mesh mesh, int seg, float h, float hH, float hH2, float r, bool isVerNor, bool isEraser = false, int eRing = 5, float eR = 0, float eH = 0, float eBorDr = 0, float eBorH = 0) {
            List<Vector2> pnts = new List<Vector2>();
            List<int> uvIdxs = new List<int>();
            if (isEraser) {
                for (int i = 0; i <= eRing; i++) {
                    pnts.Add(Crv.AngV2(90f * i / eRing - 90, eR) + V2.V(r - eR, eR));
                    uvIdxs.Add(0);
                }
                pnts.Add(V2.V(r, eH), V2.V(r, eH), V2.V(r + eBorDr, eH), V2.V(r + eBorDr, eH), V2.V(r + eBorDr, eH + eBorH), V2.V(r + eBorDr, eH + eBorH), V2.V(r, eH + eBorH), V2.V(r, eH + eBorH), V2.V(r, h - hH), V2.V(r, h - hH), V2.V(hH2 / hH * r, h - hH2), V2.V(hH2 / hH * r, h - hH2), V2.Y(h));
                uvIdxs.Add(0, 1, 1, 1, 1, 1, 1, 2, 2, 3, 3, 4, 4);
            } else {
                pnts.Add(V2.X(hH2 / hH * r), V2.X(hH2 / hH * r), V2.X(r), V2.X(r), V2.V(r, h - hH), V2.V(r, h - hH), V2.V(hH2 / hH * r, h - hH2), V2.V(hH2 / hH * r, h - hH2), V2.Y(h));
                uvIdxs.Add(4, 3, 3, 2, 2, 3, 3, 4, 4);
            }
            Msh msh = new Msh(isVerNor);
            for (int i = 0; i < pnts.Count; i++)
                for (int j = 0; j < seg; j++)
                    msh.vs.Add(Crv.AngV3(360f * j / seg, pnts[i].x, pnts[i].y));
            msh.ts.TsOne(seg, msh.vs.Count - seg, 1, (pnts.Count - 1) * seg, 1, true);
            for (int i = isEraser ? 0 : 1; i < pnts.Count - 1; i += isEraser && i < eRing ? 1 : 2)
                msh.ts.TsLoop(seg, i * seg, 1, (i + 1) * seg, 1, true);
            msh.ts.TsFill(seg, 0, 1, false);
            Vector2[] uvs = Mb.Arr(V2.V(0.5f, 0.1f), V2.V(0.5f, 0.3f), V2.V(0.5f, 0.5f), V2.V(0.5f, 0.7f), V2.V(0.5f, 0.9f));
            for (int i = 0; i < pnts.Count; i++)
                for (int j = 0; j < seg; j++)
                    msh.uv.Add(uvs[uvIdxs[i]]);
            msh.Upd(ref mesh);
        }
        public static void Domino(ref Mesh mesh, int corSeg, int corRing, float corR, Vector3 sz, Vector3 w, Vector2Int n, int type, bool isVerNor) {
            List<Vector3> ps = BoxPnts.Scl(sz / 2 - V3.V(corR));
            Msh msh = new Msh(isVerNor);
            int m = (corRing + 1) * (corSeg + 1) * 8;
            System.Func<int, int, int, int> Idx = (a, b, c) => (a * (corRing + 1) + b) * (corSeg + 1) + c;
            System.Func<int, int, int, int, int> Idx2 = (a, b, c, d) => m + (a * 9 + b * 3 + c) * 4 + d;
            for (int i = 0; i < 8; i++)
                for (int j = 0; j <= corRing; j++)
                    for (int k = 0; k <= corSeg; k++)
                        msh.vs.Add(ps[i] + Crv.AngRr(90 * (2 - i) + 90f * k / corSeg, (i < 4 ? -90 : 0) + 90f * j / corRing, 0, corR, 0));
            Vector2 tSz = V2.V((sz.x - w.x * 2 - corR * 2) / 3, (sz.y - w.y * 2 - w.z - corR * 2) / 6), hSz = tSz / 2;
            for (int i = 0; i < 2; i++)
                for (int j = 0; j < 3; j++)
                    for (int k = 0; k < 3; k++) {
                        Vector3 p = V3.V((k - 1) * tSz.x, (i == 1).Sign() * (1.5f * tSz.y + w.z / 2) + (j - 1) * tSz.y, -sz.z / 2);
                        msh.vs.Add(p + V3.Xy(-hSz.x, -hSz.y), p + V3.Xy(-hSz.x, hSz.y), p + V3.Xy(hSz.x, hSz.y), p + V3.Xy(hSz.x, -hSz.y));
                    }
            msh.vs.Add(msh.vs[Idx2(0, 0, 0, 0)], msh.vs[Idx2(1, 2, 0, 1)], msh.vs[Idx2(1, 2, 2, 2)], msh.vs[Idx2(0, 0, 2, 3)], msh.vs[Idx2(0, 2, 0, 1)], msh.vs[Idx2(1, 0, 0, 0)], msh.vs[Idx2(1, 0, 2, 3)], msh.vs[Idx2(0, 2, 2, 2)]);
            for (int i = 0; i < 8; i++)
                for (int j = 0; j < corRing; j++)
                    msh.ts.TsLoop(corSeg, Idx(i, j, 0), 1, Idx(i, j + 1, 0), 1, true, 1, 1);
            for (int i = 0; i < 8; i++)
                msh.ts.TsLoop(corRing, Idx(i, 0, 0), corSeg + 1, Idx(i == 3 ? 0 : i == 7 ? 4 : i + 1, 0, corSeg), corSeg + 1, true, 1, 1000000);
            for (int i = 0; i < 4; i++)
                msh.ts.TsLoop(corSeg, Idx(i, corRing, 0), 1, Idx(i + 4, 0, 0), 1, true, 1, 1);
            msh.ts.Fc(Idx(1, corRing, corSeg), Idx(5, 0, corSeg), Idx(4, 0, 0), Idx(0, corRing, 0)).Fc(Idx(3, corRing, corSeg), Idx(7, 0, corSeg), Idx(6, 0, 0), Idx(2, corRing, 0)).Fc(Idx(1, 0, 0), Idx(0, 0, 0), Idx(3, 0, 0), Idx(2, 0, 0)).Fc(Idx(4, corRing, 0), Idx(5, corRing, 0), Idx(6, corRing, 0), Idx(7, corRing, 0)).Fc(Idx(2, corRing, corSeg), Idx(6, 0, corSeg), Idx(5, 0, 0), Idx(1, corRing, 0));
            for (int i = 0; i < 2; i++)
                for (int j = 0; j < 3; j++)
                    for (int k = 0; k < 3; k++)
                        msh.ts.Fc(Idx2(i, j, k, 0), Idx2(i, j, k, 1), Idx2(i, j, k, 2), Idx2(i, j, k, 3));
            msh.ts.Fc(Idx(0, corRing, corSeg), Idx(4, 0, corSeg), msh.vs.Count - 7, msh.vs.Count - 8).Fc(msh.vs.Count - 7, Idx(4, 0, corSeg), Idx(7, 0, 0), msh.vs.Count - 6).Fc(msh.vs.Count - 6, Idx(7, 0, 0), Idx(3, corRing, 0), msh.vs.Count - 5).Fc(Idx(0, corRing, corSeg), msh.vs.Count - 8, msh.vs.Count - 5, Idx(3, corRing, 0)).Fc(msh.vs.Count - 4, msh.vs.Count - 3, msh.vs.Count - 2, msh.vs.Count - 1);
            string[] arr = Mb.Arr("---------", "----+----", "+-------+", "+---+---+", "+-+---+-+", "+-+-+-+-+", "+-++-++-+");
            System.Func<int, int, Vector2> Uv = (a, b) => V2.V(a / 3f, type % 2 * 0.5f + b * 0.48f + 0.01f);
            Vector2 bgUv = Uv(0, 0);
            for (int i = 0; i < m; i++)
                msh.uv.Add(bgUv);
            for (int i = 0; i < 2; i++) {
                string s = arr[i == 0 ? n.x % 7 : n.y % 7];
                for (int j = 0; j < 3; j++)
                    for (int k = 0; k < 3; k++)
                        msh.uv.Add(s[j * 3 + k] == '+' ? Mb.List(Uv(0, 0), Uv(0, 1), Uv(1, 1), Uv(1, 0)) : Mb.List(bgUv, bgUv, bgUv, bgUv));
            }
            msh.uv.Add(bgUv, bgUv, bgUv, bgUv).Add(Uv(2, 0), Uv(2, 1), Uv(3, 1), Uv(3, 0));
            msh.Upd(ref mesh);
        }
        static System.Func<float, float, int, Vector2> MatchUv = (a, b, c) => V2.V((c + a) / 2f, b / 18f);
        public static void MatchBox(ref Mesh boxMesh, ref Mesh boxInMesh, Vector4 boxSz, int type) {
            Vector2 iUv = MatchUv(0.25f, 1.5f, type), oUv = MatchUv(0.75f, 1.5f, type);
            Vector3 box2Sz = boxSz.V3() - V3.Yz(boxSz.w * 2, boxSz.w * 2), boxInSz = box2Sz - V3.Yz(boxSz.w, boxSz.w), boxIn2Sz = boxInSz - V3.V(boxSz.w * 4, boxSz.w, boxSz.w * 2);
            new Msh(BoxPnts.Scl(boxSz / 2).Add(BoxPnts.Scl(boxSz / 2)).Add(BoxPnts.Scl(boxSz / 2)).Add(BoxPnts.Scl(box2Sz / 2)), Fc(4, 5, 6, 7).Fc(1, 0, 3, 2).Fc(8, 12, 15, 11).Fc(10, 14, 13, 9).Fc(19, 23, 31, 27).Fc(31, 23, 22, 30).Fc(26, 30, 22, 18).Fc(19, 27, 26, 18).Fc(17, 21, 29, 25).Fc(29, 21, 20, 28).Fc(24, 28, 20, 16).Fc(17, 25, 24, 16).Fc(31, 30, 29, 28).Fc(27, 31, 28, 24).Fc(26, 27, 24, 25).Fc(25, 29, 30, 26), Mb.List(MatchUv(0, 9, type), MatchUv(0, 2, type), MatchUv(1, 2, type), MatchUv(1, 9, type), MatchUv(0, 11, type), MatchUv(0, 18, type), MatchUv(1, 18, type), MatchUv(1, 11, type), MatchUv(0, 9, type), MatchUv(0, 9, type), MatchUv(1, 9, type), MatchUv(1, 9, type), MatchUv(0, 11, type), MatchUv(0, 11, type), MatchUv(1, 11, type), MatchUv(1, 11, type), iUv, iUv, iUv, iUv, iUv, iUv, iUv, iUv, iUv, iUv, iUv, iUv, iUv, iUv, iUv, iUv)).Upd(ref boxMesh);
            new Msh(BoxPnts.Scl(boxInSz / 2).Add(BoxPnts.Scl(boxInSz / 2)).Add(BoxPnts.Scl(boxIn2Sz / 2).Pos(V3.Y(boxInSz.y / 2 - boxIn2Sz.y / 2))).Add(BoxPnts.Scl(boxIn2Sz / 2).Pos(V3.Y(boxInSz.y / 2 - boxIn2Sz.y / 2))), Fc(1, 5, 4, 0).Fc(0, 4, 7, 3).Fc(3, 7, 6, 2).Fc(2, 6, 5, 1).Fc(1, 0, 3, 2).Fc(4, 5, 21, 20).Fc(29, 13, 14, 30).Fc(23, 22, 6, 7).Fc(12, 28, 31, 15).Fc(16, 20, 21, 17).Fc(25, 29, 30, 26).Fc(18, 22, 23, 19).Fc(27, 31, 28, 24).Fc(24, 25, 26, 27), Mb.List(oUv, oUv, oUv, oUv, oUv, oUv, oUv, oUv, iUv, iUv, iUv, iUv, iUv, iUv, iUv, iUv, oUv, oUv, oUv, oUv, oUv, oUv, oUv, oUv, iUv, iUv, iUv, iUv, iUv, iUv, iUv, iUv)).Upd(ref boxInMesh);
        }
        public static void MatchStick(ref Mesh mesh, float f0, float f, Vector4 stickSz, Nzp nzp, int type) {
            float fa = M.C01(f / f0), fb = M.C01(f / (1 - f0));
            Vector2 a0 = MatchUv(fa * 0.4f, 0.5f, type), a1 = MatchUv(fa * 0.4f + 0.4f, 0.5f, type), b0 = MatchUv(fb * 0.4f + 0.4f, 0.5f, type), b1 = MatchUv(fb * 0.4f, 0.5f, type), bUv = MatchUv(f > 0.001f && f0 + f > 1 - stickSz.y ? 0.6f : 0.9f, 0.5f, type);
            Msh msh = new Msh();
            List<Vector3> vs2 = Mb.List(V3.nnp, V3.npp, V3.npn, V3.nnn).Add(Mb.List(V3.nnp, V3.npp, V3.npn, V3.nnn, V3.nnp, V3.npp, V3.npn, V3.nnn).Pos(V3.X(f0 * 2))).Add(Mb.List(V3.pnp, V3.ppp, V3.ppn, V3.pnn)).Scl(V3.V(stickSz.x - stickSz.z + stickSz.y, stickSz.y, stickSz.y) / 2).Pos(V3.X(stickSz.y / 2 - stickSz.z / 2));
            List<int> ts2 = Fc(0, 1, 2, 3).Fc(0, 3, 7, 4).Fc(2, 1, 5, 6).Fc(3, 2, 6, 7).Fc(4, 5, 1, 0).Fc(8, 11, 15, 12).Fc(10, 9, 13, 14).Fc(11, 10, 14, 15).Fc(12, 13, 9, 8);
            List<Vector2> uv2 = Mb.List(a0, a0, a0, a0, a1, a1, a1, a1, b0, b0, b0, b0, b1, b1, b1, b1);
            for (int i = 0; i < ts2.Count; i++) {
                msh.vs.Add(vs2[ts2[i]]);
                msh.ts.Add(i);
                msh.uv.Add(uv2[ts2[i]]);
            }
            Mesh stickHeadMesh = new Mesh();
            Sphere(ref stickHeadMesh, SphereTp.Icosahedron, 1, 0.5f, true);
            stickHeadMesh.triangles.List().ForEach(x => msh.ts.Add(msh.vs.Count + x));
            stickHeadMesh.vertices.List().ForEach(x => msh.uv.Add(bUv));
            msh.vs.Add(stickHeadMesh.vertices.List().Scl(V3.V(stickSz.z, stickSz.w, stickSz.w)).Pos(V3.X(stickSz.x / 2 - stickSz.z / 2))).Pos(V3.X(stickSz.x / 2 * nzp.I()));
            msh.Upd(ref mesh);
        }
        static Vector2 CardUvSz = V2.V(1f / 26, 1f / 18);
        public static void Card(ref Mesh mesh, int corN, float corR, Vector3 sz, CardTp type, CardLetter letter, CardBackTp backType) {
            char l1 = letter.tS()[0], l2 = letter.tS()[1];
            Vector2 uvF1 = V2.Scl(CardUvSz,
                type == CardTp.Russian_A && letter == CardLetter.DA ? V2.V(8, 1) :
                type == CardTp.Old_A && letter == CardLetter.CA ? V2.V(9, 1) :
                l1 == 'J' ? (
                    type == CardTp.English_Black ? V2.V(l2 == 'R' ? 0 : 1, 1) :
                    type == CardTp.English ? V2.V(l2 == 'R' ? 2 : 3, 1) :
                    type == CardTp.Russian || type == CardTp.Russian_A ? V2.V(l2 == 'R' ? 4 : 5, 1) :
                    type == CardTp.Old || type == CardTp.Old_A ? V2.V(l2 == 'R' ? 6 : 7, 1) :
                    type == CardTp.Face ? V2.V(l2 == 'R' ? 0 : 1, 0) :
                    type == CardTp.Letter ? V2.V(l2 == 'R' ? 2 : 3, 0) :
                    V2.V(l2 == 'R' ? 6 : 7, 0)
                ) + V2.X(13) : V2.V(
                    (l2 == 'A' ? 0 : l2 == 'J' ? 10 : l2 == 'Q' ? 11 : l2 == 'K' ? 12 : l2.tS().I() - 1) +
                    (type == CardTp.Face || type == CardTp.Letter || type == CardTp.Uno ? 13 : 0),
                    (type == CardTp.English_Black || type == CardTp.Face ? 14 :
                    type == CardTp.English || type == CardTp.Letter ? 10 :
                    type == CardTp.Russian || type == CardTp.Russian_A || type == CardTp.Uno ? 6 : 2) +
                    (l1 == 'D' ? 3 : l1 == 'C' ? 2 : l1 == 'H' ? 1 : 0)
                )), uvB = V2.Scl(CardUvSz, V2.V((int)backType % 13 + 13, (int)backType / 13));
            Card(ref mesh, corN, corR, sz, uvF1, uvB);
        }
        public static void UnoCard(ref Mesh mesh, int corN, float corR, Vector3 sz, UnoCardLetter unoLetter) {
            char l1 = unoLetter.tS()[0], l2 = unoLetter.tS()[1];
            Vector2 uvF1 = V2.Scl(CardUvSz,
                unoLetter == UnoCardLetter.W ? V2.X(19) : unoLetter == UnoCardLetter.WD4 ? V2.X(20) :
                V2.V(l2 == 'S' ? 23 : l2 == 'R' ? 24 : l2 == 'D' ? 25 : l2.tS().I() + 13, l1 == 'R' ? 9 : l1 == 'Y' ? 8 : l1 == 'G' ? 7 : 6)
            ), uvB = V2.Scl(CardUvSz, V2.X(21));
            Card(ref mesh, corN, corR, sz, uvF1, uvB);
        }
        static void Card(ref Mesh mesh, int corN, float corR, Vector3 sz, Vector2 uvF1, Vector2 uvB) {
            List<Vector3> vs = new List<Vector3>();
            List<Vector3> ps = Mb.List(V3.npn, V3.ppn, V3.pnn, V3.nnn, V3.npp, V3.ppp, V3.pnp, V3.nnp).Scl(V3.V(sz.x / 2 - corR, sz.y / 2 - corR, sz.z / 2));
            for (int i = 0; i < 8; i++)
                for (int j = 0; j <= corN; j++)
                    vs.Add(ps[i] + Crv.AngV2(Ang.Unity((i % 4 - 1 + j.F() / corN) * 90), corR).V3());
            List<int> ts = new List<int>();
            int n = vs.Count / 2, n2 = n - 2;
            for (int i = 0, i1 = 1; i < n; i++, i1 = (i + 1) % n)
                ts.Add(Mb.Arr(i, n + i, n + i1, i, n + i1, i1));
            for (int i = 0; i < n2; i++)
                ts.Add(Mb.Arr(0, i + 1, i + 2));
            for (int i = 0; i < n2; i++)
                ts.Add(Mb.Arr(n + i + 2, n + i + 1, n));
            Msh msh = new Msh(true);
            for (int i = 0; i < ts.Count; i++) {
                msh.vs.Add(vs[ts[i]]);
                msh.ts.Add(i);
            }
            Vector2 uvF2 = uvF1 + CardUvSz, uvB1 = uvB + CardUvSz.Y(0), uvB2 = uvB + CardUvSz.X(0);
            System.Func<Vector3, Vector3, Vector2, Vector2, Vector2> Uv = (a, b, c, d) => V2.V(M.Remap(a.x, -b.x / 2, b.x / 2, c.x, d.x), M.Remap(a.y, -b.y / 2, b.y / 2, c.y, d.y));
            for (int i = 0; i < n; i++)
                msh.uv.Add(V2.O, V2.O, V2.O, V2.O, V2.O, V2.O);
            for (int i = 0, j = n * 6; i < n2; i++, j += 3)
                msh.uv.Add(Uv(msh.vs[j], sz, uvF1, uvF2), Uv(msh.vs[j + 1], sz, uvF1, uvF2), Uv(msh.vs[j + 2], sz, uvF1, uvF2));
            for (int i = 0, j = n * 6 + n2 * 3; i < n2; i++, j += 3)
                msh.uv.Add(Uv(msh.vs[j], sz, uvB1, uvB2), Uv(msh.vs[j + 1], sz, uvB1, uvB2), Uv(msh.vs[j + 2], sz, uvB1, uvB2));
            msh.Upd(ref mesh);
        }
        public static void Rm(ref Mesh mesh, Tf tf, List<Vector2> pnts, int seg, int corSeg, List<int> ringNs, List<Vector4> rs, FillTp staFt, FillTp endFt, float staDh, float endDh, UvTp segUt, UvTp ringUt, bool isVerNor, bool isUvFull, string fc = "", float uvAng = 0, Vector4 bodyUv = default, Vector4 staUv = default, Vector4 endUv = default) {
            A.Def(ref bodyUv, V4.V(0, 0, 4f / 6f, 1), ref staUv, V4.V(4f / 6f, 1, 5f / 6f, 0), ref endUv, V4.V(5f / 6f, 0, 1, 1));
            List<List<Vector4>> rs2 = new List<List<Vector4>>();
            for (int i = 0; i < pnts.Count; i++)
                rs2.Add((rs.NotNull() && i < rs.Count ? rs[i] : V4.O).ListN(seg));
            SegRing(ref mesh, Crv.RmPnts(tf, pnts, seg), rs2, corSeg.ListN(seg), ringNs, "", fc, segUt, ringUt, pnts[0].X(0).V3() + V3.Y(staDh), pnts.Last().X(0).V3() + V3.Y(endDh), staFt, endFt, bodyUv, staUv, endUv, isVerNor, isUvFull, uvAng, true);
        }
        public static Vector3 StaEndP(Vector3 a, Vector3 b, Vector3 c, Vector3 p) {
            Vector3 cen = (V3.MoveUnc(b, a, 1) + V3.MoveUnc(b, c, 1)) / 2;
            float t = V3.Dis(b, cen.Y(b.y)) / V3.Dis(b, p.Y(b.y));
            return V3.MoveUnc(b, cen.Y(M.Lerp(b.y, p.y, t)), 1000);
        }
        public static void SegRing(ref Mesh mesh, List<List<Vector3>> pnts, List<List<Vector4>> rs, List<int> segNs, List<int> ringNs, string segFc, string ringFc, UvTp segUt, UvTp ringUt, Vector3 staP, Vector3 endP, FillTp staFt, FillTp endFt, Vector4 bodyUv, Vector4 staUv, Vector4 endUv, bool isVerNor, bool isUvFull, float uvAng, bool useStaEnd) {
            A.Def(ref bodyUv, V4.V(0, 0, M._2_3, 1), ref staUv, V4.V(M._2_3, 0, M._5_6, 1), ref endUv, V4.V(M._5_6, 0, 1, 1));
            int segCnt = pnts[0].Count, ringCnt = pnts.Count;
            A.Fill2(ref rs, V4.O, ringCnt, segCnt);
            A.Fill(ref segNs, 0, segCnt);
            A.Fill(ref ringNs, 0, ringCnt);
            segFc = segFc.Fill('0', segCnt);
            ringFc = ringFc.Fill('0', ringCnt);
            List<List<PntData>> segs = new List<List<PntData>>();
            for (int i = 0; i < ringCnt; i++)
                segs.Add(Crv.PntDatas(pnts[i], rs[i].Parse(x => V2.V(x.x, x.z)), segNs, segFc, segUt, V3.O, V3.O, true, rs[i].Parse(x => V2.V(x.y, x.w))));
            bool isConnect = staFt == FillTp.Connect && endFt == FillTp.Connect;
            List<PntData> seg = segs[0], seg2 = segs.Last();
            List<List<PntData>> rings = new List<List<PntData>>();
            for (int i = 0; i < seg.Count; i++) {
                List<Vector3> ringPnts = new List<Vector3>();
                List<Vector2> ringRs = new List<Vector2>();
                for (int j = 0; j < ringCnt; j++) {
                    ringPnts.Add(segs[j][i].p);
                    ringRs.Add(segs[j][i].r);
                }
                int i1 = i == 0 ? seg.Count - 2 : i - 1, i2 = i == seg.Count - 1 ? 1 : i + 1;
                rings.Add(Crv.PntDatas(ringPnts, ringRs, ringNs, ringFc, ringUt, useStaEnd ? staP : StaEndP(seg[i1].p, seg[i].p, seg[i2].p, staP), useStaEnd ? endP : StaEndP(seg2[i1].p, seg2[i].p, seg2[i2].p, endP), isConnect, ringRs));
            }
            List<PntData> ring = rings[0].Copy();
            Msh msh = new Msh(isVerNor);
            for (int i = 0; i < ring.Count; i++)
                for (int j = 0; j < seg.Count; j++) {
                    msh.vs.Add(rings[j][i].p);
                    msh.uv.Add(bodyUv.Uv(seg[j].uv, ring[i].uv));
                }
            for (int i = 0; i < ring.Count - 1; i++)
                if (ring[i].isFc)
                    for (int j = 0; j < seg.Count - 1; j++)
                        if (seg[j].isFc)
                            msh.ts.Fc(i * seg.Count + j, (i + 1) * seg.Count + j, (i + 1) * seg.Count + j + 1, i * seg.Count + j + 1);
            List<Vector3> staPnts = isUvFull ? pnts[0].Copy().Add1(pnts[0][0]) : msh.vs.Rng(seg.Count), endPnts = isUvFull ? pnts.Last().Copy().Add1(pnts.Last()[0]) : msh.vs.RngLast(seg.Count);
            Tf staTf = Tf.PR(staP, uvAng), endTf = Tf.PR(endP, uvAng);
            for (int i = 0; i < staPnts.Count; i++) {
                staPnts[i] = staTf.Pnt(staPnts[i]);
                endPnts[i] = endTf.Pnt(endPnts[i]);
            }
            Vector3 staMin = V3.Min(staPnts), staMax = V3.Max(staPnts), endMin = V3.Min(endPnts), endMax = V3.Max(endPnts);
            System.Func<Vector3, Vector2> StaUv = a => staUv.Uv(staMin, staMax, a), EndUv = a => endUv.Uv(endMin, endMax, a);
            (FillTp, bool, Vector3, System.Func<Vector3, Vector2>, Tf, int)[] arr = Mb.Arr((staFt, false, staP, StaUv, staTf, 0), (endFt, true, endP, EndUv, endTf, (ring.Count - 1) * seg.Count));
            for (int i = 0; i < 2; i++)
                if (arr[i].Item1 != FillTp.None && arr[i].Item1 != FillTp.Connect) {
                    List<Vector3> vs2 = msh.vs.Rng(arr[i].Item6, seg.Count);
                    msh.vs.Add(vs2);
                    vs2.ForEach(x => msh.uv.Add(arr[i].Item4(arr[i].Item5.Pnt(x))));
                    if (arr[i].Item1 == FillTp.One) {
                        msh.vs.Add(arr[i].Item3.ListN(seg.Count - 1));
                        msh.uv.Add(arr[i].Item4(arr[i].Item3).ListN(seg.Count - 1));
                        msh.ts.TsOne(seg.Count - 1, msh.vs.Count - seg.Count + 1, 1, msh.vs.Count - seg.Count * 2 + 1, 1, arr[i].Item2, false);
                    } else if (arr[i].Item1 == FillTp.Fill) {
                        msh.ts.Add(Geometry.Shp2D.Crt(vs2.Rng(0, vs2.Count - 1)).Ts(arr[i].Item2, msh.vs.Count - seg.Count));
                    }
                }
            msh.Upd(ref mesh);
        }
        public static void AddFaces(ref Mesh mesh, int sub, float r, bool isVerNor, List<Vector3> vs, List<int> ts, params int[] n) {
            AddMidPnt(ref vs, ref ts, n);
            SubFaces(sub, ref vs, ref ts, true, true, r);
            new Msh(vs, ts, isVerNor).Upd(ref mesh);
        }
        public static void AddMidPnt(ref List<Vector3> vs, ref List<int> ts, params int[] n) {
            if (n.Length > 0) {
                List<int> tmp = ts.Copy();
                ts.Clear();
                for (int i = 0, j = 0, m; i < tmp.Count; i += m, j++) {
                    m = n[j % n.Length];
                    Vector3 p = V3.O;
                    for (int k = 0; k < m; k++)
                        p += vs[tmp[i + k]];
                    vs.Add(p / m);
                    for (int k = 0; k < m; k++)
                        ts.FcT(vs.Count - 1, tmp[i + k], tmp[i + (k + 1) % m]);
                }
            }
        }
        public static void SubFaces(int n, ref List<Vector3> vs, ref List<int> ts, bool isVerR = false, bool isR = false, float r = 0.5f) {
            for (int i = ts.Count - 3; i >= 0; i -= 3)
                SubFace(n, i, i + 1, i + 2, ref vs, ref ts, isVerR, isR, r);
        }
        public static void SubFace(int n, int t0, int t1, int t2, ref List<Vector3> vs, ref List<int> ts, bool isVerR = false, bool isR = false, float r = 0.5f) {
            if (n >= 2) {
                int vsCnt = vs.Count, i0 = ts[t0], i1 = ts[t1], i2 = ts[t2];
                ts.RmvAt(t0);
                ts.RmvAt(t0 < t1 ? t1 - 1 : t1);
                ts.RmvAt(t2 < t0 && t2 < t1 ? t2 : t2 > t0 && t2 > t1 ? t2 - 2 : t2 - 1);
                if (isVerR && isR) {
                    vs[i0] = vs[i0].normalized * r;
                    vs[i1] = vs[i1].normalized * r;
                    vs[i2] = vs[i2].normalized * r;
                }
                for (int i = 0; i < n; i++) {
                    Vector3 a = V3.Lerp(vs[i0], vs[i1], i.F() / n), b = V3.Lerp(vs[i2], vs[i1], i.F() / n);
                    for (int j = 0; j <= n - i; j++)
                        if (!(i == 0 && (j == 0 || j == n))) {
                            Vector3 p = V3.Lerp(a, b, j.F() / (n - i));
                            vs.Add(isR ? p.normalized * r : p);
                        }
                }
                System.Func<int, int, int> Idx = (a, b) => a == 0 ? (b == 0 ? i0 : b == n ? i2 : vsCnt + b - 1) : a == n ? i1 : vsCnt + n * (n + 1) / 2 - (n - a) * (n - a + 1) / 2 + a - 2 + b;
                for (int i = 0; i < n; i++)
                    for (int j = 0; j < n - i; j++)
                        ts.Add(j == n - i - 1 ? Mb.List(Idx(i, j), Idx(i + 1, j), Idx(i, j + 1)) : Mb.List(Idx(i, j), Idx(i + 1, j), Idx(i, j + 1), Idx(i, j + 1), Idx(i + 1, j), Idx(i + 1, j + 1)));
            }
        }
    }
    public static class G {
        public static Gradient C(GradientColorKey[] cKeys, GradientAlphaKey[] aKeys, GradientMode mode = GradientMode.Blend) { Gradient g = new Gradient(); g.SetKeys(cKeys, aKeys); g.mode = mode; return g; }
        public static GradientColorKey Ck(Color c, float t) { return new GradientColorKey(c, t); }
        public static GradientAlphaKey Ak(float a, float t) { return new GradientAlphaKey(a, t); }
    }
    public static class Ps {
        public const ParticleSystemAnimationMode AnimMdGrid = ParticleSystemAnimationMode.Grid, AnimMdSpr = ParticleSystemAnimationMode.Sprites;
        public const ParticleSystemAnimationRowMode AnimRowMdCus = ParticleSystemAnimationRowMode.Custom, AnimRowMdRnd = ParticleSystemAnimationRowMode.Random, AnimRowMdMshIdx = ParticleSystemAnimationRowMode.MeshIndex;
        public const ParticleSystemAnimationTimeMode AnimTmMdLt = ParticleSystemAnimationTimeMode.Lifetime, AnimTmMdSpd = ParticleSystemAnimationTimeMode.Speed, AnimTmMdFPS = ParticleSystemAnimationTimeMode.FPS;
        public const ParticleSystemAnimationType AnimTpWholeSheet = ParticleSystemAnimationType.WholeSheet, AnimTpSingleRow = ParticleSystemAnimationType.SingleRow;
        public const ParticleSystemCollisionMode ColMdCol3D = ParticleSystemCollisionMode.Collision3D, ColMdCol2D = ParticleSystemCollisionMode.Collision2D;
        public const ParticleSystemCollisionQuality ColQltHigh = ParticleSystemCollisionQuality.High, ColQltMed = ParticleSystemCollisionQuality.Medium, ColQltLow = ParticleSystemCollisionQuality.Low;
        public const ParticleSystemCollisionType ColTpPln = ParticleSystemCollisionType.Planes, ColTpWor = ParticleSystemCollisionType.World;
        public const ParticleSystemCullingMode CulMdAuto = ParticleSystemCullingMode.Automatic, CulMdPauseAndCatchup = ParticleSystemCullingMode.PauseAndCatchup, CulMdPause = ParticleSystemCullingMode.Pause, CulMdAlwaysSim = ParticleSystemCullingMode.AlwaysSimulate;
        public const ParticleSystemCurveMode CrvMdCnst = ParticleSystemCurveMode.Constant, CrvMdCrv = ParticleSystemCurveMode.Curve, CrvMd2Crv = ParticleSystemCurveMode.TwoCurves, CrvMd2Cnst = ParticleSystemCurveMode.TwoConstants;
        public const ParticleSystemCustomData CusDatCus1 = ParticleSystemCustomData.Custom1, CusDatCus2 = ParticleSystemCustomData.Custom2;
        public const ParticleSystemCustomDataMode CusDatMdDisabled = ParticleSystemCustomDataMode.Disabled, CusDatMdV = ParticleSystemCustomDataMode.Vector, CusDatMdC = ParticleSystemCustomDataMode.Color;
        public const ParticleSystemEmitterVelocityMode EmVelMdTf = ParticleSystemEmitterVelocityMode.Transform, EmVelMdRb = ParticleSystemEmitterVelocityMode.Rigidbody;
        public const ParticleSystemForceFieldShape FrcFldShpSphere = ParticleSystemForceFieldShape.Sphere, FrcFldShpHemisphere = ParticleSystemForceFieldShape.Hemisphere, FrcFldShpCylinder = ParticleSystemForceFieldShape.Cylinder, FrcFldShpBox = ParticleSystemForceFieldShape.Box;
        public const ParticleSystemGameObjectFilter GoFilLm = ParticleSystemGameObjectFilter.LayerMask, GoFilList = ParticleSystemGameObjectFilter.List, GoFilLmList = ParticleSystemGameObjectFilter.LayerMaskAndList;
        public const ParticleSystemGradientMode GMdC = ParticleSystemGradientMode.Color, GMdG = ParticleSystemGradientMode.Gradient, GMd2C = ParticleSystemGradientMode.TwoColors, GMd2G = ParticleSystemGradientMode.TwoGradients, GMdRndC = ParticleSystemGradientMode.RandomColor;
        public const ParticleSystemInheritVelocityMode InhVelMdInit = ParticleSystemInheritVelocityMode.Initial, InhVelMdCur = ParticleSystemInheritVelocityMode.Current;
        public const ParticleSystemMeshShapeType MshShpTpVer = ParticleSystemMeshShapeType.Vertex, MshShpTpEdg = ParticleSystemMeshShapeType.Edge, MshShpTpTri = ParticleSystemMeshShapeType.Triangle;
        public const ParticleSystemNoiseQuality NosQltLow = ParticleSystemNoiseQuality.Low, NosQltMed = ParticleSystemNoiseQuality.Medium, NosQltHigh = ParticleSystemNoiseQuality.High;
        public const ParticleSystemOverlapAction OvlActIgn = ParticleSystemOverlapAction.Ignore, OvlActKill = ParticleSystemOverlapAction.Kill, OvlActCallback = ParticleSystemOverlapAction.Callback;
        public const ParticleSystemRenderMode RenMdBillboard = ParticleSystemRenderMode.Billboard, RenMdStretch = ParticleSystemRenderMode.Stretch, RenMdHorBillboard = ParticleSystemRenderMode.HorizontalBillboard, RenMdVerBillboard = ParticleSystemRenderMode.VerticalBillboard, RenMdMsh = ParticleSystemRenderMode.Mesh, RenMdNone = ParticleSystemRenderMode.None;
        public const ParticleSystemRenderSpace RenSpcVw = ParticleSystemRenderSpace.View, RenSpcWor = ParticleSystemRenderSpace.World, RenSpcLoc = ParticleSystemRenderSpace.Local, RenSpcFc = ParticleSystemRenderSpace.Facing, RenSpcVel = ParticleSystemRenderSpace.Velocity;
        public const ParticleSystemRingBufferMode RingBufMdDisabled = ParticleSystemRingBufferMode.Disabled, RingBufMdPauseUntilRpl = ParticleSystemRingBufferMode.PauseUntilReplaced, RingBufMdLoopUntilRpl = ParticleSystemRingBufferMode.LoopUntilReplaced;
        public const ParticleSystemScalingMode SclMdHierarchy = ParticleSystemScalingMode.Hierarchy, SclMdLoc = ParticleSystemScalingMode.Local, SclMdShp = ParticleSystemScalingMode.Shape;
        public const ParticleSystemShapeMultiModeValue ShpMulMdValRnd = ParticleSystemShapeMultiModeValue.Random, ShpMulMdValLoop = ParticleSystemShapeMultiModeValue.Loop, ShpMulMdValPingPong = ParticleSystemShapeMultiModeValue.PingPong, ShpMulMdValBurstSpread = ParticleSystemShapeMultiModeValue.BurstSpread;
        public const ParticleSystemShapeTextureChannel ShpTexChlR = ParticleSystemShapeTextureChannel.Red, ShpTexChlG = ParticleSystemShapeTextureChannel.Green, ShpTexChlB = ParticleSystemShapeTextureChannel.Blue, ShpTexChlA = ParticleSystemShapeTextureChannel.Alpha;
        public const ParticleSystemShapeType ShpTpSphere = ParticleSystemShapeType.Sphere, ShpTpSphereShell = ParticleSystemShapeType.SphereShell, ShpTpHemisphere = ParticleSystemShapeType.Hemisphere, ShpTpHemisphereShell = ParticleSystemShapeType.HemisphereShell, ShpTpCone = ParticleSystemShapeType.Cone, ShpTpBox = ParticleSystemShapeType.Box, ShpTpMesh = ParticleSystemShapeType.Mesh, ShpTpConeShell = ParticleSystemShapeType.ConeShell, ShpTpConeVol = ParticleSystemShapeType.ConeVolume, ShpTpConeVolShell = ParticleSystemShapeType.ConeVolumeShell, ShpTpCircle = ParticleSystemShapeType.Circle, ShpTpCircleEdg = ParticleSystemShapeType.CircleEdge, ShpTpSingleSideEdg = ParticleSystemShapeType.SingleSidedEdge, ShpTpMeshRen = ParticleSystemShapeType.MeshRenderer, ShpTpSkinMshRen = ParticleSystemShapeType.SkinnedMeshRenderer, ShpTpBoxShell = ParticleSystemShapeType.BoxShell, ShpTpBoxEdg = ParticleSystemShapeType.BoxEdge, ShpTpDonut = ParticleSystemShapeType.Donut, ShpTpRect = ParticleSystemShapeType.Rectangle, ShpTpSpr = ParticleSystemShapeType.Sprite, ShpTpSprRen = ParticleSystemShapeType.SpriteRenderer;
        public const ParticleSystemSimulationSpace SimSpcLoc = ParticleSystemSimulationSpace.Local, SimSpcWor = ParticleSystemSimulationSpace.World, SimSpcCus = ParticleSystemSimulationSpace.Custom;
        public const ParticleSystemSortMode SortMdNone = ParticleSystemSortMode.None, SortMdDis = ParticleSystemSortMode.Distance, SortMdOldestInF = ParticleSystemSortMode.OldestInFront, SortMdYoungestInF = ParticleSystemSortMode.YoungestInFront;
        public const ParticleSystemStopAction StopActNone = ParticleSystemStopAction.None, StopActDis = ParticleSystemStopAction.Disable, StopActDst = ParticleSystemStopAction.Destroy, StopActCallback = ParticleSystemStopAction.Callback;
        public const ParticleSystemStopBehavior StopBehStopEmClr = ParticleSystemStopBehavior.StopEmittingAndClear, StopBehStopEm = ParticleSystemStopBehavior.StopEmitting;
        public const ParticleSystemSubEmitterProperties SubEmPrpInhNothing = ParticleSystemSubEmitterProperties.InheritNothing, SubEmPrpInhC = ParticleSystemSubEmitterProperties.InheritColor, SubEmPrpInhSz = ParticleSystemSubEmitterProperties.InheritSize, SubEmPrpInhRot = ParticleSystemSubEmitterProperties.InheritRotation, SubEmPrpInhLt = ParticleSystemSubEmitterProperties.InheritLifetime, SubEmPrpInhDur = ParticleSystemSubEmitterProperties.InheritDuration, SubEmPrpInhEverything = ParticleSystemSubEmitterProperties.InheritEverything;
        public const ParticleSystemSubEmitterType SubEmTpBirth = ParticleSystemSubEmitterType.Birth, SubEmTpCol = ParticleSystemSubEmitterType.Collision, SubEmTpDth = ParticleSystemSubEmitterType.Death, SubEmTpTrg = ParticleSystemSubEmitterType.Trigger, SubEmTpManual = ParticleSystemSubEmitterType.Manual;
        public const ParticleSystemTrailMode TrlMdPerP = ParticleSystemTrailMode.PerParticle, TrlMdRibbon = ParticleSystemTrailMode.Ribbon;
        public const ParticleSystemTrailTextureMode TrlTexMdStretch = ParticleSystemTrailTextureMode.Stretch, TrlTexMdTile = ParticleSystemTrailTextureMode.Tile, TrlTexMdDistributePerSeg = ParticleSystemTrailTextureMode.DistributePerSegment, TrlTexMdRepPerSeg = ParticleSystemTrailTextureMode.RepeatPerSegment;
        public const ParticleSystemTriggerEventType TrgEvtTpIn = ParticleSystemTriggerEventType.Inside, TrgEvtTpOut = ParticleSystemTriggerEventType.Outside, TrgEvtTpEnter = ParticleSystemTriggerEventType.Enter, TrgEvtTpExit = ParticleSystemTriggerEventType.Exit;
        public const ParticleSystemVertexStream VerStmPos = ParticleSystemVertexStream.Position, VerStmNor = ParticleSystemVertexStream.Normal, VerStmTan = ParticleSystemVertexStream.Tangent, VerStmC = ParticleSystemVertexStream.Color, VerStmUV = ParticleSystemVertexStream.UV, VerStmUV2 = ParticleSystemVertexStream.UV2, VerStmUV3 = ParticleSystemVertexStream.UV3, VerStmUV4 = ParticleSystemVertexStream.UV4, VerStmAnimBlend = ParticleSystemVertexStream.AnimBlend, VerStmAnimFrm = ParticleSystemVertexStream.AnimFrame, VerStmCen = ParticleSystemVertexStream.Center, VerStmVerID = ParticleSystemVertexStream.VertexID, VerStmSzX = ParticleSystemVertexStream.SizeX, VerStmSzXY = ParticleSystemVertexStream.SizeXY, VerStmSzXYZ = ParticleSystemVertexStream.SizeXYZ, VerStmRot = ParticleSystemVertexStream.Rotation, VerStmRot3D = ParticleSystemVertexStream.Rotation3D, VerStmRotSpd = ParticleSystemVertexStream.RotationSpeed, VerStmRotSpd3D = ParticleSystemVertexStream.RotationSpeed3D, VerStmVel = ParticleSystemVertexStream.Velocity, VerStmSpd = ParticleSystemVertexStream.Speed, VerStmAgePct = ParticleSystemVertexStream.AgePercent, VerStmInvStaLt = ParticleSystemVertexStream.InvStartLifetime, VerStmStableRndX = ParticleSystemVertexStream.StableRandomX, VerStmStableRndXY = ParticleSystemVertexStream.StableRandomXY, VerStmStableRndXYZ = ParticleSystemVertexStream.StableRandomXYZ, VerStmStableRndXYZW = ParticleSystemVertexStream.StableRandomXYZW, VerStmVaryRndX = ParticleSystemVertexStream.VaryingRandomX, VerStmVaryRndXY = ParticleSystemVertexStream.VaryingRandomXY, VerStmVaryRndXYZ = ParticleSystemVertexStream.VaryingRandomXYZ, VerStmVaryRndXYZW = ParticleSystemVertexStream.VaryingRandomXYZW, VerStmCus1X = ParticleSystemVertexStream.Custom1X, VerStmCus1XY = ParticleSystemVertexStream.Custom1XY, VerStmCus1XYZ = ParticleSystemVertexStream.Custom1XYZ, VerStmCus1XYZW = ParticleSystemVertexStream.Custom1XYZW, VerStmCus2X = ParticleSystemVertexStream.Custom2X, VerStmCus2XY = ParticleSystemVertexStream.Custom2XY, VerStmCus2XYZ = ParticleSystemVertexStream.Custom2XYZ, VerStmCus2XYZW = ParticleSystemVertexStream.Custom2XYZW, VerStmNosSumX = ParticleSystemVertexStream.NoiseSumX, VerStmNosSumXY = ParticleSystemVertexStream.NoiseSumXY, VerStmNosSumXYZ = ParticleSystemVertexStream.NoiseSumXYZ, VerStmNosImpX = ParticleSystemVertexStream.NoiseImpulseX, VerStmNosImpXY = ParticleSystemVertexStream.NoiseImpulseXY, VerStmNosImpXYZ = ParticleSystemVertexStream.NoiseImpulseXYZ, VerStmMshIdx = ParticleSystemVertexStream.MeshIndex;
        public static ParticleSystem.Burst B(float t, short cnt) { return new ParticleSystem.Burst(t, cnt); }
        public static ParticleSystem.Burst B(float t, ParticleSystem.MinMaxCurve cnt) { return new ParticleSystem.Burst(t, cnt); }
        public static ParticleSystem.Burst B(float t, short minCnt, short maxCnt) { return new ParticleSystem.Burst(t, minCnt, maxCnt); }
        public static ParticleSystem.Burst B(float t, ParticleSystem.MinMaxCurve cnt, int cycleCnt, float repInterval) { return new ParticleSystem.Burst(t, cnt, cycleCnt, repInterval); }
        public static ParticleSystem.Burst B(float t, short minCnt, short maxCnt, int cycleCnt, float repInterval) { return new ParticleSystem.Burst(t, minCnt, maxCnt, cycleCnt, repInterval); }
        public static ParticleSystem.Burst B(float t, short cnt, int cycleCnt, float repInterval) { return new ParticleSystem.Burst(t, Mmc(cnt), cycleCnt, repInterval); }
        public static ParticleSystem.Burst Bp(float t, short cnt, int cycleCnt, float repInterval, float p) { ParticleSystem.Burst b = new ParticleSystem.Burst(t, Mmc(cnt), cycleCnt, repInterval); b.probability = p; return b; }
        public static ParticleSystem.MinMaxCurve Mmc(float f) { return new ParticleSystem.MinMaxCurve(f); }
        public static ParticleSystem.MinMaxCurve Mmc(float mul, AnimationCurve ac) { return new ParticleSystem.MinMaxCurve(mul, ac); }
        public static ParticleSystem.MinMaxCurve Mmc(float min, float max) { return new ParticleSystem.MinMaxCurve(min, max); }
        public static ParticleSystem.MinMaxCurve Mmc(float mul, AnimationCurve min, AnimationCurve max) { return new ParticleSystem.MinMaxCurve(mul, min, max); }
        public static ParticleSystem.MinMaxGradient Mmg(Color c) { return new ParticleSystem.MinMaxGradient(c); }
        public static ParticleSystem.MinMaxGradient Mmg(Gradient g) { return new ParticleSystem.MinMaxGradient(g); }
        public static ParticleSystem.MinMaxGradient Mmg(Color min, Color max) { return new ParticleSystem.MinMaxGradient(min, max); }
        public static ParticleSystem.MinMaxGradient Mmg(Gradient min, Gradient max) { return new ParticleSystem.MinMaxGradient(min, max); }
    }
    public static class M {
        public const float PI = 3.14159265359F, e = 2.71828182846F, Epsilon = 1.401298E-45F, MaxVal = 3.40282347E+38F, MinVal = -3.40282347E+38F, NaN = 0F / 0F, NegInf = -1F / 0F, PosInf = 1F / 0F, GoldenRatio = 1.61803398875F, _1_3 = 1F / 3F, _2_3 = 2F / 3F, _1_6 = 1F / 6F, _5_6 = 5F / 6F, _1_7 = 1F / 7F, _2_7 = 2F / 7F, _3_7 = 3F / 7F, _4_7 = 4F / 7F, _5_7 = 5F / 7F, _6_7 = 6F / 7F, _1_9 = 1F / 9F, _2_9 = 2F / 9F, _4_9 = 4F / 9F, _5_9 = 5F / 9F, _7_9 = 7F / 9F, _8_9 = 8F / 9F, Sqrt2 = 1.41421356237F, Sqrt3 = 1.73205080757F, Sqrt5 = 2.2360679775F, Sqrt6 = 2.44948974278F, Sqrt7 = 2.64575131106F, Sqrt8 = 2.82842712475F, DegRad = 0.0174532924F, RadDeg = 57.29578F, KmMi = 0.621371192F, MiKm = 1.609344F, MFt = 3.2808399F, FtM = 0.3048F, CmIn = 0.393700787F, InCm = 2.54F, KgLbs = 2.20462262F, LbsKg = 0.45359237F, LOz = 33.8140227F, OzL = 0.0295735296F;
        public static float Ck(float f) { return f + 273.15f; }
        public static float Cf(float f) { return f * 9f / 5f + 32f; }
        public static float Kc(float f) { return f - 273.15f; }
        public static float Kf(float f) { return (f - 273.15f) * 9f / 5f + 32f; }
        public static float Fc(float f) { return (f - 32f) * 5f / 9f; }
        public static float Fk(float f) { return (f - 32f) * 5f / 9f + 273.15f; }
        public static float GammaLinear(float v) { return Mathf.GammaToLinearSpace(v); }
        public static float LinearGamma(float v) { return Mathf.LinearToGammaSpace(v); }
        public static float Gamma(float v, float absMax, float gamma) { return Mathf.Gamma(v, absMax, gamma); }
        public static Color TemperatureCol(float k) { return Mathf.CorrelatedColorTemperatureToRGB(k); }
        public static float Evaluate(string exp) {
            var dt = new System.Data.DataTable();
            var dc = new System.Data.DataColumn("Eval", typeof(float), exp);
            dt.Columns.Add(dc);
            dt.Rows.Add(0);
            return (float)(dt.Rows[0]["Eval"]);
        }
        public static int Fibonacci(int n) {
            int res = n, n1 = 0, n2 = 1;
            if (n > 1)
                for (int i = 2; i <= n; i++) {
                    res = n1 + n2;
                    n2 = n1;
                    n1 = res;
                }
            return res;
        }
        public static int Gcd(int a, int b) {
            while (a != 0 && b != 0)
                if (a > b)
                    a %= b;
                else
                    b %= a;
            return a == 0 ? b : a;
        }
        public static int Lcm(int a, int b) { return a * b / Gcd(a, b); }
        public static int Factorial(int n) {
            int res = 1;
            if (n > 1)
                for (int i = 2; i <= n; i++)
                    res *= i;
            return res;
        }
        public static float Permutation(int n, int k) { return (float)Factorial(n) / Factorial(n - k); }
        public static float Combination(int n, int k) { return (float)Factorial(n) / (Factorial(k) * Factorial(n - k)); }
        public static Vector2 QuadEq(float a, float b, float c) {
            if (Is0(a))
                return V2.V(Is0(b) ? NaN : -c / b, NaN);
            float d = b * b - 4f * a * c;
            if (d >= 0f)
                return V2.V((-b + Sqrt(d)) / (2f * a), (-b - Sqrt(d)) / (2f * a));
            return V2.V(NaN);
        }
        public static bool IsBet(float f, float a, float b) { return Min(a, b) <= f && f <= Max(a, b); }
        public static bool IsBet(int i, int a, int b) { return Min(a, b) <= i && i <= Max(a, b); }
        public static float Abs(float f) { return Mathf.Abs(f); }
        public static int Abs(int i) { return Mathf.Abs(i); }
        public static float PerlinNoise(float x, float y) { return Mathf.PerlinNoise(x, y); }
        public static float Dis(float a, float b) { return Abs(a - b); }
        public static float Near(float f, float a, float b) { return Abs(f - a) < Abs(f - b) ? a : b; }
        public static float Remap(float f, float a, float b, float a2, float b2) { return C_(RemapUnc(f, a, b, a2, b2), a2, b2); }
        public static float RemapUnc(float f, float a, float b, float a2, float b2) { return (f - a) / (b - a) * (b2 - a2) + a2; }
        public static int RemapI(float f, float a, float b, float a2, float b2) { return (int)Remap(f, a, b, a2, b2); }
        public static int RemapUncI(float f, float a, float b, float a2, float b2) { return (int)RemapUnc(f, a, b, a2, b2); }
        public static int Prime(int n) {
            int res = 2;
            for (int i = 0; i <= n; res++)
                if (IsPrime(res))
                    i++;
            return res;
        }
        public static bool IsPrime(int n) {
            for (int i = 2, m = n / 2; i <= m; i++)
                if (n % i == 0)
                    return false;
            return n >= 2;
        }
        public static bool Apx(float a, float b, float d = 0.001f) { return Abs(a - b) < d; }
        public static bool Is0(float f, float d = 0.001f) { return Apx(f, 0f, d); }
        public static float Pow(float b, float n) { return Mathf.Pow(b, n); }
        public static int Pow(int b, int n) { return Mathf.RoundToInt(Mathf.Pow(b, n)); }
        public static float Pow10(float n) { return Pow(10, n); }
        public static int Pow10(int n) { return Pow(10, n); }
        public static float Pow2(float n) { return Pow(2, n); }
        public static int Pow2(int n) { return Pow(2, n); }
        public static float PowNeg1(float n) { return Pow(-1, n); }
        public static int PowNeg1(int n) { return Pow(-1, n); }
        public static bool IsPow2(float v) { return Mathf.IsPowerOfTwo((int)v); }
        public static int CloPow2(float v) { return Mathf.ClosestPowerOfTwo((int)v); }
        public static int NxtPow2(float v) { return Mathf.NextPowerOfTwo((int)v); }
        public static float Exp(float n) { return Mathf.Exp(n); }
        public static float Sqrt(float x) { return Mathf.Sqrt(x); }
        public static float Cbrt(float x) { return NthRoot(x, 3f); }
        public static float NthRoot(float x, float n) { return x < 0f ? -Pow(-x, 1f / n) : Pow(x, 1f / n); }
        public static float Log(float x, float b) { return Mathf.Log(x, b); }
        public static float Lb(float x) { return Log(x, 2); }
        public static float Ln(float x) { return Mathf.Log(x); }
        public static float Lg(float x) { return Mathf.Log10(x); }
        public static int CIdx(int idx, int n) { return C(idx, 0, n - 1); }
        public static int C(int i, int n) { return C(i, 0, n); }
        public static int C_(int i, int n) { return C_(i, 0, n); }
        public static int C(int i, int a, int b) { return Mathf.Clamp(i, a, b); }
        public static int C_(int i, int a, int b) { return a < b ? C(i, a, b) : C(i, b, a); }
        public static float C(float f, float n) { return C(f, 0f, n); }
        public static float C_(float f, float n) { return C_(f, 0f, n); }
        public static float C(float f, float a, float b) { return Mathf.Clamp(f, a, b); }
        public static float C_(float f, float a, float b) { return a < b ? C(f, a, b) : C(f, b, a); }
        public static float C(float f) { return C(f, -1f, 1f); }
        public static float C01(float f) { return Mathf.Clamp01(f); }
        public static int RepIdx(int i, int n) { return (int)Rep(i, (float)n); }
        public static int Rep(int i, int n) { return RepIdx(i, n + 1); }
        public static int Rep_(int i, int n) { return Rep_(i, 0, n); }
        public static int Rep(int i, int a, int b) { return Rep(i - a, b - a) + a; }
        public static int Rep_(int i, int a, int b) { return (b - a >= 0 ? Rep(i - a, b - a) : -Rep(a - i, a - b)) + a; }
        public static float Rep(float f, float n) { return Mathf.Repeat(f, n); }
        public static float Rep_(float f, float n) { return Rep_(f, 0f, n); }
        public static float Rep(float f, float a, float b) { return Rep(f - a, b - a) + a; }
        public static float Rep_(float f, float a, float b) { return (b - a >= 0f ? Rep(f - a, b - a) : -Rep(a - f, a - b)) + a; }
        public static float Rep(float f) { return Rep(f, -1f, 1f); }
        public static float Rep01(float f) { return Rep(f, 1f); }
        public static int PingPongIdx(int i, int n) { return PingPong(i, n - 1); }
        public static int PingPong(int i, int n) { return (int)PingPong(i, (float)n); }
        public static int PingPong_(int i, int n) { return PingPong_(i, 0, n); }
        public static int PingPong(int i, int a, int b) { return PingPong(i - a, b - a) + a; }
        public static int PingPong_(int i, int a, int b) { return (b - a >= 0 ? PingPong(i - a, b - a) : -PingPong(a - i, b - a)) + a; }
        public static float PingPong(float f, float n) { return Mathf.PingPong(f, n); }
        public static float PingPong_(float f, float n) { return PingPong_(f, 0, n); }
        public static float PingPong(float f, float a, float b) { return PingPong(f - a, b - a) + a; }
        public static float PingPong_(float f, float a, float b) { return (b - a >= 0 ? PingPong(f - a, b - a) : -PingPong(a - f, b - a)) + a; }
        public static float PingPong(float f) { return PingPong(f, -1f, 1f); }
        public static float PingPong01(float f) { return PingPong(f, 1f); }
        public static int RepCIdx(int i, int n, bool isRep) { return isRep ? RepIdx(i, n) : CIdx(i, n); }
        public static int RepC(int i, int n, bool isRep) { return isRep ? Rep(i, n) : C(i, n); }
        public static int RepC_(int i, int n, bool isRep) { return isRep ? Rep_(i, n) : C_(i, n); }
        public static int RepC(int i, int a, int b, bool isRep) { return isRep ? Rep(i, a, b) : C(i, a, b); }
        public static int RepC_(int i, int a, int b, bool isRep) { return isRep ? Rep_(i, a, b) : C_(i, a, b); }
        public static float RepC(float i, float n, bool isRep) { return isRep ? Rep(i, n) : C(i, n); }
        public static float RepC_(float i, float n, bool isRep) { return isRep ? Rep_(i, n) : C_(i, n); }
        public static float RepC(float i, float a, float b, bool isRep) { return isRep ? Rep(i, a, b) : C(i, a, b); }
        public static float RepC_(float i, float a, float b, bool isRep) { return isRep ? Rep_(i, a, b) : C_(i, a, b); }
        public static float RepC(float f, bool isRep) { return isRep ? Rep(f) : C(f); }
        public static float RepC01(float f, bool isRep) { return isRep ? Rep01(f) : C01(f); }
        public static int RepPingPongIdx(int i, int n, bool isRep) { return isRep ? RepIdx(i, n) : PingPongIdx(i, n); }
        public static int RepPingPong(int i, int n, bool isRep) { return isRep ? Rep(i, n) : PingPong(i, n); }
        public static int RepPingPong_(int i, int n, bool isRep) { return isRep ? Rep_(i, n) : PingPong_(i, n); }
        public static int RepPingPong(int i, int a, int b, bool isRep) { return isRep ? Rep(i, a, b) : PingPong(i, a, b); }
        public static int RepPingPong_(int i, int a, int b, bool isRep) { return isRep ? Rep_(i, a, b) : PingPong_(i, a, b); }
        public static float RepPingPong(float i, float n, bool isRep) { return isRep ? Rep(i, n) : PingPong(i, n); }
        public static float RepPingPong_(float i, float n, bool isRep) { return isRep ? Rep_(i, n) : PingPong_(i, n); }
        public static float RepPingPong(float i, float a, float b, bool isRep) { return isRep ? Rep(i, a, b) : PingPong(i, a, b); }
        public static float RepPingPong_(float i, float a, float b, bool isRep) { return isRep ? Rep_(i, a, b) : PingPong_(i, a, b); }
        public static float RepPingPong(float f, bool isRep) { return isRep ? Rep(f) : PingPong(f); }
        public static float RepPingPong01(float f, bool isRep) { return isRep ? Rep01(f) : PingPong01(f); }
        public static int PingPongCIdx(int i, int n, bool isPingPong) { return isPingPong ? PingPongIdx(i, n) : CIdx(i, n); }
        public static int PingPongC(int i, int n, bool isPingPong) { return isPingPong ? PingPong(i, n) : C(i, n); }
        public static int PingPongC_(int i, int n, bool isPingPong) { return isPingPong ? PingPong_(i, n) : C_(i, n); }
        public static int PingPongC(int i, int a, int b, bool isPingPong) { return isPingPong ? PingPong(i, a, b) : C(i, a, b); }
        public static int PingPongC_(int i, int a, int b, bool isPingPong) { return isPingPong ? PingPong_(i, a, b) : C_(i, a, b); }
        public static float PingPongC(float i, float n, bool isPingPong) { return isPingPong ? PingPong(i, n) : C(i, n); }
        public static float PingPongC_(float i, float n, bool isPingPong) { return isPingPong ? PingPong_(i, n) : C_(i, n); }
        public static float PingPongC(float i, float a, float b, bool isPingPong) { return isPingPong ? PingPong(i, a, b) : C(i, a, b); }
        public static float PingPongC_(float i, float a, float b, bool isPingPong) { return isPingPong ? PingPong_(i, a, b) : C_(i, a, b); }
        public static float PingPongC(float f, bool isPingPong) { return isPingPong ? PingPong(f) : C(f); }
        public static float PingPongC01(float f, bool isPingPong) { return isPingPong ? PingPong01(f) : C01(f); }
        public static int RepIdx(int i, int n, RepTp rt) { return rt.IsC() ? CIdx(i, n) : rt.IsRep() ? RepIdx(i, n) : PingPongIdx(i, n); }
        public static int Rep(int i, int n, RepTp rt) { return rt.IsC() ? C(i, n) : rt.IsRep() ? Rep(i, n) : PingPong(i, n); }
        public static int Rep_(int i, int n, RepTp rt) { return rt.IsC() ? C_(i, n) : rt.IsRep() ? Rep_(i, n) : PingPong_(i, n); }
        public static int Rep(int i, int a, int b, RepTp rt) { return rt.IsC() ? C(i, a, b) : rt.IsRep() ? Rep(i, a, b) : PingPong(i, a, b); }
        public static int Rep_(int i, int a, int b, RepTp rt) { return rt.IsC() ? C_(i, a, b) : rt.IsRep() ? Rep_(i, a, b) : PingPong_(i, a, b); }
        public static float Rep(float i, float n, RepTp rt) { return rt.IsC() ? C(i, n) : rt.IsRep() ? Rep(i, n) : PingPong(i, n); }
        public static float Rep_(float i, float n, RepTp rt) { return rt.IsC() ? C_(i, n) : rt.IsRep() ? Rep_(i, n) : PingPong_(i, n); }
        public static float Rep(float i, float a, float b, RepTp rt) { return rt.IsC() ? C(i, a, b) : rt.IsRep() ? Rep(i, a, b) : PingPong(i, a, b); }
        public static float Rep_(float i, float a, float b, RepTp rt) { return rt.IsC() ? C_(i, a, b) : rt.IsRep() ? Rep_(i, a, b) : PingPong_(i, a, b); }
        public static float Rep(float f, RepTp rt) { return rt.IsC() ? C(f) : rt.IsRep() ? Rep(f) : PingPong(f); }
        public static float Rep01(float f, RepTp rt) { return rt.IsC() ? C01(f) : rt.IsRep() ? Rep01(f) : PingPong01(f); }
        public static float Round(float x) { return Mathf.Round(x); }
        public static int RoundI(float i) { return Mathf.RoundToInt(i); }
        public static float RoundBase(float x, float b, float c = 0) { return Round((x - c) / b) * b + c; }
        public static float Floor(float x) { return Mathf.Floor(x); }
        public static int FloorI(float i) { return Mathf.FloorToInt(i); }
        public static float FloorBase(float x, float b, float c = 0) { return Floor((x - c) / b) * b + c; }
        public static float Ceil(float x) { return Mathf.Ceil(x); }
        public static int CeilI(float i) { return Mathf.CeilToInt(i); }
        public static float CeilBase(float x, float b, float c = 0) { return Ceil((x - c) / b) * b + c; }
        public static float Round0(float x) { return x >= 0f ? Floor(x) : Ceil(x); }
        public static int Round0I(float x) { return x >= 0f ? FloorI(x) : CeilI(x); }
        public static float Round0Base(float x, float b, float c = 0) { return x >= c ? FloorBase(x, b, c) : CeilBase(x, b, c); }
        public static float RoundN(float x) { return x >= 0f ? Ceil(x) : Floor(x); }
        public static int RoundNI(float x) { return x >= 0f ? CeilI(x) : FloorI(x); }
        public static float RoundNBase(float x, float b, float c = 0) { return x >= c ? CeilBase(x, b, c) : FloorBase(x, b, c); }
        public static float RoundDir(float x, bool isCeil) { return isCeil ? Ceil(x) : Floor(x); }
        public static int RoundDirI(float x, bool isCeil) { return isCeil ? CeilI(x) : FloorI(x); }
        public static float RoundDirBase(float x, float b, bool isCeil, float c = 0) { return isCeil ? CeilBase(x, b, c) : FloorBase(x, b, c); }
        public static float Round(float x, RoundTp rt, bool isCeil = false) { return rt.IsRound() ? Round(x) : rt.IsFloor() ? Floor(x) : rt.IsCeil() ? Ceil(x) : rt.IsO() ? Round0(x) : rt.IsN() ? RoundN(x) : RoundDir(x, isCeil); }
        public static int RoundI(float x, RoundTp rt, bool isCeil = false) { return rt.IsRound() ? RoundI(x) : rt.IsFloor() ? FloorI(x) : rt.IsCeil() ? CeilI(x) : rt.IsO() ? Round0I(x) : rt.IsN() ? RoundNI(x) : RoundDirI(x, isCeil); }
        public static float RoundBase(float x, float b, RoundTp rt, bool isCeil = false, float c = 0) { return rt.IsRound() ? RoundBase(x, b, c) : rt.IsFloor() ? FloorBase(x, b, c) : rt.IsCeil() ? CeilBase(x, b, c) : rt.IsO() ? Round0Base(x, b, c) : rt.IsN() ? RoundNBase(x, b, c) : RoundDirBase(x, b, isCeil, c); }
        public static int Min(int a, int b) { return Mathf.Min(a, b); }
        public static float Min(float a, float b) { return Mathf.Min(a, b); }
        public static int Min(params int[] arr) { return Mathf.Min(arr); }
        public static float Min(params float[] arr) { return Mathf.Min(arr); }
        public static int Max(int a, int b) { return Mathf.Max(a, b); }
        public static float Max(float a, float b) { return Mathf.Max(a, b); }
        public static int Max(params int[] arr) { return Mathf.Max(arr); }
        public static float Max(params float[] arr) { return Mathf.Max(arr); }
        public static Vector2 Lerps(int type, float t, float t2) {
            return type == 0 ? V3.Lerp(t, V2.O, V2.Y(t2), V2.V(1, 1 - t2), V2.I) :
                type == 1 ? V3.Lerp(t, V2.O, V2.V(1, 1 - t2), V2.Y(t2), V2.I) :
                type == 2 ? V3.Lerp(t, V2.O, V2.Y(t2), V2.V(1, t2), V2.I) :
                type == 3 ? V3.Lerp(t, V2.O, V2.V(t2, 1 - t2), V2.I) :
                type == 4 ? V3.Lerp(t, V2.O, V2.V(1 - t2, t2), V2.V(t2, 1 - t2), V2.I) :
                type == 5 ? V3.Lerp(t, V2.O, V2.V(0.5f, t2 * 2), V2.r) :
                type == 6 ? V3.Lerp(t, V2.u, V2.V(0.5f, 1 - t2 * 2), V2.I) :
                type == 7 ? V3.Lerp(t, V2.Y(0.5f), V2.V(0.5f, t2 * 2 - 0.5f), V2.V(1, 0.5f)) :
                type == 8 ? V3.Lerp(t, V2.O, V2.V(0.25f, t2 * 3), V2.V(0.75f, 1 - t2 * 3), V2.I) :
                    V3.Lerp(t, V2.Y(0.5f), V2.V(0.5f, (t2 - 0.5f) * 3.45f + 0.5f), V2.V(1f / 2, (0.5f - t2) * 3.45f + 0.5f), V2.V(1, 0.5f));
        }
        public static float Lerp(float t, params float[] arr) { return Gen.F2((a, b) => Lerp(a, b, t), arr); }
        public static float Lerp(float a, float b, float t) { return t < 0f ? a : t > 1f ? b : a + (b - a) * t; }
        public static float LerpUnc(float a, float b, float t) { return a + (b - a) * t; }
        public static float LerpInv(float a, float b, float t) { return a < b ? (t < a ? 0f : t > b ? 1f : (t - a) / (b - a)) : (t < b ? 1f : t > a ? 0f : (t - a) / (b - a)); }
        public static float LerpInvUnc(float a, float b, float t) { return (t - a) / (b - a); }
        public static float Smt(float a, float b, float t) { return t < 0f ? a : t > 1f ? b : a + (b - a) * (t * t * (3f - 2f * t)); } // _/‾
        public static float SmtUnc(float a, float b, float t) { return a + (b - a) * (t * t * (3f - 2f * t)); } // _/‾
        public static float LerpSuper(float a, float b, float a2, float b2, float t) { return a + (b - a) * (((a2 < b2 ? (t < a2 ? a2 : t > b2 ? b2 : t) : (t < b2 ? b2 : t > a2 ? a2 : t)) - a2) / (b2 - a2)); }
        public static float LerpSuperUnc(float a, float b, float a2, float b2, float t) { return a + (b - a) * ((t - a2) / (b2 - a2)); }
        public static float Sinerp(float a, float b, float t) { return a + (b - a) * Sin(t * 90f); } // /‾
        public static float Coserp(float a, float b, float t) { return a + (b - a) * (1f - Cos(t * 90f)); } // _/
        public static float LerpSin(float a, float b, float t) { return a + (b - a) * Sin(t * 180f); } // /‾\
        public static float LerpCos(float a, float b, float t) { return a + (b - a) * (1f - Cos(t * 180f)); } // \_/
        public static float Berp(float a, float b, float t) { return a + (b - a) * (Sin(t * 180f * (0.2f + 2.5f * t * t * t)) * Pow(1f - t, 2.2f) + t) * (1f + (1.2f * (1f - t))); } // /~
        public static float Bounce(float a, float b, float t) { return a + (b - a) * Abs(Sin(360f * (t + 1f) * (t + 1f)) * (1f - t)); } // /\^ˆ
        public static float SmtDamp(float a, float b, ref float curVel, float smtTm) { return Mathf.SmoothDamp(a, b, ref curVel, smtTm); }
        public static float SmtDamp(float a, float b, ref float curVel, float smtTm, float maxSpd) { return Mathf.SmoothDamp(a, b, ref curVel, smtTm, maxSpd); }
        public static float SmtDamp(float a, float b, ref float curVel, float smtTm, float maxSpd, float dt) { return Mathf.SmoothDamp(a, b, ref curVel, smtTm, maxSpd, dt); }
        public static float Move(float a, float b, float d) { return Mathf.MoveTowards(a, b, d); }
        public static float MoveUnc(float a, float b, float d) { return a < b ? a + d : a - d; }
        public static float Sin(float f) { return RadSin(f * DegRad); }
        public static float Cos(float f) { return RadCos(f * DegRad); }
        public static float Tan(float f) { return RadTan(f * DegRad); }
        public static float Csc(float f) { return 1f / Sin(f); }
        public static float Sec(float f) { return 1f / Cos(f); }
        public static float Cot(float f) { return 1f / Tan(f); }
        public static float Asin(float f) { return RadAsin(f) * RadDeg; }
        public static float Acos(float f) { return RadAcos(f) * RadDeg; }
        public static float Atan(float f) { return RadAtan(f) * RadDeg; }
        public static float Acsc(float f) { return Asin(1f / f); }
        public static float Asec(float f) { return Acos(1f / f); }
        public static float Acot(float f) { return Atan(1f / f); }
        public static float Atan2(float y, float x) { return RadAtan2(y, x) * RadDeg; }
        public static float RadSin(float f) { return Mathf.Sin(f); }
        public static float RadCos(float f) { return Mathf.Cos(f); }
        public static float RadTan(float f) { return Mathf.Tan(f); }
        public static float RadCsc(float f) { return 1f / RadSin(f); }
        public static float RadSec(float f) { return 1f / RadCos(f); }
        public static float RadCot(float f) { return 1f / RadTan(f); }
        public static float RadAsin(float f) { return Mathf.Asin(f); }
        public static float RadAcos(float f) { return Mathf.Acos(f); }
        public static float RadAtan(float f) { return Mathf.Atan(f); }
        public static float RadAcsc(float f) { return RadAsin(1f / f); }
        public static float RadAsec(float f) { return RadAcos(1f / f); }
        public static float RadAcot(float f) { return RadAtan(1f / f); }
        public static float RadAtan2(float y, float x) { return Mathf.Atan2(y, x); }
        public static float T_bh_S(float b, float h) { return h * b / 2f; }
        public static float T_abc_S(float a, float b, float c) { return Sqrt((a + b - c) * (a - b + c) * (-a + b + c) * (a + b + c)) / 4f; }
        public static float T_abC_S(float a, float b, float C) { return a * b * Sin(C) / 2f; }
        public static float Tr_ab_c(float a, float b) { return Sqrt(a * a + b * b); }
        public static float Tr_bc_a(float b, float c) { return Sqrt(c * c - b * b); }
        public static float T_abc_A(float a, float b, float c) { return Acos((b * b + c * c - a * a) / (2f * b * c)); }
        public static float T_abC_c(float a, float b, float C) { return Sqrt(a * a + b * b - 2f * a * b * Cos(C)); }
        public static float T_abA_c(float a, float b, float A) { return QuadEq(1f, -2f * b * Cos(A), b * b - a * a).x; }
        public static float T_aBC_b(float a, float B, float C) { return (a * Sin(B)) / Sin(180f - B - C); }
        public static float Ti_bB_a(float b, float B) { return Sqrt((b * b) / (1f - 2f * Cos(B))); }
        public static float Ap(float a, float d, int n) { return a + (n - 1) * d; } // Arithmetic Progression
        public static int Ap(int a, int d, int n) { return a + (n - 1) * d; }
        public static float ApSum(float a1, float an, int n) { return n * (a1 + an) / 2f; }
        public static float Gp(float a1, float r, int n) { return a1 * Pow(r, n - 1); } // Geometric Progression
        public static int Gp(int a1, int r, int n) { return a1 * Pow(r, n - 1); }
        public static float GpSum(float a1, float r, int n) { return a1 * (1 - Pow(r, n)) / (1 - r); }
        public static float Agp(float a, float d, float r, int n) { return Gp(Ap(a, d, n), r, n); } // Arithmetic-Geometric Progression
        public static float AgpSum(float a, float d, float r, int n) { return (a - Ap(a, d, n) * Pow(r, n)) / (1 - r) + d * r * (1 - Pow(r, n - 1)) / Pow(1 - r, 2); }
        public static float Hp(float a, float d, int n) { return 1f / Ap(a, d, n); } // Harmonic Progression
    }
    public static class Gen {
        public static T Op<T>(T a, T b, OpTp ot) {
            ParameterExpression pa = Expression.Parameter(typeof(T), "a"), pb = Expression.Parameter(typeof(T), "b");
            BinaryExpression body = ot == OpTp.Add ? Expression.Add(pa, pb) : ot == OpTp.Sub ? Expression.Subtract(pa, pb) : ot == OpTp.Mul ? Expression.Multiply(pa, pb) : Expression.Divide(pa, pb);
            System.Func<T, T, T> op = Expression.Lambda<System.Func<T, T, T>>(body, pa, pb).Compile();
            return op(a, b);
        }
        public static float F1<T>(System.Func<T, T, float> f, params T[] arr) {
            if (arr.IsNe() || arr.Length == 1)
                return 0;
            float res = 0;
            for (int i = 1; i < arr.Length; i++)
                res += f(arr[i - 1], arr[i]);
            return res;
        }
        public static V F1<T, V>(System.Func<T, T, V> f, OpTp op, params T[] arr) {
            if (arr.IsNe() || arr.Length == 1)
                return default;
            V res = default;
            for (int i = 1; i < arr.Length; i++)
                res = Op(res, f(arr[i - 1], arr[i]), op);
            return res;
        }
        public static T F1<T>(System.Func<T, T, T> f, params T[] arr) {
            if (arr.IsNe())
                return default;
            if (arr.Length == 1)
                return arr[0];
            T res = arr[0];
            for (int i = 1; i < arr.Length; i++)
                res = f(res, arr[i]);
            return res;
        }
        public static T F2<T>(System.Func<T, T, T> f, params T[] arr) {
            if (arr.IsNe())
                return default;
            if (arr.Length == 1)
                return arr[0];
            List<T> lis = new List<T>(arr);
            for (int i = lis.Count - 1; i > 0; i--)
                for (int j = 0; j < i; j++)
                    lis[j] = f(lis[j], lis[j + 1]);
            return lis[0];
        }
    }
    public static class Dbg {
        public static void Road(Tf tf, List<Vector3> pnts, List<Vector2> data, bool isLoop, RoadCrtDirTp tp, Color c, float dur = 0) {
            List<List<Vector3>> pnts2 = Orgil.Crv.RoadPnts(tf, pnts, data, isLoop, tp);
            for (int i = 0, n1 = pnts.Count + (isLoop ? 0 : -1); i < pnts.Count; i++)
                for (int j = 0; j < data.Count; j++) {
                    if (i < n1)
                        Line(pnts2[i][j], pnts2[(i + 1) % pnts.Count][j], c, dur);
                    Line(pnts2[i][j], pnts2[i][(j + 1) % data.Count], c, dur);
                }
        }
        public static void Crv(Tf tf, List<Vector3> pnts, Color c, float dur = 0) {
            for (int i = 1; i < pnts.Count; i++)
                Line(tf.Pnt(pnts[i - 1]), tf.Pnt(pnts[i]), c, dur);
        }
        public static void Sphere(Tf tf, int seg, int ring, Color c, float dur = 0) { Rm(tf, Orgil.Crv.AngPnts(V3.O, ring, 0.5f, -90, 180, true, true), seg, c, dur); }
        public static void Capsule(Tf tf, int seg, int ring, float r, float h, Color c, float dur = 0) { Rm(tf, Orgil.Crv.AngPnts(V2.Y(r - h / 2), ring, r, -90, 90, true, true).Add(Orgil.Crv.AngPnts(V2.Y(h / 2 - r), ring, r, 0, 90, true, true)), seg, c, dur); }
        public static void Cylinder(Tf tf, int seg, float rd, float ru, float h, Color c, float dur = 0) { Rm(tf, new List<Vector2>() { V2.Y(-h / 2), V2.V(rd, -h / 2), V2.V(ru, h / 2), V2.Y(h / 2) }, seg, c, dur); }
        public static void Cube(Tf tf, Vector3 sz, Color c, float dur = 0) { Rm(tf * Tf.RS(45, sz), new List<Vector2>() { V2.V(M.Sqrt2 / 2, -0.5f), V2.V(M.Sqrt2 / 2, 0.5f) }, 4, c, dur); }
        public static void Rm(Tf tf, List<Vector2> pnts, int seg, Color c, float dur = 0) {
            List<List<Vector3>> pnts2 = Orgil.Crv.RmPnts(tf, pnts, seg);
            for (int i = 0; i < pnts.Count; i++)
                for (int j = 0; j < seg; j++) {
                    Line(pnts2[i][j], pnts2[i][(j + 1) % seg], c, dur);
                    if (i < pnts.Count - 1)
                        Line(pnts2[i][j], pnts2[(i + 1) % pnts.Count][j], c, dur);
                }
        }
        public static void Line(Vector3 a, Vector3 b) { Debug.DrawLine(a, b); }
        public static void Line(Vector3 a, Vector3 b, Color c) { Debug.DrawLine(a, b, c); }
        public static void Line(Vector3 a, Vector3 b, Color c, float dur) { Debug.DrawLine(a, b, c, dur); }
        public static void Line(Vector3 a, Vector3 b, Color c, float dur, bool depthTest) { Debug.DrawLine(a, b, c, dur, depthTest); }
        public static void Ray(Vector3 a, Vector3 dir) { Debug.DrawRay(a, dir); }
        public static void Ray(Vector3 a, Vector3 dir, Color c) { Debug.DrawRay(a, dir, c); }
        public static void Ray(Vector3 a, Vector3 dir, Color c, float dur) { Debug.DrawRay(a, dir, c, dur); }
        public static void Ray(Vector3 a, Vector3 dir, Color c, float dur, bool depthTest) { Debug.DrawRay(a, dir, c, dur, depthTest); }
        public static void Brk() { Debug.Break(); }
        public static void DbgBrk() { Debug.DebugBreak(); }
        public static void Log(object msg) { Debug.Log(msg); }
        public static void Log(object msg, Object context) { Debug.Log(msg, context); }
        public static void LogFmt(string fmt, params object[] args) { Debug.LogFormat(fmt, args); }
        public static void LogFmt(Object context, string fmt, params object[] args) { Debug.LogFormat(context, fmt, args); }
        public static void LogFmt(LogType tp, LogOption option, Object context, string fmt, params object[] args) { Debug.LogFormat(tp, option, context, fmt, args); }
        public static void LogErr(object msg) { Debug.LogError(msg); }
        public static void LogErr(object msg, Object context) { Debug.LogError(msg, context); }
        public static void LogErrFmt(string fmt, params object[] args) { Debug.LogErrorFormat(fmt, args); }
        public static void LogErrFmt(Object context, string fmt, params object[] args) { Debug.LogErrorFormat(context, fmt, args); }
        public static void LogExc(System.Exception e) { Debug.LogException(e); }
        public static void LogExc(System.Exception e, Object context) { Debug.LogException(e, context); }
        public static void LogWar(object msg) { Debug.LogWarning(msg); }
        public static void LogWar(object msg, Object context) { Debug.LogWarning(msg, context); }
        public static void LogWarFmt(string fmt, params object[] args) { Debug.LogWarningFormat(fmt, args); }
        public static void LogWarFmt(Object context, string fmt, params object[] args) { Debug.LogWarningFormat(context, fmt, args); }
        public static void Ass(bool cond) { Debug.Assert(cond); }
        public static void Ass(bool cond, Object context) { Debug.Assert(cond, context); }
        public static void Ass(bool cond, object msg) { Debug.Assert(cond, msg); }
        public static void Ass(bool cond, object msg, Object context) { Debug.Assert(cond, msg, context); }
        public static void AssFmt(bool cond, string fmt, params object[] args) { Debug.AssertFormat(cond, fmt, args); }
        public static void AssFmt(bool cond, Object context, string fmt, params object[] args) { Debug.AssertFormat(cond, context, fmt, args); }
        public static void LogAss(object msg) { Debug.LogAssertion(msg); }
        public static void LogAss(object msg, Object context) { Debug.LogAssertion(msg, context); }
        public static void LogAssFmt(string fmt, params object[] args) { Debug.LogAssertionFormat(fmt, args); }
    }
    public static class Crv {
        public static List<Vector2> StarPnts(int seg, float r, float r2) {
            List<Vector2> pnts = new List<Vector2>();
            for (int j = 0, m = seg * 2; j < m; j++)
                pnts.Add(AngV2(360f * j / m, j.IsEven() ? r : r2));
            return pnts;
        }
        public static List<Vector2> AngPnts(Vector2 pos, int seg, float r, float staAng, float ang, bool isStaIn, bool isEndIn) {
            List<Vector2> pnts = new List<Vector2>();
            for (int i = isStaIn ? 0 : 1, n = isEndIn ? seg + 1 : seg; i < n; i++)
                pnts.Add(pos + AngV2(staAng + ang * i / seg, r));
            return pnts;
        }
        public static Vector3 AngRr(float angXz, float angY, float r, float r2, float y) {
            float r1 = r + M.Cos(angY) * r2;
            return V3.V(M.Cos(angXz) * r1, y + M.Sin(angY) * r2, M.Sin(angXz) * r1);
        }
        public static Vector3 AngV3(float ang, float r, float y) { return V3.V(M.Cos(ang) * r, y, M.Sin(ang) * r); }
        public static Vector2 AngV2(float ang, float r) { return V2.V(M.Cos(ang) * r, M.Sin(ang) * r); }
        public static List<PntData> PntDatas(List<Vector3> pnts, List<Vector2> rs, List<int> ns, string fc, UvTp ut, Vector3 staP, Vector3 endP, bool isLoop, List<Vector2> rs2 = null) {
            A.Fill(ref rs, V2.O, pnts.Count);
            A.Fill(ref rs2, V2.O, pnts.Count);
            A.Fill(ref ns, 0, pnts.Count);
            fc = fc.Fill('0', pnts.Count);
            if (isLoop) {
                staP = pnts.Last();
                endP = pnts[0];
            }
            List<PntData> datas = new List<PntData>();
            for (int i = 0, j, y = 0; i < pnts.Count; i++, y++) {
                if (fc[i] == '0') {
                    if (ns[i] > 0) {
                        Vector3 p1 = V3.Move(pnts[i], i == 0 ? staP : pnts[i - 1], rs[i].x), p2 = V3.Move(pnts[i], i == pnts.Count - 1 ? endP : pnts[i + 1], rs[i].y);
                        for (j = 0, y++; j <= ns[i]; j++)
                            datas.Add(new PntData(V3.Lerp(j.F() / ns[i], p1, pnts[i], p2), true, y - 1 + j.F() / ns[i], rs2[i]));
                    } else
                        datas.Add(new PntData(pnts[i], true, y, rs2[i]));
                } else {
                    if (ns[i] > 0) {
                        Vector3 p1 = V3.Move(pnts[i], i == 0 ? staP : pnts[i - 1], rs[i].x), p2 = V3.Move(pnts[i], i == pnts.Count - 1 ? endP : pnts[i + 1], rs[i].y);
                        if (fc[i] == '1' || fc[i] == '2')
                            datas.Add(new PntData(p1, false, y, rs2[i]));
                        datas.Add(new PntData(p1, true, y, rs2[i]));
                        for (j = 1, y++; j < ns[i]; j++)
                            datas.Add(new PntData(V3.Lerp(j.F() / ns[i], p1, pnts[i], p2), true, y - 1 + j.F() / ns[i], rs2[i]));
                        if (fc[i] == '1' || fc[i] == '3')
                            datas.Add(new PntData(p2, false, y, rs2[i]));
                        datas.Add(new PntData(p2, true, y, rs2[i]));
                    } else
                        datas.Add(new PntData(pnts[i], false, y, rs2[i]), new PntData(pnts[i], true, y, rs2[i]));
                }
            }
            if (isLoop)
                datas.Add(new PntData(datas[0].p, false, datas.Last().uv + 1, rs2[0]));
            else if (!datas.Last().isFc)
                datas.RemoveAt(datas.Count - 1);
            if (!datas[0].isFc)
                datas.RemoveAt(0);
            if (ut == UvTp.Cor) {
                float maxY = datas.Last().uv;
                for (int i = 0; i < datas.Count; i++)
                    datas[i].uv /= maxY;
            } else if (ut == UvTp.Dis) {
                float dis = 0;
                for (int i = 0; i < datas.Count; i++) {
                    datas[i].uv = dis;
                    if (i < datas.Count - 1)
                        dis += V3.Dis(datas[i].p, datas[i + 1].p);
                }
                datas.ForEach(x => x.uv /= dis);
            } else {
                float fcCnt = datas.FindAll(x => x.isFc).Count - isLoop.I2();
                for (int i = 0, fcI = 0; i < datas.Count; i++) {
                    datas[i].uv = fcI / fcCnt;
                    if (datas[i].isFc)
                        fcI++;
                }
            }
            return datas;
        }
        public static Vector3 SpiralPnt(float f, Vector2 v, float l, float r1, float r2, float h, bool isRh, bool isRound = true) { return AngV3(-isRh.Sign() * 360f * l * f, r1 + (r2 - r1) * f + v.x * (isRound ? -isRh.Sign() : 1), v.y + h * f); }
        public static List<List<Vector3>> SpiralPnts(Tf tf, List<Vector2> data, int seg, float l, float r1, float r2, float h, bool isRh, bool isRound = true) {
            List<List<Vector3>> pnts2 = new List<List<Vector3>>();
            for (int i = 0; i <= seg; i++) {
                List<Vector3> pntLis = new List<Vector3>();
                for (int j = 0; j < data.Count; j++)
                    pntLis.Add(tf.Pnt(SpiralPnt(i.F() / seg, data[j], l, r1, r2, h, isRh, isRound)));
                pnts2.Add(pntLis);
            }
            return pnts2;
        }
        public static List<List<Vector3>> RmPnts(Tf tf, List<Vector2> pnts, int seg) {
            List<List<Vector3>> pnts2 = new List<List<Vector3>>();
            for (int i = 0; i < pnts.Count; i++) {
                List<Vector3> pntLis = new List<Vector3>();
                for (int j = 0; j < seg; j++)
                    pntLis.Add(tf.Pnt(AngV3(360f * j / seg, pnts[i].x, pnts[i].y)));
                pnts2.Add(pntLis);
            }
            return pnts2;
        }
        public static List<List<Vector3>> RoadPnts(Tf tf, List<Vector3> pnts, List<Vector2> data, bool isLoop, RoadCrtDirTp tp) {
            List<List<Vector3>> pnts2 = new List<List<Vector3>>();
            Vector3 d = V3.O, dir = V3.O;
            bool isNxt = tp == RoadCrtDirTp.Nxt || tp == RoadCrtDirTp.NxtAvg, isAvg = tp == RoadCrtDirTp.NxtAvg || tp == RoadCrtDirTp.PrvNxtAvg;
            for (int i = 0, n = pnts.Count, m = data.Count; i < n; i++) {
                dir = (!isLoop && i == n - 1) ? pnts[i] - pnts[i - 1] : (isLoop && isNxt && i == n - 1) ? pnts[0] - pnts[i] : (isLoop && !isNxt || !isLoop && !isNxt && i != 0 && i != n - 1) ? (pnts.RepIdx(i + 1) - pnts.RepIdx(i - 1)) / 2 : pnts[i + 1] - pnts[i];
                d = ((isAvg ? d : V3.O) + V3.Xz(dir.z, -dir.x)).Nor();
                List<Vector3> pntLis = new List<Vector3>();
                for (int j = 0; j < m; j++)
                    pntLis.Add(tf.Pnt(pnts[i] + d * data[j].x + V3.Y(data[j].y)));
                pnts2.Add(pntLis);
            }
            return pnts2;
        }
        public static List<Vector3> PntsSameDis(List<Vector3> pnts, float dis) {
            List<Vector3> res = new List<Vector3>();
            if (pnts.IsCount()) {
                Vector3 p = pnts[0];
                res.Add(p);
                for (int i = 1; i < pnts.Count; i++)
                    while (V3.Dis(p, pnts[i]) > dis)
                        res.Add(p = V3.Move(p, pnts[i], dis));
                res.Add(pnts.Last());
            }
            return res;
        }
        // Bezier Curve
        public static List<Vector3> BzrCrv(List<Vector3> pnts, int n) {
            List<Vector3> res = new List<Vector3>();
            for (int i = 0; i <= n; i++)
                res.Add(V3.Lerp(i.F() / n, pnts));
            return res;
        }
        public static List<Vector3> BzrCrv(List<Vector3> pnts, int n, float spc) { return PntsSameDis(BzrCrv(pnts, n), spc); }
        // Catmull Rom Spline
        public static List<Vector3> Crs(List<Vector3> pnts, int smt) {
            List<Vector3> res = new List<Vector3>();
            for (int i = 0, n = pnts.Count - 3; i < n; i++)
                res.Add(Crs(pnts[i], pnts[i + 1], pnts[i + 2], pnts[i + 3], smt, i == 0, true));
            return res;
        }
        public static List<Vector3> Crs(List<Vector3> pnts, int smt, CrsTp tp = CrsTp.None, Vector3 sta = default, Vector3 end = default) { return tp == CrsTp.None ? Crs(pnts, smt) : tp == CrsTp.StaEnd ? Crs(new List<Vector3> { pnts[0] * 2 - pnts[1] }.Add(pnts).Add1(pnts.Last() * 2 - pnts.Last(1)), smt) : tp == CrsTp.StaEndAdd ? Crs(new List<Vector3> { sta }.Add(pnts).Add1(end), smt) : Crs(new List<Vector3> { pnts.Last() }.Add(pnts).Add(pnts[0], pnts[1]), smt); }
        public static List<Vector3> Crs(List<Vector3> pnts, int smt, float spc, CrsTp tp = CrsTp.None, Vector3 sta = default, Vector3 end = default) { return PntsSameDis(Crs(pnts, smt, tp, sta, end), spc); }
        public static List<Vector3> Crs(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, int smt, bool isStaIn = true, bool isEndIn = true) {
            List<Vector3> res = new List<Vector3>();
            if (isStaIn)
                res.Add(p1);
            for (int i = 1; i < smt; i++)
                res.Add(CrPos(p0, p1, p2, p3, (float)i / smt));
            if (isEndIn)
                res.Add(p2);
            return res;
        }
        public static Vector3 CrPos(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float t) { return 0.5f * ((2f * p1) + (p2 - p0) * t + (2f * p0 - 5f * p1 + 4f * p2 - p3) * t * t + (-p0 + 3f * p1 - 3f * p2 + p3) * t * t * t); }
    }
    public static class Crt {
        public static void Effect(Vector3 pos, Color col) {
            GameObject effectGo = Mb.Ins(A.LoadGo("Main/Effect"), pos, Q.O, Gc.I.transform);
            effectGo.RenMatCol(col);
            Mb.Dst(effectGo, 1);
        }
        public static GameObject Pri(PrimitiveType type, Tf tf, Color c = default, Transform parTf = null) {
            GameObject go = GameObject.CreatePrimitive(type);
            go.Tp(tf.p);
            go.Te(tf.r);
            go.Tls(tf.s);
            go.RenMatCol(c.IsDef() ? C.I : c);
            if (!tf.n.IsNe())
                go.name = tf.n;
            go.Par(parTf);
            return go;
        }
        public static GameObject NewGo(Tf tf, Transform parTf = null) {
            GameObject go = new GameObject(tf.n.IsNe() ? "GameObject" : tf.n);
            go.Tp(tf.p);
            go.Te(tf.r);
            go.Tls(tf.s);
            go.Par(parTf);
            return go;
        }
        public static Transform NewTf(Tf tf, Transform parTf = null) { return NewGo(tf, parTf).transform; }
        public static GameObject PfGo(GameObject pf, Tf tf, Transform parTf = null) {
            GameObject go = Mb.Ins(pf, tf.p, tf.q, parTf);
            go.Tls(tf.s);
            if (!tf.n.IsNe())
                go.name = tf.n;
            return go;
        }
        public static Transform PfTf(GameObject pf, Tf tf, Transform parTf = null) { return PfGo(pf, tf, parTf).transform; }
        public static T Pf<T>(GameObject pf, Tf tf, Transform parTf = null) { return PfGo(pf, tf, parTf).Gc<T>(); }
        public static GameObject PfChildGo(GameObject pf, Tf tf, string childs = "", Transform parTf = null) { return PfGo(pf, tf, parTf).ChildGo(childs); }
        public static Transform PfChildTf(GameObject pf, Tf tf, string childs = "", Transform parTf = null) { return PfGo(pf, tf, parTf).Child(childs); }
        public static T PfChild<T>(GameObject pf, Tf tf, string childs = "", Transform parTf = null) { return PfGo(pf, tf, parTf).Child<T>(childs); }
        public static GameObject RndGo(List<GameObject> pfs, Tf a, Tf b, Transform parTf = null) { return PfGo(pfs.Rnd(), Tf.NPRS(a.n, Rnd.Pos(a.p, b.p), Rnd.Pos(a.r, b.r), Rnd.Pos(a.s, b.s)), parTf); }
        public static List<GameObject> RndGos(List<GameObject> pfs, Tf a, Tf b, int n, float dis, Transform parTf = null) {
            List<GameObject> res = new List<GameObject>();
            List<Vector3> ps = new List<Vector3>();
            for (int i = 0; i < 100000 && ps.Count < n; i++) {
                Vector3 p = Rnd.Pos(a.p, b.p);
                if (ps.FindIndex(0, ps.Count, x => V3.Dis(x, p) < dis) < 0) {
                    ps.Add(p);
                    res.Add(PfGo(pfs.Rnd(), Tf.NPRS(a.n, p, Rnd.Pos(a.r, b.r), Rnd.Pos(a.s, b.s)), parTf));
                }
            }
            return res;
        }
    }
    public static class Wf {
        public static WaitForEndOfFrame eof = new WaitForEndOfFrame();
        public static WaitForFixedUpdate fu = new WaitForFixedUpdate();
        public static WaitForSeconds s001 = S(0.01f), s005 = S(0.05f), s01 = S(0.1f), s02 = S(0.2f), s025 = S(0.25f), s03 = S(0.3f), s04 = S(0.4f), s05 = S(0.5f), s1 = S(1f), f1 = F(1f);
        public static WaitForSeconds S(float s) { return new WaitForSeconds(s); }
        public static WaitForSeconds F(float f, float fps = 60) { return new WaitForSeconds(f / fps); }
        public static WaitForSecondsRealtime Sr(float s) { return new WaitForSecondsRealtime(s); }
    }
    public static class Rgx {
        public const string I = "[-+]?[0-9]+", F = "[-+]?[0-9]*\\.?[0-9]+", Fe = "[-+]?[0-9]*\\.?[0-9]+([eE][-+]?[0-9]+)?", Hex = "[0-9A-Fa-f]+", HexCol = "#?(([0-9A-Fa-f]{6})|([0-9A-Fa-f]{8}))";
        public static bool IsMatch(string input, string pattern) { return Regex.IsMatch(input, pattern); }
        public static string Rpl(string input, string pattern, string replacement) { return Regex.Replace(input, pattern, replacement); }
        public static string Rmv(string input, string pattern) { return Regex.Replace(input, pattern, ""); }
        public static string[] Split(string input, string pattern) { return Regex.Split(input, pattern); }
    }
    public static class Ang {
        public static readonly Vector3 O = V3.O, z90 = V3.V(0, 0, 90), z180 = V3.V(0, 0, 180), z270 = V3.V(0, 0, 270), y90 = V3.V(0, 90, 0), y90z90 = V3.V(0, 90, 90), y90z180 = V3.V(0, 90, 180), y90z270 = V3.V(0, 90, 270), y180 = V3.V(0, 180, 0), y180z90 = V3.V(0, 180, 90), y180z180 = V3.V(0, 180, 180), y180z270 = V3.V(0, 180, 270), y270 = V3.V(0, 270, 0), y270z90 = V3.V(0, 270, 90), y270z180 = V3.V(0, 270, 180), y270z270 = V3.V(0, 270, 270), x90 = V3.V(90, 0, 0), x90z90 = V3.V(90, 0, 90), x90z180 = V3.V(90, 0, 180), x90z270 = V3.V(90, 0, 270), x90y90 = V3.V(90, 90, 0), x90y90z90 = V3.V(90, 90, 90), x90y90z180 = V3.V(90, 90, 180), x90y90z270 = V3.V(90, 90, 270), x90y180 = V3.V(90, 180, 0), x90y180z90 = V3.V(90, 180, 90), x90y180z180 = V3.V(90, 180, 180), x90y180z270 = V3.V(90, 180, 270), x90y270 = V3.V(90, 270, 0), x90y270z90 = V3.V(90, 270, 90), x90y270z180 = V3.V(90, 270, 180), x90y270z270 = V3.V(90, 270, 270), x180 = V3.V(180, 0, 0), x180z90 = V3.V(180, 0, 90), x180z180 = V3.V(180, 0, 180), x180z270 = V3.V(180, 0, 270), x180y90 = V3.V(180, 90, 0), x180y90z90 = V3.V(180, 90, 90), x180y90z180 = V3.V(180, 90, 180), x180y90z270 = V3.V(180, 90, 270), x180y180 = V3.V(180, 180, 0), x180y180z90 = V3.V(180, 180, 90), x180y180z180 = V3.V(180, 180, 180), x180y180z270 = V3.V(180, 180, 270), x180y270 = V3.V(180, 270, 0), x180y270z90 = V3.V(180, 270, 90), x180y270z180 = V3.V(180, 270, 180), x180y270z270 = V3.V(180, 270, 270), x270 = V3.V(270, 0, 0), x270z90 = V3.V(270, 0, 90), x270z180 = V3.V(270, 0, 180), x270z270 = V3.V(270, 0, 270), x270y90 = V3.V(270, 90, 0), x270y90z90 = V3.V(270, 90, 90), x270y90z180 = V3.V(270, 90, 180), x270y90z270 = V3.V(270, 90, 270), x270y180 = V3.V(270, 180, 0), x270y180z90 = V3.V(270, 180, 90), x270y180z180 = V3.V(270, 180, 180), x270y180z270 = V3.V(270, 180, 270), x270y270 = V3.V(270, 270, 0), x270y270z90 = V3.V(270, 270, 90), x270y270z180 = V3.V(270, 270, 180), x270y270z270 = V3.V(270, 270, 270);
        public static bool IsNear(float a, float b, bool cw = true) { return M.Dis(a, Rep(b, a, cw)) < 180; }
        public static bool IsBet(float f, float a, float b) { return Dis(f, a) < Dis(a, b) && Dis(f, b) < Dis(a, b); }
        public static bool IsBet(float f, float a, float b, bool cw) { return IsNear(a, b, cw) == M.IsBet(Rep(f, a, cw), a, Rep(b, a, cw)); }
        public static float C(float f, float a, float b) { return IsNear(a, b) ? M.C(Rep(f, a), a, Rep(b, a)) : M.C(Rep(f, b), b, Rep(a, b)); }
        public static float Rep(float f, float a = 0, bool cw = true) { return M.Rep_(f, a, a + cw.Sign() * 360); }
        public static float Rep180(float f) { return M.Rep_(f, -180, 180); }
        public static float Dis(float a, float b) { return Mathf.DeltaAngle(a, b); }
        public static float Lerp(float a, float b, float t) { return Mathf.LerpAngle(a, b, t); }
        public static Vector3 Lerp(Vector3 a, Vector3 b, float t) { return Quaternion.Lerp(Quaternion.Euler(a), Quaternion.Euler(b), t).eulerAngles; }
        public static float Smt(float a, float b, float t) { return Mathf.LerpAngle(a, b, M.Smt(0f, 1f, t)); }
        public static float Lerp(float a, float b, float t, bool cw) { return M.Lerp(a, Rep(b, a, cw), t); }
        public static float Move(float a, float b, float d) { return Mathf.MoveTowardsAngle(a, b, d); }
        public static float Move(float a, float b, float d, bool cw) { return M.Move(a, Rep(b, a, cw), d); }
        public static float SmtDamp(float a, float b, ref float curVel, float smtTm, float maxSpd) { return Mathf.SmoothDampAngle(a, b, ref curVel, smtTm, maxSpd); }
        public static float SmtDamp(float a, float b, ref float curVel, float smtTm) { return Mathf.SmoothDampAngle(a, b, ref curVel, smtTm); }
        public static float SmtDamp(float a, float b, ref float curVel, float smtTm, float maxSpd, float dt) { return Mathf.SmoothDampAngle(a, b, ref curVel, smtTm, maxSpd, dt); }
        public static float LookD(Vector3 a, Vector3 b, bool isUnity = true) { return isUnity ? Unity(M.Atan2(b.z - a.z, b.x - a.x)) : M.Atan2(b.z - a.z, b.x - a.x); }
        public static float LookF(Vector3 a, Vector3 b, bool isUnity = true) { return isUnity ? Unity(M.Atan2(b.y - a.y, b.x - a.x)) : M.Atan2(b.y - a.y, b.x - a.x); }
        public static float Unity(float ang) { return Rep(90 - ang); }
        public static Vector3 Xz(float ang, float r = 1) { return new Vector3(M.Cos(ang), 0, M.Sin(ang)) * r; }
    }
    public static class Hpm {
        public static List<Vector3> Pnts(Vector3 a, Vector3 v0, float time, int n) {
            List<Vector3> res = new List<Vector3>() { a };
            for (int i = 1; i <= n; i++)
                res.Add(Pnt(a, v0, time * i / n));
            return res;
        }
        public static Vector3 Pnt(Vector3 a, Vector3 v0, float time) { return a + v0 * time + V3.u * A.G.y * time * time / 2f; }
        public static Vector3 H_V0(Vector3 a, Vector3 b, float h) {
            float time = 0, ang = 0;
            return H_V0(a, b, h, ref time, ref ang);
        }
        public static Vector3 H_V0(Vector3 a, Vector3 b, float h, ref float time, ref float ang) {
            time = M.Sqrt(-2 * h / A.G.y) + M.Sqrt(2 * (b.y - a.y - h) / A.G.y);
            Vector3 v0 = (b - a).Y(0) / time + V3.u * M.Sqrt(-2 * A.G.y * h) * -A.G.y.Sign();
            ang = V3.Ang(v0, v0.Y(0));
            return v0;
        }
        public static Vector3 Ang_V0(Vector3 a, Vector3 b, float ang) {
            float time = 0, h = 0;
            return Ang_V0(a, b, ang, ref time, ref h);
        }
        public static Vector3 Ang_V0(Vector3 a, Vector3 b, float ang, ref float time, ref float h) {
            float disXz = V3.Dis(a.Y(0), b.Y(0)), tan = M.Tan(ang), disY = b.y - a.y, vZ = M.Sqrt(A.G.y * disXz * disXz / (2 * (disY - disXz * tan)));
            Vector3 v0 = Q.LookRot((b - a).Y(0)) * V3.Yz(tan * vZ, vZ);
            h = -v0.y * v0.y / (2 * A.G.y);
            time = M.Sqrt(-2 * h / A.G.y) + M.Sqrt(2 * (disY - h) / A.G.y);
            return v0;
        }
        public static float V0_H(Vector3 v0) { return -v0.y * v0.y / (2 * A.G.y); }
        public static float V0_Ang(Vector3 v0) { return v0.y.Sign() * V3.Ang(v0, v0.Y(0)); }
    }
    public static class Rnd {
        public static Quaternion R => Random.rotation;
        public static Quaternion Ru => Random.rotationUniform;
        public static Quaternion Ry => Q.Y(Ang);
        public static Vector3 OnUnitSphere => Random.onUnitSphere;
        public static Vector3 InUnitSphere => Random.insideUnitSphere;
        public static Vector2 OnUnitCircle => Random.insideUnitCircle.normalized;
        public static Vector2 InUnitCircle => Random.insideUnitCircle;
        public static Color C => C.Rgba(F, F, F, 1);
        public static Color Ca => Random.ColorHSV();
        public static float F => Random.value;
        public static int I => Rng(0, 2);
        public static float F1 => Rng(-1f, 1f);
        public static int I1 => Rng(-1, 2);
        public static int Sign => B ? -1 : 1;
        public static float Ang => Rng(0f, 360f);
        public static bool B => F < 0.5f;
        public static float N(float n) { return Rng(0f, n); }
        public static int N(int n) { return Rng(0, n + 1); }
        public static bool P(float p) { return F < p; }
        public static float Rng(float a, float b) { return Random.Range(a, b); }
        public static int Rng(int a, int b) { return Random.Range(a, b); }
        public static int RngIn(int a, int b) { return Rng(a, b + 1); }
        public static int Idx(int n) { return Rng(0, n); }
        public static int Idx(int i, int n) {
            int res = 0;
            if (i < 0)
                i = n - 1;
            if (n > 1)
                do {
                    res = Idx(n);
                } while (res == i);
            return res;
        }
        public static Vector3 Pos(Vector3 a, Vector3 b) {
            Vector3 min = V3.Min(a, b), max = V3.Max(a, b);
            return V3.V(Rng(min.x, max.x), Rng(min.y, max.y), Rng(min.z, max.z));
        }
        public static Vector3 CenSz(Vector3 cen, Vector3 sz) {
            Vector3 min = cen - sz / 2f, max = cen + sz / 2f;
            return V3.V(Rng(min.x, max.x), Rng(min.y, max.y), Rng(min.z, max.z));
        }
        public static T Enum<T>() {
            var values = System.Enum.GetValues(typeof(T));
            return (T)values.GetValue(Idx(values.Length));
        }
        public static T Select<T>(T a, T b) { return F < 0.5f ? a : b; }
        public static T Select<T>(float p, T a, T b) { return F < p ? a : b; }
        public static T Arr<T>(params T[] arr) { return arr[Idx(arr.Length)]; }
        public static T ArrI<T>(int i, params T[] arr) { return arr[Idx(i, arr.Length)]; }
        public static T ArrE<T>(T e, params T[] arr) { return ListE(e, arr.List()); }
        public static T[] Arr<T>(int n, params T[] arr) { return List(n, arr.List()).Arr(); }
        public static T Arr<T>(params (T, float)[] arr) {
            if (arr.Length > 0) {
                float s = 0, p = 0, r = F;
                arr.List().ForEach(x => s += x.Item2);
                for (int i = 0; i < arr.Length; i++) {
                    p += arr[i].Item2 / s;
                    if (r <= p)
                        return arr[i].Item1;
                }
                return arr.Last().Item1;
            }
            return default;
        }
        public static T List<T>(List<T> lis) { return lis[Idx(lis.Count)]; }
        public static T ListI<T>(int i, List<T> lis) { return lis[Idx(i, lis.Count)]; }
        public static T ListE<T>(T e, List<T> lis) {
            T res;
            do {
                res = lis[Idx(lis.Count)];
            } while (res.Equals(e));
            return res;
        }
        public static List<T> List<T>(int n, List<T> lis) {
            List<T> res = new List<T>();
            if (1 <= n && n <= lis.Count)
                while (res.Count < n) {
                    T t = lis[Idx(lis.Count)];
                    if (!res.Contains(t))
                        res.Add(t);
                }
            else if (n > lis.Count)
                while (res.Count < n)
                    res.Add(lis[Idx(lis.Count)]);
            return res;
        }
        public static Color ColHsv(float hMin, float hMax, float sMin, float sMax, float vMin, float vMax, float aMin, float aMax) { return Random.ColorHSV(hMin, hMax, sMin, sMax, vMin, vMax, aMin, aMax); }
        public static Color ColHsv(float hMin, float hMax, float sMin, float sMax, float vMin, float vMax) { return Random.ColorHSV(hMin, hMax, sMin, sMax, vMin, vMax); }
        public static Color ColHsv(float hMin, float hMax, float sMin, float sMax) { return Random.ColorHSV(hMin, hMax, sMin, sMax); }
        public static Color ColHsv(float hMin, float hMax) { return Random.ColorHSV(hMin, hMax); }
    }
    public static class C {
        public static readonly Color O = Color.clear, I = Color.white, r = Color.red, g = Color.blue, b = Color.blue, c = Color.cyan, m = Color.magenta, y = Color.yellow, _g = Color.gray, _b = Color.black, _black = new Color32(85, 85, 85, 255), _darkBlue = new Color32(85, 85, 170, 255), _blue = new Color32(85, 85, 255, 255), _darkGreen = new Color32(85, 170, 85, 255), _darkCyan = new Color32(85, 170, 170, 255), _blueCyan = new Color32(85, 170, 255, 255), _green = new Color32(85, 255, 85, 255), _greenCyan = new Color32(85, 255, 170, 255), _cyan = new Color32(85, 255, 255, 255), _darkRed = new Color32(170, 85, 85, 255), _darkMagenta = new Color32(170, 85, 170, 255), _blueMagenta = new Color32(170, 85, 255, 255), _darkYellow = new Color32(170, 170, 85, 255), _gray = new Color32(170, 170, 170, 255), _lightBlue = new Color32(170, 170, 255, 255), _yellowGreen = new Color32(170, 255, 85, 255), _lightGreen = new Color32(170, 255, 170, 255), _lightCyan = new Color32(170, 255, 255, 255), _red = new Color32(255, 85, 85, 255), _redMagenta = new Color32(255, 85, 170, 255), _magenta = new Color32(255, 85, 255, 255), _orange = new Color32(255, 170, 85, 255), _lightRed = new Color32(255, 170, 170, 255), _lightMagenta = new Color32(255, 170, 255, 255), _yellow = new Color32(255, 255, 85, 255), _lightYellow = new Color32(255, 255, 170, 255), _white = new Color32(255, 255, 255, 255), clear = Color.clear, aliceBlue = new Color32(240, 248, 255, 255), antiqueWhite = new Color32(250, 235, 215, 255), aqua = new Color32(0, 255, 255, 255), aquamarine = new Color32(127, 255, 212, 255), azure = new Color32(240, 255, 255, 255), beige = new Color32(245, 245, 220, 255), bisque = new Color32(255, 228, 196, 255), black = Color.black, blanchedAlmond = new Color32(255, 235, 205, 255), blue = Color.blue, blueViolet = new Color32(138, 43, 226, 255), brown = new Color32(165, 42, 42, 255), burlyWood = new Color32(222, 184, 135, 255), cadetBlue = new Color32(95, 158, 160, 255), chartreuse = new Color32(127, 255, 0, 255), chocolate = new Color32(210, 105, 30, 255), coral = new Color32(255, 127, 80, 255), cornflowerBlue = new Color32(100, 149, 237, 255), cornsilk = new Color32(255, 248, 220, 255), crimson = new Color32(220, 20, 60, 255), cyan = Color.cyan, darkBlue = new Color32(0, 0, 139, 255), darkCyan = new Color32(0, 139, 139, 255), darkGoldenRod = new Color32(184, 134, 11, 255), darkGray = new Color32(169, 169, 169, 255), darkGrey = new Color32(169, 169, 169, 255), darkGreen = new Color32(0, 100, 0, 255), darkKhaki = new Color32(189, 183, 107, 255), darkMagenta = new Color32(139, 0, 139, 255), darkOliveGreen = new Color32(85, 107, 47, 255), darkOrange = new Color32(255, 140, 0, 255), darkOrchid = new Color32(153, 50, 204, 255), darkRed = new Color32(139, 0, 0, 255), darkSalmon = new Color32(233, 150, 122, 255), darkSeaGreen = new Color32(143, 188, 143, 255), darkSlateBlue = new Color32(72, 61, 139, 255), darkSlateGray = new Color32(47, 79, 79, 255), darkSlateGrey = new Color32(47, 79, 79, 255), darkTurquoise = new Color32(0, 206, 209, 255), darkViolet = new Color32(148, 0, 211, 255), deepPink = new Color32(255, 20, 147, 255), deepSkyBlue = new Color32(0, 191, 255, 255), dimGray = new Color32(105, 105, 105, 255), dimGrey = new Color32(105, 105, 105, 255), dodgerBlue = new Color32(30, 144, 255, 255), fireBrick = new Color32(178, 34, 34, 255), floralWhite = new Color32(255, 250, 240, 255), forestGreen = new Color32(34, 139, 34, 255), fuchsia = new Color32(255, 0, 255, 255), gainsboro = new Color32(220, 220, 220, 255), ghostWhite = new Color32(248, 248, 255, 255), gold = new Color32(255, 215, 0, 255), goldenRod = new Color32(218, 165, 32, 255), gray = Color.gray, grey = Color.grey, green = Color.green, greenYellow = new Color32(173, 255, 47, 255), honeyDew = new Color32(240, 255, 240, 255), hotPink = new Color32(255, 105, 180, 255), indianRed = new Color32(205, 92, 92, 255), indigo = new Color32(75, 0, 130, 255), ivory = new Color32(255, 255, 240, 255), khaki = new Color32(240, 230, 140, 255), lavender = new Color32(230, 230, 250, 255), lavenderBlush = new Color32(255, 240, 245, 255), lawnGreen = new Color32(124, 252, 0, 255), lemonChiffon = new Color32(255, 250, 205, 255), lightBlue = new Color32(173, 216, 230, 255), lightCoral = new Color32(240, 128, 128, 255), lightCyan = new Color32(224, 255, 255, 255), lightGoldenRodYellow = new Color32(250, 250, 210, 255), lightGray = new Color32(211, 211, 211, 255), lightGrey = new Color32(211, 211, 211, 255), lightGreen = new Color32(144, 238, 144, 255), lightPink = new Color32(255, 182, 193, 255), lightSalmon = new Color32(255, 160, 122, 255), lightSeaGreen = new Color32(32, 178, 170, 255), lightSkyBlue = new Color32(135, 206, 250, 255), lightSlateGray = new Color32(119, 136, 153, 255), lightSlateGrey = new Color32(119, 136, 153, 255), lightSteelBlue = new Color32(176, 196, 222, 255), lightYellow = new Color32(255, 255, 224, 255), lime = new Color32(0, 255, 0, 255), limeGreen = new Color32(50, 205, 50, 255), linen = new Color32(250, 240, 230, 255), magenta = Color.magenta, maroon = new Color32(128, 0, 0, 255), mediumAquaMarine = new Color32(102, 205, 170, 255), mediumBlue = new Color32(0, 0, 205, 255), mediumOrchid = new Color32(186, 85, 211, 255), mediumPurple = new Color32(147, 112, 219, 255), mediumSeaGreen = new Color32(60, 179, 113, 255), mediumSlateBlue = new Color32(123, 104, 238, 255), mediumSpringGreen = new Color32(0, 250, 154, 255), mediumTurquoise = new Color32(72, 209, 204, 255), mediumVioletRed = new Color32(199, 21, 133, 255), midnightBlue = new Color32(25, 25, 112, 255), mintCream = new Color32(245, 255, 250, 255), mistyRose = new Color32(255, 228, 225, 255), moccasin = new Color32(255, 228, 181, 255), navajoWhite = new Color32(255, 222, 173, 255), navy = new Color32(0, 0, 128, 255), oldLace = new Color32(253, 245, 230, 255), olive = new Color32(128, 128, 0, 255), oliveDrab = new Color32(107, 142, 35, 255), orange = new Color32(255, 165, 0, 255), orangeRed = new Color32(255, 69, 0, 255), orchid = new Color32(218, 112, 214, 255), paleGoldenRod = new Color32(238, 232, 170, 255), paleGreen = new Color32(152, 251, 152, 255), paleTurquoise = new Color32(175, 238, 238, 255), paleVioletRed = new Color32(219, 112, 147, 255), papayaWhip = new Color32(255, 239, 213, 255), peachPuff = new Color32(255, 218, 185, 255), peru = new Color32(205, 133, 63, 255), pink = new Color32(255, 192, 203, 255), plum = new Color32(221, 160, 221, 255), powderBlue = new Color32(176, 224, 230, 255), purple = new Color32(128, 0, 128, 255), rebeccaPurple = new Color32(102, 51, 153, 255), red = Color.red, rosyBrown = new Color32(188, 143, 143, 255), royalBlue = new Color32(65, 105, 225, 255), saddleBrown = new Color32(139, 69, 19, 255), salmon = new Color32(250, 128, 114, 255), sandyBrown = new Color32(244, 164, 96, 255), seaGreen = new Color32(46, 139, 87, 255), seaShell = new Color32(255, 245, 238, 255), sienna = new Color32(160, 82, 45, 255), silver = new Color32(192, 192, 192, 255), skyBlue = new Color32(135, 206, 235, 255), slateBlue = new Color32(106, 90, 205, 255), slateGray = new Color32(112, 128, 144, 255), slateGrey = new Color32(112, 128, 144, 255), snow = new Color32(255, 250, 250, 255), springGreen = new Color32(0, 255, 127, 255), steelBlue = new Color32(70, 130, 180, 255), tan = new Color32(210, 180, 140, 255), teal = new Color32(0, 128, 128, 255), thistle = new Color32(216, 191, 216, 255), tomato = new Color32(255, 99, 71, 255), turquoise = new Color32(64, 224, 208, 255), violet = new Color32(238, 130, 238, 255), wheat = new Color32(245, 222, 179, 255), white = Color.white, whiteSmoke = new Color32(245, 245, 245, 255), yellow = Color.yellow, yellowGreen = new Color32(154, 205, 50, 255);
        public static Color R(float r) { return new Color(r, 0, 0, 1); }
        public static Color G(float g) { return new Color(0, g, 0, 1); }
        public static Color B(float b) { return new Color(0, 0, b, 1); }
        public static Color A(float a) { return new Color(0, 0, 0, a); }
        public static Color Rg(float r, float g) { return new Color(r, g, 0, 1); }
        public static Color Rb(float r, float b) { return new Color(r, 0, b, 1); }
        public static Color Ra(float r, float a) { return new Color(r, 0, 0, a); }
        public static Color Gb(float g, float b) { return new Color(0, g, b, 1); }
        public static Color Ga(float g, float a) { return new Color(0, g, 0, a); }
        public static Color Ba(float b, float a) { return new Color(0, 0, b, a); }
        public static Color Rgb(float r, float g, float b) { return new Color(r, g, b, 1); }
        public static Color Rga(float r, float g, float a) { return new Color(r, g, 0, a); }
        public static Color Rba(float r, float b, float a) { return new Color(r, 0, b, a); }
        public static Color Gba(float g, float b, float a) { return new Color(0, g, b, a); }
        public static Color Rgba(float r, float g, float b, float a) { return new Color(r, g, b, a); }
        public static Color H(float h) { return HsvaRgba(h, 1, 1, 1); }
        public static Color S(float s) { return HsvaRgba(0, s, 1, 1); }
        public static Color V(float v) { return HsvaRgba(0, 1, v, 1); }
        public static Color Hs(float h, float s) { return HsvaRgba(h, s, 1, 1); }
        public static Color Hv(float h, float v) { return HsvaRgba(h, 1, v, 1); }
        public static Color Ha(float h, float a) { return HsvaRgba(h, 1, 1, a); }
        public static Color Sv(float s, float v) { return HsvaRgba(0, s, v, 1); }
        public static Color Sa(float s, float a) { return HsvaRgba(0, s, 1, a); }
        public static Color Va(float v, float a) { return HsvaRgba(0, 1, v, a); }
        public static Color Hsv(float h, float s, float v) { return HsvaRgba(h, s, v, 1); }
        public static Color Hsa(float h, float s, float a) { return HsvaRgba(h, s, 1, a); }
        public static Color Hva(float h, float v, float a) { return HsvaRgba(h, 1, v, a); }
        public static Color Sva(float s, float v, float a) { return HsvaRgba(0, s, v, a); }
        public static Color Hsva(float h, float s, float v, float a) { return HsvaRgba(h, s, v, a); }
        public static Color Lerp(Color a, Color b, float t) { return Color.Lerp(a, b, t); }
        public static Color LerpUnc(Color a, Color b, float t) { return Color.LerpUnclamped(a, b, t); }
        public static void RgbHsv(Color c, out float h, out float s, out float v) { Color.RGBToHSV(c, out h, out s, out v); }
        public static Color HsvRgb(float h, float s, float v) { return Color.HSVToRGB(h, s, v); }
        public static Color HsvRgb(float h, float s, float v, bool isHdr) { return Color.HSVToRGB(h, s, v, isHdr); }
        public static Color HsvaRgba(float h, float s, float v, float a) { return Color.HSVToRGB(h, s, v).A(a); }
        public static Color HsvaRgba(float h, float s, float v, float a, bool isHdr) { return Color.HSVToRGB(h, s, v, isHdr).A(a); }
    }
    public static class Q {
        public static readonly Quaternion O = Quaternion.identity;
        public static Quaternion X(float x) { return Quaternion.Euler(x, 0, 0); }
        public static Quaternion Y(float y) { return Quaternion.Euler(0, y, 0); }
        public static Quaternion Z(float z) { return Quaternion.Euler(0, 0, z); }
        public static Quaternion Xy(float x, float y) { return Quaternion.Euler(x, y, 0); }
        public static Quaternion Xz(float x, float z) { return Quaternion.Euler(x, 0, z); }
        public static Quaternion Yz(float y, float z) { return Quaternion.Euler(0, y, z); }
        public static Quaternion E(float x, float y, float z) { return Quaternion.Euler(x, y, z); }
        public static Quaternion E(Vector3 e) { return Quaternion.Euler(e); }
        public static Quaternion Rot(Vector3 aDir, Vector3 bDir) { return Quaternion.FromToRotation(aDir, bDir); }
        public static Quaternion Inv(Quaternion r) { return Quaternion.Inverse(r); }
        public static Quaternion Slerp(Quaternion a, Quaternion b, float t) { return Quaternion.Slerp(a, b, t); }
        public static Quaternion SlerpUnc(Quaternion a, Quaternion b, float t) { return Quaternion.SlerpUnclamped(a, b, t); }
        public static Quaternion Lerp(Quaternion a, Quaternion b, float t) { return Quaternion.Lerp(a, b, t); }
        public static Quaternion LerpUnc(Quaternion a, Quaternion b, float t) { return Quaternion.LerpUnclamped(a, b, t); }
        public static Quaternion AngAxis(float ang, Vector3 axis) { return Quaternion.AngleAxis(ang, axis); }
        public static Quaternion LookRot(Vector3 f, Vector3 up) { return Quaternion.LookRotation(f, up); }
        public static Quaternion LookRot(Vector3 f) { return Quaternion.LookRotation(f); }
        public static float Dot(Quaternion a, Quaternion b) { return Quaternion.Dot(a, b); }
        public static float Ang(Quaternion a, Quaternion b) { return Quaternion.Angle(a, b); }
        public static Quaternion Rot(Quaternion a, Quaternion b, float maxDegD) { return Quaternion.RotateTowards(a, b, maxDegD); }
        public static Quaternion Nor(Quaternion q) { return Quaternion.Normalize(q); }
    }
    public static class V2 {
        public static readonly Vector2 O = Vector2.zero, I = Vector2.one, l = Vector2.left, r = Vector2.right, d = Vector2.down, u = Vector2.up, negInf = Vector2.negativeInfinity, posInf = Vector2.positiveInfinity, nn = V(-1, -1), zn = V(0, -1), pn = V(1, -1), nz = V(-1, 0), zz = V(0, 0), pz = V(1, 0), np = V(-1, 1), zp = V(0, 1), pp = V(1, 1);
        public static Vector2 X(float x) { return new Vector2(x, 0); }
        public static Vector2 Y(float y) { return new Vector2(0, y); }
        public static Vector2 V(float x, float y) { return new Vector2(x, y); }
        public static Vector2 V(float v) { return new Vector2(v, v); }
        public static Vector2 C(Vector2 v, Vector2 a, Vector2 b) { return V(M.C_(v.x, a.x, b.x), M.C_(v.y, a.y, b.y)); }
        public static bool IsBet(Vector2 v, Vector2 a, Vector2 b) { return M.IsBet(v.x, a.x, b.x) && M.IsBet(v.y, a.y, b.y); }
        public static Vector2 Lerp(float t, params Vector2[] arr) { return Gen.F2((a, b) => Vector2.Lerp(a, b, t), arr); }
        public static Vector2 Scl(params Vector2[] arr) { return Gen.F1((a, b) => Vector2.Scale(a, b), arr); }
        public static Vector2 Div(params Vector2[] arr) { return Gen.F1((a, b) => V(a.x / b.x, a.y / b.y), arr); }
        public static Vector2 Min(params Vector2[] arr) { return Gen.F1((a, b) => Vector2.Min(a, b), arr); }
        public static Vector2 Max(params Vector2[] arr) { return Gen.F1((a, b) => Vector2.Max(a, b), arr); }
        public static float Dis(params Vector2[] arr) { return Gen.F1((a, b) => Vector2.Distance(a, b), arr); }
        public static Vector2 Lerp(float t, List<Vector2> l) { return Lerp(t, l.Arr()); }
        public static Vector2 Scl(List<Vector2> l) { return Scl(l.Arr()); }
        public static Vector2 Div(List<Vector2> l) { return Div(l.Arr()); }
        public static Vector2 Min(List<Vector2> l) { return Min(l.Arr()); }
        public static Vector2 Max(List<Vector2> l) { return Max(l.Arr()); }
        public static float Dis(List<Vector2> l) { return Dis(l.Arr()); }
        public static Vector2 Nor(Vector2 v) { return v.normalized; }
        public static float Mag(Vector2 v) { return v.magnitude; }
        public static Vector2 MagC(Vector2 v, float maxLen) { return Vector2.ClampMagnitude(v, maxLen); }
        public static float SqrMag(Vector2 a) { return Vector2.SqrMagnitude(a); }
        public static float Dot(Vector2 a, Vector2 b) { return Vector2.Dot(a, b); }
        public static Vector2 Lerp(Vector2 a, Vector2 b, float t) { return Vector2.Lerp(a, b, t); }
        public static Vector2 LerpUnc(Vector2 a, Vector2 b, float t) { return Vector2.LerpUnclamped(a, b, t); }
        public static Vector2 Move(Vector2 a, Vector2 b, float d) { return Vector2.MoveTowards(a, b, d); }
        public static Vector2 MoveUnc(Vector2 a, Vector2 b, float d) { return a + (b - a).normalized * d; }
        public static Vector2 SmtDamp(Vector2 a, Vector2 b, ref Vector2 curVel, float smtTm, float maxSpd) { return Vector2.SmoothDamp(a, b, ref curVel, smtTm, maxSpd); }
        public static Vector2 SmtDamp(Vector2 a, Vector2 b, ref Vector2 curVel, float smtTm) { return Vector2.SmoothDamp(a, b, ref curVel, smtTm); }
        public static Vector2 SmtDamp(Vector2 a, Vector2 b, ref Vector2 curVel, float smtTm, float maxSpd, float dt) { return Vector2.SmoothDamp(a, b, ref curVel, smtTm, maxSpd, dt); }
        public static Vector2 Ref(Vector2 inDir, Vector2 inNor) { return Vector2.Reflect(inDir, inNor); }
        public static Vector2 Per(Vector2 inDir) { return Vector2.Perpendicular(inDir); }
        public static float Ang(Vector2 a, Vector2 b) { return Vector2.Angle(a, b); }
        public static float SignAng(Vector2 a, Vector2 b) { return Vector2.SignedAngle(a, b); }
    }
    public static class V2I {
        public static readonly Vector2Int O = Vector2Int.zero, I = Vector2Int.one, l = Vector2Int.left, r = Vector2Int.right, d = Vector2Int.down, u = Vector2Int.up, nn = V(-1, -1), zn = V(0, -1), pn = V(1, -1), nz = V(-1, 0), zz = V(0, 0), pz = V(1, 0), np = V(-1, 1), zp = V(0, 1), pp = V(1, 1);
        public static Vector2Int X(int x) { return new Vector2Int(x, 0); }
        public static Vector2Int Y(int y) { return new Vector2Int(0, y); }
        public static Vector2Int V(int x, int y) { return new Vector2Int(x, y); }
        public static Vector2Int V(int v) { return new Vector2Int(v, v); }
        public static Vector2Int C(Vector2Int v, Vector2Int a, Vector2Int b) { return V(M.C_(v.x, a.x, b.x), M.C_(v.y, a.y, b.y)); }
        public static bool IsBet(Vector2Int v, Vector2Int a, Vector2Int b) { return M.IsBet(v.x, a.x, b.x) && M.IsBet(v.y, a.y, b.y); }
        public static Vector2Int Scl(params Vector2Int[] arr) { return Gen.F1((a, b) => Vector2Int.Scale(a, b), arr); }
        public static Vector2Int Div(params Vector2Int[] arr) { return Gen.F1((a, b) => V(a.x / b.x, a.y / b.y), arr); }
        public static Vector2Int Min(params Vector2Int[] arr) { return Gen.F1((a, b) => Vector2Int.Min(a, b), arr); }
        public static Vector2Int Max(params Vector2Int[] arr) { return Gen.F1((a, b) => Vector2Int.Max(a, b), arr); }
        public static float Dis(params Vector2Int[] arr) { return Gen.F1((a, b) => Vector2Int.Distance(a, b), arr); }
        public static Vector2Int Scl(List<Vector2Int> l) { return Scl(l.Arr()); }
        public static Vector2Int Div(List<Vector2Int> l) { return Div(l.Arr()); }
        public static Vector2Int Min(List<Vector2Int> l) { return Min(l.Arr()); }
        public static Vector2Int Max(List<Vector2Int> l) { return Max(l.Arr()); }
        public static float Dis(List<Vector2Int> l) { return Dis(l.Arr()); }
        public static float Mag(Vector2Int v) { return v.magnitude; }
        public static int SqrMag(Vector2Int a) { return a.sqrMagnitude; }
        public static Vector2Int FloorI(Vector2 v) { return Vector2Int.FloorToInt(v); }
        public static Vector2Int CeilI(Vector2 v) { return Vector2Int.CeilToInt(v); }
        public static Vector2Int RoundI(Vector2 v) { return Vector2Int.RoundToInt(v); }
    }
    public static class V3 {
        public static readonly Vector3 O = Vector3.zero, I = Vector3.one, l = Vector3.left, r = Vector3.right, d = Vector3.down, u = Vector3.up, b = Vector3.back, f = Vector3.forward, negInf = Vector3.negativeInfinity, posInf = Vector3.positiveInfinity, nnn = V(-1, -1, -1), znn = V(0, -1, -1), pnn = V(1, -1, -1), nzn = V(-1, 0, -1), zzn = V(0, 0, -1), pzn = V(1, 0, -1), npn = V(-1, 1, -1), zpn = V(0, 1, -1), ppn = V(1, 1, -1), nnz = V(-1, -1, 0), znz = V(0, -1, 0), pnz = V(1, -1, 0), nzz = V(-1, 0, 0), zzz = V(0, 0, 0), pzz = V(1, 0, 0), npz = V(-1, 1, 0), zpz = V(0, 1, 0), ppz = V(1, 1, 0), nnp = V(-1, -1, 1), znp = V(0, -1, 1), pnp = V(1, -1, 1), nzp = V(-1, 0, 1), zzp = V(0, 0, 1), pzp = V(1, 0, 1), npp = V(-1, 1, 1), zpp = V(0, 1, 1), ppp = V(1, 1, 1);
        public static Vector3 X(float x) { return new Vector3(x, 0, 0); }
        public static Vector3 Y(float y) { return new Vector3(0, y, 0); }
        public static Vector3 Z(float z) { return new Vector3(0, 0, z); }
        public static Vector3 Xy(float x, float y) { return new Vector3(x, y, 0); }
        public static Vector3 Xz(float x, float z) { return new Vector3(x, 0, z); }
        public static Vector3 Yz(float y, float z) { return new Vector3(0, y, z); }
        public static Vector3 Xy(float v) { return new Vector3(v, v, 0); }
        public static Vector3 Xz(float v) { return new Vector3(v, 0, v); }
        public static Vector3 Yz(float v) { return new Vector3(0, v, v); }
        public static Vector3 V(float x, float y, float z) { return new Vector3(x, y, z); }
        public static Vector3 V(float v) { return new Vector3(v, v, v); }
        public static Vector3 C(Vector3 v, Vector3 a, Vector3 b) { return V(M.C_(v.x, a.x, b.x), M.C_(v.y, a.y, b.y), M.C_(v.z, a.z, b.z)); }
        public static bool IsBet(Vector3 v, Vector3 a, Vector3 b) { return M.IsBet(v.x, a.x, b.x) && M.IsBet(v.y, a.y, b.y) && M.IsBet(v.z, a.z, b.z); }
        public static Vector3 Lerp(float t, params Vector3[] arr) { return Gen.F2((a, b) => Vector3.Lerp(a, b, t), arr); }
        public static Vector3 Scl(params Vector3[] arr) { return Gen.F1((a, b) => Vector3.Scale(a, b), arr); }
        public static Vector3 Div(params Vector3[] arr) { return Gen.F1((a, b) => V(a.x / b.x, a.y / b.y, a.z / b.z), arr); }
        public static Vector3 Min(params Vector3[] arr) { return Gen.F1((a, b) => Vector3.Min(a, b), arr); }
        public static Vector3 Max(params Vector3[] arr) { return Gen.F1((a, b) => Vector3.Max(a, b), arr); }
        public static float Dis(params Vector3[] arr) { return Gen.F1((a, b) => Vector3.Distance(a, b), arr); }
        public static Vector3 Lerp(float t, List<Vector3> l) { return Lerp(t, l.Arr()); }
        public static Vector3 Scl(List<Vector3> l) { return Scl(l.Arr()); }
        public static Vector3 Div(List<Vector3> l) { return Div(l.Arr()); }
        public static Vector3 Min(List<Vector3> l) { return Min(l.Arr()); }
        public static Vector3 Max(List<Vector3> l) { return Max(l.Arr()); }
        public static float Dis(List<Vector3> l) { return Dis(l.Arr()); }
        public static Vector3 Nor(Vector3 v) { return Vector3.Normalize(v); }
        public static void OrthoNor(ref Vector3 nor, ref Vector3 tan) { Vector3.OrthoNormalize(ref nor, ref tan); }
        public static void OrthoNor(ref Vector3 nor, ref Vector3 tan, ref Vector3 biNor) { Vector3.OrthoNormalize(ref nor, ref tan, ref biNor); }
        public static float Mag(Vector3 v) { return Vector3.Magnitude(v); }
        public static Vector3 MagC(Vector3 v, float maxLen) { return Vector3.ClampMagnitude(v, maxLen); }
        public static float SqrMag(Vector3 v) { return Vector3.SqrMagnitude(v); }
        public static Vector3 Rot(Vector3 a, Vector3 b, float maxRadD, float maxMagD) { return Vector3.RotateTowards(a, b, maxRadD, maxMagD); }
        public static Vector3 Slerp(Vector3 a, Vector3 b, float t) { return Vector3.Slerp(a, b, t); }
        public static Vector3 SlerpUnc(Vector3 a, Vector3 b, float t) { return Vector3.SlerpUnclamped(a, b, t); }
        public static Vector3 Lerp(Vector3 a, Vector3 b, float t) { return Vector3.Lerp(a, b, t); }
        public static Vector3 LerpUnc(Vector3 a, Vector3 b, float t) { return Vector3.LerpUnclamped(a, b, t); }
        public static Vector3 Move(Vector3 a, Vector3 b, float d) { return Vector3.MoveTowards(a, b, d); }
        public static Vector3 MoveUnc(Vector3 a, Vector3 b, float d) { return a + (b - a).normalized * d; }
        public static Vector3 SmtDamp(Vector3 a, Vector3 b, ref Vector3 curVel, float smtTm, float maxSpd) { return Vector3.SmoothDamp(a, b, ref curVel, smtTm, maxSpd); }
        public static Vector3 SmtDamp(Vector3 a, Vector3 b, ref Vector3 curVel, float smtTm) { return Vector3.SmoothDamp(a, b, ref curVel, smtTm); }
        public static Vector3 SmtDamp(Vector3 a, Vector3 b, ref Vector3 curVel, float smtTm, float maxSpd, float dt) { return Vector3.SmoothDamp(a, b, ref curVel, smtTm, maxSpd, dt); }
        public static Vector3 Ref(Vector3 inDir, Vector3 inNor) { return Vector3.Reflect(inDir, inNor); }
        public static float Ang(Vector3 a, Vector3 b) { return Vector3.Angle(a, b); }
        public static float SignAng(Vector3 a, Vector3 b, Vector3 axis) { return Vector3.SignedAngle(a, b, axis); }
        public static float Dot(Vector3 a, Vector3 b) { return Vector3.Dot(a, b); }
        public static Vector3 Cross(Vector3 a, Vector3 b) { return Vector3.Cross(a, b); }
        public static Vector3 Prj(Vector3 v, Vector3 onNor) { return Vector3.Project(v, onNor); }
        public static Vector3 PrjOnPln(Vector3 v, Vector3 plnNor) { return Vector3.ProjectOnPlane(v, plnNor); }
    }
    public static class V3I {
        public static readonly Vector3Int O = Vector3Int.zero, I = Vector3Int.one, l = Vector3Int.left, r = Vector3Int.right, d = Vector3Int.down, u = Vector3Int.up, b = V(0, 0, -1), f = V(0, 0, 1), nnn = V(-1, -1, -1), znn = V(0, -1, -1), pnn = V(1, -1, -1), nzn = V(-1, 0, -1), zzn = V(0, 0, -1), pzn = V(1, 0, -1), npn = V(-1, 1, -1), zpn = V(0, 1, -1), ppn = V(1, 1, -1), nnz = V(-1, -1, 0), znz = V(0, -1, 0), pnz = V(1, -1, 0), nzz = V(-1, 0, 0), zzz = V(0, 0, 0), pzz = V(1, 0, 0), npz = V(-1, 1, 0), zpz = V(0, 1, 0), ppz = V(1, 1, 0), nnp = V(-1, -1, 1), znp = V(0, -1, 1), pnp = V(1, -1, 1), nzp = V(-1, 0, 1), zzp = V(0, 0, 1), pzp = V(1, 0, 1), npp = V(-1, 1, 1), zpp = V(0, 1, 1), ppp = V(1, 1, 1);
        public static Vector3Int X(int x) { return new Vector3Int(x, 0, 0); }
        public static Vector3Int Y(int y) { return new Vector3Int(0, y, 0); }
        public static Vector3Int Z(int z) { return new Vector3Int(0, 0, z); }
        public static Vector3Int Xy(int x, int y) { return new Vector3Int(x, y, 0); }
        public static Vector3Int Xz(int x, int z) { return new Vector3Int(x, 0, z); }
        public static Vector3Int Yz(int y, int z) { return new Vector3Int(0, y, z); }
        public static Vector3Int Xy(int v) { return new Vector3Int(v, v, 0); }
        public static Vector3Int Xz(int v) { return new Vector3Int(v, 0, v); }
        public static Vector3Int Yz(int v) { return new Vector3Int(0, v, v); }
        public static Vector3Int V(int x, int y, int z) { return new Vector3Int(x, y, z); }
        public static Vector3Int V(int v) { return new Vector3Int(v, v, v); }
        public static Vector3Int C(Vector3Int v, Vector3Int a, Vector3Int b) { return V(M.C_(v.x, a.x, b.x), M.C_(v.y, a.y, b.y), M.C_(v.z, a.z, b.z)); }
        public static bool IsBet(Vector3Int v, Vector3Int a, Vector3Int b) { return M.IsBet(v.x, a.x, b.x) && M.IsBet(v.y, a.y, b.y) && M.IsBet(v.z, a.z, b.z); }
        public static Vector3Int Scl(params Vector3Int[] arr) { return Gen.F1((a, b) => Vector3Int.Scale(a, b), arr); }
        public static Vector3Int Div(params Vector3Int[] arr) { return Gen.F1((a, b) => V(a.x / b.x, a.y / b.y, a.z / b.z), arr); }
        public static Vector3Int Min(params Vector3Int[] arr) { return Gen.F1((a, b) => Vector3Int.Min(a, b), arr); }
        public static Vector3Int Max(params Vector3Int[] arr) { return Gen.F1((a, b) => Vector3Int.Max(a, b), arr); }
        public static float Dis(params Vector3Int[] arr) { return Gen.F1((a, b) => Vector3Int.Distance(a, b), arr); }
        public static Vector3Int Scl(List<Vector3Int> l) { return Scl(l.Arr()); }
        public static Vector3Int Div(List<Vector3Int> l) { return Div(l.Arr()); }
        public static Vector3Int Min(List<Vector3Int> l) { return Min(l.Arr()); }
        public static Vector3Int Max(List<Vector3Int> l) { return Max(l.Arr()); }
        public static float Dis(List<Vector3Int> l) { return Dis(l.Arr()); }
        public static float Mag(Vector3Int v) { return v.magnitude; }
        public static int SqrMag(Vector3Int v) { return v.sqrMagnitude; }
        public static Vector3Int FloorI(Vector3 v) { return Vector3Int.FloorToInt(v); }
        public static Vector3Int CeilI(Vector3 v) { return Vector3Int.CeilToInt(v); }
        public static Vector3Int RoundI(Vector3 v) { return Vector3Int.RoundToInt(v); }
    }
    public static class V4 {
        public static readonly Vector4 O = Vector4.zero, I = Vector4.one, negInf = Vector4.negativeInfinity, posInf = Vector4.positiveInfinity, nnnn = V(-1, -1, -1, -1), znnn = V(0, -1, -1, -1), pnnn = V(1, -1, -1, -1), nznn = V(-1, 0, -1, -1), zznn = V(0, 0, -1, -1), pznn = V(1, 0, -1, -1), npnn = V(-1, 1, -1, -1), zpnn = V(0, 1, -1, -1), ppnn = V(1, 1, -1, -1), nnzn = V(-1, -1, 0, -1), znzn = V(0, -1, 0, -1), pnzn = V(1, -1, 0, -1), nzzn = V(-1, 0, 0, -1), zzzn = V(0, 0, 0, -1), pzzn = V(1, 0, 0, -1), npzn = V(-1, 1, 0, -1), zpzn = V(0, 1, 0, -1), ppzn = V(1, 1, 0, -1), nnpn = V(-1, -1, 1, -1), znpn = V(0, -1, 1, -1), pnpn = V(1, -1, 1, -1), nzpn = V(-1, 0, 1, -1), zzpn = V(0, 0, 1, -1), pzpn = V(1, 0, 1, -1), nppn = V(-1, 1, 1, -1), zppn = V(0, 1, 1, -1), pppn = V(1, 1, 1, -1), nnnz = V(-1, -1, -1, 0), znnz = V(0, -1, -1, 0), pnnz = V(1, -1, -1, 0), nznz = V(-1, 0, -1, 0), zznz = V(0, 0, -1, 0), pznz = V(1, 0, -1, 0), npnz = V(-1, 1, -1, 0), zpnz = V(0, 1, -1, 0), ppnz = V(1, 1, -1, 0), nnzz = V(-1, -1, 0, 0), znzz = V(0, -1, 0, 0), pnzz = V(1, -1, 0, 0), nzzz = V(-1, 0, 0, 0), zzzz = V(0, 0, 0, 0), pzzz = V(1, 0, 0, 0), npzz = V(-1, 1, 0, 0), zpzz = V(0, 1, 0, 0), ppzz = V(1, 1, 0, 0), nnpz = V(-1, -1, 1, 0), znpz = V(0, -1, 1, 0), pnpz = V(1, -1, 1, 0), nzpz = V(-1, 0, 1, 0), zzpz = V(0, 0, 1, 0), pzpz = V(1, 0, 1, 0), nppz = V(-1, 1, 1, 0), zppz = V(0, 1, 1, 0), pppz = V(1, 1, 1, 0), nnnp = V(-1, -1, -1, 1), znnp = V(0, -1, -1, 1), pnnp = V(1, -1, -1, 1), nznp = V(-1, 0, -1, 1), zznp = V(0, 0, -1, 1), pznp = V(1, 0, -1, 1), npnp = V(-1, 1, -1, 1), zpnp = V(0, 1, -1, 1), ppnp = V(1, 1, -1, 1), nnzp = V(-1, -1, 0, 1), znzp = V(0, -1, 0, 1), pnzp = V(1, -1, 0, 1), nzzp = V(-1, 0, 0, 1), zzzp = V(0, 0, 0, 1), pzzp = V(1, 0, 0, 1), npzp = V(-1, 1, 0, 1), zpzp = V(0, 1, 0, 1), ppzp = V(1, 1, 0, 1), nnpp = V(-1, -1, 1, 1), znpp = V(0, -1, 1, 1), pnpp = V(1, -1, 1, 1), nzpp = V(-1, 0, 1, 1), zzpp = V(0, 0, 1, 1), pzpp = V(1, 0, 1, 1), nppp = V(-1, 1, 1, 1), zppp = V(0, 1, 1, 1), pppp = V(1, 1, 1, 1);
        public static Vector4 X(float x) { return new Vector4(x, 0, 0, 0); }
        public static Vector4 Y(float y) { return new Vector4(0, y, 0, 0); }
        public static Vector4 Z(float z) { return new Vector4(0, 0, z, 0); }
        public static Vector4 W(float w) { return new Vector4(0, 0, 0, w); }
        public static Vector4 Xy(float x, float y) { return new Vector4(x, y, 0, 0); }
        public static Vector4 Xz(float x, float z) { return new Vector4(x, 0, z, 0); }
        public static Vector4 Xw(float x, float w) { return new Vector4(x, 0, 0, w); }
        public static Vector4 Yz(float y, float z) { return new Vector4(0, y, z, 0); }
        public static Vector4 Yw(float y, float w) { return new Vector4(0, y, 0, w); }
        public static Vector4 Zw(float z, float w) { return new Vector4(0, 0, z, w); }
        public static Vector4 Xyz(float x, float y, float z) { return new Vector4(x, y, z, 0); }
        public static Vector4 Xyw(float x, float y, float w) { return new Vector4(x, y, 0, w); }
        public static Vector4 Xzw(float x, float z, float w) { return new Vector4(x, 0, z, w); }
        public static Vector4 Yzw(float y, float z, float w) { return new Vector4(0, y, z, w); }
        public static Vector4 V(float x, float y, float z, float w) { return new Vector4(x, y, z, w); }
        public static Vector4 V(float v) { return new Vector4(v, v, v, v); }
        public static Vector4 C(Vector4 v, Vector4 a, Vector4 b) { return V(M.C_(v.x, a.x, b.x), M.C_(v.y, a.y, b.y), M.C_(v.z, a.z, b.z), M.C_(v.w, a.w, b.w)); }
        public static bool IsBet(Vector4 v, Vector4 a, Vector4 b) { return M.IsBet(v.x, a.x, b.x) && M.IsBet(v.y, a.y, b.y) && M.IsBet(v.z, a.z, b.z) && M.IsBet(v.w, a.w, b.w); }
        public static Vector4 Lerp(float t, params Vector4[] arr) { return Gen.F2((a, b) => Vector4.Lerp(a, b, t), arr); }
        public static Vector4 Scl(params Vector4[] arr) { return Gen.F1((a, b) => Vector4.Scale(a, b), arr); }
        public static Vector4 Div(params Vector4[] arr) { return Gen.F1((a, b) => V(a.x / b.x, a.y / b.y, a.z / b.z, a.w / b.w), arr); }
        public static Vector4 Min(params Vector4[] arr) { return Gen.F1((a, b) => Vector4.Min(a, b), arr); }
        public static Vector4 Max(params Vector4[] arr) { return Gen.F1((a, b) => Vector4.Max(a, b), arr); }
        public static float Dis(params Vector4[] arr) { return Gen.F1((a, b) => Vector4.Distance(a, b), arr); }
        public static Vector4 Lerp(float t, List<Vector4> l) { return Lerp(t, l.Arr()); }
        public static Vector4 Scl(List<Vector4> l) { return Scl(l.Arr()); }
        public static Vector4 Div(List<Vector4> l) { return Div(l.Arr()); }
        public static Vector4 Min(List<Vector4> l) { return Min(l.Arr()); }
        public static Vector4 Max(List<Vector4> l) { return Max(l.Arr()); }
        public static float Dis(List<Vector4> l) { return Dis(l.Arr()); }
        public static Vector4 Nor(Vector4 v) { return Vector4.Normalize(v); }
        public static float Mag(Vector4 v) { return Vector4.Magnitude(v); }
        public static Vector4 MagC(Vector4 v, float maxLen) { return v.magnitude < maxLen ? v : v.normalized * maxLen; }
        public static float SqrMag(Vector4 v) { return Vector4.SqrMagnitude(v); }
        public static float Dot(Vector4 a, Vector4 b) { return Vector4.Dot(a, b); }
        public static Vector4 Prj(Vector4 a, Vector4 b) { return Vector4.Project(a, b); }
        public static Vector4 Lerp(Vector4 a, Vector4 b, float t) { return Vector4.Lerp(a, b, t); }
        public static Vector4 LerpUnc(Vector4 a, Vector4 b, float t) { return Vector4.LerpUnclamped(a, b, t); }
        public static Vector4 Move(Vector4 a, Vector4 b, float d) { return Vector4.MoveTowards(a, b, d); }
        public static Vector4 MoveUnc(Vector4 a, Vector4 b, float d) { return a + (b - a).normalized * d; }
    }
}
// Processes array of shapes into a single mesh
// Automatically determines which shapes are solid, and which are holes
// Ignores invalid shapes (contain self-intersections, too few points, overlapping holes)
namespace Orgil.Geometry {
    public class Shp2D {
        public Vector3[] vs;
        public int[] ts;
        List<List<Vector3>> points = new List<List<Vector3>>();
        float height = 0;
        public Shp2D(List<List<Vector3>> points) { this.points = points; }
        public Shp2D(List<Vector3> points) { this.points.Add(points); }
        public Mesh GetMesh() {
            Process();
            return new Mesh() { vertices = vs, triangles = ts, normals = vs.Select(x => Vector3.up).ToArray() };
        }
        public List<int> Ts(bool isCw = true, int tsStaIdx = 0) {
            List<int> idx = new List<int>();
            for (int i = 0; i < vs.Length; i++)
                for (int j = 0; j < points.Count; j++)
                    for (int k = 0; k < points[j].Count; k++)
                        if (M.Apx(vs[i].x, points[j][k].x) && M.Apx(vs[i].z, points[j][k].z)) {
                            idx.Add(k);
                            break;
                        }
            List<int> ts2 = new List<int>();
            for (int i = 0; i < ts.Length; i += 3)
                ts2.FcT(isCw, tsStaIdx + idx[ts[i]], tsStaIdx + idx[ts[i + 1]], tsStaIdx + idx[ts[i + 2]]);
            return ts2;
        }
        public static Shp2D Crt(List<Vector2> points) { Shp2D shp = new Shp2D(points.ListXz()); shp.Process(); return shp; }
        public static Shp2D Crt(List<Vector3> points) { Shp2D shp = new Shp2D(points); shp.Process(); return shp; }
        public static Shp2D Crt(List<List<Vector3>> points) { Shp2D shp = new Shp2D(points); shp.Process(); return shp; }
        public void Process() {
            try {
                // Generate array of valid shape data
                ShpData[] eligibleShapes = points.Select(x => new ShpData(x.ToArray())).Where(x => x.IsValidShape).ToArray();
                // Set parents for all shapes. A parent is a shape which completely contains another shape.
                for (int i = 0; i < eligibleShapes.Length; i++)
                    for (int j = 0; j < eligibleShapes.Length; j++) {
                        if (i == j)
                            continue;
                        if (eligibleShapes[i].IsParentOf(eligibleShapes[j]))
                            eligibleShapes[j].parents.Add(eligibleShapes[i]);
                    }
                // Holes are shapes with an odd number of parents.
                ShpData[] holeShapes = eligibleShapes.Where(x => x.parents.Count % 2 != 0).ToArray();
                foreach (ShpData holeShape in holeShapes) {
                    // The most immediate parent (i.e the smallest parent shape) will be the one that has the highest number of parents of its own. 
                    ShpData immediateParent = holeShape.parents.OrderByDescending(x => x.parents.Count).First();
                    immediateParent.holes.Add(holeShape);
                }
                // Solid shapes have an even number of parents
                ShpData[] solidShapes = eligibleShapes.Where(x => x.parents.Count % 2 == 0).ToArray();
                foreach (ShpData solidShape in solidShapes)
                    solidShape.ValidateHoles();
                // Create polygons from the solid shapes and their associated hole shapes
                Polygon[] polygons = solidShapes.Select(x => new Polygon(x.polygon.points, x.holes.Select(h => h.polygon.points).ToArray())).ToArray();
                // Flatten the points arrays from all polygons into a single array, and convert the vector2s to vector3s.
                vs = polygons.SelectMany(x => x.points.Select(v2 => new Vector3(v2.x, height, v2.y))).ToArray();
                // Triangulate each polygon and flatten the triangle arrays into a single array.
                List<int> allTriangles = new List<int>();
                int startVertexIndex = 0;
                for (int i = 0; i < polygons.Length; i++) {
                    Triangulator triangulator = new Triangulator(polygons[i]);
                    int[] polygonTriangles = triangulator.Triangulate();
                    for (int j = 0; j < polygonTriangles.Length; j++)
                        allTriangles.Add(polygonTriangles[j] + startVertexIndex);
                    startVertexIndex += polygons[i].numPoints;
                }
                ts = allTriangles.ToArray();
            } catch (System.Exception e) {
                vs = new Vector3[] { };
                ts = new int[] { };
            }
        }
    }
    // Holds data for each shape needed when calculating composite shapes.
    public class ShpData {
        public readonly Vector2[] points;
        public readonly Polygon polygon;
        public readonly int[] triangles;
        public List<ShpData> parents = new List<ShpData>();
        public List<ShpData> holes = new List<ShpData>();
        public bool IsValidShape { get; private set; }
        public ShpData(Vector3[] points) {
            this.points = points.Select(v => v.Xz()).ToArray();
            IsValidShape = points.Length >= 3 && !IntersectsWithSelf();
            if (IsValidShape) {
                polygon = new Polygon(this.points);
                Triangulator t = new Triangulator(polygon);
                triangles = t.Triangulate();
            }
        }
        // Removes any holes which overlap with another hole
        public void ValidateHoles() {
            for (int i = 0; i < holes.Count; i++)
                for (int j = i + 1; j < holes.Count; j++) {
                    bool overlap = holes[i].OverlapsPartially(holes[j]);
                    if (overlap) {
                        holes[i].IsValidShape = false;
                        break;
                    }
                }
            for (int i = holes.Count - 1; i >= 0; i--)
                if (!holes[i].IsValidShape)
                    holes.RemoveAt(i);
        }
        // A parent is a shape which fully contains another shape
        public bool IsParentOf(ShpData otherShape) {
            if (otherShape.parents.Contains(this))
                return true;
            if (parents.Contains(otherShape))
                return false;
            // check if first point in otherShape is inside this shape. If not, parent test fails.
            // if yes, then continue to line seg intersection test between the two shapes
            // (this point test is important because without it, if all line seg intersection tests fail,
            // we wouldn't know if otherShape is entirely inside or entirely outside of this shape)
            bool pointInsideShape = false;
            for (int i = 0; i < triangles.Length; i += 3)
                if (Maths2D.PointInTriangle(polygon.points[triangles[i]], polygon.points[triangles[i + 1]], polygon.points[triangles[i + 2]], otherShape.points[0])) {
                    pointInsideShape = true;
                    break;
                }
            if (!pointInsideShape)
                return false;
            // Check for intersections between line segs of this shape and otherShape (any intersections will fail the parent test)
            for (int i = 0; i < points.Length; i++) {
                LineSegment parentSeg = new LineSegment(points[i], points[(i + 1) % points.Length]);
                for (int j = 0; j < otherShape.points.Length; j++) {
                    LineSegment childSeg = new LineSegment(otherShape.points[j], otherShape.points[(j + 1) % otherShape.points.Length]);
                    if (Maths2D.LineSegmentsIntersect(parentSeg.a, parentSeg.b, childSeg.a, childSeg.b))
                        return false;
                }
            }
            return true;
        }
        // Test if the shapes overlap partially (test will fail if one shape entirely contains other shape, i.e. one is parent of the other).
        public bool OverlapsPartially(ShpData otherShape) {
            // Check for intersections between line segs of this shape and otherShape (any intersection will validate the overlap test)
            for (int i = 0; i < points.Length; i++) {
                LineSegment segA = new LineSegment(points[i], points[(i + 1) % points.Length]);
                for (int j = 0; j < otherShape.points.Length; j++) {
                    LineSegment segB = new LineSegment(otherShape.points[j], otherShape.points[(j + 1) % otherShape.points.Length]);
                    if (Maths2D.LineSegmentsIntersect(segA.a, segA.b, segB.a, segB.b))
                        return true;
                }
            }
            return false;
        }
        // Checks if any of the line segments making up this shape intersect
        public bool IntersectsWithSelf() {
            for (int i = 0; i < points.Length; i++) {
                LineSegment segA = new LineSegment(points[i], points[(i + 1) % points.Length]);
                for (int j = i + 2; j < points.Length; j++) {
                    if ((j + 1) % points.Length == i)
                        continue;
                    LineSegment segB = new LineSegment(points[j], points[(j + 1) % points.Length]);
                    if (Maths2D.LineSegmentsIntersect(segA.a, segA.b, segB.a, segB.b))
                        return true;
                }
            }
            return false;
        }
        public struct LineSegment {
            public readonly Vector2 a, b;
            public LineSegment(Vector2 a, Vector2 b) {
                this.a = a;
                this.b = b;
            }
        }
    }
    public static class Maths2D {
        public static float PseudoDistanceFromPointToLine(Vector2 a, Vector2 b, Vector2 c) { return Mathf.Abs((c.x - a.x) * (-b.y + a.y) + (c.y - a.y) * (b.x - a.x)); }
        public static int SideOfLine(Vector2 a, Vector2 b, Vector2 c) { return (int)Mathf.Sign((c.x - a.x) * (-b.y + a.y) + (c.y - a.y) * (b.x - a.x)); }
        public static int SideOfLine(float ax, float ay, float bx, float by, float cx, float cy) { return (int)Mathf.Sign((cx - ax) * (-by + ay) + (cy - ay) * (bx - ax)); }
        public static bool PointInTriangle(Vector2 a, Vector2 b, Vector2 c, Vector2 p) {
            float area = 0.5f * (-b.y * c.x + a.y * (-b.x + c.x) + a.x * (b.y - c.y) + b.x * c.y), s = 1 / (2 * area) * (a.y * c.x - a.x * c.y + (c.y - a.y) * p.x + (a.x - c.x) * p.y), t = 1 / (2 * area) * (a.x * b.y - a.y * b.x + (a.y - b.y) * p.x + (b.x - a.x) * p.y);
            return s >= 0 && t >= 0 && (s + t) <= 1;
        }
        public static bool LineSegmentsIntersect(Vector2 a, Vector2 b, Vector2 c, Vector2 d) {
            float denominator = ((b.x - a.x) * (d.y - c.y)) - ((b.y - a.y) * (d.x - c.x));
            if (Mathf.Approximately(denominator, 0))
                return false;
            float numerator1 = ((a.y - c.y) * (d.x - c.x)) - ((a.x - c.x) * (d.y - c.y)), numerator2 = ((a.y - c.y) * (b.x - a.x)) - ((a.x - c.x) * (b.y - a.y));
            if (Mathf.Approximately(numerator1, 0) || Mathf.Approximately(numerator2, 0))
                return false;
            float r = numerator1 / denominator, s = numerator2 / denominator;
            return (r > 0 && r < 1) && (s > 0 && s < 1);
        }
    }
    // Processes given arrays of hull and hole points into single array, enforcing correct -wiseness.
    // Also provides convenience methods for accessing different hull/hole points
    public class Polygon {
        public readonly Vector2[] points;
        public readonly int numPoints, numHullPoints, numHoles;
        public readonly int[] numPointsPerHole;
        readonly int[] holeStartIndices;
        public Polygon(Vector2[] hull, Vector2[][] holes) {
            numHullPoints = hull.Length;
            numHoles = holes.GetLength(0);
            numPointsPerHole = new int[numHoles];
            holeStartIndices = new int[numHoles];
            int numHolePointsSum = 0;
            for (int i = 0; i < holes.GetLength(0); i++) {
                numPointsPerHole[i] = holes[i].Length;
                holeStartIndices[i] = numHullPoints + numHolePointsSum;
                numHolePointsSum += numPointsPerHole[i];
            }
            numPoints = numHullPoints + numHolePointsSum;
            points = new Vector2[numPoints];
            // add hull points, ensuring they wind in counterclockwise order
            bool reverseHullPointsOrder = !PointsAreCounterClockwise(hull);
            for (int i = 0; i < numHullPoints; i++)
                points[i] = hull[(reverseHullPointsOrder) ? numHullPoints - 1 - i : i];
            // add hole points, ensuring they wind in clockwise order
            for (int i = 0; i < numHoles; i++) {
                bool reverseHolePointsOrder = PointsAreCounterClockwise(holes[i]);
                for (int j = 0; j < holes[i].Length; j++)
                    points[IndexOfPointInHole(j, i)] = holes[i][(reverseHolePointsOrder) ? holes[i].Length - j - 1 : j];
            }
        }
        public Polygon(Vector2[] hull) : this(hull, new Vector2[0][]) { }
        bool PointsAreCounterClockwise(Vector2[] testPoints) {
            float signedArea = 0;
            for (int i = 0; i < testPoints.Length; i++) {
                int nextIndex = (i + 1) % testPoints.Length;
                signedArea += (testPoints[nextIndex].x - testPoints[i].x) * (testPoints[nextIndex].y + testPoints[i].y);
            }
            return signedArea < 0;
        }
        public int IndexOfFirstPointInHole(int holeIndex) { return holeStartIndices[holeIndex]; }
        public int IndexOfPointInHole(int index, int holeIndex) { return holeStartIndices[holeIndex] + index; }
        public Vector2 GetHolePoint(int index, int holeIndex) { return points[holeStartIndices[holeIndex] + index]; }
    }
    // Handles triangulation of given polygon using the 'ear-clipping' algorithm.
    // The implementation is based on the following paper:
    // https://www.geometrictools.com/Documentation/TriangulationByEarClipping.pdf
    public class Triangulator {
        LinkedList<Vertex> vertsInClippedPolygon;
        int[] tris;
        int triIndex;
        public Triangulator(Polygon polygon) {
            // 2 verts are added when connecting a hole to the hull.
            int numHoleToHullConnectionVerts = 2 * polygon.numHoles, totalNumVerts = polygon.numPoints + numHoleToHullConnectionVerts;
            tris = new int[(totalNumVerts - 2) * 3];
            vertsInClippedPolygon = GenerateVertexList(polygon);
        }
        public int[] Triangulate() {
            while (vertsInClippedPolygon.Count >= 3) {
                bool hasRemovedEarThisIteration = false;
                LinkedListNode<Vertex> vertexNode = vertsInClippedPolygon.First;
                for (int i = 0; i < vertsInClippedPolygon.Count; i++) {
                    LinkedListNode<Vertex> prevVertexNode = vertexNode.Previous ?? vertsInClippedPolygon.Last;
                    LinkedListNode<Vertex> nextVertexNode = vertexNode.Next ?? vertsInClippedPolygon.First;
                    if (vertexNode.Value.isConvex) {
                        if (!TriangleContainsVertex(prevVertexNode.Value, vertexNode.Value, nextVertexNode.Value)) {
                            // check if removal of ear makes prev/next vertex convex (if was previously reflex)
                            if (!prevVertexNode.Value.isConvex) {
                                LinkedListNode<Vertex> prevOfPrev = prevVertexNode.Previous ?? vertsInClippedPolygon.Last;
                                prevVertexNode.Value.isConvex = IsConvex(prevOfPrev.Value.position, prevVertexNode.Value.position, nextVertexNode.Value.position);
                            }
                            if (!nextVertexNode.Value.isConvex) {
                                LinkedListNode<Vertex> nextOfNext = nextVertexNode.Next ?? vertsInClippedPolygon.First;
                                nextVertexNode.Value.isConvex = IsConvex(prevVertexNode.Value.position, nextVertexNode.Value.position, nextOfNext.Value.position);
                            }
                            // add triangle to tri array
                            tris[triIndex * 3 + 2] = prevVertexNode.Value.index;
                            tris[triIndex * 3 + 1] = vertexNode.Value.index;
                            tris[triIndex * 3] = nextVertexNode.Value.index;
                            triIndex++;
                            hasRemovedEarThisIteration = true;
                            vertsInClippedPolygon.Remove(vertexNode);
                            break;
                        }
                    }
                    vertexNode = nextVertexNode;
                }
                if (!hasRemovedEarThisIteration) {
                    //Debug.LogError("Error triangulating mesh. Aborted.");
                    return null;
                }
            }
            return tris;
        }
        // Creates a linked list of all vertices in the polygon, with the hole vertices joined to the hull at optimal points.
        LinkedList<Vertex> GenerateVertexList(Polygon polygon) {
            LinkedList<Vertex> vertexList = new LinkedList<Vertex>();
            LinkedListNode<Vertex> currentNode = null;
            // Add all hull points to the linked list
            for (int i = 0; i < polygon.numHullPoints; i++) {
                int prevPointIndex = (i - 1 + polygon.numHullPoints) % polygon.numHullPoints, nextPointIndex = (i + 1) % polygon.numHullPoints;
                bool vertexIsConvex = IsConvex(polygon.points[prevPointIndex], polygon.points[i], polygon.points[nextPointIndex]);
                Vertex currentHullVertex = new Vertex(polygon.points[i], i, vertexIsConvex);
                if (currentNode == null)
                    currentNode = vertexList.AddFirst(currentHullVertex);
                else
                    currentNode = vertexList.AddAfter(currentNode, currentHullVertex);
            }
            // Process holes:
            List<HoleData> sortedHoleData = new List<HoleData>();
            for (int holeIndex = 0; holeIndex < polygon.numHoles; holeIndex++) {
                // Find index of rightmost point in hole. This 'bridge' point is where the hole will be connected to the hull.
                Vector2 holeBridgePoint = new Vector2(float.MinValue, 0);
                int holeBridgeIndex = 0;
                for (int i = 0; i < polygon.numPointsPerHole[holeIndex]; i++)
                    if (polygon.GetHolePoint(i, holeIndex).x > holeBridgePoint.x) {
                        holeBridgePoint = polygon.GetHolePoint(i, holeIndex);
                        holeBridgeIndex = i;
                    }
                sortedHoleData.Add(new HoleData(holeIndex, holeBridgeIndex, holeBridgePoint));
            }
            // Sort hole data so that holes furthest to the right are first
            sortedHoleData.Sort((x, y) => (x.bridgePoint.x > y.bridgePoint.x) ? -1 : 1);
            foreach (HoleData holeData in sortedHoleData) {
                // Find first edge which intersects with rightwards ray originating at the hole bridge point.
                Vector2 rayIntersectPoint = new Vector2(float.MaxValue, holeData.bridgePoint.y);
                List<LinkedListNode<Vertex>> hullNodesPotentiallyInBridgeTriangle = new List<LinkedListNode<Vertex>>();
                LinkedListNode<Vertex> initialBridgeNodeOnHull = null;
                currentNode = vertexList.First;
                while (currentNode != null) {
                    LinkedListNode<Vertex> nextNode = (currentNode.Next == null) ? vertexList.First : currentNode.Next;
                    Vector2 p0 = currentNode.Value.position, p1 = nextNode.Value.position;
                    // at least one point must be to right of holeData.bridgePoint for intersection with ray to be possible
                    if (p0.x > holeData.bridgePoint.x || p1.x > holeData.bridgePoint.x) {
                        // one point is above, one point is below
                        if (p0.y > holeData.bridgePoint.y != p1.y > holeData.bridgePoint.y) {
                            float rayIntersectX = p1.x; // only true if line p0,p1 is vertical
                            if (!Mathf.Approximately(p0.x, p1.x)) {
                                float intersectY = holeData.bridgePoint.y, gradient = (p0.y - p1.y) / (p0.x - p1.x), c = p1.y - gradient * p1.x;
                                rayIntersectX = (intersectY - c) / gradient;
                            }
                            // intersection must be to right of bridge point
                            if (rayIntersectX > holeData.bridgePoint.x) {
                                LinkedListNode<Vertex> potentialNewBridgeNode = (p0.x > p1.x) ? currentNode : nextNode;
                                // if two intersections occur at same x position this means is duplicate edge
                                // duplicate edges occur where a hole has been joined to the outer polygon
                                bool isDuplicateEdge = Mathf.Approximately(rayIntersectX, rayIntersectPoint.x);
                                // connect to duplicate edge (the one that leads away from the other, already connected hole, and back to the original hull) if the
                                // current hole's bridge point is higher up than the bridge point of the other hole (so that the new bridge connection doesn't intersect).
                                bool connectToThisDuplicateEdge = holeData.bridgePoint.y > potentialNewBridgeNode.Previous.Value.position.y;
                                // if this is the closest ray intersection thus far, set bridge hull node to point in line having greater x pos (since def to right of hole).
                                if (!isDuplicateEdge || connectToThisDuplicateEdge)
                                    if (rayIntersectX < rayIntersectPoint.x || isDuplicateEdge) {
                                        rayIntersectPoint.x = rayIntersectX;
                                        initialBridgeNodeOnHull = potentialNewBridgeNode;
                                    }
                            }
                        }
                    }
                    // Determine if current node might lie inside the triangle formed by holeBridgePoint, rayIntersection, and bridgeNodeOnHull
                    // We only need consider those which are reflex, since only these will be candidates for visibility from holeBridgePoint.
                    // A list of these nodes is kept so that in next step it is not necessary to iterate over all nodes again.
                    if (currentNode != initialBridgeNodeOnHull)
                        if (!currentNode.Value.isConvex && p0.x > holeData.bridgePoint.x)
                            hullNodesPotentiallyInBridgeTriangle.Add(currentNode);
                    currentNode = currentNode.Next;
                }
                // Check triangle formed by hullBridgePoint, rayIntersection, and bridgeNodeOnHull.
                // If this triangle contains any points, those points compete to become new bridgeNodeOnHull
                LinkedListNode<Vertex> validBridgeNodeOnHull = initialBridgeNodeOnHull;
                foreach (LinkedListNode<Vertex> nodePotentiallyInTriangle in hullNodesPotentiallyInBridgeTriangle) {
                    if (nodePotentiallyInTriangle.Value.index == initialBridgeNodeOnHull.Value.index)
                        continue;
                    // if there is a point inside triangle, this invalidates the current bridge node on hull.
                    if (Maths2D.PointInTriangle(holeData.bridgePoint, rayIntersectPoint, initialBridgeNodeOnHull.Value.position, nodePotentiallyInTriangle.Value.position)) {
                        // Duplicate points occur at hole and hull bridge points.
                        bool isDuplicatePoint = validBridgeNodeOnHull.Value.position == nodePotentiallyInTriangle.Value.position;
                        // if multiple nodes inside triangle, we want to choose the one with smallest angle from holeBridgeNode.
                        // if is a duplicate point, then use the one occurring later in the list
                        float currentDstFromHoleBridgeY = Mathf.Abs(holeData.bridgePoint.y - validBridgeNodeOnHull.Value.position.y), pointInTriDstFromHoleBridgeY = Mathf.Abs(holeData.bridgePoint.y - nodePotentiallyInTriangle.Value.position.y);
                        if (pointInTriDstFromHoleBridgeY < currentDstFromHoleBridgeY || isDuplicatePoint)
                            validBridgeNodeOnHull = nodePotentiallyInTriangle;
                    }
                }
                // Insert hole points (starting at holeBridgeNode) into vertex list at validBridgeNodeOnHull
                currentNode = validBridgeNodeOnHull;
                for (int i = holeData.bridgeIndex; i <= polygon.numPointsPerHole[holeData.holeIndex] + holeData.bridgeIndex; i++) {
                    int previousIndex = currentNode.Value.index, currentIndex = polygon.IndexOfPointInHole(i % polygon.numPointsPerHole[holeData.holeIndex], holeData.holeIndex), nextIndex = polygon.IndexOfPointInHole((i + 1) % polygon.numPointsPerHole[holeData.holeIndex], holeData.holeIndex);
                    if (i == polygon.numPointsPerHole[holeData.holeIndex] + holeData.bridgeIndex) // have come back to starting point
                        nextIndex = validBridgeNodeOnHull.Value.index; // next point is back to the point on the hull
                    bool vertexIsConvex = IsConvex(polygon.points[previousIndex], polygon.points[currentIndex], polygon.points[nextIndex]);
                    Vertex holeVertex = new Vertex(polygon.points[currentIndex], currentIndex, vertexIsConvex);
                    currentNode = vertexList.AddAfter(currentNode, holeVertex);
                }
                // Add duplicate hull bridge vert now that we've come all the way around. Also set its concavity
                Vector2 nextVertexPos = (currentNode.Next == null) ? vertexList.First.Value.position : currentNode.Next.Value.position;
                bool isConvex = IsConvex(holeData.bridgePoint, validBridgeNodeOnHull.Value.position, nextVertexPos);
                Vertex repeatStartHullVert = new Vertex(validBridgeNodeOnHull.Value.position, validBridgeNodeOnHull.Value.index, isConvex);
                vertexList.AddAfter(currentNode, repeatStartHullVert);
                //Set concavity of initial hull bridge vert, since it may have changed now that it leads to hole vert
                LinkedListNode<Vertex> nodeBeforeStartBridgeNodeOnHull = (validBridgeNodeOnHull.Previous == null) ? vertexList.Last : validBridgeNodeOnHull.Previous;
                LinkedListNode<Vertex> nodeAfterStartBridgeNodeOnHull = (validBridgeNodeOnHull.Next == null) ? vertexList.First : validBridgeNodeOnHull.Next;
                validBridgeNodeOnHull.Value.isConvex = IsConvex(nodeBeforeStartBridgeNodeOnHull.Value.position, validBridgeNodeOnHull.Value.position, nodeAfterStartBridgeNodeOnHull.Value.position);
            }
            return vertexList;
        }
        // check if triangle contains any verts (note, only necessary to check reflex verts).
        bool TriangleContainsVertex(Vertex v0, Vertex v1, Vertex v2) {
            LinkedListNode<Vertex> vertexNode = vertsInClippedPolygon.First;
            for (int i = 0; i < vertsInClippedPolygon.Count; i++) {
                if (!vertexNode.Value.isConvex) { // convex verts will never be inside triangle
                    Vertex vertexToCheck = vertexNode.Value;
                    if (vertexToCheck.index != v0.index && vertexToCheck.index != v1.index && vertexToCheck.index != v2.index) // dont check verts that make up triangle
                        if (Maths2D.PointInTriangle(v0.position, v1.position, v2.position, vertexToCheck.position))
                            return true;
                }
                vertexNode = vertexNode.Next;
            }
            return false;
        }
        // v1 is considered a convex vertex if v0-v1-v2 are wound in a counter-clockwise order.
        bool IsConvex(Vector2 v0, Vector2 v1, Vector2 v2) { return Maths2D.SideOfLine(v0, v2, v1) == -1; }
        public struct HoleData {
            public readonly int holeIndex, bridgeIndex;
            public readonly Vector2 bridgePoint;
            public HoleData(int holeIndex, int bridgeIndex, Vector2 bridgePoint) {
                this.holeIndex = holeIndex;
                this.bridgeIndex = bridgeIndex;
                this.bridgePoint = bridgePoint;
            }
        }
        public class Vertex {
            public readonly Vector2 position;
            public readonly int index;
            public bool isConvex;
            public Vertex(Vector2 position, int index, bool isConvex) {
                this.position = position;
                this.index = index;
                this.isConvex = isConvex;
            }
        }
    }
}