using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;

[RequireComponent(typeof(Rigidbody))]
public class PlayerCollectCubesController : Mb {
    public float speed = 10, posScl = 0.02f, disTh = 10;
    public bool isPlaying = false;
    float dis;
    Vector3 staPos, pos;
    void Start() {
        rb.NoG();
        rb.Con(false, true, false, true, true, true);
    }
    void Update() {
        if (IsPlaying || isPlaying) {
            if (IsMbD) {
                mp = Mp;
                staPos = Tp;
            }
            if (IsMb)
                pos = staPos + new Vector3(Mp.x - mp.x, 0, Mp.y - mp.y) * posScl;
            dis = V3.Dis(Tp, pos);
            if (dis < 0.1f) {
                Tp = pos;
                rb.V0();
            } else {
                tf.LookAt(pos);
                rb.V(F * M.C01(dis / disTh) * speed);
            }
        } else
            rb.V0();
    }
}