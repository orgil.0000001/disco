﻿using Orgil;

public enum UIButtonType { Pause, Settings, Replay, Resume, Play, Save, Done, Clear }

public class UIButton : Mb {
    public UIButtonType type;
    public void Click() {
        switch (type) {
            case UIButtonType.Pause: Cc.I.Pause(); break;
            case UIButtonType.Settings: Gs.I.Settings(); break;
            case UIButtonType.Replay: Cc.I.Replay(); break;
            case UIButtonType.Resume: Cc.I.Resume(); break;
            case UIButtonType.Play: Cc.I.Play(); break;
            case UIButtonType.Save: Gs.I.Save(); break;
            case UIButtonType.Done: Gs.I.Done(); break;
            case UIButtonType.Clear: Gs.I.Clear(); break;
        }
    }
}