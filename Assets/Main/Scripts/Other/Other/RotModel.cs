﻿using System.Collections.Generic;
using UnityEngine;
using Orgil;

[ExecuteInEditMode]
[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class RotModel : Mb {
    public bool isFill = true, isDisUv = true;
    public int n = 8;
    public List<Vector2> points = new List<Vector2>() {
        new Vector2 (-0.5f, -0.5f),
        new Vector2 (-0.5f, 0.5f),
    };
    public Tf t = new Tf(V3.O, V3.O, V3.I);
    Mesh mesh;
    public void UpdateMesh() {
        Msh.Init(ref mesh, go);
        //Msh.RotModel(ref mesh, isFill, isDisUv, n, points, t);
    }
}