using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;

public class Ball : Mb {
    bool isDst = false, isDf = false;
    float t = 0;
    private void Update() {
        if (isDst) {
            t += Dt;
            Tls = V3.V(Ls.I.ballSz * (1 - t / Ls.I.ballDstTm));
            if (t >= Ls.I.ballDstTm)
                Dst(go);
        }
    }
    static string[] danceNames = Arr("Dance", "Lock", "Robot", "Slide", "Tut", "Wave");
    IEnumerator DfCor(DanceFloor df) {
        Vector3 pos1 = Tlp, pos2 = V3.Y(Ls.I.h - 1), scl1 = Tls, scl2 = V3.I;
        Quaternion rot1 = Tlr, rot2 = Q.O;
        for (float t = 0, tm = 0.5f; t < tm; t += Dt) {
            Tlp = V3.Lerp(pos1, pos2, t / tm);
            Tlr = Q.Lerp(rot1, rot2, t / tm);
            Tls = V3.Lerp(scl1, scl2, t / tm);
            yield return null;
        }
        Tlp = pos2;
        Tlr = rot2;
        Tls = scl2;
        string name = danceNames.Rnd();
        df.bots.ForEach(x => x.Anim(name));
        if (!df.isBonus) {
            Ac.I.Play(df.data.song);
            Ls.I.AddPnt();
            Player.I.NxtMv();
        }
        go.ChildShow(1);
        go.Child(0).RenMatCol(Ls.I.e.ball2);
        df.ball = this;
    }
    private void OnCollisionEnter(Collision collision) {
        if (collision.Tag(Tag.DanceFloor)) {
            isDst = false;
            DanceFloor df = collision.Par<DanceFloor>();
            if (!df.isBall && df.IsBotDone) {
                df.isBall = true;
                isDf = true;
                P = df.tf;
                Dst(rb);
                Dst(go.Col());
                StaCor(DfCor(df));
                Player.Score(20);
            }
        } else if (!isDf) {
            if (collision.Tag(Tag.Bot)) {
                Dst(Ins(Ls.I.impactDirtPf, Tp, Q.O, Ls.I.tf), 1);
                collision.Gc<Bot>().Hit();
            }
            isDst = true;
        }
    }
    private void OnTriggerEnter(Collider other) {
        if (other.Tag(Tag.Bot)) {
            Dst(go);
        }
    }
}
